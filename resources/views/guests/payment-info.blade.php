@extends('layouts.layout')
<? 	foreach($Results as $val){
		$outPut = $val;
	}
?>
@section('title')
	Guest : {{$outPut->GuestLname}}, {{$outPut->GuestFname}}
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Guest : {{$outPut->GuestLname}}, {{$outPut->GuestFname}}</h3></div>
            @if($outPut->RsrvSource)
                <div class="col-md-3 col-sm-3 col-xs-6 fltR zeroRightPadd">
                    <h3 class="AlgnCenter">{{$outPut->RsrvSource}}</h3>
                </div>
            @endif
        </div>
        {{--*/ $PayBalance = ($outPut->PayAmount-$outPut->PayDeposit); /*--}}
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('guest-info', $outPut->RsrvId)}}">Guest Info</a></li>
                <li class="active"><a href="javascript:void(0);">Payment Info</a></li>
                <li><a href="{{url('change-room', $outPut->RsrvId)}}">Room Change</a></li>
                <li><a href="{{url('guest-folio', $outPut->RsrvId)}}">Guest Folio</a></li>
                <li><a href="{{url('change-stay', $outPut->RsrvId)}}">Change Stay</a></li>
                <li><a href="{{url('view-changes', $outPut->RsrvId)}}">View Changes</a></li>
                @if($outPut->RsrvStat == '' && $PayBalance == 0)
                <li><a href="{{url('cancel-reservation', $outPut->RsrvId)}}">Cancel Reservation</a></li>
                @endif
            </ul>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                    	@if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @elseif ($message = Session::get('danger'))
                            <div class="alert alert-danger">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="table-responsive">
                        	<div class="col-md-12 col-sm-12 col-xs-12 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Guest Name:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->GuestLname}}, {{$outPut->GuestFname}}
                                </div>
                                @if($outPut->RsrvSource)
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    {{$outPut->RsrvSource}} Reservation#
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    {{$outPut->SourceRsrvId}}
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Company Name:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->GuestCompany}}
                                </div>                        	
                                @if($outPut->RsrvSource)
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Confirmation#
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->SourceRsrvCNFId}}
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Reservation#:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    {{$outPut->RsrvId}}
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Estimated Cost:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    $ {{$outPut->EstAmount}} {!!($outPut->StayNights > 1)?'<i class="fa fa-info-circle cursorPointer font16 marginLeft5" id="dayWiseRate" title="Daywise Room Charges" aria-hidden="true"></i>':''!!}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Arrival Date:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->ArvlDate}}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Departure Date:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->DprtDate}}
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Chargeable Amount:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    $ {{$outPut->PayAmount}}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Room#
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter" id="RoomAssignId_{!!$outPut->RsrvId!!}">
                                    @if($outPut->RsrvStat == 'Checked Out' || $outPut->RsrvStat == 'Cancelled')
                                       <div class="fltL" style="position:relative">{{$outPut->RoomNumber}}</div>
                                       <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                    @else
                                        @if($outPut->RoomNumber)
                                            @if($outPut->RsrvLock)
                                                <div class="fltL" style="position:relative">{{$outPut->RoomNumber}}</div>
                                                <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                                <i class="fa fa-lock lockFeaturButton cursorPointer" id="unLockButton" data-id="{!!$outPut->RsrvId!!}" aria-hidden="true"></i>
                                            @else
                                                <div class="fltL" style="position:relative">
                                                <a href="{{url('change-room', $outPut->RsrvId)}}" class="btn-list-link">{{$outPut->RoomNumber}}</a>
                                                @if($outPut->RsrvStat == "")
                                                <i class="closeButton fa fa-times" data-id="{!!$outPut->RsrvId!!}"></i>
                                                @endif
                                                </div>
                                                <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                                <i class="fa fa-unlock lockFeaturButton cursorPointer" id="LockButton" data-id="{!!$outPut->RsrvId!!}" aria-hidden="true"></i> 
                                            @endif
                                        @else
                                            <a href="{{url('change-room', $outPut->RsrvId)}}" class="fltL btn-list-link">Select Room</a>
                                            <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                        @endif
                                    @endif
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Received Amount:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    $ {{$outPut->PayDeposit}}
                                </div>
                           	</div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Number of Nights:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->StayNights}}
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Balance:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                	{{--*/ $remainBalance = $outPut->PayAmount-$outPut->PayDeposit; /*--}}
                                    {!!($remainBalance < 0)?'- $'.($remainBalance*-1):'$'.$remainBalance!!}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop24 LR_Padd0">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th>Trx Date</th>
                                            <th>Trx Id</th>
                                            <th>Trx Type</th>
                                            <th>Comment</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($PaymentRst))
                                    	{{--*/$paymentModeArray = array("Cash Payment"=>"success", "CC Payment"=>"success", "CC Payment Auth Only"=>"warning",
                                                                  		"Check Payment"=>"success", "Company Payment"=>"warning");
                                       	/*--}}
                                        @foreach($PaymentRst as $vals)
                                        <tr class="even pointer {!!(isset($vals->folio_type) && $vals->folio_type == 2)?'paymentModeColor_adjustment':'paymentModeColor_'.$paymentModeArray[(isset($vals->folio_pay_type))?$vals->folio_pay_type:'CC Payment Auth Only']!!}">
                                            <td>{!!date('m/d/Y h:i A', strtotime($vals->created_at))!!}</td>
                                            <td>{{$vals->transaction_id}} <i class="fa fa-info-circle cursorPointer font16 marginLeft5" id="transactionDetail" title="Transaction Detail" data-id="{{$vals->log_id}}" aria-hidden="true"></i></td>
                                            @if(isset($vals->folio_type) && $vals->folio_type == 2)
                                            <td class="UpperLetter">{{(isset($vals->folio_amount) && $vals->folio_amount > 0 )?'Payment Reversal':'Card Auth'}}</td>
                                            @else
                                            <td class="UpperLetter">{{(isset($vals->folio_amount) && $vals->folio_amount > 0 )?'Payment Received':'Card Auth'}}</td>
                                            @endif
                                            <td class="UpperLetter" style="max-width:200px;">{{(isset($vals->folio_comment))?$vals->folio_comment:''}}</td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr class="even pointer">
                                            <td colspan="6" class="errorMessageTR">No Payment Received</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                @if($remainBalance < 0)
                            	<form action="{!!url('payment-reversal')!!}" method="POST" id="myform" data-parsley-validate class="form-horizontal form-label-left">
                                {!!csrf_field()!!}
                                <input type="hidden" name="rsrv_id" id="rsrv_id" value="{{$outPut->RsrvId}}"/>
                                <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                    <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0 paddingTop8">Payment Refund Mode</div>
                                    <div class="col-md-8 col-sm-6 col-xs-12 LR_Padd5">
                                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="radio" name="payment_mode" id="payment_mode1" value="1" class="radioButtonCustom_Option" checked="checked"/>
                                                <div class="labelSwitchCheck">Cash</div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="radio" name="payment_mode" id="payment_mode4" value="4" class="radioButtonCustom_Option"/>
                                                <div class="labelSwitchCheck">Check</div>
                                            </div>
                                        </div>
                                        @if(count($CardDtlRst))
                                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="radio" name="payment_mode" id="payment_mode2" value="2" class="radioButtonCustom_Option"/>
                                                <div class="labelSwitchCheck">CC Reversal</div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 paddingLeft20 displayNone" id="cardOption">
                                            	{{--*/ $SJ = 1; /*--}}
                                                @foreach($CardDtlRst as $CardDtlOut)
                                                <div class="col-md-12 col-sm-12 col-xs-12 TB_Padd5" style="padding-left:5px !important">
                                                	<input type="radio" name="card_log" id="card_log_{{$SJ}}" value="{{$CardDtlOut->log_id}}" class="fltL radioButtonCustom zeroMargin"/>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">{{$CardDtlOut->cc_number}}</div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">{{$CardDtlOut->cc_holder}}</div>
                                                    <div class="col-md-2 col-sm-12 col-xs-12">{{$CardDtlOut->cc_exp}}</div>
                                                    <div class="col-md-2 col-sm-12 col-xs-12">{{($CardDtlOut->cc_amt)?'$ '.$CardDtlOut->cc_amt:'Card Auth'}}</div>
                                                </div>
                                                {{--*/ $SJ++; /*--}}
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                        @if($outPut->GuestCompany)
                                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="radio" name="payment_mode" id="payment_mode5" value="5" class="radioButtonCustom_Option"/>
                                                <div class="labelSwitchCheck">Company Account</div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                              	</div>
                                <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                    <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0 paddingTop8">Amount To Refund <span class="required">*</span></div>
                                    <div class="col-md-4 col-sm-8 col-xs-12 LR_Padd5">
                                        <span class="input-group-btn fltL widthAuto"><div class="btn btn-primary cursorInitial">$</div></span>
                                        <input type="text" name="folio_amount" id="folio_amount" value="{{($remainBalance<0)?($remainBalance*-1):0}}" placeholder="Enter Amount" class="form-control width40" required="required"min="0.1" maxlength="6" max="{{($remainBalance<0)?($remainBalance*-1):999.99}}">
                                    </div>
                             	</div>
                                <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0 displayNone" id="checkOption">
                                    <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0 paddingTop8">Check Detail <span class="required">*</span></div>
                                    <div class="col-md-4 col-sm-8 col-xs-12 LR_Padd5">
                                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                            <input type="text" name="bank_name" id="bank_name" placeholder="Name of Bank" value="" class="fltL form-control col-md-12 col-xs-12 checkOptionInput"/>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                            <input type="text" name="holder_name" id="holder_name" placeholder="Account Holder Name" value="" class="fltL form-control col-md-12 col-xs-12 checkOptionInput"/>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                            <div class="col-md-6 col-sm-3 col-xs-12 zeroPadd">
                                                <input type="text" name="check_number" id="check_number" placeholder="Check Number" value="" class="fltL form-control col-md-12 col-xs-12 checkOptionInput"/>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                            <div class="col-md-6 col-sm-3 col-xs-12 zeroPadd">
                                                <input type="text" name="check_date" id="check_date" placeholder="Check Date" value="" class="fltL form-control has-feedback-left checkOptionInput" readonly="readonly"/>
                                                <span class="fa fa-calendar-o form-control-feedback left" style="left:0" aria-hidden="true"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0" id="commentOption">
                                    <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0 paddingTop8">Comment Box</div>
                                    <div class="col-md-4 col-sm-8 col-xs-12 LR_Padd5">
                                        <textarea name="folio_comment" id="folio_comment" placeholder="Comment Box" class="fltL form-control col-md-12 col-xs-12" maxlength="70"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 marginTop20 LR_Padd0">
                                    <div class="col-md-6 col-sm-4 col-xs-4 col-md-offset-2 LR_Padd5">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-check-circle-o"></i> Payment Reversal</button>
                                    </div>
                                </div>
                                </form>
                                @endif
                           	</div>
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<!-- dayWiseRatePlan -->
<div id="dayWiseRatePlan" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="model_RsrvId"></h4>
            </div>
            <div class="modal-footer AlgnLeft" id="modal-matter"></div>
        </div>
    </div>
</div>
<div id="popup_dayWiseRatePlan" data-toggle="modal" data-target="#dayWiseRatePlan"></div>

<!-- transactionDetailPayment -->
<div id="transactionDetailPayment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:400px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="transactionDetail_Title"></h4>
            </div>
            <div class="modal-footer AlgnLeft" id="transactionDetail_Content"></div>
        </div>
    </div>
</div>
<div id="popup_transactionDetailPayment" data-toggle="modal" data-target="#transactionDetailPayment"></div>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
@stop 

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var checkDate = $('#check_date').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		checkDate.hide();
	}).data('datepicker');
	
	$('input[class=radioButtonCustom_Option]').change( function() {
	   	var paymentType = $(this).val();
		
		if(paymentType == 2){
			$("#cardOption").show();
			$("#card_log_1").prop("checked", true);
		}else{
			$("#cardOption").hide();
			$("#card_log_1").prop("checked", false);
		}
		
		if(paymentType == 4){
			$("#checkOption").show();
			$(".checkOptionInput").prop("required", true);
			$(".checkOptionInput").val("");
		}else{
			$("#checkOption").hide();
			$(".checkOptionInput").prop("required", false);
			$(".checkOptionInput").val("");
		}
	});

	$(document).on('click', '#dayWiseRate', function(e){
		var rsrvId = '{{$outPut->RsrvId}}';
		$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		$.ajax({
			url: "{{ url('/dayWiseRatePlan') }}",
			type: 'POST',
			data:{"rsrvId":rsrvId, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				if ( msg.status === 'success' ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					$("#model_RsrvId").html("Reservation ID: "+rsrvId);
					$("#modal-matter").html(msg.response);
					$('#popup_dayWiseRatePlan').click();
				}
				if ( msg.status === 'error' ) {
					toastr.error( msg.response );
				}
			},
			error: function( data ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				if ( data.status === 422 ) {
					toastr.error('Try Again.');
				}
			}
		});
		return false;
	});
	
	$(document).on('click', '#transactionDetail', function(e){
		var rsrvId = '{{$outPut->RsrvId}}';
		var logId  = $(this).attr('data-id');
		$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		$.ajax({
			url: "{{ url('/transactionDetailPayment') }}",
			type: 'POST',
			data:{"rsrvId":rsrvId, "logId":logId, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				if ( msg.status === 'success' ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					$("#transactionDetail_Title").html("Transaction ID: "+msg.transId);
					$("#transactionDetail_Content").html(msg.response);
					$('#popup_transactionDetailPayment').click();
				}
				if ( msg.status === 'error' ) {
					toastr.error( msg.response );
				}
			},
			error: function( data ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				if ( data.status === 422 ) {
					toastr.error('Try Again.');
				}
			}
		});
		return false;
	});
	
	$(document).on('click', '.lockFeaturButton', function(e){
		var selfId = $(this).attr('id');
		if(selfId == 'LockButton'){
			var C_selfId = 'unLockButton';
			var rsrvLock = 1;
			var Msg = "Are you sure you want to Lock this Room Number?";
		}else{
			var C_selfId = 'LockButton';
			var rsrvLock = 0;
			var Msg = "Are you sure you want to Unlock this Room Number?";
		}
		var x = confirm(Msg);
		if (x){
			var rsrvId = $(this).attr('data-id');
			$("#main_container_Loading").show();
			$("#main_container_overlay").show();
			$.ajax({
				url: "{{ url('/lockAssignRoomNumber') }}",
				type: 'POST',
				data:{"rsrvId":rsrvId, "rsrvLock":rsrvLock, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( msg.status === 'success' ) {
						$('#RoomAssignId_'+rsrvId).html(msg.content);
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( data.status === 422 ) {
						toastr.error('Cannot handle Request.');
					}
				}
			});
			return true;
		}else{
			return false;
		}
	});
});
</script>
@stop