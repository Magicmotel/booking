<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Hotel;
use App\HotelPayment;
use App\RoomReservation;
use YahooWeather;
use Helper;
use DB;
use Auth;

class AdminController extends Controller
{
   	public function __construct()
    {
   		$this->middleware('admin');
    }
    public function dashboard()
    {
		$Hotel_ID = Helper::getHotelId();
		
		$HotelRst = Hotel::where('hotel_id', $Hotel_ID)->first();
		
		$toDay   = date('Y-m-d');
		$nextDay = date('Y-m-d', strtotime($toDay.'+1 days'));
		
		if($HotelRst->hotel_city){
			$HotelCity = $HotelRst->hotel_city;
		}else{
			$HotelCity = 'California';
		}
		
		$weatherSchene = YahooWeather::Country($HotelCity, 'en');
		$weatherDesc   = explode("Forecast:", $weatherSchene['description']);
		$weatherCondit = str_replace("Current Conditions:","",strip_tags($weatherDesc[0]));
		
		
		$occupancyRoomSql= DB::select("SELECT count(R1.hotel_id) as TotCnt,
									   ( SELECT COUNT(R.hotel_id)
										 FROM room_reservation as RSV
										 INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
										 INNER JOIN room_assign as R ON R.room_assign_id = RRA.room_assign_id
										 WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.arrival <= '".$toDay."' AND RSV.departure >= '".$toDay."'
										 AND RSV.status != 'Checked Out' AND RSV.status != 'Cancelled'
										 GROUP BY R.hotel_id
									   ) as RsrvCnt
									   FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
									   WHERE R1.hotel_id = ".$Hotel_ID." AND R1.status = 1 AND R1.room_number != ''
									   AND R1.room_assign_id NOT IN (
									   	 SELECT BR.room_assign_id FROM block_rooms as BR WHERE BR.hotel_id = ".$Hotel_ID."
                                  		 AND NOT ((BR.block_to <= '".$toDay."') OR (BR.block_from >= '".$nextDay."')) GROUP BY BR.room_assign_id
									   )
									   GROUP BY R1.hotel_id
									   ORDER BY R1.room_number ASC
									  ");
		foreach($occupancyRoomSql as $occupancyRoomVal){
			$TotalRooms = $occupancyRoomVal->TotCnt;
			$RsrvRooms  = $occupancyRoomVal->RsrvCnt;
		}
		
		$occupancyRoom = ($TotalRooms)?round(($RsrvRooms/$TotalRooms)*100):0;
		
		$todayCheckIN = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['arrival', $toDay], ['status', '!=', 'Checked Out'], ['status', '!=', 'Cancelled'] ])->count();
		$cmpltCheckIN = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['arrival', $toDay], ['status', 'Checked In'] ])->count();
		$rmainCheckIN = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['arrival', $toDay], ['status', ''] ])->count();
		
		$todayDeparture = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['departure', $toDay], ['status', '=', 'Checked In'] ])->count();
		
		$vacantDirtySql = DB::select("SELECT R1.room_number as RoomNumber, R1.room_stat as RoomStat,
									   CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType, RT.room_type as RoomTypes
									   FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
									   WHERE R1.hotel_id = ".$Hotel_ID." AND R1.status = 1 AND R1.room_stat = 2 AND R1.room_number != ''
									   AND R1.room_assign_id NOT IN (
									     SELECT R.room_assign_id
										 FROM room_reservation as RSV
										 INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
										 INNER JOIN room_assign as R ON R.room_assign_id = RRA.room_assign_id
										 WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.arrival <= '".$toDay."' AND RSV.departure >= '".$toDay."'
										 AND RSV.status != 'Checked Out' AND RSV.status != 'Cancelled'
										 GROUP BY R.room_assign_id
									   )
									   AND R1.room_assign_id NOT IN (
									   	 SELECT BR.room_assign_id FROM block_rooms as BR WHERE BR.hotel_id = ".$Hotel_ID."
                                  		 AND NOT ((BR.block_to <= '".$toDay."') OR (BR.block_from >= '".$nextDay."')) GROUP BY BR.room_assign_id
									   )
									   ORDER BY R1.room_number ASC
									  ");
		$vacantDirty = count($vacantDirtySql);
		
		$availRoomSql   = DB::select("SELECT R1.room_number as RoomNumber, R1.room_stat as RoomStat,
									   CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType, RT.room_type as RoomTypes
									   FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
									   WHERE R1.hotel_id = ".$Hotel_ID." AND R1.status = 1 AND R1.room_number != ''
									   AND R1.room_assign_id NOT IN (
									     SELECT R.room_assign_id
										 FROM room_reservation as RSV
										 INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
										 INNER JOIN room_assign as R ON R.room_assign_id = RRA.room_assign_id
										 WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.arrival <= '".$toDay."' AND RSV.departure >= '".$toDay."'
										 AND RSV.status != 'Checked Out' AND RSV.status != 'Cancelled'
										 GROUP BY R.room_assign_id
									   )
									   AND R1.room_assign_id NOT IN (
									   	 SELECT BR.room_assign_id FROM block_rooms as BR WHERE BR.hotel_id = ".$Hotel_ID."
                                  		 AND NOT ((BR.block_to <= '".$toDay."') OR (BR.block_from >= '".$nextDay."')) GROUP BY BR.room_assign_id
									   )
									   ORDER BY R1.room_number ASC
									  ");
		$availRoom = count($availRoomSql);
		
		$maintRoomSql = DB::select("SELECT R1.room_number as RoomNumber
									FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
									WHERE R1.hotel_id = ".$Hotel_ID." AND R1.status = 0 AND R1.room_number != ''
									ORDER BY R1.room_number ASC
								   ");
		$maintRoom = count($maintRoomSql);
		
		$blockRoomSql = DB::select("SELECT R1.room_number as RoomNumber
									FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
									WHERE R1.hotel_id = ".$Hotel_ID." AND R1.status = 1 AND R1.room_number != ''
									AND R1.room_assign_id IN (
										SELECT BR.room_assign_id FROM block_rooms as BR WHERE BR.hotel_id = ".$Hotel_ID."
                                  		AND NOT ((BR.block_to <= '".$toDay."') OR (BR.block_from >= '".$nextDay."')) GROUP BY BR.room_assign_id
									)
									ORDER BY R1.room_number ASC
								   ");
		$blockRoom = count($blockRoomSql)+$maintRoom;
		
		$stayRoomSql   = DB::select("SELECT R1.room_number as RoomNumber, R1.room_stat as RoomStat,
									 CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType, RT.room_type as RoomTypes
									 FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
									 WHERE R1.hotel_id = ".$Hotel_ID." AND R1.status = 1 AND R1.room_number != ''
									 AND R1.room_assign_id IN (
									     SELECT R.room_assign_id
										 FROM room_reservation as RSV
										 INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
										 INNER JOIN room_assign as R ON R.room_assign_id = RRA.room_assign_id
										 WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.arrival <= '".$toDay."' AND RSV.departure >= '".$toDay."'
										 AND RSV.status = 'Checked In'
										 GROUP BY R.room_assign_id
									 )
									 ORDER BY R1.room_number ASC
									");
		$stayRoom = count($stayRoomSql);
		
		$TA_CheckInPrct = ($todayCheckIN)?round(($cmpltCheckIN/$todayCheckIN)*100):0;
		$TA_VacantRPrct = ($TotalRooms)?round(($availRoom/$TotalRooms)*100):0;
		$TA_StayPrct    = ($TotalRooms)?round(($stayRoom/$TotalRooms)*100):0;
		$TA_BlockPrct   = ($TotalRooms)?round(($blockRoom/($TotalRooms+$blockRoom))*100):0;
		
		$rquestFromDate = $toDay; $rquestToDate = date('Y-m-d', strtotime($toDay.'+1 days'));
		$queryPart = "SELECT PRC.room_rate_price FROM room_rate_option as PRC
					  WHERE PRC.room_type_id = RT.room_type_id AND PRC.hotel_id = ".$Hotel_ID." AND (PRC.room_rate_date between '".$toDay."' AND '".$toDay."')
					  ORDER BY PRC.room_rate_date ASC LIMIT 0, 1";
                
                                                    
		$RoomListQry  = DB::select("SELECT RT.room_type_id, RT.room_type, RT.room_count, RT.room_pic_1, RT.guest_adult, RT.guest_child,
									CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as BedSize,
									( ".$queryPart." ) as room_rate,
									(   room_count - 
										( IF(	( SELECT SUM(RSV.qty_reserve) FROM room_reservation as RSV WHERE RSV.room_type_id = RT.room_type_id AND RSV.status != 'Cancelled'
												  AND NOT ((RSV.departure <= '".$rquestFromDate."') OR (RSV.arrival >= '".$rquestToDate."')) GROUP BY RSV.room_type_id
												) != 0, 
												( SELECT SUM(RSV.qty_reserve) FROM room_reservation as RSV WHERE RSV.room_type_id = RT.room_type_id AND RSV.status != 'Cancelled'
												  AND NOT ((RSV.departure <= '".$rquestFromDate."') OR (RSV.arrival >= '".$rquestToDate."')) GROUP BY RSV.room_type_id
												), 0              
											)
										) - 
										( IF(	( SELECT count(R.room_type_id) FROM room_assign as R
												  WHERE R.room_type_id = RT.room_type_id AND (R.status = 0 OR R.trash = 1 OR R.room_number = '') GROUP BY R.room_type_id
												) != 0, 
												( SELECT count(R.room_type_id) FROM room_assign as R
												  WHERE R.room_type_id = RT.room_type_id AND (R.status = 0 OR R.trash = 1 OR R.room_number = '') GROUP BY R.room_type_id
												), 0
											)
										) - 
										( IF(
												( SELECT count(DISTINCT(BR.room_assign_id)) FROM block_rooms as BR WHERE BR.room_type_id = RT.room_type_id
												  AND NOT ((BR.block_to <= '".$rquestFromDate."') OR (BR.block_from >= '".$rquestToDate."')) GROUP BY BR.room_type_id
												) != 0, 
												( SELECT count(DISTINCT(BR.room_assign_id)) FROM block_rooms as BR WHERE BR.room_type_id = RT.room_type_id
												  AND NOT ((BR.block_to <= '".$rquestFromDate."') OR (BR.block_from >= '".$rquestToDate."')) GROUP BY BR.room_type_id
												), 0              
											)
										)
									) as CNT
									FROM roomtypes as RT INNER JOIN room_assign as RA ON RA.room_type_id = RT.room_type_id
									WHERE RT.hotel_id = ".$Hotel_ID." AND RA.room_number != '' GROUP BY RT.room_type_id ORDER BY RT.room_order ASC, CNT DESC");
               
		return view('pages.dashboard')->with("HotelRst", $HotelRst)->with("RoomListQry", $RoomListQry)->with("occupancyRoom", $occupancyRoom)
									  ->with("todayDeparture", $todayDeparture)->with("vacantDirty", $vacantDirty)->with("availRoom", $availRoom)
									  ->with("todayCheckIN", $todayCheckIN)->with("cmpltCheckIN", $cmpltCheckIN)->with("rmainCheckIN", $rmainCheckIN)
									  ->with("TA_CheckInPrct", $TA_CheckInPrct)->with("TA_VacantRPrct", $TA_VacantRPrct)->with("TotalRooms", $TotalRooms)
									  ->with("stayRoom", $stayRoom)->with("TA_StayPrct", $TA_StayPrct)->with("blockRoom", $blockRoom)->with("TA_BlockPrct", $TA_BlockPrct)
									  ->with("weatherSchene", $weatherSchene)->with("weatherCondit", $weatherCondit);
    }
}
