<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class PageController extends Controller
{
    // Index Page
	public function getIndex()
    {
        return view('pages.index');
    }
	
	public function getDeshBoard()
    {
        return view('pages.dashboard');
    }
	
	// About Us Page
	public function getAbout()
    {
		$CompanyName = "Cogent Infoway Ltd";
		$isUserRegistered = true;
       	return view('pages.about-us')
					->with("cName", $CompanyName)
					->with("cUser", $isUserRegistered);
    }
	
	// Contact Us Page
	public function getContact()
    {
		return view('pages.contact-us');
    }
	
	// Privacy Policy Page
	public function getPrivacy()
    {
        return view('pages.privacy');
    }
	
	// Privacy Policy Page
	public function getTest()
    {
        return view('pages.test');
    }
}
