@extends('layouts.layout')

{{--*/ $TitleSubject = "Hotel# MMKiD".$Hotel_ID.", ".$MotelInfo->hotel_name." :: Print Assignment Sheets";/*--}}
@section('title')
	{{$TitleSubject}}
@stop

@section('CascadingSheet')
    {!!Html::script('vendors/pdfmake/build/sprintf.js')!!}
    {!!Html::script('vendors/pdfmake/build/jspdf.js')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Print Assignment Sheets</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('housekeeping-headquarters')}}">HouseKeeping Central</a></li>
                <li class="active"><a href="javascript:void(0);">Print Assignment Sheets</a></li>
            </ul>
        </div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                    	{{--*/
                        $EmpSql = DB::select("SELECT E.emp_id, E.emp_fname, E.emp_mname, E.emp_lname
                                               FROM housekeeping as H INNER JOIN employees as E ON E.emp_id = H.emp_id
                                               WHERE H.hotel_id = ".$Hotel_ID." AND H.status = 0
                                               GROUP BY E.emp_id
                                               ORDER BY E.emp_fname ASC");
                        /*--}}
                        @if(count($EmpSql))
                        <div class="form-horizontal form-label-left">
                            <div class="col-md-3 col-sm-3 col-xs-12 zeroPadd">
                                <select name="emp_id" id="emp_id" class="form-control col-md-3 col-xs-6"  onchange="FilterSource(this.value)">
                                    <option value="">Select Employee</option>
                                    @foreach( $EmpSql as $employee )
                                    <option value="{!!$employee->emp_id!!}"{!!($request->emp_id == $employee->emp_id)?" Selected":"";!!}>
                                        {!!$employee->emp_fname!!}{!!($employee->emp_mname)?" $employee->emp_mname":""!!}{!!" ".$employee->emp_lname!!}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @else
                        <div class="col-md-12 col-sm-12 col-xs-12 labelText alert alert-warning marginZero AlgnCenter">
                        	No Record Found
                        </div>
                        @endif
                    </div>
                    @if($request->emp_id)
                    <div class="x_content" id="printableArea">
                        <table class="table table-striped" id="pdfableArea">
                            <tr>
                                <th colspan="4" class="AlgnCenter font16">Housekeeping Room Assignment</th>
                            </tr>
                            <tr>
                                <td width="16%">
                                	<div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5">Housekeeper Name:</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5">Time In:</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5">Break In:</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5">Break Out:</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5">Time Out:</div>
                              	</td>
                                <td width="42%">
                                	{{--*/ $EmpNameSql = App\Employee::where('emp_id', $request->emp_id)->first(); /*--}}
                                	<div class="col-md-12 col-sm-12 col-xs-12 marginBottom5">
                                    	{!!$EmpNameSql->emp_fname!!}{!!($EmpNameSql->emp_mname)?" $EmpNameSql->emp_mname":""!!}{!!" ".$EmpNameSql->emp_lname!!}
                                   	</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5">&nbsp;</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5">&nbsp;</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5">&nbsp;</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5">&nbsp;</div>
                                </td>
                                <td width="10%">
                                	<div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnRight">Date:</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnRight">Time:</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnRight">User:</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5">&nbsp;</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5">&nbsp;</div>
                               	</td>
                                <td width="20%">
                                	<div class="col-md-12 col-sm-12 col-xs-12 marginBottom5">{{date('m/d/Y')}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5">{{date('h:i A')}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5">{{Helper::getAdminUserName()}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5">&nbsp;</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5">&nbsp;</div>
                                </td>
                            </tr>
                            <tr class="backgroundNone">
                            	<td colspan="4" class="zeroPadd borderNone">
                                    <table id="exelableArea" class="table table-bordered table-striped jambo_table">
                                        <tr>
                                            <th width="7%">Room#</th>
                                            <th width="7%">Type</th>
                                            <th width="7%">Condition</th>
                                            <th width="7%">Status</th>
                                            <th width="7%">&nbsp;</th>
                                            <th width="8%">Deep Cleaning</th>
                                            <th width="8%">Bed Sheet</th>
                                            <th width="8%">Pillow Cases</th>
                                            <th width="8%">Bath Towels</th>
                                            <th width="8%">Floor Mat</th>
                                            <th width="8%">Wash Cloth</th>
                                            <th width="17%">Comments:</th>
                                        </tr>
                                        {{--*/
                                        $roomSql = DB::select("SELECT R.room_number as RoomNumber, R.room_stat as RoomStat,
                                        					   CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType, RT.room_type as RoomTypes
                                                               FROM housekeeping as H INNER JOIN employees as E ON E.emp_id = H.emp_id
                                                               INNER JOIN room_assign as R ON R.room_assign_id = H.room_assign_id
                                                               INNER JOIN roomtypes as RT ON RT.room_type_id = R.room_type_id
                                                               WHERE H.hotel_id = ".$Hotel_ID." AND H.status = 0 AND E.emp_id = ".$request->emp_id."
                                                               ORDER BY RoomNumber ASC");
                                        /*--}}
                                        @foreach($roomSql as $roomVals)
                                        <tr>
                                            <td>{{$roomVals->RoomNumber}}</td>
                                            <td>{{$roomVals->RoomTypes}}</td>
                                            <td>{{($roomVals->RoomStat==1)?"Clean":"Dirty"}}</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        @endforeach
                                    </table>
                              	</td>
                            </tr>
                        </table>
                    </div>
                    <button class="btn btn-primary marginTop20 margin5" id="btnPrint"><i class="fa fa-print"></i> Print</button>
                    <button class="btn btn-success marginTop20 margin5" id="btnExcel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</button>
                    <button class="btn btn-danger marginTop20 margin5" id="btnPDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</button>
                    @endif
                </div>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function FilterSource(str){
		if(str){
			RedirectStr = '?';
			if(str){
				RedirectStr += "emp_id="+str+"&";
			}
			RedirectStr = RedirectStr.replace(/&+$/,'');
			window.location.href="{!!url('housekeeping-print')!!}"+RedirectStr;
		}else if(!str){
			window.location.href="{!!url('housekeeping-print')!!}";
		}
	}
</script>
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	$("#btnPrint").click(function(e){
		var printContents = document.getElementById('printableArea').innerHTML;
		var originalContents = document.body.innerHTML;
		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
		window.location.reload();
	});
	$("#btnExcel").click(function(e){
		e.preventDefault();
		
		var data_type = 'data:application/vnd.ms-excel';
		var table_div = document.getElementById('exelableArea');
		var table_html = table_div.outerHTML.replace(/ /g, '%20');
		
		var a = document.createElement('a');
		a.href = data_type + ', ' + table_html;
		a.download = '{{$TitleSubject}}.xls';
		a.click();
	});
	$("#btnPDF").click(function(e){
		var pdf = new jsPDF('p', 'pt', 'letter');
		source = $('#pdfableArea')[0];
		specialElementHandlers = {
			// element with id of "bypass" - jQuery style selector
			'#bypassme': function(element, renderer) {
				// true = "handled elsewhere, bypass text extraction"
				return true
			}
		};
		margins = {
			top: 40,
			bottom: 40,
			left: 40,
			width:572
		};
		
		pdf.fromHTML(
				source, // HTML string or DOM elem ref.
				margins.left, // x coord
				margins.top,
				{// y coord
					'width': margins.width, // max width of content on PDF
					'elementHandlers': specialElementHandlers
				},
				function(dispose) {
					pdf.save('{{$TitleSubject}}.pdf');
				},
				margins
		);
	});
});
</script>
@stop