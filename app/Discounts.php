<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discounts extends Model
{
    protected $fillable = [
        'discount_id_unq', 'hotel_id', 'discount_code', 'discount_value',
		'discount_type', 'discount_from', 'discount_to',
		'reg_ip', 'added_by'
    ];
	protected $primaryKey = 'discount_id';
	protected $table = 'discounts';
}
