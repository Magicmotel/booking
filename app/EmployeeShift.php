<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeShift extends Model
{
    protected $table = 'employee_shifts';
	protected $primaryKey = 'emp_shift_id';
	protected $fillable = [
        'hotel_id', 'emp_id', 'shift_date', 'shift_id',
		'reg_ip', 'added_by'
    ];
}
