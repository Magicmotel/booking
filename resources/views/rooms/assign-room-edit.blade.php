@extends('layouts.layout')

@section('title')
	Manage Room Numbers for {{$Rst->room_type}}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Room Numbers for {{$Rst->room_type}}</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('room/assign-room')}}">Manage Room Numbers</a></li>
                <li class="active"><a href="javascript:void(0);">Assign Room Numbers</a></li>
                <li><a href="{{url('room/room-type')}}">Manage Room Types</a></li>
                <li><a href="{{url('room/room-type/create')}}">Create Room Type</a></li>
            </ul>
        </div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                    <form id="demo-form2" action="{!!url('room/assign-room', $Rst->room_type_id)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                        {!!method_field('PATCH')!!}
                        {!!csrf_field()!!}
                    	@foreach($Results as $val )
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Room Number <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                            	<input type="hidden" name="room_assign_id[]" value="{!!$val->room_assign_id!!}" id="room_assign_id_{!!$val->room_assign_id!!}" class="form-control col-md-7 col-xs-12 inputRoomId">
                                <input type="text" name="room_number[]" value="{!!$val->room_number!!}" id="room_number_{!!$val->room_assign_id!!}" class="form-control col-md-7 col-xs-12 inputRoomNumber" placeholder="Room Number" required="required">
                            </div>
                            <div class="control-label labelText col-md-4 col-sm-6 col-xs-12 alertFont AlgnLeft alertBox_Error" id="alertBox_{!!$val->room_assign_id!!}"></div>
                        </div>
                        @endforeach
                    	<div class="form-group form-group-last marginTop20">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success" onclick="return getResults();"><i class="fa fa-check-square-o"></i> Update Room Numbers</button>
                                <button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript">
var js_array = new Array();
<?	foreach($allRooms as $value) { ?>
	js_array["<?=$value->room_assign_id?>"] = "<?=$value->room_number?>";
<?  } ?>
//console.log(js_array);

function getResults(){
 	var inputRoomId     = $("input.inputRoomId");
	var inputRoomNumber = $("input.inputRoomNumber");
	inputRoomNumberLength = (inputRoomNumber.length);
	var dupDetector = {};
	var RoomValue = ""; var RoomId = "";
    for(PS = 0; PS < inputRoomNumberLength; PS++)
	{
		RoomValue = inputRoomNumber[PS].value;
		RoomId    = inputRoomId[PS].value;
		$(".alertBox_Error").html("");
		if (RoomValue) {
			var a = js_array.indexOf(RoomValue);
			if(a < 0){
				//console.log("New: "+RoomId+"---"+RoomValue);
			}else if(RoomId != a){
				//console.log("Error: "+RoomId+"---"+RoomValue);
				$("#alertBox_"+RoomId).html("Room Number already exists!!");
				return false;
			}
			
			if (dupDetector[RoomValue]) { 
				$("#alertBox_"+RoomId).html("Room Number already exists!!");
				return false;
			}
			dupDetector[RoomValue] = true;
		}else{
			$("#alertBox_"+RoomId).html("Room Number is required!!");
			return false;
		}
	}
	return true;
}
</script>
@stop