@extends('layouts.layout')

@section('title')
	Front-Desk
@stop

@section('CascadingSheet')
    {!!Html::script('vendors/daypilot/jquery-1.9.1.min.js')!!}
    {!!Html::script('vendors/daypilot/daypilot-all.min.js')!!}
	<style type="text/css">
	.icon{font-size:14px;text-align:center;line-height:14px;vertical-align:middle;cursor:pointer;}
	.scheduler_default_rowheader_inner{border-right:1px solid #ccc;}
	.scheduler_default_rowheadercol2{background:White;}
	.scheduler_default_rowheadercol2 .scheduler_default_rowheader_inner{top:1px;bottom:2px;left:1px;right:2px;background:#1A9D13; text-align:center; font-weight:500; color:#FFFFFF}
	.status_dirty.scheduler_default_rowheadercol2 .scheduler_default_rowheader_inner{background:#F9BA25;}
	.status_cleanup.scheduler_default_rowheadercol2 .scheduler_default_rowheader_inner{background:#EA3624;}
	.scheduler_default_corner div:nth-of-type(4){display:none !important;}
	</style>
     <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
            	<div class="col-md-2 col-sm-2 col-xs-12 zeroPadd marginRight10">
                    <input type="text" name="from" id="from" class="form-control has-feedback-left" placeholder="Arrival Date" value="" required="required" readonly="readonly">
                    <span class="fa fa-calendar-o form-control-feedback left" style="left:0" aria-hidden="true"></span>
                </div>
            	<div class="widht100 marginTop10 marginBottom40 AlgnRight">
                	<div class="fltL marginRight15"><div class="TapRoomBox TapRoomClean">C</div>Clean Room</div>
                	<div class="fltL marginRight15"><div class="TapRoomBox TapRoomDirty">D</div>Dirty Room</div>
                	<div class="fltL marginRight15"><div class="TapRoomBox TapRoomMaintenance">X</div>Maintenance</div>
                    
                    <div class="fltR marginLeft15"><div class="TapRoomBox TapRsrvOut"><i class="fa fa-ban" aria-hidden="true"></i></div>Blocked</div>
                    <div class="fltR marginLeft15"><div class="TapRoomBox TapRsrvLock"><i class="fa fa-lock" aria-hidden="true"></i></div>Do Not Move</div>
                    <div class="fltR marginLeft15"><div class="TapRsrvBox TapRsrvExpired"></div>Incoming</div>
                    <div class="fltR marginLeft15"><div class="TapRsrvBox TapRsrvInHouse"></div>In House</div>
                	<div class="fltR marginLeft15"><div class="TapRsrvBox TapRsrvNew"></div>Reserved</div>
                </div>
                <div id="dp"></div>
  			</div>
      	</div>
   	</div>
</div>
<!-- calendar modal -->
<div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="myModalLabel">Confirm</h4>
            </div>
            <div class="modal-footer AlgnCenter">
                <button type="button" class="btn btn-success antosubmit" data-dismiss="modal">Change Room with same Rate</button>
                <button type="button" class="btn btn-warning antisubmit" data-dismiss="modal">Change Room with new Rate</button>
                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
@stop

@section('jQuery')
<script>
	var TodayDate = DayPilot.Date.today();
	var now = new Date(TodayDate);
	var todayMonth = now.getMonth()+1;
	var todayDay = now.getDate();
	$('#from').val(((todayMonth<10)?"0":"")+todayMonth+"/"+((todayDay<10)?"0":"")+todayDay+"/"+now.getFullYear());

	var dp = new DayPilot.Scheduler("dp");

	dp.allowEventOverlap = false;

	//dp.scale = "Day";
	//dp.startDate = new DayPilot.Date().firstDayOfMonth();
	dp.days = dp.startDate.daysInMonth();
	var checkin = $('#from').datepicker({
		startDate: moment(),
		onRender: function(date){
			now.setHours(0); now.setMinutes(0); now.setSeconds(0); now.setMilliseconds(0);
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		checkin.hide();
		var startVal = $('#from').val();
		var dddd = startVal.split("/");
		dateStart = new Date(DayPilot.Date.today());
		dateStart.setMonth(dddd[0]-1, dddd[1]);
		dateStart.setYear(dddd[2]);
		loadTimeline(new DayPilot.Date(new Date(dateStart.setUTCMinutes(dateStart.getUTCMinutes()+330))));
		loadEvents();
	}).data('datepicker');
	loadTimeline(TodayDate);
	//loadTimeline(DayPilot.Date.today().firstDayOfMonth());

	dp.eventDeleteHandling = "Update";

	dp.timeHeaders = [
		{ groupBy: "Month", format: "MMMM yyyy" },
		{ groupBy: "Day", format: "d" }
	];

	dp.eventHeight = 30;
	//dp.bubble = new DayPilot.Bubble({});

	dp.rowHeaderColumns = [
		{title: "Room", width: 70},
		{title: "Type", width: 70},
		{title: " ", width: 30}
	];

	dp.onBeforeResHeaderRender = function(args)
	{
		args.resource.columns[0].html = args.resource.room_type;
		args.resource.columns[1].html = args.resource.status;
		switch (args.resource.status) {
			case "C":
				args.resource.cssClass = "status_ready";
				break;
			case "D":
				args.resource.cssClass = "status_dirty";
				break;
			case "X":
				args.resource.cssClass = "status_cleanup";
				break;
		}
		
		args.resource.areas = [{
			top:3,
			right:4,
			height:14,
			width:14,
			action:"JavaScript",
			js: function(r) {
				var modal = new DayPilot.Modal();
				modal.onClosed = function(args) {
					loadResources();
				};
			},
		}];
	};

	// NEW RESERVATION
	dp.onTimeRangeSelected = function (args)
	{
		if(args.resstatus == 'X'){
			dp.message('Room is under Maintenance Please choose another Room!');
			alert('Room is under Maintenance Please choose another Room!');
			dp.clearSelection();
			loadEvents();	
		}else{
			var startDate = USAFormatDate(dateSplit(args.start.value));
			var endDate   = USAFormatDate(dateSplit(args.end.value));
			var x = confirm("Are you sure you want to create new Reservation?\n\n from "+startDate+" to "+endDate+"");
			if (x)
			{
				var modal = new DayPilot.Modal();
				modal.closed = function() {
					dp.clearSelection();
					var data = this.result;
					if (data && data.result === "OK") {
						loadEvents();
					}
				};
				window.location.href="{!!url('reservation/support')!!}?from="+args.start+"&to="+args.end;
				//modal.showUrl("new.php?start=" + args.start + "&end=" + args.end + "&resource=" + args.resource);
			}else{
				dp.clearSelection();
				loadEvents();
			}
		}
	};
	
	// EDIT RESERVATION
	dp.onEventClick = function(args)
	{
		if(args.e.data.rsrvlock==1 && args.e.data.status == 'Blocked'){
			dp.message('Room Blocked');
			loadEvents();
		}else{
			//window.location.href="{!!url('guest-info')!!}/"+args.e.id();
			var modal = new DayPilot.Modal();
			modal.closed = function() {
				// reload all events
				var data = this.result;
				if (data) {
					window.location.href=data;
				}
			};
			modal.showUrl("{!!url('showReservation')!!}/"+args.e.id());
		}
	};
	
	// RESIZE RESERVATION
	dp.onEventResized = function (args){
		if(args.e.data.rsrvlock==1 && args.e.data.status == 'Blocked'){
			dp.message('Room Blocked');
			loadEvents();
		}else{
			var startDate = USAFormatDate(dateSplit(args.newStart.toString()));
			var endDate   = USAFormatDate(dateSplit(args.newEnd.toString()));
			var x = confirm("Are you sure you to change (arrival or departure date)?\n\nFrom: "+startDate+" to: "+endDate+".");
			if (x)
			{
				var arrivalD = dateSplit(args.newStart.toString());
				var departD  = dateSplit(args.newEnd.toString());
				var reserv_night = daydiff(parseDate(arrivalD), parseDate(departD));
				$.post("{!!url('updateReservation')!!}",
				{
					reservation_id: args.e.id(),
					arrival  : arrivalD,
					departure: departD,
					reserv_night: reserv_night,
					"_token": "{{ csrf_token() }}"
				},
				function(data) {
					dp.message(data.message);
					loadEvents();
				});
			}else{
				loadEvents();
			}
		}
	};
	
	// MOVE RESERVATION
	dp.onEventMoved = function (args){
		if(args.e.data.rsrvlock==1 && args.e.data.status == 'Blocked'){
			dp.message('Room Blocked');
			loadEvents();
		}else if(args.e.data.rsrvlock!=1){
			if(args.fullResource.status == 'X'){
				dp.message('Room is under Maintenance Please choose another Room!');
				alert('Room is under Maintenance Please choose another Room!');
				dp.clearSelection();
				loadEvents();	
			}else{
				var startDate = USAFormatDate(dateSplit(args.newStart.toString()));
				var endDate   = USAFormatDate(dateSplit(args.newEnd.toString()));
				if(args.e.data.room_type_id != args.fullResource.room_type_id){
					var Msg = "Are you sure you want change Room Type?\n\nFrom: "+startDate+" to: "+endDate+"."
				}else{
					var Msg = "Are you sure you to change (arrival or departure date)?\n\nFrom: "+startDate+" to: "+endDate+"."
				}
				var x = confirm(Msg);
				if (x)
				{
					var rsrvId   = args.e.id()
					var arrivalD = dateSplit(args.newStart.toString());
					var departD  = dateSplit(args.newEnd.toString());
					var reserv_night = daydiff(parseDate(arrivalD), parseDate(departD));
					var room_id  = args.newResource;
					if(args.e.data.room_type_id != args.fullResource.room_type_id){
						$('#fc_create').click();
						$(".antosubmit").on("click", function() {
							var price_stat = 0;
							$.post("{!!url('updateReservation')!!}",
							{
								reservation_id: rsrvId,
								arrival  	: arrivalD,
								departure	: departD,
								reserv_night: reserv_night,
								price_stat  : price_stat,
								room_id     : room_id,
								"_token"	: "{{ csrf_token() }}"
							},
							function(data) {
								dp.message(data.message);
								loadEvents();
							});
							return true;
						});
						$(".antisubmit").on("click", function() {
							var price_stat = 1;
							$.post("{!!url('updateReservation')!!}",
							{
								reservation_id: rsrvId,
								arrival  	: arrivalD,
								departure	: departD,
								reserv_night: reserv_night,
								price_stat  : price_stat,
								room_id     : room_id,
								"_token"	: "{{ csrf_token() }}"
							},
							function(data) {
								dp.message(data.message);
								loadEvents();
							});
							return true;
						});
						$(".antoclose").on("click", function() {
							loadEvents();
							return true;
						});
					}else{
						var price_stat = 0;
						$.post("{!!url('updateReservation')!!}",
						{
							reservation_id: rsrvId,
							arrival  	: arrivalD,
							departure	: departD,
							reserv_night: reserv_night,
							price_stat  : price_stat,
							room_id     : room_id,
							"_token"	: "{{ csrf_token() }}"
						},
						function(data) {
							dp.message(data.message);
							loadEvents();
						});
					}
				}else{
					loadEvents();
				}
			}
		}else{
			dp.message('Reservation Locked');
			loadEvents();
		}
	};
	
	function dateSplit(str){
		var datvar = str.split('T');
		return datvar[0];
	}
	function USAFormatDate(str){
		var mdy = str.split('-');
		return mdy[1]+'/'+mdy[2]+'/'+mdy[0];
	}
	function parseDate(str) {
		var mdy = str.split('-');
		return new Date(mdy[0], mdy[1]-1, mdy[2]);
	}
	
	function daydiff(first, second) {
		return Math.round((second-first)/(1000*60*60*24));
	}
	
	// CANCEL RESERVATION
	dp.onEventDeleted = function(args) {
		if(args.e.data.status == 'Blocked'){
			var x = confirm("Are you sure you want to unblock this Room?");
			if (x)
			{	
				$.get("{!!url('unBlockRoom')!!}?block_room_id="+args.e.id(), function(msg) {
					dp.message(msg.message);
					loadEvents();
				});
			}
			loadEvents();
		}else{
			var x = confirm("Are you sure you want to cancel this Reservation?");
			if (x)
			{
				window.location.href="{!!url('cancel-reservation')!!}/"+args.e.id();
				//$.get("{!!url('cancelReservation')!!}?room_reservation_id="+args.e.id(), function(msg) {
					//dp.message(msg.message);
				//});
			}
			loadEvents();
		}
	};

	dp.onBeforeCellRender = function(args) {
		var dayOfWeek = args.cell.start.getDayOfWeek();
		if (dayOfWeek === 6 || dayOfWeek === 0) {
			args.cell.backColor = "#f8f8f8";
		}
	};

	dp.onBeforeEventRender = function(args) {
		var start = new DayPilot.Date(args.e.start);
		var end = new DayPilot.Date(args.e.end);

		var today = DayPilot.Date.today();
		var now = new DayPilot.Date();
		var hoverTitle = args.e.bubbleHtml+" (" + start.toString("MM/dd/yyyy") + " - " + end.toString("MM/dd/yyyy") + ")\n";
		args.e.html = ( (args.e.rsrvlock==1 && args.e.status == 'Blocked')?'<i class="fa fa-ban " aria-hidden="true"></i> ':((args.e.rsrvlock==1)?'<i class="fa fa-lock " aria-hidden="true"></i> ':''))+args.e.text + " (" + start.toString("MM/dd/yyyy") + " - " + end.toString("MM/dd/yyyy") + ")";

		switch (args.e.status) {
			case 'Blocked': // checked out
				args.e.barColor = "#626262";
				hoverTitle += "Blocked";
				break;
			case 'Checked In': // arrived
				var checkoutDeadline = today.addHours(12);

				if (end < today || (end.getDatePart() === today.getDatePart() && now > checkoutDeadline)) { // must checkout before 12 PM
					args.e.barColor = "#FF0000";  // red
					hoverTitle += "Late checkout";
				}
				else
				{
					args.e.barColor = "#3DB82B";  // blue
					hoverTitle += "Checked In";
				}
				break;
			case 'Checked Out': // checked out
				args.e.barColor = "#626262";
				hoverTitle += "Checked Out";
				break;
			default:
				var in2days = today.addDays(1);

				if (start < in2days) {
					args.e.barColor = '#DDE50B';
					hoverTitle += 'Incoming Reservation';
				}
				else {
					args.e.barColor = '#1691F4';
					hoverTitle += 'Reserved';
				}
				break;
		}
		args.e.toolTip = hoverTitle;
		/*
		args.e.html = args.e.html + "<br /><span style='color:gray'>" + args.e.toolTip + "</span>";
		
		var paid = args.e.paid;
		var paidColor = "#aaaaaa";

		args.e.areas = [
			{ bottom: 10, right: 4, html: "<div style='color:" + paidColor + "; font-size: 8pt;'>Paid: " + paid + "%</div>", v: "Visible"},
			{ left: 4, bottom: 8, right: 4, height: 2, html: "<div style='background-color:" + paidColor + "; height: 100%; width:" + paid + "%'></div>", v: "Visible" }
		];*/
	};

	dp.init();

	loadResources();
	loadEvents();

	function loadTimeline(date) {
		dp.scale = "Manual";
		dp.timeline = [];
		var start = date.getDatePart().addHours(12);
		for (var i = 0; i < dp.days; i++) {
			dp.timeline.push({start: start.addDays(i), end: start.addDays(i+1)});
		}
		dp.update();
	}

	function loadEvents() {
		var start = dp.visibleStart();
		var end = dp.visibleEnd();
		
		$.ajax({
			url: "{{ url('/backend_Events') }}",
			type: 'POST',
			data:{start: start.toString(), end: end.toString(), "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				dp.events.list = msg;
				dp.update();
			},
			error: function( data ) {
			}
		});
	}

	function loadResources()
	{
		$.ajax({
			url: "{{ url('/backend_Rooms') }}",
			type: 'POST',
			data:{"_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				dp.resources = msg;
				dp.update();
			},
			error: function( data ) {
			}
		});
	}
</script>
@stop