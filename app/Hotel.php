<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
	protected $fillable= [
		'hotel_id_unq', 'hotel_name', 'hotel_rooms', 'hotel_plan',
		'hotel_address', 'hotel_city', 'hotel_county', 'hotel_state', 'hotel_zip', 'hotel_country',
		'hotel_phone_1', 'hotel_phone_2', 'hotel_phone_3', 'hotel_phone_4', 'hotel_fax_1', 'hotel_fax_2', 'hotel_website', 'hotel_email',
		'hotel_o_fname', 'hotel_o_mname', 'hotel_o_lname', 'hotel_m_fname', 'hotel_m_mname', 'hotel_m_lname',
		'hotel_o_phone_1', 'hotel_o_phone_2', 'hotel_o_email', 'hotel_m_phone_1', 'hotel_m_phone_2', 'hotel_m_email',
		'hotel_logo', 'hotel_pic_1', 'hotel_pic_2', 'hotel_pic_3', 'hotel_pic_4', 'hotel_wifi_pass',
		'hotel_checkin_time', 'hotel_checkout_time', 'hotel_cancel_period', 'hotel_disclaimer',
		'trash', 'reg_ip', 'added_by'		
	];
	protected $primaryKey = 'hotel_id';
	protected $table = 'hotels';
}
