<!DOCTYPE html>
<html>
<head>
    <title>404 Error Page</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    {!!Html::style('vendors/bootstrap/dist/css/bootstrap.min.css')!!}
    {!!Html::style('vendors/font-awesome/css/font-awesome.min.css')!!}
    <style>
		html, body{height:100%;}
		body{margin:0;padding:0;width:100%;color:#B0BEC5;display:table;font-weight:100;}
		.container{text-align:center;display:table-cell;vertical-align:middle;}
		.content{text-align:center;display:inline-block;}
		.title{font-size:72px;margin-bottom:0;}
        .marginLeft10{margin-left:10px !important; }
		.delayMsg{margin-bottom:20px;color:#FF6600;font:bold 13px arial;}
   </style>
</head>
<body>
    <div class="container">
        <div class="content">
            {{--*/ $fileFolderPath = Helper::getImageFolderPath()."/build/images/404.jpg"; /*--}}
            <div class="title">{!!Html::image($fileFolderPath, 'alt')!!}</div>
            <div class="delayMsg" id="delayMsg"></div>
            <a href="{{URL::previous()}}" class="btn btn-default"><i class="fa fa-chevron-left"></i> Back</a>
            <a href="{{url('/dashboard')}}" class="btn btn-primary marginLeft10"><i class="fa fa-home"></i> Go to Dashboard</a>
        </div>
    </div>
    {!!Html::script('vendors/jquery/dist/jquery.min.js')!!}
    {!!Html::script('vendors/jquery/dist/jquery-ui.min.js')!!}
    {!!Html::script('vendors/bootstrap/dist/js/bootstrap.min.js')!!}
    <script type="text/javascript" language="javascript">
		document.getElementById('delayMsg').innerHTML = 'Please wait you\'ll be redirected after <span id="countDown">5</span> seconds....';
		var count = 5;
		setInterval(function(){
			count--;
			if (count == 0) {
				$("body").fadeOut(1000,function(){
				   window.location.href = "{{url('/dashboard')}}";
				})				
			}else{
				document.getElementById('countDown').innerHTML = count;
			}
		},1000);
	</script>
</body>
</html>
