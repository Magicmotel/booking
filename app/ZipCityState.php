<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZipCityState extends Model
{
    protected $table = 'zip_city_states';
	protected $primaryKey = 'zip_id';
	protected $fillable = [
        'zip_code', 'zip_city', 'zip_state', 'zip_country', 'zip_latitude', 'zip_longitude'
    ];
}
