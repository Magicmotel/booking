@extends('layouts.layout')

{{--*/ $TitleSubject = "Hotel# MMKiD".$Hotel_ID.", ".$MotelInfo->hotel_name." :: Employee Log History ".$startDate." to ".$endDate;/*--}} 
@section('title')
	{{$TitleSubject}}
@stop

@section('CascadingSheet')
    {!!Html::script('vendors/pdfmake/build/sprintf.js')!!}
    {!!Html::script('vendors/pdfmake/build/jspdf.js')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Employee Log History</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class="active"><a href="javascript:void(0);">Employee Log History</a></li>
                <li><a href="{{url('report-employee-list')}}">Employee List Report</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
          	    <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <form id="demo-form2" action="{!!url('report-employee-log-history')!!}" method="get" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                            	<div class="col-md-2 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group form-group-last">
                                        <label class="control-label AlgnLeft col-md-12 col-sm-12 col-xs-12">Start Date</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" name="start_date" id="start_date" class="form-control has-feedback-left" placeholder="Start Date" value="{!!$startDate!!}" readonly="readonly" >
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                    </div>
                               	</div>
                                <div class="col-md-2 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group form-group-last">
                                        <label class="control-label AlgnLeft col-md-12 col-sm-12 col-xs-12">End Date</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" name="end_date" id="end_date" class="form-control has-feedback-left" placeholder="End Date" value="{!!$endDate!!}" readonly="readonly" >
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                    </div>
                               	</div>
                                <div class="col-md-2 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group form-group-last">
                                        <label class="control-label AlgnLeft col-md-12 col-sm-12 col-xs-12">Employee</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                        	{{--*/
                                            $empQry = DB::select("SELECT id, fname, lname FROM users WHERE hotel_id = ".$Hotel_ID." ORDER BY id ASC, fname ASC");
                                            /*--}}
                                            <select name="emp_id" id="emp_id" class="form-control">
                                                <option value="">Select Employee</option>
                                                @foreach($empQry as $val)
                                                <option value="{!!$val->id!!}"{!!($request->emp_id == $val->id)?" Selected":""!!}>{!!$val->fname." ".$val->lname!!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                               	</div>
                                <div class="col-md-2 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group form-group-last">
                                        <label class="control-label AlgnLeft col-md-12 col-sm-12 col-xs-12">Reservation#</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" name="rsrv_id" id="rsrv_id" class="form-control" placeholder="Enter Reservation Id" value="{!!$request->rsrv_id!!}">
                                        </div>
                                    </div>
                               	</div>
                                <div class="col-md-2 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group form-group-last">
                                    	<label class="control-label AlgnLeft col-md-12 col-sm-12 col-xs-12">&nbsp;</label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
                                        </div>
                                    </div>
                               	</div>
                            </div>
                        </form>
                  	</div>
                </div>
          	</div>
            @if($startDate)
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  	<div class="x_content" id="printableArea">
                        <table class="table table-striped" id="pdfableArea">
                            <tr class="backgroundNone">
                                <td width="20%" class="borderNone height140">
                                	{!!Html::image('motel/'.$MotelInfo->hotel_logo, 'alt', array( 'title' => 'Motel Logo', 'style' => 'max-width:120px; max-height:120px;'))!!}
                              	</td>
                                <td width="60%" align="center" valign="middle" class="zeroPadd borderNone labelText font16">
                                	Employees List<br /><br />
                                    From: {{$startDate}} to {{$endDate}}
                                </td>
                                <td width="20%" class="zeroPadd borderNone">
                                	<div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnLeft LneHeight24">Date: {{date('m/d/Y')}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnLeft LneHeight24">Time: {{date('h:i A')}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnLeft LneHeight24">User: {{Helper::getAdminUserName()}}</div>
                               	</td>
                            </tr>
                            <tr class="backgroundNone">
                            	<td colspan="3" class="zeroPadd borderNone">
                                	<table id="exelableArea" class="table table-bordered table-striped jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>Change Date</th>
                                                <th>Change User</th>
                                                <th>Category</th>
                                                <th>Field Name</th>
                                                <th>Reservation#</th>
                                                <th>Previous Value</th>
                                                <th>New Value</th>
                                            </tr>
                                        </thead>
                                    	<tbody>
                                        @if(count($AlltRst))
                                            @foreach($AlltRst as $vals)
                                            <tr class="even pointer">
                                                <td>{!!date('m/d/Y h:i A', strtotime($vals->ChangeDate))!!}</td>
                                                <td>{!!$vals->Fname." ".$vals->Lname!!}</td>
                                                <td>Stay Info</td>
                                                <td>Room</td>
                                                <td>{!!$vals->Rsrv_ID!!}</td>
                                                <td>{{$vals->PrvsRoomNumber}}</td>
                                                <td>{{$vals->RoomNumber}}</td>
                                            </tr>
                                            @endforeach
                                        @else
                                            <tr class="even pointer">
                                                <td colspan="7" class="errorMessageTR">Log History Not Found</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <button class="btn btn-primary marginTop20 margin5" id="btnPrint"><i class="fa fa-print"></i> Print</button>
                    <button class="btn btn-success marginTop20 margin5" id="btnExcel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</button>
                    <button class="btn btn-danger marginTop20 margin5" id="btnPDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</button>
                </div>
         	</div>
            @endif
      	</div>
    </div>
</div>
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var checkin = $('#start_date').datepicker({
		startDate: moment()
	}).on('changeDate', function(ev){
		var newDate = new Date(ev.date);
		newDate.setDate(newDate.getDate());
		checkout.setValue(newDate);
		checkin.hide();
		$('#end_date')[0].focus();
	}).data('datepicker');
	
	var checkout = $('#end_date').datepicker({
		onRender: function(date) {
			return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		checkout.hide();
	}).data('datepicker');
	
	$("#btnPrint").click(function(e){
		var printContents = document.getElementById('printableArea').innerHTML;
		var originalContents = document.body.innerHTML;
		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
		window.location.reload();
	});
	$("#btnExcel").click(function(e){
		e.preventDefault();
		
		var data_type = 'data:application/vnd.ms-excel';
		var table_div = document.getElementById('exelableArea');
		var table_html = table_div.outerHTML.replace(/ /g, '%20');
		
		var a = document.createElement('a');
		a.href = data_type + ', ' + table_html;
		a.download = '{{$TitleSubject}}.xls';
		a.click();
	});
	$("#btnPDF").click(function(e){
		var pdf = new jsPDF('p', 'pt', 'letter');
		source = $('#pdfableArea')[0];
		specialElementHandlers = {
			// element with id of "bypass" - jQuery style selector
			'#bypassme': function(element, renderer) {
				// true = "handled elsewhere, bypass text extraction"
				return true
			}
		};
		margins = {
			top: 40,
			bottom: 40,
			left: 40,
			width:572
		};
		
		pdf.fromHTML(
				source, // HTML string or DOM elem ref.
				margins.left, // x coord
				margins.top,
				{// y coord
					'width': margins.width, // max width of content on PDF
					'elementHandlers': specialElementHandlers
				},
				function(dispose) {
					pdf.save('{{$TitleSubject}}.pdf');
				},
				margins
		);
	});
});
</script>
@stop