<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Roomtype;
use App\RoomRate;
use App\RoomRateOption;
use Helper;
use DB;

class RoomRateController extends Controller
{
    public function index(Request $request)
    {
       	//return $request->all();
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		/*if($request->RoomTypeSelectedId){
			$Results = RoomRate::where([ ['room_type_id', '=', $request->RoomTypeSelectedId], ['hotel_id', '=', $Hotel_ID], ['room_rate_from', '=', '0000-00-00'] ])->first();;
			return view('rooms.room-rate')->with("Hotel_ID", $Hotel_ID)->with("request", $request)->with("Results", $Results);
		}else{
			return view('rooms.room-rate')->with("Hotel_ID", $Hotel_ID)->with("request", $request);
		}*/	
		return view('rooms.room-rate')->with("Hotel_ID", $Hotel_ID)->with("request", $request);
    }
	
	public function setavailRate(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$RoomRateRst = RoomRateOption::where([ ['hotel_id', '=', $Hotel_ID], ['room_rate_option_id', '=', $request->room_rate_option_id] ])->first();
		$RoomRateRst->room_rate_price = $request->room_rate_price;
		$RoomRateRst->save();
		if($request->ajax()) {
			return response()->json(['response' => 'Room Rate updated successfully', 'status' => 'success']);
		}else{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }

    public function create(Request $request)
    {	
		
    }

    public function store(Request $request)
    {
		$Helper = new Helper();
		$admin_ID  = $Helper->getAdminUserId();
		$Hotel_ID  = $Helper->getHotelId();
		
		$room_type_id   = $request->room_type_id;
		$room_rate_from = $request->room_rate_from;
		$room_rate_to   = $request->room_rate_to;
		$room_rate      = $request->room_rate;
		//  Monday
		if($request->room_rate_mon == 1){
			$room_rate_mon = $room_rate;
			$room_chck_mon = 1;
		}else{
			$room_rate_mon = 0;
			$room_chck_mon = 0;
		}
		//  Tuesday
		if($request->room_rate_tue == 1){
			$room_rate_tue = $room_rate;
			$room_chck_tue = 1;
		}else{
			$room_rate_tue = 0;
			$room_chck_tue = 0;
		}
		//  Wednesday
		if($request->room_rate_wed == 1){
			$room_rate_wed = $room_rate;
			$room_chck_wed = 1;
		}else{
			$room_rate_wed = 0;
			$room_chck_wed = 0;
		}
		//  Thursday
		if($request->room_rate_thu == 1){
			$room_rate_thu = $room_rate;
			$room_chck_thu = 1;
		}else{
			$room_rate_thu = 0;
			$room_chck_thu = 0;
		}
		//  Friday
		if($request->room_rate_fri == 1){
			$room_rate_fri = $room_rate;
			$room_chck_fri = 1;
		}else{
			$room_rate_fri = 0;
			$room_chck_fri = 0;
		}
		//  Saturday
		if($request->room_rate_sat == 1){
			$room_rate_sat = $room_rate;
			$room_chck_sat = 1;
		}else{
			$room_rate_sat = 0;
			$room_chck_sat = 0;
		}
		//  Sunday
		if($request->room_rate_sun == 1){
			$room_rate_sun = $room_rate;
			$room_chck_sun = 1;
		}else{
			$room_rate_sun = 0;
			$room_chck_sun = 0;
		}
		
		if($room_rate_from && $room_rate_to)
		{
			$room_rate_option_from = $room_rate_from = date('Y-m-d', strtotime($room_rate_from));
			$room_rate_option_to   = $room_rate_to   = date('Y-m-d', strtotime($room_rate_to."+1 day"));
		}
		elseif(!$room_rate_from && $room_rate_to)
		{
			$room_rate_from = date('m/d/Y');
			$room_rate_option_from = $room_rate_from = date('Y-m-d', strtotime($room_rate_from));
			$room_rate_option_to   = $room_rate_to   = date('Y-m-d', strtotime($room_rate_to."+1 day"));
			
		}
		elseif($room_rate_from && !$room_rate_to)
		{
			$room_rate_option_from = $room_rate_from = date('Y-m-d', strtotime($room_rate_from));
			$room_rate_option_to   = $room_rate_to   = date('Y-m-d', strtotime($room_rate_from."+1 day"));
		}
		else
		{
			$room_rate_option_from = date('Y-m-d');
			$room_rate_option_to   = date('Y-m-d', strtotime($room_rate_option_from.'+12 months'));
			$room_rate_from = "";
			$room_rate_to   = "";
		}
		
		$RoomTypeRst = Roomtype::find($room_type_id);
		$RoomTypeValue = $RoomTypeRst->room_type;
		
		/*$RateRst = RoomRate::where([ ['room_type_id', '=', $room_type_id], ['hotel_id', '=', $Hotel_ID], ['room_rate_from', '=', $room_rate_from], ['room_rate_to', '=', $room_rate_to] ])->first();
		if(!count($RateRst)){
			$RateRst = new RoomRate;
		}*/
		$RateRst = new RoomRate;
		$RateRst->hotel_id 		 = $Hotel_ID;
		$RateRst->room_type_id   = $room_type_id;
		$RateRst->room_rate_from = $room_rate_from;
		$RateRst->room_rate_to   = $room_rate_to;
		$RateRst->room_rate   	 = $room_rate;
		$RateRst->room_rate_mon  = $room_chck_mon;
		$RateRst->room_rate_tue  = $room_chck_tue;
		$RateRst->room_rate_wed  = $room_chck_wed;
		$RateRst->room_rate_thu  = $room_chck_thu;
		$RateRst->room_rate_fri  = $room_chck_fri;
		$RateRst->room_rate_sat  = $room_chck_sat;
		$RateRst->room_rate_sun  = $room_chck_sun;
		$RateRst->reg_ip 		 = $_SERVER['REMOTE_ADDR'];
		$RateRst->added_by 		 = $admin_ID;
		//$RateRst->save();
		
		$date1 = date_create($room_rate_option_from);
		$date2 = date_create($room_rate_option_to);
		$diff = date_diff($date1, $date2);
		if($date1 && $date2)
		{
			$diff = date_diff($date1, $date2);
			$dayNum = $diff->format("%a");
			for($i = 1; $i <= $dayNum; $i++)
			{
				if(('$room_rate_option_from') <= ('$room_rate_option_to'))
				{
					$dayWisePrice = ${'room_rate_'.strtolower(date('D', strtotime($room_rate_option_from)))};
					if($dayWisePrice){
						$RoomRateRst = RoomRateOption::where([ ['room_type_id', '=', $room_type_id], ['hotel_id', '=', $Hotel_ID], ['room_rate_date', '=', $room_rate_option_from] ])->first();
						if(!count($RoomRateRst)){
							$RoomRateRst = new RoomRateOption;	
						}
						$RoomRateRst->hotel_id = $Hotel_ID;
						$RoomRateRst->room_type_id = $room_type_id;
						$RoomRateRst->room_rate_date = $room_rate_option_from;
						$RoomRateRst->room_rate_price = $dayWisePrice;
						$RoomRateRst->save();
					}
				} 
				$room_rate_option_from = date('Y-m-d', strtotime($room_rate_option_from.'+1 days'));
			}
		}
		return redirect('room/room-rate?type=room_price&RoomTypeSelectedId='.$room_type_id)->with('success','Rate added successfully for '.$RoomTypeValue.'.');
    }

    public function show($id)
    {
       	
    }

    public function edit($id)
    {
		
    }

    public function update(Request $request, $id)
    {
		
    }

    public function destroy($id)
    {
		
    }
}