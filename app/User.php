<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_unq', 'hotel_id', 'fname', 'mname', 'lname', 'email', 'password', 'role_id', 'shift_id', 'have_role',
		'reg_ip', 'added_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	protected $table = 'users';
	
	/*public static function validate($input) {
		$rules = array(
			'real_name' => 'Required|Min:3|Max:80|Alpha',
			'email'     => 'Required|Between:3,64|Email|Unique:users',
			'password'  => 'Required|AlphaNum|Between:4,8|Confirmed',
			'password_confirmation'=>'Required|AlphaNum|Between:4,8'
		);

		return Validator::make($input, $rules);
	}*/
	
	
	public function role()
	{
		return $this->hasOne('App\Role', 'id', 'role_id');
	}
	
	public function hasRole($roles)
	{
		$this->have_role = $this->getUserRole();
		// Check if the user is a root account
		if($this->have_role->name == 'Root') {
			return true;
		}
		if(is_array($roles)){
			foreach($roles as $need_role){
				if($this->checkIfUserHasRole($need_role)) {
					return true;
				}
			}
		} else{
			return $this->checkIfUserHasRole($roles);
		}
		return false;
	}
	
	private function getUserRole()
	{
		return $this->role()->getResults();
	}
	
	private function checkIfUserHasRole($need_role)
	{
		return (strtolower($need_role)==strtolower($this->have_role->name)) ? true : false;
	}
}
