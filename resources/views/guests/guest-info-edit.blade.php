@extends('layouts.layout')

<? 	foreach($RsrvRst as $val){
		$outPut = $val;
	}
?>

@section('title')
	Edit Guest Information
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
    	<div class="page-title">
            <div class="title_left"><h3>Edit Guest Information</h3></div>
            @if($outPut->RsrvSource)
                <div class="col-md-3 col-sm-3 col-xs-6 fltR zeroRightPadd">
                    <h3 class="AlgnCenter">{{$outPut->RsrvSource}}</h3>
                </div>
            @endif
        </div>
        {{--*/ $PayBalance = ($outPut->PayAmount-$outPut->PayDeposit); /*--}}
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('guest-info', $outPut->RsrvId)}}">Guest Info</a></li>
                <li class="active"><a href="javascript:void(0);">Edit Guest Info</a></li>
                <li><a href="{{url('payment-info', $outPut->RsrvId)}}">Payment Info</a></li>
                <li><a href="{{url('change-room', $outPut->RsrvId)}}">Room Change</a></li>
                <li><a href="{{url('guest-folio', $outPut->RsrvId)}}">Guest Folio</a></li>
                <li><a href="{{url('change-stay', $outPut->RsrvId)}}">Change Stay</a></li>
                <li><a href="{{url('view-changes', $outPut->RsrvId)}}">View Changes</a></li>
                @if($outPut->RsrvStat == '' && $PayBalance == 0)
                <li><a href="{{url('cancel-reservation', $outPut->RsrvId)}}">Cancel Reservation</a></li>
                @endif
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                    @if (count($errors))
                        <ul class="errorFormMessage">
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form id="demo-form2" action="{!!url('guest-info', $Results->guest_id)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                    	{!!method_field('PATCH')!!}
               			{!!csrf_field()!!}
                        <input type="hidden" name="room_reservation_id" id="room_reservation_id" value="{{$outPut->RsrvId}}" />
                    	<div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Title</label>
                            <div class="col-md-3 col-sm-3">
                            <select name="guest_title" class="form-control col-md-2 col-xs-12 zeroPadd">
                                <option value="">Title</option>
                                <option value="1"{!!($Results->guest_title == 1)?" Selected":"";!!}>Mr.</option>
                                <option value="2"{!!($Results->guest_title == 2)?" Selected":"";!!}>Mrs.</option>
                                <option value="3"{!!($Results->guest_title == 3)?" Selected":"";!!}>Miss</option>
                            </select>
                            </div>
                        </div>                                    
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">First Name <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_fname" id="guest_fname" value="{!!$Results->guest_fname or old('guest_fname')!!}" placeholder="First Name" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Middle Name / Initial</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_mname" id="guest_mname" value="{!!$Results->guest_mname or old('guest_mname')!!}" placeholder="Middle Name" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Last Name <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_lname" id="guest_lname" value="{!!$Results->guest_lname or old('guest_lname')!!}" placeholder="Last Name" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Date Of Birth</label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="guest_dob" id="guest_dob" value="{!!$Results->guest_dob or old('guest_dob')!!}" placeholder="mm/dd/yyyy" class="date-picker form-control col-md-7 col-xs-12" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Address <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                            <textarea name="guest_address" id="guest_address" placeholder="Address" class="form-control col-md-7 col-xs-12">{!!$Results->guest_address or old('guest_address')!!}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Country <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6">
                            <select name="guest_country" id="guest_country" class="form-control col-md-2 col-xs-12 LR_Padd5" required="required">
                                @foreach(App\ZipCountry::get() as $countryVal)
                                <option value="{{$countryVal->country_id}}"{!!($Results->guest_country == $countryVal->country_id)?"Selected":""!!}>{{$countryVal->country_name}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Zip Code <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_zip" id="guest_zip" value="{!!$Results->guest_zip or old('guest_zip')!!}" placeholder="Zip Code" class="form-control col-md-7 col-xs-12" maxlength="5" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">City <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_city" id="guest_city" value="{!!$Results->guest_city or old('guest_city')!!}" placeholder="City" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>    
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">State <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6">
                            <input type="text" name="guest_state" id="guest_state"  value="{!!$Results->guest_state or old('guest_state')!!}" placeholder="State" class="form-control col-md-7 col-xs-12" required="required" />
                            </div>
                        </div>                    
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Email</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="email" name="guest_email" id="guest_email" value="{!!$Results->guest_email or old('guest_email')!!}" placeholder="Email" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Cell Phone Number</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_phone" id="guest_phone" value="{!!$Results->guest_phone or old('guest_phone')!!}" placeholder="Cell Phone Number" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Nationality</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_nation" id="guest_nation" value="{!!$Results->guest_nation or old('guest_nation')!!}" placeholder="Nationality" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">ID Type</label>
                            <div class="col-md-2 col-sm-6">
                            <select name="guest_idproof" id="guest_idproof" class="form-control col-md-2 col-xs-12 zeroPadd" onchange="getIdProofType(this.value);">
                                <option value="">ID Type</option>
                                <option value="1"{!!($Results->guest_idproof == 1)?" Selected":"";!!}>Passport</option>
                                <option value="2"{!!($Results->guest_idproof == 2)?" Selected":"";!!}>Driving Licence</option>
                                <option value="3"{!!($Results->guest_idproof == 3)?" Selected":"";!!}>State issued Id</option>
                                <option value="4"{!!($Results->guest_idproof == 4)?" Selected":"";!!}>Other</option>
                            </select>
                            </div>
                            <div class="col-md-2 col-sm-6" id="guest_idproof_Box"{!!($Results->guest_idproof == 4)?'':' style="display:none;"';!!}>
                            <input type="text" name="guest_idproof_other" id="guest_idproof_other" value="{!!$Results->guest_idproof_other or old('guest_idproof_other')!!}" placeholder="Other Id Proof" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">ID Number</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_proofno" id="guest_proofno" value="{!!$Results->guest_proofno or old('guest_proofno')!!}" placeholder="ID Number" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Group Name</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_group" id="guest_group" value="{!!$Results->guest_group or old('guest_group')!!}" placeholder="Group Name" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Vehicle Details</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_vehicle" id="guest_vehicle" value="{!!$Results->guest_vehicle or old('guest_vehicle')!!}" placeholder="Vehicle Details" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop24">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            	<button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update Changes</button>
                                <button type="reset" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</button>
                                <a href="{{URL::previous()}}" class="btn btn-default"><i class="fa fa-chevron-left"></i> Back</a>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function getIdProofType(str){
		if(str == 4){
			$('#guest_idproof_Box').show();
			$("#guest_idproof_other").prop("required", true);
			$("#guest_idproof_other").val("");
		}else{
			$('#guest_idproof_Box').hide();
			$("#guest_idproof_other").prop("required", false);
			$('#guest_idproof_other').val("");
		}
	}
</script>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
@stop  

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	$('#guest_dob').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() > now.valueOf() ? 'disabled' : '';
		}
	});
	
	$("#guest_zip").on("keyup", function() {
		var zipCode  = $(this).val().substring(0, 5);
		var zipCntry = $('#guest_country').val();
		if (zipCode.length == 5 && /^[0-9]+$/.test(zipCode) && zipCntry == 1)
		{
			$("#main_container_Loading").show();
			$("#main_container_overlay").show();
			$("#guest_email").focus();
			$.ajax({
				url: "{{ url('/getCityData') }}",
				type: 'POST',
				data:{"zipCode":zipCode, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					if ( msg.status === 'success' ) {
						$("#guest_city").val(msg.zip_city);
						$("#guest_state").val(msg.zip_state);
						
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						$("#guest_city").val(msg.zip_city);
						$("#guest_state").val(msg.zip_state);
						
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					if ( data.status === 422 ) {
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.error('Cannot find the result');
					}
				}
			});
			return false;
		}
	});
});	
</script>
@stop      
