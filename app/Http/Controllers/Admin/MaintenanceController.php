<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Maintenance;
use App\RoomAssign;
use Helper;
use DB;

class MaintenanceController extends Controller
{
	public function index(Request $request)
    {
        $Hotel_ID = Helper::getHotelId();
		$Results = DB::table("maintenance as M")
					 ->select("R.room_number", "RT.room_smoking_type", "RT.room_type", "RT.room_bed_size", "M.maintenance_id", "M.maintenance_for", "M.maintenance_reply", "M.status")
					 ->join("roomtypes as RT", "RT.room_type_id", "=", "M.room_type_id")
					 ->join("room_assign as R", "R.room_assign_id", "=", "M.room_assign_id")
					 ->where([ ["M.hotel_id", $Hotel_ID] ])->orderBy("M.status", "ASC")->paginate(10);
		return view('services.maintenance')->with("Results", $Results)->with("Hotel_ID", $Hotel_ID)->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function create(Request $request)
    {	
		$Hotel_ID = Helper::getHotelId();
        return view('services.maintenance-create')->with('Hotel_ID', $Hotel_ID)->with('request', $request);
    }

    public function store(Request $request)
    {
		$Hotel_ID = Helper::getHotelId();
		$admin_ID = Helper::getAdminUserId();
		if($Hotel_ID == $request->hotel_id)
		{
			$RequestArray = array();
			$RequestArray[] = $Hotel_ID;
			$RequestArray[] = $request->room_type_id;
			$RequestArray[] = $request->room_assign_id;
			$RequestArray[] = $request->maintenance_for;
			$RequestArray[] = $_SERVER['REMOTE_ADDR'];
			$RequestArray[] = $admin_ID;
			$RequestArray[] = $_SERVER['REMOTE_ADDR'];
			$RequestArray[] = $admin_ID;
			
			DB::select('CALL Maintenance_Insert_SP(?,?,?,?,?,?,?,?)', $RequestArray);
			
			$RoomRst = RoomAssign::where([ ['hotel_id', $Hotel_ID], ['room_assign_id', $request->room_assign_id] ])->first();
			$RoomRst->status   = 0;
			$RoomRst->reg_ip   = $_SERVER['REMOTE_ADDR'];
			$RoomRst->added_by = $admin_ID;
			$RoomRst->save();
			
			$cRoomTypeId = $request->room_type_id;
			$toDay       = date("Y-m-d");
			$endDay      = date("Y-m-d", strtotime($toDay.'+12 months'));
			Helper::ChannelPartnerRequest($Hotel_ID, $cRoomTypeId, $toDay, $endDay);
			
			return redirect('maintenance')->with('success','Maintenance query created successfully');
		}else{
			return redirect('maintenance')->with("danger", "Please select your Motel");
		}
    }

    public function edit($id)
    {	
		$Hotel_ID = Helper::getHotelId();
		$Results  = Maintenance::where([ ['hotel_id', $Hotel_ID], ['maintenance_id', $id] ])->first();
		if(count($Results))
		{
			return view('services.maintenance-reply')->with('Hotel_ID', $Hotel_ID)->with("Results", $Results);
		}else{
			return redirect('maintenance')->with("danger", "Please select your Motel");
		}
    }

    public function update(Request $request, $id)
    {
		$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		
		$Results = Maintenance::where([ ['hotel_id', $Hotel_ID], ['maintenance_id', $id] ])->first();
		if(count($Results))
		{
			$cRoomTypeId = $Results->room_type_id;
			
			$Results->maintenance_reply = $request->maintenance_reply;
			$Results->status      = 1;
			$Results->active_by   = $admin_ID;
			$Results->active_date = date('Y-m-d H:i:s');
			$Results->active_ip   = $_SERVER['REMOTE_ADDR'];
			$Results->save();
			
			$RoomRst = RoomAssign::where([ ['hotel_id', $Hotel_ID], ['room_assign_id', $Results->room_assign_id] ])->first();
			$RoomRst->status   = 1;
			$RoomRst->reg_ip   = $_SERVER['REMOTE_ADDR'];
			$RoomRst->added_by = $admin_ID;
			$RoomRst->save();
			
			$toDay  = date("Y-m-d");
			$endDay = date("Y-m-d", strtotime($toDay.'+12 months'));
			Helper::ChannelPartnerRequest($Hotel_ID, $cRoomTypeId, $toDay, $endDay);
			
			return redirect('maintenance')->with("success", "Maintenance Query resolved successfully");
		}else{
			return redirect('maintenance')->with("danger", "Please select your Motel");
		}
    }

    public function destroy(Request $request, $id)
    {
		$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		
		if($request->hotel_id == $Hotel_ID)
		{
			Maintenance::destroy($id);
			return redirect('maintenance')->with('success', 'Maintenance Query deleted successfully');
		}else{
			return redirect('maintenance')->with('danger', 'Please select your Motel');
		}
    }
}