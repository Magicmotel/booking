@extends('layouts.layout')

@section('title')
	Expedia Channel Partner
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Expedia Channel Partner</h3></div>
            @if($loginNum)
        	<div class="fltR AlgnRight zeroRightPadd marginTop5 marginBottom5">
            	@if($loginRst->channel_status == 1)
                    <form action="{!!url('expedia-channel', $loginRst->cm_id)!!}" method="post" onsubmit="return ConfirmAction('Are you sure you want to pause this Channel?')">
                    {!!method_field('PATCH')!!}
                    {!!csrf_field()!!}
                    {!!Form::button('<i class="fa fa-pause-circle-o"></i> Deactivate', ['class'=>'left btn btn-warning zeroMargin', 'role' => 'button', 'type' => 'submit'])!!}
                    </form>
               	@else
                	<form action="{!!url('expedia-channel', $loginRst->cm_id)!!}" method="post" onsubmit="return ConfirmAction('Are you sure you want to active this Channel?')">
                    {!!method_field('PATCH')!!}
                    {!!csrf_field()!!}
                    {!!Form::button('<i class="fa fa-play-circle-o"></i> Active', ['class'=>'left btn btn-success zeroMargin', 'role' => 'button', 'type' => 'submit'])!!}
                    </form>
                @endif
            </div>
            @endif
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-8 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <div class="form-horizontal form-label-left">
                    	<span class="section">ROOM & RATE PLAN MAPPINGs:</span>
                        <table id="datatable-checkbox" class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr>
                                    <th>Room Type</th>
                                    <th>Expedia Room Type Id</th>
                                    <th>Expedia Rate Plan Id</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($mapRst as $mapVal)
                                <tr>
                                    <td class="uPPerLetter">{{$mapVal->RoomType}} &nbsp; ({{$mapVal->RoomCode}})</td>
                                    <td{!!($mapVal->cRoomTypeId)?'':' class="errorChannelLegent"'!!}>{!!($mapVal->cRoomTypeId)?$mapVal->cRoomTypeId:'***'!!}</td>
                                    <td{!!($mapVal->cRatePlanId)?'':' class="errorChannelLegent"'!!}>{!!($mapVal->cRatePlanId)?$mapVal->cRatePlanId:'***'!!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="form-group form-group-last marginTop15">
                        <div class="fltL errorChannelLegent">***</div><div class="fltL marginLeft10 errorColor">Room Type not Mapped to Channel Partner</div>
                        </div>
                        <div class="form-group form-group-last marginTop10">
                            <div class="col-md-2 col-sm-8 col-xs-12 zeroPadd">
                            	<a href="{!!url('expedia-channel/mapping')!!}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i> Edit</a>
                            </div>
                    	</div>
                    </div>
                    </div>
                </div>                
        	</div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    	{{--*/
                        	$PasLnth = strlen($loginRst->channel_password);
                            $PassVar = substr($loginRst->channel_password, 0, 2);
                            for($ps = 3; $ps <= $PasLnth; $ps++){
                            	$PassVar .= "*";
                            }
                        /*--}}
                        <div class="form-horizontal form-label-left">
                            <span class="section">CHANNEL CREDENTIALs:</span>
                            <div class="form-group">
                                <label class="control-label col-md-6 col-sm-4">Expedia Hotel Id:</label>
                                <div class="control-value col-md-6 col-sm-8">{!!$loginRst->channel_hotel_id!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-6 col-sm-4">Expedia Username:</label>
                                <div class="control-value col-md-6 col-sm-8">{!!$loginRst->channel_username!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-6 col-sm-4">Expedia Password:</label>
                                <div class="control-value col-md-6 col-sm-8">{!!$PassVar!!}</div>
                            </div>
                            <div class="form-group form-group-last marginTop24">
                                <div class="col-md-12 col-sm-8 col-xs-12 zeroPadd AlgnCenter">
                                    <a href="{!!url('expedia-channel/credential')!!}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i> Edit Credential</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop      
