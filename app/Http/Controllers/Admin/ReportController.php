<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Hotel;
use App\Taxes;
use Helper;
use DB;

class ReportController extends Controller
{
	public function inhouse_guest(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		
		if($request->search_date){
			$toDay = date("Y-m-d", strtotime($request->search_date));
			$searchDate = $request->search_date;
		}else{
			$toDay = date("Y-m-d");
			$searchDate = date("m/d/Y");
		}
		
		$MotelInfo = Hotel::where('hotel_id',$Hotel_ID)->first(['hotel_name', 'hotel_logo']);
		
		$GuestRst= DB::select("SELECT GST.guest_id as GuestId, GST.guest_fname as GuestFname, GST.guest_lname as GuestLname,
							   GST.guest_phone as GuestPhone, GST.guest_city as GuestCity, 
							   GST.guest_zip as GuestPostal, GST.guest_group as GuestGroup, GST.bad_status as GuestBadStatus,
							   RSV.reservation_locked as RsrvLock, RSV.room_reservation_id as RsrvId, RSV.reserv_night as RsrvNights,
							   RSV.status as RsrvStat, RSV.reserv_source as RsrvSource,
							   DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate,
							   (SELECT CP.company_name FROM company_profile as CP WHERE CP.profile_id = RSV.guest_profile_id) as GuestCompany,
							   (SELECT CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size)
							   	FROM roomtypes as RT WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomTypes,
							   (SELECT R.room_number
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber,
							   (SELECT R.room_stat
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomStat
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.arrival <= '".$toDay."' AND RSV.departure >= '".$toDay."' AND RSV.status = 'Checked In'
							   ORDER BY ArvlDate DESC, DprtDate ASC
							  ");
		return view('reporting.guest-inhouse')->with("Hotel_ID", $Hotel_ID)->with("MotelInfo", $MotelInfo)->with("GuestRst", $GuestRst)->with("searchDate", $searchDate);	
		
    }
	
	public function incoming_reservation(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		
		if($request->search_date){
			$toDay = date("Y-m-d", strtotime($request->search_date));
			$searchDate = $request->search_date;
		}else{
			$toDay = date("Y-m-d");
			$searchDate = date("m/d/Y");
		}
		
		$MotelInfo = Hotel::where('hotel_id',$Hotel_ID)->first(['hotel_name', 'hotel_logo']);
		
		$GuestRst= DB::select("SELECT GST.guest_id as GuestId, GST.guest_fname as GuestFname, GST.guest_lname as GuestLname,
							   GST.guest_phone as GuestPhone, GST.guest_city as GuestCity, 
							   GST.guest_zip as GuestPostal, GST.guest_group as GuestGroup, GST.bad_status as GuestBadStatus,
							   RSV.room_reservation_id as RsrvId, RSV.reserv_night as RsrvNights, RSV.status as RsrvStat, RSV.reservation_locked as RsrvLock,
							   DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate, RSV.reserv_source as RsrvSource,
							   (SELECT CP.company_name FROM company_profile as CP WHERE CP.profile_id = RSV.guest_profile_id) as GuestCompany,
							   (SELECT CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size)
							   	FROM roomtypes as RT WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomTypes,
							   (SELECT R.room_number
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber,
							   (SELECT R.room_stat
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomStat
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.arrival = '".$toDay."' AND RSV.status = ''
							   ORDER BY ArvlDate ASC, DprtDate ASC
							  ");
		return view('reporting.guest-incoming')->with("Hotel_ID", $Hotel_ID)->with("MotelInfo", $MotelInfo)->with("GuestRst", $GuestRst)->with("searchDate", $searchDate);
    }
	
	public function employee_log_history(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$MotelInfo = Hotel::where('hotel_id',$Hotel_ID)->first(['hotel_name', 'hotel_logo']);
		
		$startDate = ($request->start_date)?$request->start_date:date('m/d/Y');
		$endDate = ($request->end_date)?$request->end_date:date('m/d/Y');
		
		$dstartDate = date('Y-m-d', strtotime($startDate));
		$dendDate = date('Y-m-d', strtotime($endDate));
		
		$AlltRstPart = "";
		if($request->emp_id)
		{
			 $AlltRstPart .= " AND U.id = ".$request->emp_id;						
		}
		if($request->rsrv_id)
		{
			 $AlltRstPart .= " AND RSV.room_reservation_id = ".$request->rsrv_id;						
		}
		
		$AlltRst = DB::select("SELECT U.fname as Fname, U.mname as Mname, U.lname as Lname, RRAP.created_at as ChangeDate,
							   ( SELECT R.room_number
								 FROM room_assign as R WHERE R.room_assign_id = RRAP.p_room_assign_id
							   ) as PrvsRoomNumber,
							   ( SELECT R.room_number
								 FROM room_assign as R WHERE R.room_assign_id = RRAP.room_assign_id
							   ) as RoomNumber,
							   RSV.room_reservation_id as Rsrv_ID
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   INNER JOIN reservation_room_allot_past as RRAP ON RRAP.room_reservation_id = RSV.room_reservation_id
							   INNER JOIN users as U ON U.id = RRAP.added_by
							   WHERE RSV.hotel_id = ".$Hotel_ID." AND CAST(RRAP.created_at as date) >= '".$dstartDate."' AND CAST(RRAP.created_at as date) <= '".$dendDate."'
							   ".$AlltRstPart."
							   ORDER BY p_allot_id ASC
							  ");
		return view('reporting.employee-log-history')->with("Hotel_ID", $Hotel_ID)->with("MotelInfo", $MotelInfo)
													 ->with("AlltRst", $AlltRst)->with("request", $request)
													 ->with("startDate", $startDate)->with("endDate", $endDate);
    }
	
	public function employee_list(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$MotelInfo = Hotel::where('hotel_id',$Hotel_ID)->first(['hotel_name', 'hotel_logo']);
		
		$EmployeeRstPart = "";
		if($request->job_title)
		{
			 $EmployeeRstPart .= " AND R.id = ".$request->job_title;						
		}
		if($request->shift_type)
		{
			 $EmployeeRstPart .= " AND SF.shift_id = ".$request->shift_type;						
		}
		
		$EmployeeRst = DB::select("SELECT R.name as RoleName, SF.shift_name as ShiftType,
								   Emp.emp_id as EmpId, Emp.emp_fname as Fname, Emp.emp_mname as Mname, Emp.emp_lname as Lname,
								   Emp.emp_address as EmpAddress, Emp.emp_city as EmpCity, Emp.emp_state as EmpState, Emp.emp_zip as EmpZip,
								   Emp.emp_phone as EmpPhone, Emp.emp_email as EmpEmail
								   From employees as Emp
								   INNER JOIN roles as R ON R.id = Emp.role_id
								   INNER JOIN shifts as SF ON SF.shift_id = Emp.shift_id
								   WHERE Emp.hotel_id = ".$Hotel_ID.$EmployeeRstPart."
								   ORDER BY R.id ASC, Emp.emp_fname ASC
								  ");
		return view('reporting.employee-list')->with("Hotel_ID", $Hotel_ID)->with("MotelInfo", $MotelInfo)->with("EmployeeRst", $EmployeeRst)->with("request", $request);
    }
	
	public function housekeeping(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$MotelInfo = Hotel::where('hotel_id',$Hotel_ID)->first(['hotel_name', 'hotel_logo']);
		
		if($request->type == 'rsl')
		{
			$roomSql = DB::select("SELECT R1.room_number as RoomNumber, R1.room_stat as RoomStat,
								   CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType, RT.room_type as RoomTypes,
								   ( SELECT CONCAT(RSV.guest_adult, '-', RSV.guest_child)
									 FROM room_reservation as RSV
									 INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
									 INNER JOIN room_assign as R ON R.room_assign_id = RRA.room_assign_id
									 WHERE R.room_assign_id = R1.room_assign_id AND RSV.hotel_id = ".$Hotel_ID." AND
									 RSV.arrival <= '".$toDay."' AND RSV.departure >= '".$toDay."' AND RSV.status = 'Checked In'
									 GROUP BY R1.room_assign_id
								   ) as RoomVacant
								   FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
								   WHERE R1.hotel_id = ".$Hotel_ID." AND R1.status = 1 AND R1.trash = 0 AND R1.room_number != ''
								   ORDER BY R1.room_number ASC
								  ");
			return view('reporting.housekeeping-rsl')->with("Hotel_ID", $Hotel_ID)->with("MotelInfo", $MotelInfo)->with("roomSql", $roomSql)->with("request", $request);	
		}
		elseif($request->type == 'orl')
		{
			$roomSql = DB::select("SELECT R.room_number as RoomNumber, R.room_stat as RoomStat,
								   (SELECT CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size)
									FROM roomtypes as RT WHERE RT.room_type_id = R.room_type_id
								   ) as RoomType,
								   (SELECT RT.room_type
									FROM roomtypes as RT WHERE RT.room_type_id = R.room_type_id
								   ) as RoomTypes,
								   RSV.guest_adult as RsvAdult, RSV.guest_child as RsvChild
								   FROM room_reservation as RSV
								   INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
								   INNER JOIN room_assign as R ON R.room_assign_id = RRA.room_assign_id
								   WHERE  R.status = 1 AND R.trash = 0 AND R.room_number != '' AND RSV.hotel_id = ".$Hotel_ID." AND
								   RSV.arrival <= '".$toDay."' AND RSV.departure >= '".$toDay."' AND RSV.status = 'Checked In'
								   ORDER BY R.room_number ASC
								  ");
			return view('reporting.housekeeping-orl')->with("Hotel_ID", $Hotel_ID)->with("MotelInfo", $MotelInfo)->with("roomSql", $roomSql)->with("request", $request);	
		}
		else
		{
			$roomSql = DB::select("SELECT R1.room_number as RoomNumber, R1.room_stat as RoomStat,
									CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType, RT.room_type as RoomTypes
									FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
									WHERE R1.hotel_id = ".$Hotel_ID." AND R1.status = 1 AND R1.trash = 0 AND R1.room_number != '' AND
									R1.room_assign_id NOT IN(
										SELECT R.room_assign_id
										FROM room_reservation as RSV
										INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
										INNER JOIN room_assign as R ON R.room_assign_id = RRA.room_assign_id
										WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.arrival <= '".$toDay."' AND RSV.departure >= '".$toDay."' AND RSV.status = 'Checked In'
									) ORDER BY R1.room_number ASC
								   ");
			return view('reporting.housekeeping-vrl')->with("Hotel_ID", $Hotel_ID)->with("MotelInfo", $MotelInfo)->with("roomSql", $roomSql)->with("request", $request);
		}
    }
	
	public function financial_daily(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$MotelInfo = Hotel::where('hotel_id',$Hotel_ID)->first(['hotel_name', 'hotel_logo']);
		
		$startDate = ($request->start_date)?$request->start_date:date('m/d/Y');
		$endDate = ($request->end_date)?$request->end_date:date('m/d/Y');
		
		$dstartDate = date('Y-m-d', strtotime($startDate));
		$dendDate = date('Y-m-d', strtotime($endDate));
		
		$date1 = date_create($dstartDate);
		$date2 = date_create($dendDate);
		$diff = date_diff($date1, $date2);
		$dayNum = $diff->format("%a");
		
		return view('reporting.financial-daywise')->with("Hotel_ID", $Hotel_ID)->with("MotelInfo", $MotelInfo)
												  ->with("startDate", $startDate)->with("endDate", $endDate)
												  ->with("daystartDate", $dstartDate)->with("dayNum", $dayNum)
												  ->with("request", $request);
	}
	
	public function financial_montly(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$MotelInfo = Hotel::where('hotel_id',$Hotel_ID)->first(['hotel_name', 'hotel_logo']);
		
		$startDate = ($request->start_date)?$request->start_date:date('m/01/Y');
		$endDate  = date('m/t/Y',strtotime($startDate));
		
		$dstartDate = date('Y-m-d', strtotime($startDate));
		$dendDate = date('Y-m-d', strtotime($endDate));
		
		if($dendDate > $toDay){
			$dendDate = date('Y-m-d', strtotime($toDay));
			$endDate  = date('m/d/Y',strtotime($toDay));
		}
		
		$TaxSubQry = "";
		$TaxVal = Taxes::where('hotel_id', $Hotel_ID)->groupBy('tax_type')->get();
		foreach($TaxVal as $TaxValRes)
		{
		 	$TaxSubQry .="( (  SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
							   FROM room_reservation_folio_meta as RF1
							   WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date = RF.folio_date AND RF1.folio_pay_type = '".$TaxValRes->tax_type."' AND RF1.folio_type = 1
							) - 
							(  SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
							   FROM room_reservation_folio_meta as RF1
							   WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date = RF.folio_date AND RF1.folio_pay_type = '".$TaxValRes->tax_type."' AND RF1.folio_type = 2
							)
						  ) as ".str_replace(" ", "", $TaxValRes->tax_type).",
						 ";
		}
		
		
		$Results = DB::select("SELECT DATE_FORMAT(RF.folio_date, '%m/%d/%Y') as FolioDate,
							   ( (	SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
									FROM room_reservation_folio_meta as RF1
									WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date = RF.folio_date AND RF1.folio_pay_type = 'Room Charge' AND RF1.folio_type = 1
								 ) - 
								 (	SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
									FROM room_reservation_folio_meta as RF1
									WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date = RF.folio_date AND RF1.folio_pay_type = 'Room Charge' AND RF1.folio_type = 2
								 )
							   ) as RoomCharge,
							   ( (	SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
									FROM room_reservation_folio_meta as RF1
									WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date = RF.folio_date AND RF1.folio_order >= 11 AND RF1.folio_type = 1
								 ) - 
								 (	SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
									FROM room_reservation_folio_meta as RF1
									WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date = RF.folio_date AND RF1.folio_order >= 11 AND RF1.folio_type = 2
								 )
							   ) as OtherCharge,
							   ".$TaxSubQry."
							   ( SELECT SUM(RF1.folio_amount) FROM room_reservation_folio_meta as RF1
								 WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date = RF.folio_date
								 AND (RF1.folio_pay_type = 'Cash Payment' OR RF1.folio_pay_type = 'Check Payment')
							   ) as CashCredit,
							   ( SELECT GROUP_CONCAT(RF1.folio_comment SEPARATOR ', ') FROM room_reservation_folio_meta as RF1
								 WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date = RF.folio_date AND RF1.folio_pay_type = 'CC Payment'
							   ) as CCType,
							   ( SELECT SUM(RF1.folio_amount) FROM room_reservation_folio_meta as RF1
								 WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date = RF.folio_date AND RF1.folio_pay_type = 'CC Payment'
							   ) as CCCredit,
							   ( SELECT SUM(RF1.folio_amount) FROM room_reservation_folio_meta as RF1
								 WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date = RF.folio_date AND RF1.folio_pay_type = 'Company Payment'
							   ) as DirectCredit
							   FROM room_reservation as RSV 
							   INNER JOIN room_reservation_folio_meta as RF ON RF.room_reservation_id = RSV.room_reservation_id
							   WHERE RSV.hotel_id = ".$Hotel_ID." AND RF.folio_date >= '".$dstartDate."' AND RF.folio_date <= '".$dendDate."'
							   GROUP BY RF.folio_date
							   ORDER BY FolioDate ASC
							  ");
		
		return view('reporting.financial-monthly')->with("Hotel_ID", $Hotel_ID)->with("MotelInfo", $MotelInfo)
												  ->with("startDate", $startDate)->with("endDate", $endDate)
												  ->with("Results", $Results)->with("request", $request);
	}
	
	public function financial_annual(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$MotelInfo = Hotel::where('hotel_id',$Hotel_ID)->first(['hotel_name', 'hotel_logo']);
		
		$startDate = ($request->start_date)?$request->start_date:date('01/01/Y');
		$endDate  = date('12/31/Y',strtotime($startDate));
		
		$dendDate = date('Y-m-d', strtotime($endDate));
		
		if($dendDate > $toDay){
			$lstMthDate = date('m', strtotime($toDay));
			$endDate = date('m/d/Y',strtotime($toDay));
		}else{
			$lstMthDate = date('m', strtotime($endDate));
		}
		$lstYrsDate = date('Y', strtotime($startDate));
		
		$TaxSubQry = "";
		$TaxVal = Taxes::where('hotel_id', $Hotel_ID)->groupBy('tax_type')->get();
		foreach($TaxVal as $TaxValRes)
		{
			$TaxSubQry .="( ( SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
							  FROM room_reservation_folio_meta as RF1
							  WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date >= '[STARTDATE]' AND RF1.folio_date <= '[ENDDATE]'
							  AND RF1.folio_pay_type = '".$TaxValRes->tax_type."' AND RF1.folio_type = 1
							) - 
							( SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
							  FROM room_reservation_folio_meta as RF1
							  WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date >= '[STARTDATE]' AND RF1.folio_date <= '[ENDDATE]'
							  AND RF1.folio_pay_type = '".$TaxValRes->tax_type."' AND RF1.folio_type = 2
							)
						  ) as ".str_replace(" ", "", $TaxValRes->tax_type).",
						 ";
		}
		
		$CompleteQry    =    ("SELECT DATE_FORMAT(RF.folio_date, '%M %Y') as FolioDate, DATE_FORMAT(RF.folio_date, '%Y%m') as FolioDateOrder,
							   ( (	SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
									FROM room_reservation_folio_meta as RF1
									WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date >= '[STARTDATE]' AND RF1.folio_date <= '[ENDDATE]'
									AND RF1.folio_pay_type = 'Room Charge' AND RF1.folio_type = 1
								 ) - 
								 (	SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
									FROM room_reservation_folio_meta as RF1
									WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date >= '[STARTDATE]' AND RF1.folio_date <= '[ENDDATE]'
									AND RF1.folio_pay_type = 'Room Charge' AND RF1.folio_type = 2
								 )
							   ) as RoomCharge,
							   ( (	SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
									FROM room_reservation_folio_meta as RF1
									WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date >= '[STARTDATE]' AND RF1.folio_date <= '[ENDDATE]'
									AND RF1.folio_order >= 11 AND RF1.folio_type = 1
								 ) - 
								 (	SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
									FROM room_reservation_folio_meta as RF1
									WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date >= '[STARTDATE]' AND RF1.folio_date <= '[ENDDATE]'
									AND RF1.folio_order >= 11 AND RF1.folio_type = 2
								 )
							   ) as OtherCharge,
							   ".$TaxSubQry."
							   ( SELECT SUM(RF1.folio_amount) FROM room_reservation_folio_meta as RF1
								 WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date >= '[STARTDATE]' AND RF1.folio_date <= '[ENDDATE]'
								 AND (RF1.folio_pay_type = 'Cash Payment' OR RF1.folio_pay_type = 'Check Payment')
							   ) as CashCredit,
							   ( SELECT GROUP_CONCAT(RF1.folio_comment SEPARATOR ', ') FROM room_reservation_folio_meta as RF1
								 WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date >= '[STARTDATE]' AND RF1.folio_date <= '[ENDDATE]'
								 AND RF1.folio_pay_type = 'CC Payment'
							   ) as CCType,
							   ( SELECT SUM(RF1.folio_amount) FROM room_reservation_folio_meta as RF1
								 WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date >= '[STARTDATE]' AND RF1.folio_date <= '[ENDDATE]'
								 AND RF1.folio_pay_type = 'CC Payment'
							   ) as CCCredit,
							   ( SELECT SUM(RF1.folio_amount) FROM room_reservation_folio_meta as RF1
								 WHERE RF1.hotel_id = ".$Hotel_ID." AND RF1.folio_date >= '[STARTDATE]' AND RF1.folio_date <= '[ENDDATE]'
								 AND RF1.folio_pay_type = 'Company Payment'
							   ) as DirectCredit
							   FROM room_reservation as RSV 
							   INNER JOIN room_reservation_folio_meta as RF ON RF.room_reservation_id = RSV.room_reservation_id							   
							   WHERE RSV.hotel_id = ".$Hotel_ID." AND RF.folio_date >= '[STARTDATE]' AND RF.folio_date <= '[ENDDATE]'
							   GROUP BY FolioDateOrder
							   ORDER BY FolioDateOrder ASC
							  ");
		
		return view('reporting.financial-annual')->with("Hotel_ID", $Hotel_ID)->with("MotelInfo", $MotelInfo)
												  ->with("startDate", $startDate)->with("endDate", $endDate)
												  ->with("CompleteQry", $CompleteQry)->with("lstMthDate", $lstMthDate)
												  ->with("toDay", $toDay)->with("lstYrsDate", $lstYrsDate)->with("request", $request);
	}
	
	public function tax_exemption(Request $request)
    {		
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$MotelInfo = Hotel::where('hotel_id',$Hotel_ID)->first(['hotel_name', 'hotel_logo']);
		
		$startDate = ($request->start_date)?$request->start_date:date('m/d/Y');
		$endDate = ($request->end_date)?$request->end_date:date('m/d/Y');
		
		$dstartDate = date('Y-m-d', strtotime($startDate));
		$dendDate = date('Y-m-d', strtotime($endDate));	
		
		$Results = DB::select("SELECT GST.guest_fname as GuestFname, GST.guest_lname as GuestLname,
							   ( SELECT CP.company_name FROM company_profile as CP WHERE CP.profile_id = RSV.guest_profile_id ) as GuestCompany,
							   RSV.room_reservation_id as RsrvId, DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate,
							   ( SELECT R.room_number
								 FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								 WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber,
							   DATE_FORMAT(RF.folio_date, '%m/%d/%Y') as FolioDate,
							   ( (	SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
									FROM room_reservation_folio_exempt as RF1
									WHERE RF1.folio_date = RF.folio_date
									AND RF1.folio_pay_type = 'Room Charge' AND RF1.folio_type = 1
								 ) - 
								 (	SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
									FROM room_reservation_folio_exempt as RF1
									WHERE RF1.folio_date = RF.folio_date
									AND RF1.folio_pay_type = 'Room Charge' AND RF1.folio_type = 2
								 )
							   ) as RoomCharge,
							   ( (	SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
									FROM room_reservation_folio_exempt as RF1
									WHERE RF1.folio_date = RF.folio_date
									AND RF1.folio_pay_type != 'Room Charge' AND RF1.folio_type = 1
								 ) - 
								 (	SELECT CASE WHEN SUM(RF1.folio_amount) IS NOT NULL THEN SUM(RF1.folio_amount) ELSE 0 END
									FROM room_reservation_folio_exempt as RF1
									WHERE RF1.folio_date = RF.folio_date
									AND RF1.folio_pay_type != 'Room Charge' AND RF1.folio_type = 2
								 )
							   ) as TaxCharge
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   INNER JOIN room_reservation_folio_exempt as RF ON RF.room_reservation_id = RSV.room_reservation_id
							   WHERE RSV.hotel_id = ".$Hotel_ID." AND RF.folio_date >= '".$dstartDate."' AND RF.folio_date <= '".$dendDate."'
							   GROUP BY RSV.room_reservation_id, RF.folio_date
							   ORDER BY FolioDate ASC
							  ");
		
		return view('reporting.tax-exemption')->with("Hotel_ID", $Hotel_ID)->with("MotelInfo", $MotelInfo)
												  ->with("startDate", $startDate)->with("endDate", $endDate)
												  ->with("Results", $Results)->with("request", $request);
	}
}