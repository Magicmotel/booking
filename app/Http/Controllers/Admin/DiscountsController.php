<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Discounts;
use Helper;
use DB;

class DiscountsController extends Controller
{
    public function index(Request $request)
    {
		$Hotel_ID = Helper::getHotelId();
		$Results  = Discounts::where('hotel_id', $Hotel_ID)->orderBy('discount_id', 'DESC')->get(['discount_id', 'discount_code', 'discount_value', 'discount_type', 'discount_from', 'discount_to']);
		return view('rooms.discounts')->with("Hotel_ID", $Hotel_ID)->with("Results", $Results);
	}

    public function store(Request $request)
    {
		$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		$unique_ID = Helper::getUniqueId();
		
		$discount_code  = $request->discount_code;
		$discount_value = $request->discount_value;
		$discount_type  = $request->discount_type;
		$discount_from  = ($request->discount_from)?date("Y-m-d", strtotime($request->discount_from)):date("Y-m-d");
		$discount_to    = ($request->discount_to)?date("Y-m-d", strtotime($request->discount_to)):"";
		
		if($discount_code && $discount_value)
		{
			$Results  = Discounts::where([ ['hotel_id', $Hotel_ID], ['discount_code', $discount_code] ])->first();
			if(count($Results)){
				return redirect('room/discounts')->with('error', 'Discount code already Exists.');
			}else{
				$RequestArray = array();	
				$RequestArray[] = $unique_ID;
				$RequestArray[] = $Hotel_ID;
				$RequestArray[] = $discount_code;
				$RequestArray[] = $discount_value;
				$RequestArray[] = $discount_type;
				$RequestArray[] = $discount_from;
				$RequestArray[] = $discount_to;
				$RequestArray[] = $_SERVER['REMOTE_ADDR'];
				$RequestArray[] = $admin_ID;
				DB::select('CALL Discounts_Insert_SP(?,?,?,?,?,?,?,?,?)', $RequestArray);
				return redirect('room/discounts')->with('success', 'Discount Code added Successfully.');
			}
		}else{
			return redirect('room/discounts')->with('error', 'Discount Code & Value is Required.');
		}
    }

    public function edit($id)
    {
		$Hotel_ID = Helper::getHotelId();
		$Results  = Discounts::where([ ["hotel_id", $Hotel_ID], ["discount_id", $id] ])
							 ->get(["discount_id", "discount_code", "discount_value", "discount_type", "discount_from", "discount_to"]);
		if(count($Results)){
			return view('rooms.discounts-edit')->with("Results", $Results);
		}else{
			return redirect('room/discounts')->with('danger','Selected Discount Code# '.$id.' not Found!');
		}	
    }

    public function update(Request $request, $id)
    {
		$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		$unique_ID = Helper::getUniqueId();
		
		$discount_code  = $request->discount_code;
		$discount_value = $request->discount_value;
		$discount_type  = $request->discount_type;
		$discount_from  = ($request->discount_from)?date("Y-m-d", strtotime($request->discount_from)):date("Y-m-d");
		$discount_to    = ($request->discount_to)?date("Y-m-d", strtotime($request->discount_to)):"";
		
		$Results = Discounts::where([ ['discount_id', $id], ['hotel_id', $Hotel_ID] ])->first();
		if(count($Results)){
			$Results->hotel_id 		 = $Hotel_ID;
			$Results->discount_code  = $discount_code;
			$Results->discount_value = $discount_value;
			$Results->discount_type  = $discount_type;
			$Results->discount_from  = $discount_from;
			$Results->discount_to 	 = $discount_to;
			$Results->reg_ip 		 = $_SERVER['REMOTE_ADDR'];
			$Results->added_by 		 = $admin_ID;
			$Results->save();
			return redirect('room/discounts')->with('update','Discount Code updated successfully');
		}else{
			return redirect('room/discounts')->with('danger','Selected Discount Code# '.$id.' not Found!');
		}
    }
	
    public function destroy(Request $request, $id)
    {
		$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		
		if($request->hotel_id == $Hotel_ID)
		{
			Discounts::destroy($id);
			return redirect('room/discounts')->with('success','Coupon Code deleted successfully');
		}else{
			return redirect('room/discounts')->with('danger','Selected Discount Code# '.$id.' not Found!');
		}
	}
}