@extends('layouts.layout')

@section('title')
	Manage HouseKeeping Central
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>HouseKeeping Central</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class="active"><a href="javascript:void(0);">HouseKeeping Central</a></li>
                <li><a href="{{url('housekeeping-print')}}">Print Assignment Sheets</a></li>
            </ul>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @elseif ($message = Session::get('danger'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            
                           	<div class="col-md-6 col-sm-12 col-xs-12 zeroLeftPadd">
                            <span class="section">ROOMS LIST:</span>
                            {{--*/	$toDay = date("Y-m-d");
                            		$QryPart = "";
                            		if($request->room_stat == 'clean'){
                                    	$QryPart = " AND R.room_stat = 1";
                                    }elseif($request->room_stat == 'dirty'){
                                    	$QryPart = " AND R.room_stat = 2";
                                    }
                            $Sql = DB::select("SELECT R.room_assign_id as RoomId, R.room_number as RoomNumber, R.room_stat as RoomCondition,
                                               CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType, RT.room_type as RoomTypes,
                                               (SELECT 'Occupied'
                                                    FROM room_reservation as RSV
                                                    INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
                                                    INNER JOIN room_assign as R1 ON R1.room_assign_id = RRA.room_assign_id
                                                    WHERE R1.room_assign_id = R.room_assign_id AND RSV.hotel_id = ".$Hotel_ID." AND
                                                    RSV.arrival <= '".$toDay."' AND RSV.departure >= '".$toDay."' AND RSV.status = 'Checked In'
                                                    GROUP BY R1.room_assign_id
                                                ) as RoomVacant
                                               FROM room_assign as R INNER JOIN roomtypes as RT ON RT.room_type_id = R.room_type_id
                                               WHERE R.hotel_id = ".$Hotel_ID." AND R.trash = 0 AND R.room_number != '' AND
                                               R.room_assign_id NOT IN(SELECT room_assign_id FROM housekeeping WHERE status = 0)".$QryPart."
                                               ORDER BY RoomCondition DESC, RoomVacant DESC, RoomNumber ASC");
                            /*--}}
                            @if(count($Sql))
                            <form id="demo-form1" action="{!!url('housekeeping-headquarters')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                                {!!csrf_field()!!}
                            	<input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                                <div class="form-group">
                                	<label class="control-label col-md-2 col-sm-2 col-xs-12">Employees</label>
                                    <div class="col-md-6 col-sm-6 col-xs-12 zeroPadd paddingLeft5">
                                    <select name="emp_id" id="emp_id" class="form-control col-md-3 col-xs-6 LR_Padd5" required="required">
                                        <option value="">Select Employee</option>
                                        @foreach( App\Employee::where([ ['hotel_id', $Hotel_ID], ['role_id', 4] ])->orderBy('emp_fname', 'ASC')->get() as $employee )
                                        <option value="{!!$employee->emp_id!!}">
                                            {!!$employee->emp_fname!!}{!!($employee->emp_mname)?" $employee->emp_mname":""!!}{!!" ".$employee->emp_lname!!}
                                        </option>
                                        @endforeach
                                    </select>
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12 zeroPadd paddingLeft5">
                                    <select name="room_stat" id="room_stat" class="form-control col-md-3 col-xs-6 LR_Padd5">
                                        <option value="">Select Status</option>
                                        <option value="clean"{{($request->room_stat == 'clean')?' Selected':''}}>Clean</option>
                                        <option value="dirty"{{($request->room_stat == 'dirty')?' Selected':''}}>Dirty</option>
                                    </select>
                                    </div>
                                </div>
                                <table id="datatable-checkbox" class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="check-all" class="flat" data-id="room_assign_id[]"></th>
                                            <th>Room Number</th>
                                            <th>Type</th>
                                            <th>Condition</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($Sql as $RoomVal)
                                        <tr>
                                            <td><input type="checkbox" class="flat" name="room_assign_id[]" value="{{$RoomVal->RoomId}}"{!!($RoomVal->RoomCondition == 2 && $RoomVal->RoomVacant == '')?" Checked":""!!}></td>
                                            <td>{{$RoomVal->RoomNumber}}</td>
                                            <td>{{$RoomVal->RoomTypes}}</td>
                                            <td>{{($RoomVal->RoomCondition==1)?"Clean":"Dirty"}}</td>
                                            <td>{{($RoomVal->RoomVacant)?$RoomVal->RoomVacant:""}}</td>
                                        </tr>
                                   	@endforeach
                                    </tbody>
                                </table>
                                <div class="form-group form-group-last marginTop10">
                                    <div class="col-md-6 col-sm-6 col-xs-12 zeroPadd">
                                        <button type="submit" class="btn btn-info buTTonResize" onclick="return ConfirmAssign();"><i class="fa fa-check-square-o"></i> Assign Room</button>
                                    </div>
                                </div>
                           	</form>
                            @else
                          	<div class="col-md-12 col-sm-12 col-xs-12 labelText alert alert-warning marginZero AlgnCenter">
                           		No Dirty Rooms Found
                            </div>
                            @endif
                            </div>
                            
                            <div class="col-md-6 col-sm-12 col-xs-12 zeroRightPadd">
                            <span class="section">DIRTY ROOM ASSIGNMENT LIST:</span>
                            {{--*/
                            $Sql = DB::select("SELECT R.room_assign_id as RoomId, R.room_number as RoomNumber, R.room_stat as RoomCondition,
                                               CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType, RT.room_type as RoomTypes,
                                               E.emp_fname as EmpName, H.housekeeping_id as HouseKeepingId
                                               FROM housekeeping as H INNER JOIN room_assign as R ON R.room_assign_id = H.room_assign_id
                                               INNER JOIN roomtypes as RT ON RT.room_type_id = R.room_type_id
                                               INNER JOIN employees as E ON E.emp_id = H.emp_id
                                               WHERE H.hotel_id = ".$Hotel_ID." AND H.status = 0 AND R.trash = 0
                                               ORDER BY R.room_number ASC");
                            /*--}}
                            @if(count($Sql))
                                <form id="demo-form2" action="{!!url('housekeeping-reverse')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                                    {!!csrf_field()!!}
                                    <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                                    <input type="hidden" name="form_type" id="form_type" value="">
                                    <table id="datatable-checkbox" class="table table-striped jambo_table bulk_action">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" id="check-all" class="flat" data-id="housekeeping_id[]"></th>
                                                <th>Room Number</th>
                                                <th>Type</th>
                                                <th>Assignment</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($Sql as $RoomVal)
                                            <tr>
                                                <td><input type="checkbox" class="flat" name="housekeeping_id[]" value="{{$RoomVal->HouseKeepingId}}" data-parsley-type="1" required></td>
                                                <td>{{$RoomVal->RoomNumber}}</td>
                                                <td>{{$RoomVal->RoomTypes}}</td>
                                                <td>{{$RoomVal->EmpName}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="form-group form-group-last marginTop10">
                                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                                            <button type="submit" class="btn btn-danger buTTonResize" onclick="return ConfirmRemove();"><i class="fa fa-undo"></i> Remove Assignments</button>
                                            <button type="submit" class="btn btn-success buTTonResize" onclick="return ConfirmClean();"><i class="fa fa-check-square-o"></i> Clean Rooms</button>
                                        </div>
                                    </div>
                                </form>
                            @else
                                <div class="col-md-12 col-sm-12 col-xs-12 labelText alert alert-warning marginZero AlgnCenter">
                                	No Dirty Rooms Assignments Found
                                </div>
                            @endif
                            </div>
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function FilterSource(str){
		if(str){
			RedirectStr = '?';
			if(str){
				RedirectStr += "job_title="+str+"&";
			}
			RedirectStr = RedirectStr.replace(/&+$/,'');
			window.location.href="{!!url('housekeeping-headquarters')!!}"+RedirectStr;
		}else if(!str){
			window.location.href="{!!url('housekeeping-headquarters')!!}";
		}
	}
	function ConfirmAssign()
	{
		var x = confirm("Are you sure you want to assign dirty room to HouseKeeping Staff?");
		if (x)
			return true;
		else
			return false;
	}
	function ConfirmRemove()
	{
		$("#form_type").val("1");
		var x = confirm("Are you sure you want to remove selected Assignments?");
		if (x)
			return true;
		else
			return false;
	}
	function ConfirmClean()
	{
		$("#form_type").val("2");
		var x = confirm("Are you sure selected rooms are cleaned by Housekeeping?");
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop     

@section('jQuery')
<script>
$(document).ready(function() {
	$("#room_stat").on("change",function () {
		var stat = $(this).val();
		if(stat){
		window.location.href="{{url('/housekeeping-headquarters')}}?room_stat="+stat;
		}else{
		window.location.href="{{url('/housekeeping-headquarters')}}";
		}
	});
});
</script>
@stop 
