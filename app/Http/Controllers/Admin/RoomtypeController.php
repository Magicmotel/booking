<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Roomtype;
use App\RoomAssign;
use App\RoomtypeFacility;
use Image;
use Helper;
use Auth;
use DB;

class RoomtypeController extends Controller
{
    public function index(Request $request)
    {
		$Hotel_ID = Helper::getHotelId();
		$Results  = Roomtype::where([ ['hotel_id', $Hotel_ID], ['trash', '0'] ])->orderBy('room_type', 'ASC')->paginate(10);
		return view('rooms.room-type')->with("Results", $Results);
	}

    public function create()
    {	
		$Hotel_ID = Helper::getHotelId();
		$Results  = Roomtype::where([ ['hotel_id', $Hotel_ID], ['trash', 0] ])->get();
		$totRslt  = count($Results);
        return view('rooms.room-type-create')->with("Hotel_ID", $Hotel_ID)->with("totRslt", $totRslt);
    }

    public function store(Request $request)
    {
		$Hotel_ID  = Helper::getHotelId();
		$unique_ID = Helper::getUniqueId();
		$admin_ID  = Helper::getAdminUserId();
		$imageName = Helper::setImgName($request->room_type);
		$rootImage_Path = Helper::getImageRootPath();
		
		$imagePrefix = $imageName."-".$request->hotel_id;
		$TotalCount  = $request->room_count; // Total No of Room in Room Type
		
		if($Hotel_ID == $request->hotel_id)
		{
			$RoomTypeExst = Roomtype::where([['hotel_id', $Hotel_ID], ['room_type', $request->room_type] ])->first();
			if(count($RoomTypeExst) == 0)
			{
				$fileFolderPath = $rootImage_Path."/room_type";
				
				$this->validate($request, [
					'room_pic_1' => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:2048',
					'room_pic_2' => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:2048',
					'room_pic_3' => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:2048',
					'room_pic_4' => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:2048'
				]);
				
				$pic_1 = $request->file('room_pic_1');
				$pic_1_name = "";
				if($pic_1){
					$pic_1_name = $imagePrefix."-1.".$pic_1->getClientOriginalExtension();
					$pic_1->move($fileFolderPath, $pic_1_name);
					Image::make($fileFolderPath."/".$pic_1_name)->resize(200, 150, function ($constraint) { $constraint->aspectRatio(); })->save();
				}
				
				$pic_2 = $request->file('room_pic_2');
				$pic_2_name = "";
				if($pic_2){
					$pic_2_name = $imagePrefix."-2.".$pic_2->getClientOriginalExtension();
					$pic_2->move($fileFolderPath, $pic_2_name);
					Image::make($fileFolderPath."/".$pic_2_name)->resize(200, 150, function ($constraint) { $constraint->aspectRatio(); })->save();
				}
				
				$pic_3 = $request->file('room_pic_3');
				$pic_3_name = "";
				if($pic_3){
					$pic_3_name = $imagePrefix."-3.".$pic_3->getClientOriginalExtension();
					$pic_3->move($fileFolderPath, $pic_3_name);
					Image::make($fileFolderPath."/".$pic_3_name)->resize(200, 150, function ($constraint) { $constraint->aspectRatio(); })->save();
				}
				
				$pic_4 = $request->file('room_pic_4');
				$pic_4_name = "";
				if($pic_4){
					$pic_4_name = $imagePrefix."-4.".$pic_4->getClientOriginalExtension();
					$pic_4->move($fileFolderPath, $pic_4_name);
					Image::make($fileFolderPath."/".$pic_4_name)->resize(200, 150, function ($constraint) { $constraint->aspectRatio(); })->save();
				}		
				
				$RequestArray = array();
				$RequestArray[] = $unique_ID;
				$RequestArray[] = $request->hotel_id;
				$RequestArray[] = $request->room_display;
				$RequestArray[] = $request->room_type;
				$RequestArray[] = $request->room_count;
				$RequestArray[] = $request->guest_adult;
				$RequestArray[] = $request->guest_child;
				$RequestArray[] = $request->room_smoking_type;
				$RequestArray[] = $request->room_bed_size;
				$RequestArray[] = $request->room_rate;
				$RequestArray[] = $pic_1_name;
				$RequestArray[] = $pic_2_name;
				$RequestArray[] = $pic_3_name;
				$RequestArray[] = $pic_4_name;
				$RequestArray[] = $request->description;
				
				foreach(RoomtypeFacility::all(['rtf_name as FacilityName', 'rtf_key as FacilityKey']) as $vals)
				{
					if(isset($request->room_faclty) && !empty($request->room_faclty))
					{
						if(in_array($vals->FacilityKey, $request->room_faclty)){
							$RequestArray[] = 1;
						}else{
							$RequestArray[] = 0;
						}
					}else{
						$RequestArray[] = 0;
					}
				}
				
				$RequestArray[] = $request->room_order;	
				$RequestArray[] = $_SERVER['REMOTE_ADDR'];
				$RequestArray[] = $admin_ID;
				
				$Results = DB::select('CALL RoomType_Insert_SP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $RequestArray);
				$RoomTypeIdArray = DB::select('SELECT LAST_INSERT_ID()');
				foreach(json_decode(json_encode($RoomTypeIdArray), true)[0] as $val){
					$RoomTypeId = $val;
				}
				
				for($i = 1; $i <= $TotalCount; $i++)
				{
					$RoomAssignArray = array();
					$RoomAssignArray[] = $request->hotel_id;
					$RoomAssignArray[] = $RoomTypeId;		
					$RoomAssignArray[] = $_SERVER['REMOTE_ADDR'];
					$RoomAssignArray[] = $admin_ID;
					DB::select('CALL RoomAssign_Insert_SP(?,?,?,?)', $RoomAssignArray);
				}
				
				$room_rate_option_from = date('Y-m-d');
				$room_rate_option_to   = date('Y-m-d', strtotime($room_rate_option_from.'+12 months'));
				
				$date1 = date_create($room_rate_option_from);
				$date2 = date_create($room_rate_option_to);
				if($date1 && $date2)
				{
					$diff = date_diff($date1, $date2);
					$dayNum = $diff->format("%a");
					for($i = 1; $i <= $dayNum; $i++)
					{
						if(('$room_rate_option_from') <= ('$room_rate_option_to'))
						{
							$RoomRateOptionArray = array();
							$RoomRateOptionArray[] = $request->hotel_id;
							$RoomRateOptionArray[] = $RoomTypeId;
							$RoomRateOptionArray[] = $room_rate_option_from;
							$RoomRateOptionArray[] = $request->room_rate;
							$RoomRateOptionArray[] = $_SERVER['REMOTE_ADDR'];
							$RoomRateOptionArray[] = $admin_ID;
							
							DB::select('CALL RoomRateOption_Insert_SP(?,?,?,?,?,?)', $RoomRateOptionArray);
						} 
						$room_rate_option_from = date('Y-m-d', strtotime($room_rate_option_from.'+1 days'));
					}
				}
				return redirect('room/room-type')->with('success','Room Type created successfully.');
			}else{
				return redirect('room/room-type/create')->with("danger", "Room Type ".$request->room_type." already Exists!!")->withInput();
			}
		}
		else
		{
			return redirect('room/room-type')->with("danger", "Please select your Motel");
		}
    }

    public function edit($id)
    {
		$Hotel_ID = Helper::getHotelId();
		$totQry   = Roomtype::where([ ['hotel_id', $Hotel_ID], ['trash', 0] ])->get();
		$totRslt  = count($totQry);
		
		$Results = Roomtype::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $id] ])->first();
		if(count($Results))
		{
			return view('rooms.room-type-edit')->with("Results", $Results)->with("totRslt", $totRslt);
		}
		else
		{
			return redirect('room/room-type')->with("danger", "Room Type# ".$id." not Found");
		}
    }

    public function update(Request $request, $id)
    {
		$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		$imageName = Helper::setImgName($request->room_type);
		$rootImage_Path = Helper::getImageRootPath();
		
		$imagePrefix  = $imageName."-".$Hotel_ID;
		$returnOutput = ""; $outPutValue = array();
		
		$Results = Roomtype::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $id] ])->first();
		if(count($Results))
		{
			$RoomTypeExst = Roomtype::where([['hotel_id', $Hotel_ID], ['room_type', $request->room_type], ['room_type_id', '!=', $id] ])->first();
			if(count($RoomTypeExst) == 0)
			{
			
				$this->validate($request, [
					'room_pic_1' => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:2048',
					'room_pic_2' => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:2048',
					'room_pic_3' => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:2048',
					'room_pic_4' => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:2048'
				]);
				
				$lastRoomInvent = $Results->room_count;
				$newRoomInvent  = $request->room_count;
				
				if($lastRoomInvent != $newRoomInvent)
				{
					$cRoomTypeId = $id;
					$toDay       = date("Y-m-d");
					$endDay      = date("Y-m-d", strtotime($toDay.'+12 months'));
					$returnOutput = Helper::roomTypeInventoryAdjustment($Hotel_ID, $cRoomTypeId, $toDay, $endDay);
					if($returnOutput)
					{
						$outPutValue = explode(",", $returnOutput);
						$inventoryMax = $outPutValue[0];
						$inventoryDate = $outPutValue[1];
						if($inventoryMax > $request->room_count){
							$rUrl = "room/room-type/".$id."/edit";
							return redirect($rUrl)->with('danger','Room Type have '.$inventoryMax.' Reservations on '.date("m/d/Y", strtotime($inventoryDate)).'.');
						}
					}
				}
				
				$Results->room_display = $request->room_display;
				$Results->room_type    = $request->room_type;
				$Results->room_count   = $request->room_count;
				$Results->guest_adult  = $request->guest_adult;
				$Results->guest_child  = $request->guest_child;
				$Results->room_smoking_type = $request->room_smoking_type;
				$Results->room_bed_size = $request->room_bed_size;
				$Results->room_rate     = $request->room_rate;
				$Results->description   = $request->description;
				$Results->room_order = $request->room_order;
				$Results->reg_ip 	 = $_SERVER['REMOTE_ADDR'];
				$Results->added_by 	 = $admin_ID;
				
				
				$fileFolderPath = $rootImage_Path."/room_type";
				
				$pic_1 = $request->file('room_pic_1');
				$pic_1_name = "";
				if($pic_1){
					$pic_1_name = $imagePrefix."-1.".$pic_1->getClientOriginalExtension();
					$pic_1->move($fileFolderPath, $pic_1_name);
					Image::make($fileFolderPath."/".$pic_1_name)->resize(200, 150, function ($constraint) { $constraint->aspectRatio(); })->save();
				}else{
					$pic_1_name = $Results->room_pic_1;
				}
				
				$pic_2 = $request->file('room_pic_2');
				$pic_2_name = "";
				if($pic_2){
					$pic_2_name = $imagePrefix."-2.".$pic_2->getClientOriginalExtension();
					$pic_2->move($fileFolderPath, $pic_2_name);
					Image::make($fileFolderPath."/".$pic_2_name)->resize(200, 150, function ($constraint) { $constraint->aspectRatio(); })->save();
				}else{
					$pic_2_name = $Results->room_pic_2;
				}
				
				$pic_3 = $request->file('room_pic_3');
				$pic_3_name = "";
				if($pic_3){
					$pic_3_name = $imagePrefix."-3.".$pic_3->getClientOriginalExtension();
					$pic_3->move($fileFolderPath, $pic_3_name);
					Image::make($fileFolderPath."/".$pic_3_name)->resize(200, 150, function ($constraint) { $constraint->aspectRatio(); })->save();
				}else{
					$pic_3_name = $Results->room_pic_3;
				}
				
				$pic_4 = $request->file('room_pic_4');
				$pic_4_name = "";
				if($pic_4){
					$pic_4_name = $imagePrefix."-4.".$pic_4->getClientOriginalExtension();
					$pic_4->move($fileFolderPath, $pic_4_name);
					Image::make($fileFolderPath."/".$pic_4_name)->resize(200, 150, function ($constraint) { $constraint->aspectRatio(); })->save();
				}else{
					$pic_4_name = $Results->room_pic_4;
				}
				
				$Results->room_pic_1 = $pic_1_name;
				$Results->room_pic_2 = $pic_2_name;
				$Results->room_pic_3 = $pic_3_name;
				$Results->room_pic_4 = $pic_4_name;
				
				foreach(RoomtypeFacility::all(['rtf_name as FacilityName', 'rtf_key as FacilityKey']) as $vals)
				{
					if(isset($request->room_faclty) && !empty($request->room_faclty))
					{
						if(in_array($vals->FacilityKey, $request->room_faclty)){
							$Results->{"$vals->FacilityKey"} = 1;
						}else{
							$Results->{"$vals->FacilityKey"} = 0;
						}
					}else{
						$Results->{"$vals->FacilityKey"} = 0;
					}
				}
					
				$Results->save();
				
				if($lastRoomInvent != $newRoomInvent)
				{
					$cRoomTypeId = $id;
					$toDay       = date("Y-m-d");
					$endDay      = date("Y-m-d", strtotime($toDay.'+12 months'));
					Helper::ChannelPartnerRequest($Hotel_ID, $cRoomTypeId, $toDay, $endDay);
				}		
				return redirect('room/room-type')->with('success','Room Type updated successfully.');
			}else{
				return redirect('room/room-type/'.$id.'/edit')->with("danger", "Room Type ".$request->room_type." already Exists!!")->withInput();
			}
		}
		else
		{
			return redirect('room/room-type')->with("danger", "Please select your Motel");
		}
    }
	
	public function trash($id)
    {	
		$Hotel_ID = Helper::getHotelId();
		$admin_ID = Helper::getAdminUserId();
		
		$Results = Roomtype::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $id] ])->first();
		if(count($Results))
		{
			if($Results->trash == 1){
				$Results->trash = 0;
			}else{
				$Results->trash = 1;
			}
			
			$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
			$Results->added_by = $admin_ID;
			$Results->save();
			return redirect('room/room-type')->with('success','Room Type deleted successfully');
		}
		else
		{
			return redirect('room/room-type')->with("danger", "Please select your Motel");
		}
    }

    public function destroy($id)
    {
		$Hotel_ID = Helper::getHotelId();
		$rootImage_Path = Helper::getImageRootPath();
				
		$Results = Roomtype::where([ ['room_type_id', '=', $id], ['hotel_id', '=', $Hotel_ID] ])->first();
		if(count($Results))
		{
			$fileFolderPath = $rootImage_Path."/room_type";
			if($Results->room_pic_1){
				Helper::removeImg($fileFolderPath.'/'.$Results->room_pic_1);
			}
			if($Results->room_pic_2){
				Helper::removeImg($fileFolderPath.'/'.$Results->room_pic_2);
			}
			if($Results->room_pic_3){
				Helper::removeImg($fileFolderPath.'/'.$Results->room_pic_3);
			}
			if($Results->room_pic_4){
				Helper::removeImg($fileFolderPath.'/'.$Results->room_pic_4);
			}
			$Results->delete();
			return redirect('room/room-type')->with('success','Room Type deleted successfully.');
		}
		else
		{
			return redirect('room/room-type')->with("danger", "Room Type# ".$id." not Found");
		}
    }
	
	public function removeImage(Request $request)
    {
		$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		
		if($request->ajax())
		{
			$room_type_id = $request->room_type_id;
			$room_pic_no  = $request->room_pic_no;
			
			$Results = Roomtype::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $room_type_id] ])->first();
			if(count($Results))
			{	
				$rootImage_Path = Helper::getImageRootPath();
				$fileFolderPath = $rootImage_Path."/room_type";
				$fileName = $fileFolderPath.'/'.$Results->{$room_pic_no};
				
				Helper::removeImg($fileName);
				
				$Results->{$room_pic_no} = "";
				$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Results->added_by = $admin_ID;
				$Results->save();
				return response()->json(['response' => 'Room Image removed Successfully', 'status' => 'success']);
			}
			else
			{
				return response()->json(['response' => 'Room Image not Found', 'status' => 'error']);
			}
		}else{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
	
	public function roomTypeAvailable(Request $request)
    {
		$Hotel_ID  = Helper::getHotelId();
		
		if($request->ajax())
		{
			$smokingType = $request->smokingType;
			$bedSize     = $request->bedSize;
			
			$Results = Roomtype::where([ ['hotel_id', '=', $Hotel_ID], ['room_smoking_type', '=', $smokingType], ['room_bed_size', '=', $bedSize] ])->first();
			if(count($Results)){
				return response()->json(['response' => 'Room Type already Exists!!!', 'status' => 'error']);		
			}else{
				return response()->json(['response' => 'Room Type available.', 'status' => 'success']);
			}			
		}
		else
		{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
}