<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HouseKeeping extends Model
{
    protected $table = 'housekeeping';
	protected $primaryKey = 'housekeeping_id';
	protected $fillable = [
        'hotel_id', 'room_assign_id', 'emp_id',
		'status', 'reg_ip', 'added_by'
    ];
}
