@extends('layouts.layout')

@section('title')
	Create Job Title
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Create Job Title</h3></div>
        </div>
        
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('job-titles')}}">Manage Job Titles</a></li>
                <li class="active"><a href="javascript:void(0);">Create Job Title</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	@if (count($errors))
                <ul class="errorFormMessage">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                    <form id="demo-form2" action="{!!url('job-titles')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                        {!!csrf_field()!!}
                        <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name of Job Title <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            	<input type="text" name="name" id="name" value="{!!old('name')!!}" class="form-control col-md-7 col-xs-12" placeholder="Name of Job Title" required="required">
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop20">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-plus-square-o"></i> Create Job Title</button>
                                <button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                    	</div>                    
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop