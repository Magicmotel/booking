<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Hotel;
use App\Roomtype;
use App\RoomRateOption;
use App\ChannelMotel;
use App\ChannelMapping;
use Helper;
use DB;

class DailyRateUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rateupdate:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$toDay = date("Y-m-d", strtotime('364 days'));
		$Channel_From = $Channel_To = $toDay;
		
		$HotelRst = Hotel::where('status', 1)->get(['hotel_id']);
		foreach($HotelRst as $HotelVal)
		{
			$Hotel_ID = $HotelVal->hotel_id;
			
			$RoomTypeRst = Roomtype::where('hotel_id', $Hotel_ID)->get(['room_type_id', 'hotel_id', 'room_rate']);
			$PSJ = 0;
			foreach($RoomTypeRst as $RoomTypeVal)
			{
				$RoomTypeId  = $RoomTypeVal->room_type_id;
				$DefaultRate = $RoomTypeVal->room_rate;
				
				$RateOptionRst = RoomRateOption::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $RoomTypeId], ['room_rate_date', $toDay] ])->first();
				if(!count($RateOptionRst))
				{
					$RateOptionOBJ = new RoomRateOption;
					$RateOptionOBJ->hotel_id        = $Hotel_ID;
					$RateOptionOBJ->room_type_id    = $RoomTypeId;
					$RateOptionOBJ->room_rate_date  = $toDay;
					$RateOptionOBJ->room_rate_price = $DefaultRate;
					$RateOptionOBJ->reg_ip   = '';
					$RateOptionOBJ->added_by = 0;
					$RateOptionOBJ->save();
					
					$channelRst = ChannelMotel::where([ ['hotel_id', $Hotel_ID], ['channel_status', 1], ['channel_id', 1] ])
											->first(['channel_hotel_id', 'channel_username', 'channel_password']);
					$channelNum = count($channelRst);
					if($channelNum)
					{
						$Channel_HotelId    = $channelRst->channel_hotel_id;
						$Channel_Username   = $channelRst->channel_username;
						$Channel_Password   = $channelRst->channel_password;
					
						$requestXML =  '';
						$channelMapRst	= ChannelMapping::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $RoomTypeId], ['channel_id', 1] ])
										  ->first(['channel_room_type_id', 'channel_rate_plan_id']);
						if(count($channelMapRst))
						{
							$Channel_RoomTypeId = $channelMapRst->channel_room_type_id;
							$Channel_RatePlanId = $channelMapRst->channel_rate_plan_id;
							
							$RoomAvailInvt = Helper::totalInventoryAvailable($Hotel_ID, $RoomTypeId, $Channel_From);
							
							$requestXML =  '<?xml version="1.0" encoding="UTF-8"?>
											<AvailRateUpdateRQ xmlns="http://www.expediaconnect.com/EQC/AR/2011/06">
												<Authentication username="'.$Channel_Username.'" password="'.$Channel_Password.'"/>
												<Hotel id="'.$Channel_HotelId.'"/>
												<AvailRateUpdate>
													<DateRange from="'.$Channel_From.'" to="'.$Channel_To.'"/>
													<RoomType id="'.$Channel_RoomTypeId.'" closed="false">
														<Inventory totalInventoryAvailable="'.$RoomAvailInvt.'"/>
														<RatePlan id="'.$Channel_RatePlanId.'">
															<Rate currency="USD">
																<PerDay rate="'.$DefaultRate.'"/>
															</Rate>
														</RatePlan>
													</RoomType>
												</AvailRateUpdate>
											</AvailRateUpdateRQ>';
							$URL = "https://services.expediapartnercentral.com/eqc/ar";
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, $URL);
							curl_setopt($ch, CURLOPT_VERBOSE, 1);
							curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
							curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
							curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
							curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
							$responseXML = curl_exec($ch);
							$headerResponse = curl_getinfo($ch);
							curl_close($ch);
							
							if(isset($headerResponse['http_code']) && $headerResponse['http_code']==200)
							{
								$responseString = simplexml_load_string($responseXML);
								$responseJSON   = json_decode(json_encode($responseString), true);
								if(isset($responseJSON['Success'])){
									if(count($responseJSON['Success']) > 0){
										$ResponseStat = 'Success with Warning';
									}else{
										$ResponseStat = 'Success';
									}
								}
								elseif(isset($responseJSON['Error'])){
									$ResponseStat = 'Error';
								}
							}else{
								$ResponseStat = 'Error';
							}
							DB::select("CALL ChannelCommunication_SP ('".$Hotel_ID."', 'MMK', '".$URL."', '".$requestXML."', 'Expedia',
																	  '".$responseXML."', '".$ResponseStat."', '')");
						}
					}
				}
			}
		}
    }
}
