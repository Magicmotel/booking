<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    
    protected $fillable = [
        'emp_id_unq', 'hotel_id', 'emp_fname', 'emp_mname', 'emp_lname',
		'emp_dob', 'emp_email', 'emp_phone', 'emp_address', 'emp_zip', 'emp_city', 'emp_state',
		'role_id', 'shift_id', 'emp_admin',
		'reg_ip', 'added_by'
    ];
	protected $primaryKey = 'emp_id';	
	protected $table = 'employees';
	
}
