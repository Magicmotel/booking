<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZipCountry extends Model
{
    protected $table = 'zip_country';
	protected $primaryKey = 'country_id';
	protected $fillable = [
        'country_name'
    ];
}
