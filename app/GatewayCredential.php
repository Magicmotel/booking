<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GatewayCredential extends Model
{
    
    protected $fillable = [
        'hotel_id', 'gateway_id', 'user_id', 'user_api_key',
		'developer_id', 'location_id', 'ticket_hash_key',
		'gateway_status', 'reg_ip', 'added_by'
    ];
	protected $primaryKey = 'gc_id';	
	protected $table = 'gateway_credentials';
	
}
