@extends('layouts.layout')

{{--*/ $TitleSubject = "Hotel# MMKiD".$Hotel_ID.", ".$MotelInfo->hotel_name." :: In House Report ".$searchDate;/*--}} 
@section('title')
	{{$TitleSubject}}
@stop

@section('CascadingSheet')
    {!!Html::script('vendors/pdfmake/build/sprintf.js')!!}
    {!!Html::script('vendors/pdfmake/build/jspdf.js')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>In House Report</h3></div>
            <div class="col-md-2 col-sm-2 col-xs-2 fltL zeroRightPadd marginTop5 marginBottom5" id="calenderBox">
            <input type="text" name="search_date" id="search_date" value="{{$searchDate}}" placeholder="mm/dd/yyyy" class="date-picker form-control col-md-7 col-xs-12  has-feedback-left" readonly="readonly">
            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class="active"><a href="javascript:void(0);">In House Report</a></li>
                <li><a href="{{url('report-incoming-reservation')}}">Incoming Reservation Report</a></li>
            </ul>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content" id="printableArea">
                        <table class="table table-striped" id="pdfableArea">
                            <tr class="backgroundNone">
                                <td width="20%" class="borderNone height140">
                                	{!!Html::image('motel/'.$MotelInfo->hotel_logo, 'alt', array( 'title' => 'Motel Logo', 'style' => 'max-width:120px; max-height:120px;'))!!}
                              	</td>
                                <td width="60%" align="center" valign="middle" class="zeroPadd borderNone labelText font16">
                                	In House Report<br />({{$searchDate}})
                                </td>
                                <td width="20%" class="zeroPadd borderNone">
                                	<div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnLeft LneHeight24">Date: {{date('m/d/Y')}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnLeft LneHeight24">Time: {{date('h:i A')}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnLeft LneHeight24">User: {{Helper::getAdminUserName()}}</div>
                               	</td>
                            </tr>
                            <tr class="backgroundNone">
                            	<td colspan="3" class="zeroPadd borderNone">
                                	<table id="exelableArea" class="table table-bordered table-striped jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>City</th>
                                                <th>Zip</th>
                                                <th>Room#</th>
                                                <th>Type</th>
                                                <th>#Nights</th>
                                                <th>Arrival</th>
                                                <th>Departure</th>
                                                <th>Company</th>
                                                <th>Reservation#</th>
                                                <th>Source</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($GuestRst))
                                            @foreach($GuestRst as $vals)
                                            <tr>
                                                <td class="uPPerLetter">{{$vals->GuestFname." ".$vals->GuestLname}}</td>
                                                <td>{{$vals->GuestPhone}}</td>
                                                <td>{{$vals->GuestCity}}</td>
                                                <td>{{$vals->GuestPostal}}</td>
                                                <td>{{$vals->RoomNumber}}</td>
                                                <td>{{$vals->RoomTypes}}</td>
                                                <td>{{$vals->RsrvNights}}</td>
                                                <td>{{$vals->ArvlDate}}</td>
                                                <td>{{$vals->DprtDate}}</td>
                                                <td>{{$vals->GuestCompany}}</td>
                                                <td>{{($vals->RsrvId)}}</td>
                                                <td>{{($vals->RsrvSource)?$vals->RsrvSource:"Local"}}</td>
                                            </tr>
                                            @endforeach
                                       	@else
                                        	<tr>
                                                <td colspan="12" class="errorMessageTR">No Guest Available</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                              	</td>
                            </tr>
                        </table>
                    </div>
                    <button class="btn btn-primary marginTop20 margin5" id="btnPrint"><i class="fa fa-print"></i> Print</button>
                    <button class="btn btn-success marginTop20 margin5" id="btnExcel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</button>
                    <button class="btn btn-danger marginTop20 margin5" id="btnPDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</button>
                </div>
         	</div>
      	</div>
    </div>
</div>
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	$('#search_date').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		var newDate = new Date(ev.date).toString("MM/dd/yyyy");
		window.location.href="{!!url('report-inhouse-guest?search_date=')!!}"+newDate;
	}).data('datepicker');
	$("#btnPrint").click(function(e){
		var printContents = document.getElementById('printableArea').innerHTML;
		var originalContents = document.body.innerHTML;
		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
		window.location.reload();
	});
	$("#btnExcel").click(function(e){
		e.preventDefault();
		
		var data_type = 'data:application/vnd.ms-excel';
		var table_div = document.getElementById('exelableArea');
		var table_html = table_div.outerHTML.replace(/ /g, '%20');
		
		var a = document.createElement('a');
		a.href = data_type + ', ' + table_html;
		a.download = '{{$TitleSubject}}.xls';
		a.click();
	});
	$("#btnPDF").click(function(e){
		var pdf = new jsPDF('p', 'pt', 'letter');
		source = $('#pdfableArea')[0];
		specialElementHandlers = {
			// element with id of "bypass" - jQuery style selector
			'#bypassme': function(element, renderer) {
				// true = "handled elsewhere, bypass text extraction"
				return true
			}
		};
		margins = {
			top: 40,
			bottom: 40,
			left: 40,
			width:572
		};
		
		pdf.fromHTML(
				source, // HTML string or DOM elem ref.
				margins.left, // x coord
				margins.top,
				{// y coord
					'width': margins.width, // max width of content on PDF
					'elementHandlers': specialElementHandlers
				},
				function(dispose) {
					pdf.save('{{$TitleSubject}}.pdf');
				},
				margins
		);
	});
});
</script>
@stop