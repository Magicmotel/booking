<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChannelMapping extends Model
{
    
    protected $fillable = [
        'hotel_id', 'room_type_id', 'channel_id',
		'channel_room_type_id', 'channel_rate_plan_id',
		'reg_ip', 'added_by'
    ];
	protected $primaryKey = 'map_id';	
	protected $table = 'channel_motel_mapping';
	
}
