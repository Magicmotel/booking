<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Guest;
use App\ZipCityState;
use Validator;
use Auth;
use Helper;
use DB;
use Session;

class GuestController extends Controller
{
	public function index(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		
		$Results = DB::table('guest as GST')
			->select('GST.guest_fname as GuestFname', 'GST.guest_mname as GuestMname', 'GST.guest_lname as GuestLname', 'GST.guest_id as GuestId')
			->join('room_reservation as RSRV', 'RSRV.guest_id', '=', 'GST.guest_id')
			->where('RSRV.hotel_id', $Hotel_ID)
			->orderBy('GuestFname', 'ASC')->orderBy('GuestMname', 'ASC')->orderBy('GuestLname', 'ASC')
			->paginate(10);
		return view('guests.guests-index')->with("Results", $Results)->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function create()
    {	
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
        return view('guests.guests-create')->with('Hotel_ID', $Hotel_ID);
    }

    public function store(Request $request)
    {
		$Helper = new Helper();
		$unique_ID = $Helper->getUniqueId();
		$admin_ID  = $Helper->getAdminUserId();
		
        $validator = Validator::make($request->all(), [
            'fname'    => 'Required|Min:3|Max:80|Alpha',
			'lname'    => 'Required|Min:3|Max:80|Alpha',
			'email'    => 'Required|Between:3,64|Email|Unique:guests',
			'password' => 'Required|AlphaNum|Min:7|Confirmed',
			'password_confirmation'=>'Required|AlphaNum|Min:7',
			'role_id'  => 'Required',
			'shift_id' => 'Required'
        ]);
		
		if ($validator->fails()) {
            return redirect('guests/create')->withErrors($validator)->withInput();
        }
		else
		{		
			$guests = new Guest;
			$guests->id_unq = $unique_ID;
			$guests->hotel_id = $request->hotel_id;
			$guests->fname = $request->fname;
			$guests->mname = $request->mname;
			$guests->lname = $request->lname;
			$guests->email = $request->email;
			$guests->password = bcrypt($request->password);
			$guests->role_id = $request->role_id;
			$guests->shift_id = $request->shift_id;
			$guests->reg_ip = $_SERVER['REMOTE_ADDR'];
			$guests->added_by = $admin_ID;
			$guests->save();
			return redirect('guests')->with('success','Guest created successfully');
		}
    }

    
    public function show($id)
    {
    	$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$Results  = Guest::find($id); // return result in json format
		return view('guests.guests-view')->with("Results", $Results);
    }

    public function edit($id)
    {	
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$Results  = Guest::find($id); // return result in json format
		if($Results){
			return view('guests.guests-edit')->with("Results", $Results);
		}else{
			return redirect('guests');
		}
    }

    public function update(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$validator = Validator::make($request->all(), [
           	'fname'    => 'Required|Min:3|Max:80|Alpha',
			'lname'    => 'Required|Min:3|Max:80|Alpha',
			'dob'      => 'Required'
        ]);
		
		if ($validator->fails()) {
            return redirect('guests/'.$id.'/edit')->withErrors($validator)->withInput();
        }
		else
		{
			$Guests = Guest::where('guest_id', $id)->first();
			if($Guests){
				$Guests->guest_title  = $request->guest_title;
				$Guests->guest_fname  = $request->guest_fname;
				$Guests->guest_mname = $request->guest_mname;
				$Guests->guest_lname = $request->guest_lname;
				$Guests->guest_address = $request->guest_address;
				$Guests->guest_zip = $request->guest_zip;
				$Guests->guest_city = $request->guest_city;
				$Guests->guest_state = $request->guest_state;
				$Guests->guest_email = $request->guest_email;
				$Guests->guest_phone = $request->guest_phone;
				$Guests->guest_dob = $request->guest_dob;
				$Guests->guest_nation = $request->guest_nation;
				$Guests->guest_idproof = $request->guest_idproof;
				$Guests->guest_idproof_other = $request->guest_idproof_other;
				$Guests->guest_proofno = $request->guest_proofno;
				$Guests->guest_group = $request->guest_group;
				$Guests->guest_vehicle = $request->guest_vehicle;
				$Guests->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Guests->added_by = $admin_ID;
				$Guests->save();
			}
			return redirect('guests')->with('success','Guest updated successfully');
		}
    }
	
	public function destroy($id)
    {
	    //$product = Product::find($id);
		//$product->delete();
		Guest::destroy($id);
		return redirect('guests')->with('success','Guest deleted permanently');	
    }
	
	public function bad_mark($id)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		return redirect('bad-guest')->with("guest_id", $id);
    }
	
	public function bad_guest(Request $request)
    {
		if(Session::get('guest_id')){
			setcookie('guest_id', Session::get('guest_id'), time()+3600, "/", "",  0);
			$id = Session::get('guest_id');
		}else{
			$id = $_COOKIE['guest_id'];
		}
		
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		
		$Results  = Guest::find($id); // return result in json format
		return view('guests.guests-bad')->with("Results", $Results);
    }
	
	public function bad_guest_create(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$bad_reason = implode(", ", $request->guest_bad_reason);
		
		$Results  = Guest::find($id); // return result in json format
		$Results->bad_status	= 0;
		$Results->bad_reason	= $bad_reason;
		$Results->bad_created_at= date("Y-m-d H:i:s");
		$Results->bad_reg_ip 	= $_SERVER['REMOTE_ADDR'];
		$Results->bad_addedby 	= $admin_ID;
		$Results->save();
		
		return redirect('checked-out-guests');
    }
	
	public function remove_bad_guest($id)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$Results  = Guest::find($id); // return result in json format
		$Results->bad_status	= 1;
		$Results->bad_created_at= date("Y-m-d H:i:s");
		$Results->bad_reg_ip 	= $_SERVER['REMOTE_ADDR'];
		$Results->bad_addedby 	= $admin_ID;
		$Results->save();
		
		return redirect('checked-out-guests');
    }
	
	public function findGuest(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$varQry = "";
		$searchPhone = trim($request->searchPhone);
		if($searchPhone){ $varQry .= " AND guest_phone LIKE '%".$searchPhone."%'"; }
		$searchName  = trim($request->searchName);
		if($searchName){
			$varQry .= " AND ( guest_fname LIKE '%".$searchName."%' OR 
							   guest_mname LIKE '%".$searchName."%' OR 
							   guest_lname LIKE '%".$searchName."%'
							 )";
		}
		$searchEmail = trim($request->searchEmail);
		if($searchEmail){ $varQry .= " AND guest_email LIKE '%".$searchEmail."%'"; }
		
		$Results = DB::select("SELECT * FROM guest WHERE 1=1".$varQry." ORDER BY guest_fname ASC LIMIT 0, 5");
		$RsltHtml = "";
		if(count($Results))
		{
			$RsltHtml .= '<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd labelText marginTop5">';
			$RsltHtml .= '<div class="col-md-4 col-sm-12 col-xs-12 zeroPadd marginTop3">Name</div>';
			$RsltHtml .= '<div class="col-md-3 col-sm-12 col-xs-12 zeroPadd marginTop3">DOB</div>';
			$RsltHtml .= '<div class="col-md-3 col-sm-12 col-xs-12 zeroPadd marginTop3">Phone</div>';
			$RsltHtml .= '<div class="col-md-2 col-sm-12 col-xs-12 zeroPadd marginTop3">&nbsp;</div>';
			$RsltHtml .= '</div>';
			foreach($Results as $vals){
				$RsltHtml .= '<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">';
				$RsltHtml .= '<div class="col-md-4 col-sm-12 col-xs-12 zeroPadd marginTop3">'.$vals->guest_fname.' '.$vals->guest_lname.(($vals->bad_status == 0)?' <i class="fa fa-user-times badGuest" title="Bad Guest"></i>':'').'</div>';
				$RsltHtml .= '<div class="col-md-3 col-sm-12 col-xs-12 zeroPadd marginTop3">'.$vals->guest_dob.'</div>';
				$RsltHtml .= '<div class="col-md-3 col-sm-12 col-xs-12 zeroPadd marginTop3">'.$vals->guest_phone.'</div>';
				$RsltHtml .= '<div class="col-md-2 col-sm-12 col-xs-12 zeroPadd AlgnRight"><div class="btn btn-info btn-xs guestButton" data-id="'.$vals->guest_id.'">GET</div></div>';
				$RsltHtml .= '</div>';
			}
		}
		else
		{
			$RsltHtml = "result not found";
		}		
		
		if($request->ajax()) {
			return response()->json(['response' => $RsltHtml, 'status' => 'success']);
		}else{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
	
	public function getCityData(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$zipCode = $request->zipCode;
		
		if($request->ajax()) {
			$Results = ZipCityState::where('zip_code', $zipCode)->first();
			if(count($Results)){
				return response()->json(['response' => "City, State get successfully", 'status' => 'success',
										 'zip_city' => $Results->zip_city, 'zip_state' => $Results->zip_state
										]);
			}else{
				return response()->json(['response' => "Zip Code not found in Table", 'status' => 'error',
										 'zip_city' => '', 'zip_state' => ''
										]);
			}
		}else{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
	
	public function fillGuest(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$guestId = $request->guestId;
		
		$Results = Guest::where('guest_id', $guestId)->first();		
		if($request->ajax()) {
			return response()->json(['response' => "Guest information filled successfully", 'status' => 'success',
									 'guest_id' => $Results->guest_id,       'guest_title' => $Results->guest_title,
									 'guest_fname' => $Results->guest_fname, 'guest_mname' => $Results->guest_mname,
									 'guest_lname' => $Results->guest_lname, 'guest_address' => $Results->guest_address, 'guest_country' => $Results->guest_country,
									 'guest_city'  => $Results->guest_city,  'guest_state'  => $Results->guest_state, 'guest_zip'   => $Results->guest_zip,
									 'guest_email' => $Results->guest_email, 'guest_phone' => $Results->guest_phone,
									 'guest_dob'   => $Results->guest_dob,   'guest_nation' => $Results->guest_nation,
									 'guest_idproof'=> $Results->guest_idproof, 'guest_idproof_other' => $Results->guest_idproof_other,
									 'guest_proofno'=> $Results->guest_proofno, 'guest_company' => $Results->guest_company,
									 'guest_group' => $Results->guest_group, 'guest_vehicle' => $Results->guest_vehicle, 'guest_tax' => $Results->guest_tax
									]);
		}else{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
}
