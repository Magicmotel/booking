@extends('layouts.layout')

@section('title')
	Fill Card Detail
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Fill Card Detail</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="errorSummary"></div>
                        @if($CardGuestNum>0)
                        <div class="form-horizontal form-label-left marginBottom24">
                        	@foreach($CardGuestRst as $CardGuestVals)
                        		<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">{{$CardGuestVals->cardName}}</label>
                                    <div class="control-value col-md-3 col-sm-3 col-xs-12 LR_Padd10">
                                    {{$CardGuestVals->firstSix}}*********{{$CardGuestVals->lastFour}}
                                    </div>
                                    <div class="control-value col-md-2 col-sm-2 col-xs-12">
                                    	<a href="{{url('/reservation/zeamster/transaction')}}?rsrvid={{$order_id}}&payamount={{$pay_Amount}}&cardholder=&action={{$pay_Mode}}&token={{$CardGuestVals->ticketToken}}&type=1" class="btn btn-info btn-xs">Pay Now</a>
                                    </div>
                                    <div class="control-value col-md-3 col-sm-3 col-xs-12">
                                    	Last Status: <b>{{$CardGuestVals->statName}}</b>
                                    </div>
                                </div>
                        	@endforeach
                        </div>
                        @endif
                    	<form action="{!!url('reservation/payment/transaction')!!}" method="POST" id="myform" data-parsley-validate class="form-horizontal form-label-left">
                            {!!csrf_field()!!}
                            <input type="hidden" name="pay_amount" id="pay_amount" value="{{$pay_Amount}}" class="getExtraVal">
                            <input type="hidden" name="pay_mode" id="pay_mode" value="{{$pay_Mode}}" class="getExtraVal">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Account Holder Name</label>
                                <div class="col-md-4 col-sm-6">
                                <input type="text" name="account_holder_name" id="account_holder_name" value="" placeholder="Account Holder Name" class="form-control col-md-7 col-xs-12 getExtraVal">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Credit Card Number <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6">
                                <input type="text" name="card_number" id="card_number" value="" placeholder="Credit Card Number" class="form-control col-md-7 col-xs-12 ticketed" data-ticket="card_number" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Expiry Date (MMYY) <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6">
                                <input type="text" name="exp_date" id="exp_date" value="" maxlength="4" size="4" placeholder="Expiry Date (MMYY)" class="form-control col-md-7 col-xs-12 ticketed" data-ticket="exp_date" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">CVV</label>
                                <div class="col-md-4 col-sm-6">
                                <input type="text" name="cvv" id="cvv" value="" placeholder="CVV" class="form-control col-md-7 col-xs-12 ticketed" data-ticket="cvv">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">ZIP Code</label>
                                <div class="col-md-4 col-sm-6">
                                <input type="text" name="zip" id="zip" value="" placeholder="ZIP Code" class="form-control col-md-7 col-xs-12 ticketed" data-ticket="billing_zip">
                                </div>
                            </div>
                            <div class="form-group form-group-last marginTop24">
                                <div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-3">
                                    <button type="submit" class="btn btn-info"><i class="fa fa-check-circle-o"></i> Pay Now</button>
                                </div>
                            </div>
                        </form>
                        
                        @if($CardGuaranteeNum>0)
                        <div class="marginTop24 form-horizontal form-label-left">
                        	@foreach($CardGuaranteeRst as $CardGuaranteeVals)
                            	{{--*/ $CardId = $CardGuaranteeVals->CardId; /*--}}
                        		<div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" id="cardName_{{$CardId}}">{{$CardGuaranteeVals->CardName}}</label>
                                    <div class="control-value col-md-3 col-sm-3 col-xs-12 LR_Padd10" id="cardNumber_{{$CardId}}">{{$CardGuaranteeVals->CardNumber}}</div>
                                    <div class="control-value col-md-2 col-sm-2 col-xs-12 LR_Padd10" id="cardExpiry_{{$CardId}}">{{$CardGuaranteeVals->CardExpiry}}</div>
                                    <div class="control-value col-md-2 col-sm-2 col-xs-12 displayNone" id="CardHolder_{{$CardId}}">{{$CardGuaranteeVals->CardHolder}}</div>
                                    <div class="control-value col-md-2 col-sm-2 col-xs-12 displayNone" id="CardCVV_{{$CardId}}">{{$CardGuaranteeVals->CardCVV}}</div>
                                    <div class="control-value col-md-2 col-sm-2 col-xs-12 displayNone" id="CardZIP_{{$CardId}}">{{$CardGuaranteeVals->CardZIP}}</div>
                                    <div class="control-value col-md-2 col-sm-2 col-xs-12 LR_Padd10">
                                    	<a href="javascript:void(0);" id="{{$CardId}}" class="btn btn-success btn-xs selectButtonCard">Select</a>
                                    </div>
                                </div>
                        	@endforeach
                        </div>
                        @endif
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script>
var zeamsterTrans = "{{url('/reservation/zeamster/transaction')}}";
function disableBackButton()
{
	window.history.forward();
}
setTimeout("disableBackButton()", 0);
</script>
@stop      

@section('JavascriptSRC')
    {!!Html::script('js/zeamster/api.jquery.js')!!}
@stop  

@section('jQuery')
<script>
$(document).ready(function(){
	$("#myform").ticket({
		timestamp : "<?=$timestamp?>",
		signature : "<?=$signature?>",
		location_id : "<?=$location_id?>",
		order_id : "<?=$order_id?>"
	});
	$(".selectButtonCard").click(function(){
		var cardId = this.id;
		$("#account_holder_name").val($("#CardHolder_"+cardId).html());
		$("#card_number").val($("#cardNumber_"+cardId).html());
		$("#exp_date").val($("#cardExpiry_"+cardId).html());
		$("#cvv").val($("#CardCVV_"+cardId).html());
		$("#zip").val($("#CardZIP_"+cardId).html());
	});
	
});
</script>
@stop