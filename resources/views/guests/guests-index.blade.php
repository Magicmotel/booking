@extends('layouts.layout')

@section('title')
	All Guests
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Guests</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                    	<th class="column-title">No</th>
                                        <th class="column-title">Guest Name</th>
                                        <th class="column-title no-link last"><span class="nobr">Action</span></th>
                                    </tr>
                                </thead>
                            
                                <tbody>
                                	@foreach($Results as $vals)
                                    	<tr class="even pointer">
                                        	<td class=" ">{{ ++$i }}</td>
                                            <td class=" ">{!!$vals->GuestFname!!}{!!($vals->GuestMname)?" $vals->GuestMname":""!!}{!!" $vals->GuestLname"!!}</td>
                                            <td class=" last">
                                                <a href="{!!url('guests', $vals->GuestId)!!}/edit" class="left btn btn-info buTTonResize"><i class="fa fa-pencil"></i> Edit</a>
                                                <a href="{!!url('guests', $vals->GuestId)!!}" class="left btn btn-primary buTTonResize marginLeft5"><i class="fa fa-folder"></i> View</a>
                                                <?php /*?>
												
												<form action="{!!url('users', $vals->GuestId)!!}" method="post" style="float:left;width:auto; margin:0;" onsubmit="return ConfirmDelete()">
                                                {!!method_field('DELETE')!!}
                                                {!!csrf_field()!!}
                                                {!!Form::submit('Delete', ['class'=>'deleteButton marginLeft10'])!!}
                                                </form><?php */?>
                                          	</td>
                                        </tr>
                                   	@endforeach
                                </tbody>
                            </table>
                            {!! $Results->render() !!}
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function ConfirmDelete()
	{
		var x = confirm("Are you sure you want to delete this User?");
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop      
