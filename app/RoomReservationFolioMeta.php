<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomReservationFolioMeta extends Model
{
	protected $fillable= [
		'hotel_id', 'room_reservation_id', 'folio_type', 'folio_date', 'folio_pay_type', 'folio_amount',
		'folio_comment', 'folio_auto', 'folio_order', 'folio_from', 'log_id', 'reg_ip', 'added_by'
	];
	protected $primaryKey = 'folio_id';
	protected $table = 'room_reservation_folio_meta';
}
