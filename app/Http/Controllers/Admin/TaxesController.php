<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Taxes;
use Helper;
use DB;

class TaxesController extends Controller
{
    public function index(Request $request)
    {
		$Hotel_ID = Helper::getHotelId();
		$Results  = Taxes::where('hotel_id', $Hotel_ID)->orderBy('status', 'ASC')->get(['tax_id', 'tax_type', 'tax_value', 'tax_value_type', 'tax_start', 'tax_end', 'status']);
		return view('rooms.taxes')->with("Results", $Results);
	}

    public function store(Request $request)
    {
		$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		$unique_ID = Helper::getUniqueId();
		
		foreach($request->tax_type as $key=>$vals)
		{
			$TaxId = $request->tax_id[$key];
			
			if($request->tax_id[$key])
			{
				$Results = Taxes::where([ ['hotel_id', $Hotel_ID], ['tax_id', $TaxId] ])->first();
				if($vals){
					$Results->tax_id_unq = $unique_ID;
					$Results->hotel_id = $Hotel_ID;
					$Results->tax_type = $vals;
					$Results->tax_value = $request->tax_value[$key];
					$Results->tax_value_type = $request->tax_value_type[$key];
					$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
					$Results->added_by = $admin_ID;
					$Results->save();
				}else{
					Taxes::destroy($request->tax_id[$key]);
				}
			}else{
				if($vals){
					$RequestArray = array();	
					$RequestArray[] = $unique_ID;
					$RequestArray[] = $Hotel_ID;
					$RequestArray[] = $vals;
					$RequestArray[] = $request->tax_value[$key];
					$RequestArray[] = $request->tax_value_type[$key];
					$RequestArray[] = $_SERVER['REMOTE_ADDR'];
					$RequestArray[] = $admin_ID;
					DB::select('CALL Taxes_Insert_SP (?,?,?,?,?,?,?)', $RequestArray);
				}
			}
		}
		
		return redirect('room/taxes')->with('success','Taxes added successfully.');
    }
}