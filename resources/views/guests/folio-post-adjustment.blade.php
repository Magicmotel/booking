@extends('layouts.layout')

@section('title')
	Post Adjustment
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Post Adjustment</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <form id="demo-form2" action="{!!url('guest-folio/post-adjustment')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                    	{!!csrf_field()!!}
                    	<input type="hidden" name="rsrv_id" id="rsrv_id" value="{{$RsrvRst->room_reservation_id}}" />
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Charge Type <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3">
                            <select name="folio_pay_type" id="folio_pay_type" class="form-control col-md-3 col-xs-12 zeroPadd chargeSwitch" required>
                                <option value="">Select Charge Type</option>
                                <option value="1">Room Charge</option>
                                <option value="11">Pet Charge</option>
                                <option value="12">Rollaway Bed</option>
                                <option value="13">Damage Charge</option>
                                <option value="15">Other Charge</option>
                            </select>
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Charge Date <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="folio_date" id="folio_date" value="" placeholder="mm/dd/yyyy" class="date-picker form-control col-md-7 col-xs-12" required="required" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Comments <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6">
                            <input type="text" name="folio_comment" id="folio_comment" value="" placeholder="Enter Comments" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Amount <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6">
                                <span class="input-group-btn fltL widthAuto"><div class="btn btn-primary">$</div></span>
                                <input type="text" name="folio_amount" id="folio_amount" value="" placeholder="Enter Amount" class="form-control width40" required="required" disabled="disabled" min="0.1" maxlength="3">
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop20">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Post Adjustment</button>
                                <button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                                <a href="{{url('guest-folio', $RsrvRst->room_reservation_id)}}" class="btn btn-default"><i class="fa fa-chevron-left"></i> Back</a>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
@stop  

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var folio_date = $('#folio_date').datepicker({
		startDate: moment()
	}).on('changeDate', function(ev){
		folio_date.hide();
		getAdjustmentRate();
	}).data('datepicker');
	
	$('.chargeSwitch').on('change', function(ev){
		getAdjustmentRate()
	})
	
	function getAdjustmentRate(){
		var folio_pay_type = $('#folio_pay_type').val();
		var folio_date = $('#folio_date').val();
		var rsrv_id = $('#rsrv_id').val();
		if(folio_pay_type && folio_date)
		{
			$("#main_container_Loading").show();
			$("#main_container_overlay").show();
			$.ajax({
				url: "{{ url('/adjustAmount') }}",
				type: 'POST',
				data:{"rsrv_id":rsrv_id, "folio_pay_type":folio_pay_type, "folio_date":folio_date, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					if ( msg.status === 'success' ) {
						$("#folio_comment").val("You can adjust max $ "+msg.response['folio_amount']);
						$("#folio_amount").prop("disabled", false);
						$("#folio_amount").attr("max", msg.response['folio_amount']);
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
					}
					if ( msg.status === 'error' ) {
						$("#folio_comment").val("Room Charge not found for "+folio_date);
						$("#folio_amount").prop("disabled", true);
						$("#folio_amount").val('');
						$("#folio_amount").attr("max", 0);
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.error("Room Charge not found for "+folio_date);
					}
				},
				error: function( data ) {
					if ( data.status === 422 ) {
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.error('Cannot find the result');
					}
				}
			});
			return false;
		}
	}
});
</script>
@stop      
