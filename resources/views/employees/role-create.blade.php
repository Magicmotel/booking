@extends('layouts.layout')

@section('title')
	Create Admin User
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Create Admin User</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	@if (count($errors))
                <ul class="errorFormMessage">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <form id="demo-form2" action="{!!url('users')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
               		{!!csrf_field()!!}
                    <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_type">Name <span class="required">*</span></label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            	<input type="text" name="fname" value="{!!old('fname')!!}" id="fname" class="form-control col-md-7 col-xs-12" placeholder="First Name" required="required">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            	<input type="text" name="mname" value="{!!old('mname')!!}" id="mname" class="form-control col-md-7 col-xs-12" placeholder="Middle Name">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            	<input type="text" name="lname" value="{!!old('lname')!!}" id="lname" class="form-control col-md-7 col-xs-12" placeholder="Last Name" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_count">Email <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<input type="email" name="email" value="{!!old('email')!!}" id="email" class="form-control col-md-7 col-xs-12" placeholder="Email Address" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="guest_adult" class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<input type="password" name="password" value="" id="password" class="form-control col-md-7 col-xs-12" placeholder="Password" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="guest_adult" class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<input type="password" name="password_confirmation" value="" id="password_confirmation" class="form-control col-md-7 col-xs-12" placeholder="Confirm Password" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="room_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Role <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<select name="role_id" id="role_id" class="form-control col-md-3 col-xs-6" required="required">
                                	<option value="">Select Role</option>
                                    @foreach( App\Role::all(['id as RoleId', 'name as RoleType']) as $roles )
                                    <option value="{!!$roles->RoleId!!}"{!!(old('role_id') == $roles->RoleId)?" Selected":"";!!}>{!!$roles->RoleType!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="room_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Shift <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<select name="shift_id" id="shift_id" class="form-control col-md-3 col-xs-6" required="required">
                                	<option value="">Select Shift</option>
                                    @foreach( App\Shift::all(['shift_id as ShiftId', 'shift_type as ShiftType']) as $vals )
                                    <option value="{!!$vals->ShiftId!!}"{!!(old('shift_id') == $vals->ShiftId)?" Selected":"";!!}>{!!$vals->ShiftType!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    	<div class="form-group form-group-last">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">ADD</button>
                                <button type="reset" class="btn btn-primary">RESET</button>
                            </div>
                    	</div>                    
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop