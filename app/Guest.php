<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
	protected $fillable= [
		'guest_id', 'guest_id_unq', 'guest_title', 'guest_fname', 'guest_mname', 'guest_lname',
		'guest_address', 'guest_city', 'guest_state', 'guest_zip', 'guest_email', 'guest_phone',
		'guest_dob', 'guest_nation', 'guest_idproof', 'guest_proofno', 'guest_company', 'guest_group',
		'guest_vehicle', 'guest_tax', 'reg_ip', 'added_by',
		'bad_status', 'bad_reason', 'bad_created_at', 'bad_reg_ip', 'bad_addedby'
	];
	protected $primaryKey = 'guest_id';
	protected $table = 'guest';
}
