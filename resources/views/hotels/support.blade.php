@extends('layouts.layout')

@section('title')
	Help Ticket
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Help Ticket</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12">
            	<div class="x_panel">
                    <div class="x_content">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @elseif ($message = Session::get('danger'))
                            <div class="alert alert-danger">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="form-horizontal form-label-left">
                            <div class="fltL marginBottom10 width100">In order for the tickets to be resolved in a timely manner.</div>
                            <form id="demo-form2" action="{!!url('support', $Hotel_ID)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                                {!!csrf_field()!!}
                                <div class="form-group">
                                    <label>Enter Message</label>
                                    <textarea name="support_body" id="support_body" class="form-control" placeholder="How can we help?" required></textarea>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group form-group-last marginTop10">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Send</button>
                                    <button type="reset" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>
              	</div>
            </div>
        </div>
        @if($supportNum)
        <div class="row">
            <div class="col-md-12">
            	<div class="x_panel">
                    <div class="x_content">
                        @foreach($supportRst as $supportVal)
                        <div class="dg_color_backup {{($supportVal->support_from == 1)?'dg_white_backup':'dg_grey_backup'}}">
                            <div class="fltL width100">{{$supportVal->support_body}}</div>
                            <div class="fltL width100 supportBy">{{($supportVal->support_from == 1)?$supportVal->S_fname." ".$supportVal->S_lname:$supportVal->R_fname." ".$supportVal->R_lname}}, {{date('jS F Y H:i:s', strtotime($supportVal->created_at))}}</div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
      	</div>
        @endif
    </div>
</div>
@endsection