<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\RoomAssign;
use App\BlockRoom;
use Helper;
use DB;

class BlockRoomController extends Controller
{
	public function index(Request $request)
    {
        $Hotel_ID  = Helper::getHotelId();
		
		$Results = DB::table('block_rooms as BR')
					 ->select('RA.room_assign_id as RoomId', 'RA.room_number as RoomNumber',
							  'RT.room_smoking_type as SmokeType', 'RT.room_bed_size as BedSize', 'RT.room_type as RoomTypes',
							  'BR.block_from as BlockFrom', 'BR.block_to as BlockTO', 'BR.block_room_id as BlockRoomId')
					 ->join('room_assign as RA', 'RA.room_assign_id', '=', 'BR.room_assign_id')
				  	 ->join('roomtypes as RT', 'RT.room_type_id', '=', 'RA.room_type_id')
					 ->where('RT.hotel_id', $Hotel_ID)->where('RA.hotel_id', $Hotel_ID)->where('BR.hotel_id', $Hotel_ID)
					 ->where('RT.trash', '0')->where('RA.trash', '0')
					 ->orderBy('BR.block_from', 'ASC')->orderBy('RA.room_number', 'ASC')
					 ->paginate(10);
		return view('rooms.block-room')->with('Hotel_ID', $Hotel_ID)->with("Results", $Results);
    }

    public function create(Request $request)
    {	
		$Hotel_ID = Helper::getHotelId();
        return view('rooms.block-room-create')->with('Hotel_ID', $Hotel_ID)->with("request", $request);
    }

    public function store(Request $request)
    {
		$Hotel_ID = Helper::getHotelId();
		$admin_ID = Helper::getAdminUserId();
		if($Hotel_ID == $request->hotel_id)
		{
			try
			{
				DB::beginTransaction();
				
				$RoomTypeId = $request->room_type_id;
				$BlockFrom  = date("Y-m-d", strtotime($request->block_from));
				$BlockTo    = date("Y-m-d", strtotime($request->block_to."+1 days"));
				foreach($request->room_assign_id as $Vals)
				{
					DB::select("DELETE FROM block_rooms WHERE room_assign_id = ".$Vals." AND NOT ((block_to <= '".$BlockFrom."') OR (block_from >= '".$BlockTo."'))");
					
					$RequestArray = array();	
					$RequestArray[] = $Hotel_ID;
					$RequestArray[] = $RoomTypeId;
					$RequestArray[] = $Vals;
					$RequestArray[] = $BlockFrom;
					$RequestArray[] = $BlockTo;
					$RequestArray[] = $_SERVER['REMOTE_ADDR'];
					$RequestArray[] = $admin_ID;
					
					DB::select('CALL BlockRoom_Insert_SP(?,?,?,?,?,?,?)', $RequestArray);
				}
				DB::commit();
			} catch (\Exception $e) {
				DB::rollback();
			}
			$killResult = DB::select("SELECT MIN(block_from) as FromDate, MAX(block_to) as ToDate FROM block_rooms
									  WHERE room_type_id = ".$RoomTypeId." AND NOT ((block_to <= '".$BlockFrom."') OR (block_from >= '".$BlockTo."')) GROUP BY room_type_id");
			if(count($killResult))
			{
				foreach($killResult as $killVal)
				{
					$dateFrom = $killVal->FromDate;
					$dateTo   = $killVal->ToDate;
					Helper::ChannelPartnerRequest($Hotel_ID, $RoomTypeId, $dateFrom, $dateTo);
				}
			}
			return redirect('block-room')->with('success','Room blocked successfully');
		}else{
			return redirect('block-room')->with("danger", "Please select your Motel");
		}
    }

    public function destroy(Request $request, $id)
    {
		$Hotel_ID  = Helper::getHotelId();
		$Results = BlockRoom::where([ ['hotel_id', $Hotel_ID], ['block_room_id', $id] ])->first();
		if(count($Results))
		{
			$ArrivalDate   = $Results->block_from;
			$DepartureDate = $Results->block_to;
			$RoomTypeId    = $Results->room_type_id;
		
			$Result = BlockRoom::destroy($id);
			
			Helper::ChannelPartnerRequest($Hotel_ID, $RoomTypeId, $ArrivalDate, $DepartureDate);
			
			return redirect('block-room')->with('success','Room unblocked successfully');
		}else{
			return redirect('block-room')->with("danger", "Please select your Motel");
		}
	}
	
	public function getRoomListBlock(Request $request)
    {
       	$Hotel_ID  = Helper::getHotelId();
		if($request->ajax())
		{
			$RoomType  = $request->RoomType;
			$BlockFrom = $request->BlockFrom;
			$BlockTo   = $request->BlockTo;
			
			$rquestFromDate = date('Y-m-d', strtotime($BlockFrom));
			$rquestToDate   = date('Y-m-d', strtotime($BlockTo.'+1 days'));
			
			$room_assignArray = array();
			$RoomListQry = DB::select("SELECT room_assign_id FROM reservation_room_allot WHERE room_reservation_id IN(
										   SELECT RSV.room_reservation_id as TotBook FROM room_reservation as RSV
										   WHERE RSV.room_type_id = ".$RoomType." AND RSV.status != 'Cancelled' AND
										   NOT ((RSV.departure <= '".$rquestFromDate."') OR (RSV.arrival >= '".$rquestToDate."'))
										   GROUP BY RSV.room_type_id
										)
									  ");
			$TotalRsrv = count($RoomListQry);
			$PSJ = 0;
			foreach($RoomListQry as $RoomListRst){
				$room_assignArray[] = $RoomListRst->room_assign_id;
				if($RoomListRst->room_assign_id == 0){
					$PSJ++;
				}
			}
			$room_assignIds = implode(",", $room_assignArray);
			$QryVar = "";
			if($room_assignIds){
				$QryVar = " AND R.room_assign_id NOT IN (".$room_assignIds.")";
			}
			
        	$Results = DB::select("SELECT R.room_assign_id as RoomId, R.room_number as RoomNumber, R.room_stat as RoomCondition,
								   CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType, RT.room_type as RoomTypes,
								   ( SELECT GROUP_CONCAT(CONCAT(DATE_FORMAT(BR.block_from, '%m/%d/%Y'),' - ',DATE_FORMAT(BR.block_to, '%m/%d/%Y')) SEPARATOR ', ')
								   	 FROM block_rooms as BR WHERE BR.room_assign_id = R.room_assign_id
									 AND NOT ((BR.block_to <= '".$rquestFromDate."') OR (BR.block_from >= '".$rquestToDate."')) GROUP BY BR.room_assign_id
								   ) as ExistBlock
								   FROM room_assign as R INNER JOIN roomtypes as RT ON RT.room_type_id = R.room_type_id
								   WHERE R.hotel_id = ".$Hotel_ID." AND R.trash = 0 AND R.status = 1 AND R.room_type_id = ".$RoomType.$QryVar."
								   AND R.room_assign_id NOT IN(
								   	 SELECT room_assign_id FROM block_rooms
									 WHERE hotel_id = ".$Hotel_ID." AND block_from = '".$rquestFromDate."' AND block_to = '".$rquestToDate."'
								   )
								   ORDER BY RoomCondition DESC, RoomNumber ASC
								  ");
			$TotalRoom = count($Results);
			if(count($Results)){
				return response()->json(['response' => view('rooms.block-room-list')->with("Results", $Results)->with("PSJ", $TotalRoom-$PSJ)->render(), 'status' => 'success']);
			}else{
				return response()->json(['response' => 'Result not Found', 'status' => 'error']);
			}
		}
		else
		{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
}
