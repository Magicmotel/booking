<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Hotel;
use App\RoomReservation;
use App\CompanyProfile;
use App\RoomReservationFolio;
use App\RoomReservationFolioMeta;
use App\RoomReservationFolioExempt;
use Helper;
use DB;

class AutoFolioUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'folioupdate:auto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto Folio Rate Update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$toDay = date("Y-m-d");
		$folio_Date = date('Y-m-d', strtotime($toDay.'-1 days'));
		
		$HotelRst = Hotel::where('status', 1)->get(['hotel_id']);
		foreach($HotelRst as $HotelVal)
		{
			$Hotel_ID = $HotelVal->hotel_id;
			
			$FolioRst = RoomReservationFolio::where([ ['hotel_id', $Hotel_ID], ['folio_date', $folio_Date], ['folio_pay_type', 'Room Charge'] ])->get();
			$PSJ = 0;
			foreach($FolioRst as $FolioVal)
			{
				$RsrvId  	  = $FolioVal->room_reservation_id;
				$FolioMetaRst = RoomReservationFolioMeta::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $RsrvId], ['folio_date', $folio_Date],
																  ['folio_pay_type', 'Room Charge'], ['folio_auto', 1] ])->get();
				if(!count($FolioMetaRst))
				{	
					$RsrvRst = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $RsrvId] ])->first();
					$rsrvTaxBool = $RsrvRst->reservation_tax_exmpt;

					$ChargeAmount  = $ChargeData = 0;
					$ChargeRsrvId  = $ChargeType = $ChargeComment = $ChargeOrder = "";
					$FolioRstAgain = RoomReservationFolio::where([ ['hotel_id', $Hotel_ID], ['folio_date', $folio_Date], ['room_reservation_id', $RsrvId],
																   ['folio_pay_type', 'Room Charge'], ['folio_order', 1] ])->first();
					$ChargeRsrvId = $FolioRstAgain->room_reservation_id;
					$ChargeDate   = $FolioRstAgain->folio_date;
					$ChargeType   = $FolioRstAgain->folio_pay_type;
					$ChargeAmount = $FolioRstAgain->folio_amount;
					$ChargeData   = $FolioRstAgain->folio_data;
					$ChargeComment= $FolioRstAgain->folio_comment;
					$ChargeOrder  = $FolioRstAgain->folio_order;
					
					$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
					$RoomReservationFolioMetaOBJ->hotel_id 		= $Hotel_ID;
					$RoomReservationFolioMetaOBJ->room_reservation_id = $ChargeRsrvId;
					$RoomReservationFolioMetaOBJ->folio_type 	= 1;
					$RoomReservationFolioMetaOBJ->folio_date 	= $ChargeDate;
					$RoomReservationFolioMetaOBJ->folio_pay_type= 'Room Charge';
					$RoomReservationFolioMetaOBJ->folio_amount  = $ChargeAmount;
					$RoomReservationFolioMetaOBJ->folio_data    = $ChargeData;
					$RoomReservationFolioMetaOBJ->folio_comment = $ChargeComment;
					$RoomReservationFolioMetaOBJ->folio_auto	= 0;
					$RoomReservationFolioMetaOBJ->folio_order 	= $ChargeOrder;
					$RoomReservationFolioMetaOBJ->folio_from	= 1;
					$RoomReservationFolioMetaOBJ->save();
					
					if($rsrvTaxBool == 1)
					{
						$TaxExemptFolio = new RoomReservationFolioExempt;
						$TaxExemptFolio->hotel_id 		= $Hotel_ID;
						$TaxExemptFolio->room_reservation_id = $ChargeRsrvId;
						$TaxExemptFolio->folio_type 	= 1;
						$TaxExemptFolio->folio_date 	= $ChargeDate;
						$TaxExemptFolio->folio_pay_type = 'Room Charge';
						$TaxExemptFolio->folio_amount 	= $ChargeAmount;
						$TaxExemptFolio->folio_data 	= $ChargeData;
						$TaxExemptFolio->folio_order 	= $ChargeOrder;
						$TaxExemptFolio->folio_from 	= 1;
						$TaxExemptFolio->save();
					}
					
					$folioTaxQry = DB::select("SELECT tax_type as fTaxType, tax_value as fTaxValue, tax_value_type as fValueType
											   FROM taxes WHERE hotel_id = ".$Hotel_ID." AND status = 0 AND
											   NOT( (tax_end != '0000-00-00' AND tax_end < '".$ChargeDate."') OR
													(tax_start != '0000-00-00' AND tax_start >= '".$ChargeDate."') OR
													(tax_end = '0000-00-00' AND tax_start > '".$ChargeDate."'AND tax_start < '".$ChargeDate."') OR
													(tax_start = '0000-00-00' AND tax_end < '".$ChargeDate."' AND tax_end > '".$ChargeDate."')
												  )
											  ");
					$SJ = 2;
					foreach($folioTaxQry as $folioTaxRst)
					{
						$folio_Data = "";
						$dayTaxAmount = "";
						if($folioTaxRst->fValueType == 1)
						{
							$dayTaxAmount = round((($ChargeAmount*$folioTaxRst->fTaxValue)/100), 2);
							$folio_Data = $folioTaxRst->fTaxValue."%";
						}
						elseif($folioTaxRst->fValueType == 2)
						{
							$dayTaxAmount = $dayTaxAmount+( round($folioTaxRst->fTaxValue, 2) );
							$folio_Data = $folioTaxRst->fTaxValue;
						}
						
						if($rsrvTaxBool == 0)
						{	
							$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
							$RoomReservationFolioMetaOBJ->hotel_id 		= $Hotel_ID;
							$RoomReservationFolioMetaOBJ->room_reservation_id = $RsrvRst->room_reservation_id;
							$RoomReservationFolioMetaOBJ->folio_type 	= 1;
							$RoomReservationFolioMetaOBJ->folio_date 	= $ChargeDate;
							$RoomReservationFolioMetaOBJ->folio_pay_type= $folioTaxRst->fTaxType;
							$RoomReservationFolioMetaOBJ->folio_amount 	= $dayTaxAmount;
							$RoomReservationFolioMetaOBJ->folio_data 	= $folio_Data;
							$RoomReservationFolioMetaOBJ->folio_comment = "";
							$RoomReservationFolioMetaOBJ->folio_auto 	= 0;
							$RoomReservationFolioMetaOBJ->folio_order 	= $SJ;
							$RoomReservationFolioMetaOBJ->folio_from	= 1;
							$RoomReservationFolioMetaOBJ->save();
						}
						else
						{
							$TaxExemptFolio = new RoomReservationFolioExempt;
							$TaxExemptFolio->hotel_id 		= $Hotel_ID;
							$TaxExemptFolio->room_reservation_id = $RsrvRst->room_reservation_id;
							$TaxExemptFolio->folio_type 	= 1;
							$TaxExemptFolio->folio_date 	= $ChargeDate;
							$TaxExemptFolio->folio_pay_type = $folioTaxRst->fTaxType;
							$TaxExemptFolio->folio_amount 	= $dayTaxAmount;
							$TaxExemptFolio->folio_data 	= $folio_Data;
							$TaxExemptFolio->folio_order 	= $SJ;
							$TaxExemptFolio->folio_from 	= 1;
							$TaxExemptFolio->save();
						}
						$SJ++;
					}
				}
			}
		}
    }
}
