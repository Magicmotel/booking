<?php

/**
 * This file contains functions related to create,update, view and delete
 * for Tags endpoints
 * 
 * Tags class
 * 
 */

namespace App\Zeamsters\lib;

//include_once BASE_PATH . DS . 'includes/Logging.php';

use App\Zeamsters\lib\ZeamsterPaymentTransaction as zeamsterApi;
use Exception;
use App\Zeamsters\includes\Logging;

class Tags extends Exception {

    public $id = "";
    public $created_ts = "";

    /**
     * @var string $location_id(Required) Zeamster API to use for requests.
     */
    public $location_id = "";
    public $modified_ts = "";

    /**
     * @var string $title(Required) Zeamster API to use for requests.
     */
    public $title = "";

    /**
     * Create new record.
     * 
     * @params object $objData  
     *  
     * @return json
     */
    public static function createTags($objData) {
        try {

            $request_data = zeamsterApi::convertObjectToArray($objData);
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . '/tags';

            if (!empty($request_data)) {
                $method = 'POST';
                if (!empty($request_data)) {
                    $request = array('tag' => array_filter($request_data));
                }
                $request = json_encode($request);
                $response = $objzeamsterApi::send_transaction($request_url, $method, null, $request);

                return $response;
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Get Tags Details.
     * 
     * @params string $tag_id  
     * 
     * @return json
     */
    public static function getTagsDetails($tag_id = null, $filter = null, $perPage = null, $sort = null, $pageNo = null) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            if (!empty($tag_id)) {
                $request_url = $api_url . "/tags/{$tag_id}";
            } else {
                if (!empty($perPage) || !empty($filter) || !empty($sort) || !empty($pageNo) || !empty($pageNo)) {
                    $request_url = $api_url . "/tags?{$filter}&page_size={$perPage}&sort={$sort}&page={$pageNo}";
                } else {
                    $request_url = $api_url . "/tags";
                }
            }

            $method = 'GET';
            $response = $objzeamsterApi::send_transaction($request_url, $method, null, null);

            return $response;
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Update Tags.
     * 
     * @params object $objData 
     * @params string $tag_id 
     *  
     * @return json
     */
    public static function updateTags($objData, $tag_id) {
        try {

            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/tags/{$tag_id}";
            $method = 'PUT';
            $request_data = zeamsterApi::convertObjectToArray($objData);
            if (!empty($request_data)) {
                $request = array('tag' => array_filter($request_data));
                $request = json_encode($request);
                $response = $objzeamsterApi::send_transaction($request_url, $method, null, $request);

                return $response;
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Delete Tag record
     * 
     * @params string $tag_id  
     * 
     * @return json
     */
    public static function deleteTags($tag_id) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/tags/{$tag_id}";
            $method = 'DELETE';
            $response = $objzeamsterApi::send_transaction($request_url, $method);

            return $response;
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

}
