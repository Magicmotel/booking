@extends('layouts.layout')

@section('title')
	Checked Out Guests : {{date('F j, Y', strtotime($searchDate))}}
@stop

@section('CascadingSheet')
    <!-- Datatables -->
    {!!Html::style('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-dragtable/css/dragtable.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Checked Out Guests : {{date('F j, Y', strtotime($searchDate))}}</h3></div>
            <div class="col-md-2 col-sm-2 col-xs-2 fltL zeroRightPadd marginTop5 marginBottom5" id="calenderBox">
            <input type="text" name="search_date" id="search_date" value="{{$searchDate}}" placeholder="mm/dd/yyyy" class="date-picker form-control col-md-7 col-xs-12  has-feedback-left" readonly="readonly">
            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <?	//table table-striped table-bordered dt-responsive nowrap sar-table   cellspacing="0" width="100%"> ?>
                            <table id="datatable-responsive" class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th id="inHouse_1"><div class="some-handle">Name</div></th>
                                    <th id="inHouse_2"><div class="some-handle">City</div></th>
                                    <th id="inHouse_4"><div class="some-handle">Room#</div></th>
                                    <th id="inHouse_5"><div class="some-handle">Arrival</div></th>
                                    <th id="inHouse_6"><div class="some-handle">Departure</div></th>
                                    <th id="inHouse_7"><div class="some-handle">Type</div></th>
                                    <th id="inHouse_8"><div class="some-handle">Condition</div></th>
                                    <th id="inHouse_9"><div class="some-handle">Reservation#</div></th>
                                    <th id="inHouse_10"><div class="some-handle">Action</div></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Results as $vals)
                                <tr>
                                    <td class="uPPerLetter">
                                    	<a href="{{url('guest-info', $vals->RsrvId)}}" class="btn-list-link">{{$vals->GuestLname}}, {{$vals->GuestFname}}</a>
                                        @if($vals->GuestBadStatus == 1)
                                    	<i class="fa fa-user activeGuest"></i>
                                        @else
                                        <i class="fa fa-user-times badGuest"></i>
                                        @endif
                                  	</td>
                                    <td>{{$vals->GuestCity}}</td>
                                    <td>{{$vals->RoomNumber}}</td>
                                  	<td>{{$vals->ArvlDate}}</td>
                                  	<td>{{$vals->DprtDate}}</td>
                                  	<td>{{$vals->RoomTypes}}</td>
                                  	<td>
                                    	@if($vals->RoomNumber)
                                    	{{($vals->RoomStat==1)?"Clean":"Dirty"}}
                                   		@endif
                                	</td>
                                    <td>{{$vals->RsrvId}}</td>
                                    <td>
                                    	@if($vals->GuestBadStatus==1)
                                    	<a href="{!!url('bad-mark', $vals->GuestId)!!}" class="left btn btn-default btn-xs buTTonResize" onclick="return ConfirmAction();" title="mark as Bad Guest"><i class="fa fa-user-times badGuest"></i> Guest</a></td>
                                        @else
                                        <a href="{!!url('remove-bad-guest', $vals->GuestId)!!}" class="left btn btn-default btn-xs buTTonResize" onclick="return rConfirmAction();" title="Unmark as Bad Guest"><i class="fa fa-user activeGuest"></i> Guest</a></td>
                                        @endif                                        
                                </tr>
                                @endforeach
                                </tbody>
                           	</table>
                            
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function ConfirmAction()
	{
		var x = confirm("Are you sure you mark as Bad Guest?");
		if (x)
			return true;
		else
			return false;
	}
	function rConfirmAction()
	{
		var x = confirm("Are you sure you un-mark as Bad Guest?");
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop     

@section('JavascriptSRC')
    <!-- Datatables -->
    {!!Html::script('vendors/datatables.net-dragtable/js/jquery.dragtable.js')!!}
    {!!Html::script('vendors/datatables.net/js/jquery.dataTables.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')!!}
    {!!Html::script('vendors/jszip/dist/jszip.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/pdfmake.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/vfs_fonts.js')!!} 
@stop  

@section('jQuery')
<script type="text/javascript">
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	$('#search_date').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() > now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		var newDate = new Date(ev.date).toString("MM/dd/yyyy");
		window.location.href="{!!url('checked-out-guests?search_date=')!!}"+newDate;
	}).data('datepicker');
	
	$('#datatable-responsive').DataTable({
		"aaSorting": [[4, 'asc']],
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bPaginate": false,			
		"columnDefs": [ { orderable: false, targets: -1 } ]
	});
	$('#datatable-responsive').dragtable({
		dragHandle:'.some-handle',
		persistState: function(table) {
			if (!window.sessionStorage) return;
			var ss = window.sessionStorage;
			table.el.find('th').each(function(i) {
				if(this.id != '') {table.sortOrder[this.id]=i;}
			});
			ss.setItem('tableorder',JSON.stringify(table.sortOrder));
		},
		restoreState: eval('(' + window.sessionStorage.getItem('tableorder') + ')')
	});
});
</script>
@stop