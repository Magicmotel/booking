@extends('layouts.layout')

@section('title')
	Expedia Room & Rate Plan Mappings
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Expedia Room & Rate Plan Mappings</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-8 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @elseif ($message = Session::get('danger'))
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <form id="demo-form2" action="{!!url('expedia-channel-mapping')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
               			{!!csrf_field()!!}
                        <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                        <div class="form-group">
                            <div class="control-value col-md-3 col-sm-6 col-xs-12 col-md-offset-5">
                            	Expedia Room Type ID
                          	</div>
                            <div class="control-value col-md-3 col-sm-6 col-xs-12 marginLeft10">
                              	Expedia Rate Plan Id
                            </div>
                        </div>
                        @foreach($roomRst as $key => $val)
                        	{{--*/
                            $mapRst = App\ChannelMapping::where([ ['hotel_id', $Hotel_ID], ['channel_id', $Channel_ID], ['room_type_id', $val->RoomTypeId] ])->first();
                            /*--}}
                        <div class="form-group">
                            <input type="hidden" name="map_id[]" id="map_id[]" value="{{isset($mapRst->map_id)?$mapRst->map_id:''}}">
                            <input type="hidden" name="room_type_id[]" id="room_type_id[]" value="{!!$val->RoomTypeId or ''!!}">
                        	<label class="control-label col-md-5 col-sm-4 col-xs-12 UpperLetter">({!!$val->RoomCode!!}) &nbsp; {!!$val->RoomType!!} <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6 col-xs-12 zeroPadd">
                            	<input type="text" name="channel_room_type_id[]" value="{{isset($mapRst->channel_room_type_id)?$mapRst->channel_room_type_id:''}}" id="channel_room_type_id[]" class="form-control col-md-7 col-xs-12" placeholder="Expedia Room Type Id" required="required">
                          	</div>
                            <div class="col-md-3 col-sm-6 col-xs-12 zeroPadd marginLeft10">
                                <input type="text" name="channel_rate_plan_id[]" value="{{isset($mapRst->channel_rate_plan_id)?$mapRst->channel_rate_plan_id:''}}" id="channel_rate_plan_id[]" class="form-control col-md-7 col-xs-12" placeholder="Expedia Rate Plan Id" required="required">
                            </div>
                        </div>
                        @endforeach
                    	<div class="form-group form-group-last marginTop24">
                            <div class="col-md-7 col-sm-7 col-xs-4 col-md-offset-5 zeroPadd">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update Credential</button>
                                <button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    	{{--*/
                        	$PasLnth = strlen($loginRst->channel_password);
                            $PassVar = substr($loginRst->channel_password, 0, 2);
                            for($ps = 3; $ps <= $PasLnth; $ps++){
                            	$PassVar .= "*";
                            }
                        /*--}}
                        <div class="form-horizontal form-label-left">
                            <span class="section">CHANNEL CREDENTIALs:</span>
                            <div class="form-group">
                                <label class="control-label col-md-6 col-sm-4">Expedia Hotel Id:</label>
                                <div class="control-value col-md-6 col-sm-8">{!!$loginRst->channel_hotel_id!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-6 col-sm-4">Expedia Username:</label>
                                <div class="control-value col-md-6 col-sm-8">{!!$loginRst->channel_username!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-6 col-sm-4">Expedia Password:</label>
                                <div class="control-value col-md-6 col-sm-8">{!!$PassVar!!}</div>
                            </div>
                            <div class="form-group form-group-last marginTop24">
                                <div class="col-md-12 col-sm-8 col-xs-12 zeroPadd AlgnCenter">
                                    <a href="{!!url('expedia-channel/credential')!!}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i> Edit Credential</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script>
	function ConfirmAction(msg)
	{
		var x = confirm(msg);
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop      
