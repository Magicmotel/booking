@extends('layouts.layout')

@section('title')
	Payment Reversal Option
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Please select the type of payment you would like to reversal.</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-8 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="errorSummary"></div>
                        <form action="{!!url('payment-reversal/card')!!}" method="POST" id="myform" data-parsley-validate class="form-horizontal form-label-left">
                            {!!csrf_field()!!}
                            <input type="hidden" name="rsrv_id" id="rsrv_id" value="{{$rsrv_Id}}"/>
                            <input type="hidden" name="CashAmount" id="CashAmount" value="{{$CashAmount}}"/>
                            <input type="hidden" name="CheckAmount" id="CheckAmount" value="{{$CheckAmount}}"/>
                            <input type="hidden" name="CCAmount" id="CCAmount" value="{{$CCAmount}}"/>
                            <input type="hidden" name="CCAuthAmount" id="CCAuthAmount" value="{{$CCAuthAmount}}"/>
                            <input type="hidden" name="CompanyAmount" id="CompanyAmount" value="{{$CompanyAmount}}"/>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-3">Reservation#</label>
                                <div class="control-value col-md-8 col-sm-6 col-xs-12 LR_Padd5">
                                    {{$rsrv_Id}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-3">Refundable Amount</label>
                                <div class="control-value col-md-8 col-sm-6 col-xs-12 LR_Padd5">
                                    $ {{$rsrv_Rate}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-3">Payment Option</label>
                                <div class="col-md-8 col-sm-6 col-xs-12 LR_Padd5">
                                    @if($CashAmount>0)
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="fltL control-value zeroPadd">
                                            <input type="radio" name="payment_mode" id="payment_mode1" value="1" class="radioButtonCustom_Option" checked="checked"/>
                                            <div class="labelSwitchCheck">Cash ($ {{$CashAmount}})</div>
                                        </div>
                                    </div>
                                    @endif
                                    @if($CheckAmount>0)
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="fltL control-value zeroPadd">
                                            <input type="radio" name="payment_mode" id="payment_mode4" value="4" class="radioButtonCustom_Option"/>
                                            <div class="labelSwitchCheck">Check ($ {{$CheckAmount}})</div>
                                        </div>
                                    </div>
                                    @endif
                                    @if($CCAmount>0)
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="fltL control-value zeroPadd">
                                            <input type="radio" name="payment_mode" id="payment_mode2" value="2" class="radioButtonCustom_Option"/>
                                            <div class="labelSwitchCheck">Card Payment ($ {{$CCAmount}})</div>
                                        </div>
                                    </div>
                                    @endif
                                    @if($CCAuthAmount>0)
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="fltL control-value zeroPadd">
                                            <input type="radio" name="payment_mode" id="payment_mode3" value="3" class="radioButtonCustom_Option"/>
                                            <div class="labelSwitchCheck">CC Payment Pre Authorization ($ {{$CCAuthAmount}})</div>
                                        </div>
                                    </div>
                                    @endif
                                    @if($CompanyAmount>0 && $Profile_Id)
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="fltL control-value zeroPadd">
                                            <input type="radio" name="payment_mode" id="payment_mode5" value="5" class="radioButtonCustom_Option"/>
                                            <div class="labelSwitchCheck">Company Account ($ {{$CompanyAmount}})</div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4">Amount To Refund <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-8 col-xs-12 LR_Padd5">
                                    <span class="input-group-btn fltL widthAuto"><div class="btn btn-primary cursorInitial">$</div></span>
                                    <input type="text" name="folio_amount" id="folio_amount" value="" placeholder="Enter Amount" class="form-control width40" required="required" max="{{($rsrv_Rate>$CashAmount)?$CashAmount:$rsrv_Rate}}" min="0.1" maxlength="3">
                                </div>
                            </div>
                            <div class="form-group displayNone" id="checkOption">
                                <label class="control-label col-md-4 col-sm-3">Check Detail</label>
                                <div class="col-md-8 col-sm-8 col-xs-12 LR_Padd5">
                                    <div class="col-md-8 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <input type="text" name="bank_name" id="bank_name" placeholder="Name of Bank" value="" class="fltL form-control col-md-12 col-xs-12 checkOptionInput"/>
                                    </div>
                                    <div class="col-md-8 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <input type="text" name="holder_name" id="holder_name" placeholder="Account Holder Name" value="" class="fltL form-control col-md-12 col-xs-12 checkOptionInput"/>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-4 col-sm-3 col-xs-12 zeroPadd">
                                            <input type="text" name="check_number" id="check_number" placeholder="Check Number" value="" class="fltL form-control col-md-12 col-xs-12 checkOptionInput"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-4 col-sm-3 col-xs-12 zeroPadd">
                                            <input type="text" name="check_date" id="check_date" placeholder="Check Date" value="" class="fltL form-control has-feedback-left checkOptionInput" readonly="readonly"/>
                                            <span class="fa fa-calendar-o form-control-feedback left" style="left:0" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="commentOption">
                                <label class="control-label col-md-4 col-sm-3">Comment Box</label>
                                <div class="col-md-8 col-sm-8 col-xs-12 LR_Padd5">
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-8 col-sm-5 col-xs-12 zeroPadd">
                                            <textarea name="folio_comment" id="folio_comment" placeholder="Comment Box" class="fltL form-control col-md-12 col-xs-12" maxlength="70"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-last marginTop20">
                                <div class="col-md-6 col-sm-4 col-xs-4 col-md-offset-4 LR_Padd5">
                                	<button type="submit" class="btn btn-success"><i class="fa fa-check-circle-o"></i> Payment Reversal</button>
                                </div>
                            </div>
                        </form>
                   	</div>
               	</div>
          	</div>
           	<div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                            <label class="control-label col-md-6 col-sm-3 zeroMargin">Estimated Cost</label>
                            <div class="col-md-6 col-sm-8 col-xs-12 LR_Padd5">$ {{$esmt_Rate}}</div>
                        </div>
                    </div>
                </div>
        	</div>
            <div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                            <label class="control-label col-md-6 col-sm-3">Chargeable Amt.</label>
                            <div class="col-md-6 col-sm-8 col-xs-12 LR_Padd5">$ {{$pay_Rate}}</div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                            <label class="control-label col-md-6 col-sm-3">Received Amount</label>
                            <div class="col-md-6 col-sm-8 col-xs-12 LR_Padd5">$ {{($dpst_Rate>0)?$dpst_Rate:0}}</div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingTop5">
                            <label class="control-label col-md-6 col-sm-3 zeroMargin">Balance</label>
                            <div class="col-md-6 col-sm-8 col-xs-12 LR_Padd5">- $ {!!$rsrv_Rate!!}</div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script>
function disableBackButton()
{
	window.history.forward();
}
setTimeout("disableBackButton()", 0);
</script>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
@stop 

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var checkin = $('#check_date').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		checkin.hide();
	}).data('datepicker');
	
	$('input[class=radioButtonCustom_Option]').change( function() {
	   	var paymentType = $(this).val();
		if(paymentType == 1 || paymentType == 4 || paymentType == 5){
			$("#commentOption").show();
			$("#folio_comment").val("");
		}else{
			$("#commentOption").hide();
			$("#folio_comment").val("");
		}
		
		if(paymentType == 4){
			$("#checkOption").show();
			$("#amount_checkOption").hide();
			if('{{$rsrv_Rate}}' <= 0){
				$(".radioButtonCustom_EventOther").prop("checked", true);
				$("#rsrv_custom").prop("disabled", false);
				$("#rsrv_custom").prop("required", true);
				$("#rsrv_custom").val("");
			}
			$(".checkOptionInput").prop("required", true);
			$(".checkOptionInput").val("");
		}else{
			$("#checkOption").hide();
			$("#amount_checkOption").show();
			if('{{$rsrv_Rate}}' <= 0){
				$(".radioButtonCustom_EventMinimum").prop("checked", true);
				$("#rsrv_custom").prop("disabled", true);
				$("#rsrv_custom").prop("required", false);
				$("#rsrv_custom").val("");
			}
			$(".checkOptionInput").prop("required", false);
			$(".checkOptionInput").val("");
		}
	});
});
</script>
@stop