@extends('layouts.layout')

@section('title')
	Manage Shifts
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Shifts</h3></div>
        </div>
        
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
            	<li><a href="{{url('record-shifts')}}">Shifts Management</a></li>
                <li><a href="{{url('assign-shifts')}}">Assign Shifts</a></li>
                <li class="active"><a href="javascript:void(0);">Manage Shifts</a></li>
                <li><a href="{{url('shifts/create')}}">Create Shift</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @elseif ($message = Session::get('danger'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                    	<th>No</th>
                                    	<th>Name of Shift</th>
                                    	<th>Shift Timing</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($Results))
                                	@foreach($Results as $vals)
                                    <tr class="even pointer">
                                        <td>{{ ++$i }}</td>
                                        <td>{!!$vals->shift_name!!}</td>
                                        <td>{!!$vals->shift_from_time." ".$vals->shift_from_meridiem!!} - {!!$vals->shift_to_time." ".$vals->shift_to_meridiem!!}</td>
                                        <td>
                                            <a href="{!!url('shifts', $vals->shift_id)!!}/edit" class="left btn btn-info buTTonResize"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                            <form action="{!!url('shifts', $vals->shift_id)!!}" method="post" onsubmit="return ConfirmAction('Are you sure you want to delete this Shift?')" class="left">
                                            {!!method_field('DELETE')!!}
                                            {!!csrf_field()!!}
                                            {!!Form::hidden('hotel_id', $Hotel_ID)!!}
                                            {!!Form::button('<i class="fa fa-trash-o"></i> Delete', ['class'=>'left btn btn-danger buTTonResize marginLeft5', 'type'=>'submit'])!!}
                                            </form>
                                        </td>
                                    </tr>
                                   	@endforeach
                                @else
                                	<tr class="even pointer">
                                        <td colspan="4" class="errorMessageTR">No Shift Found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $Results->render() !!}
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function ConfirmAction(str)
	{
		var x = confirm(str);
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop      
