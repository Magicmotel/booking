@extends('layouts.layout')

@section('title')
	Create Room Type
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Create Room Type</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('room/assign-room')}}">Manage Room Numbers</a></li>
                <li><a href="{{url('room/room-type')}}">Manage Room Types</a></li>
                <li class="active"><a href="javascript:void(0);">Create Room Type</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                    	@if (count($errors))
                        <ul class="errorFormValidator">
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @elseif ($message = Session::get('danger'))
                            <div class="alert alert-danger">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <form id="demo-form2" action="{!!url('room/room-type')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                        {!!csrf_field()!!}
                        <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Room Type <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="room_type" id="room_type" class="form-control col-md-7 col-xs-12" placeholder="Room Type" required="required" maxlength="10" value="{!!old('room_type')!!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Display Name <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="room_display" id="room_display" class="form-control col-md-7 col-xs-12" placeholder="Display Name" required="required" value="{!!old('room_display')!!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">No. of Rooms <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="number" name="room_count" id="room_count" class="form-control width40" min="1" max="50" placeholder="No. of Rooms" required="required" maxlength="2" value="{!!old('room_count')!!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Max. Guests (Max. Occupancy) <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-3 col-xs-6">
                                    <input type="number" name="guest_adult" id="guest_adult" class="form-control col-md-3 col-xs-6" min="1" max="10" placeholder="Adult" required="required" maxlength="2" value="{!!old('guest_adult')!!}">
                                </div>
                                <div class="col-md-2 col-sm-3 col-xs-6">
                                    <input type="number" name="guest_child" id="guest_child" class="form-control col-md-3 col-xs-6" min="0" max="10" placeholder="Children" maxlength="2" value="{!!old('guest_child')!!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Non-Smoking / Smoking</label>
                                <div class="col-md-6 col-sm-6">
                                <div id="smokeType" class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default{!!(old('room_smoking_type')==2)?'':' active'!!}">
                                  <input type="radio" name="room_smoking_type" id="room_smoking_type1" class="smokeTypes" placeholder="" value="1"{!!(old('room_smoking_type')==2)?'':' checked'!!}> &nbsp; Non-Smoking &nbsp;
                                </label>
                                <label class="btn btn-default{!!(old('room_smoking_type')==2)?' active':''!!}">
                                  <input type="radio" name="room_smoking_type" id="room_smoking_type2" class="smokeTypes" placeholder="" value="2" {!!(old('room_smoking_type')==2)?' checked':''!!}> Smoking
                                </label>
                                </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Size of Bed <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="room_bed_size" id="room_bed_size" class="form-control col-md-3 col-xs-6" placeholder="Example: Q, K, QQ etc." required="required" value="{!!old('room_bed_size')!!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Default Rate <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <span class="input-group-btn fltL widthAuto"><div class="btn btn-primary cursorInitial">$</div></span>
                                    <input type="text" name="room_rate" id="room_rate" placeholder="Enter Default Rate" class="form-control width40 LR_Padd5" required="required" min="15" max="999.99" maxlength="6" value="{!!old('room_rate')!!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Image of Sample Room</label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                	<div id="input_room_pic_1" class="col-md-12 zeroPadd">
                                    	<input type="file" name="room_pic_1" id="room_pic_1" class="form-control col-md-3 col-xs-6">
                                        <span class="dg_btm_text">SIZE : 200 PX x 150 PX</span>
                                        <span class="dg_btm_text">JPG/JPEG, PNG, GIF & Max 2 MB</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div id="input_room_pic_2" class="col-md-12 zeroPadd">
                                    	<input type="file" name="room_pic_2" id="room_pic_2" class="form-control col-md-3 col-xs-6">
                                        <span class="dg_btm_text">SIZE : 200 PX x 150 PX</span>
                                        <span class="dg_btm_text">JPG/JPEG, PNG, GIF & Max 2 MB</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div id="input_room_pic_3" class="col-md-12 zeroPadd">
                                    	<input type="file" name="room_pic_3" id="room_pic_3" class="form-control col-md-3 col-xs-6">
                                        <span class="dg_btm_text">SIZE : 200 PX x 150 PX</span>
                                        <span class="dg_btm_text">JPG/JPEG, PNG, GIF & Max 2 MB</span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div id="input_room_pic_4" class="col-md-12 zeroPadd">
                                    	<input type="file" name="room_pic_4" id="room_pic_4" class="form-control col-md-3 col-xs-6">
                                        <span class="dg_btm_text">SIZE : 200 PX x 150 PX</span>
                                        <span class="dg_btm_text">JPG/JPEG, PNG, GIF & Max 2 MB</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <textarea id="description" name="description" rows="7" class="form-control ckeditor" placeholder="Description">{!!old('description')!!}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Facilities</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    @foreach( App\RoomtypeFacility::all(['rtf_name as FacilityName', 'rtf_key as FacilityKey']) as $val )
                                    <div class="col-md-3 col-sm-3 col-xs-6 zeroPadd TB_Padd5">
                                        <input type="checkbox" name="room_faclty[]" id="faclty_{!!$val->FacilityKey!!}" value="{!!$val->FacilityKey!!}" class="flat" />
                                        <span>{!!$val->FacilityName!!}</span>
                                    </div>
                                    @endforeach
                                    <br /><br />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Room Display Order <span class="required">*</span></label>
                                <div class="col-md-2 col-sm-2 col-xs-12">
                                    <select name="room_order" id="room_order" class="form-control col-md-7 col-xs-12" required="required">
                                    @for($PKJ = 1; $PKJ <= $totRslt+1 ; $PKJ++)	
                                        <option value="{{$PKJ}}"{!!(old('room_order')==$PKJ)?' selected':''!!}>{{$PKJ}}</option>
                                    @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group-last marginTop20">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-plus-square-o"></i> Create Room Type</button>
                                    <button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
@stop 

<?php /*?>@section('jQuery')
<script>
$(document).ready(function() {
	$("#smokeType input:radio").change(function() {
		var smokingType = $(this).val();
		$("#smokeTypes").prop("checked", false);
		$(this).attr("checked", "checked");
		var bedSize     = $("#room_bed_size").val();
		
		if(smokingType && bedSize)
			checkRoomTypeAvailability(smokingType, bedSize);
	});
	$("#room_bed_size").keyup(function() {
		if (document.getElementById('room_smoking_type1').checked) {
		  	smokingType = document.getElementById('room_smoking_type1').value;
		}else if(document.getElementById('room_smoking_type2').checked){
			smokingType = document.getElementById('room_smoking_type2').value;
		}
		var bedSize     = $(this).val().toUpperCase();
		$("#room_bed_size").val(bedSize);
		
		if(smokingType && bedSize)
			checkRoomTypeAvailability(smokingType, bedSize);
		
	});
	function checkRoomTypeAvailability(smokingType, bedSize){
		console.log(smokingType);
		console.log(bedSize);
		$.ajax({
			url: "{{ url('/roomTypeAvailable') }}",
			type: 'POST',
			data:{"smokingType":smokingType, "bedSize":bedSize, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				if ( msg.status === 'success' ) {
					toastr.success( msg.response );
				}
				if ( msg.status === 'error' ) {
					toastr.error( msg.response );
				}
			},
			error: function( data ) {
				if ( data.status === 422 ) {
					toastr.error('Cannot find the result');
				}
			}
		});
		return false;
	}	
});
</script>
@stop<?php */?>