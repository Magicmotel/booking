<?php

/**
 * This file contains functions for process all type of transaction.
 * 
 * transactionProcess class  
 * 
 */

namespace App\Zeamsters\lib;

//include_once BASE_PATH . DS . 'includes/Logging.php';

use App\Zeamsters\lib\ZeamsterPaymentTransaction as zeamsterApi;
use Exception;
use App\Zeamsters\includes\Logging;

class transactionProcess extends Exception {

    public $id = "";
    public $account_holder_name = "";
    public $account_number = "";

    /**
     * @var string $account_type(Required) Zeamster API to use for requests.
     */
    public $account_type = "";
    public $account_vault_id = "";

    /**
     * @var string $action(Required) Zeamster API to use for requests.
     */
    public $action = "";
    public $advance_deposit = "";
    public $auth_amount = "";
    public $auth_code = "";
    public $avs = "";
    public $avs_enhanced = "";
    public $batch = "";
    public $billing_street = "";
    public $billing_zip = "";
    public $charge_back_date = "";
    public $checkin_date = "";
    public $checkout_date = "";
    public $contact_id = "";
    public $created_ts = "";
    public $customer_id = "";
    public $customer_ip = "";
    public $cvv_response = "";
    public $description = "";
    public $e_format = "";
    public $e_track_data = "";
    public $e_serial_number = "";
    public $entry_mode_id = "";
    public $effective_date = "";
    public $exp_date = "";
    public $first_six = "";
    public $is_recurring = "";
    public $Item = "";
    public $last_four = "";
    public $location_id = "";
    public $modified_ts = "";
    public $no_show = "";
    public $notification_email_address = "";
    public $notification_email_sent = "";
    public $order_num = "";

    /**
     * @var string $payment_method(Required) Zeamster API to use for requests.
     */
    public $payment_method = "";
    public $po_number = "";
    public $previous_transaction_id = "";
    public $product_transaction_id = "";
    public $reason_code_id = "";
    public $recurring_id = "";
    public $response_message = "";
    public $return_date = "";
    public $room_num = "";
    public $room_rate = "";
    public $routing = "";
    public $save_account = "";
    public $save_account_title = "";
    public $settle_date = "";
    public $status_id = "";
    public $tax = "";
    public $terminal_serial_number = "";
    public $terms_agree = "";
    public $ticket = "";
    public $tip_amount = "";
    public $track_data = "";

    /**
     * @var string $transaction_amount(Required) Zeamster API to use for requests.
     */
    public $transaction_amount = "";
    public $transaction_api_id = "";
    public $transaction_settlement_status = "";
    public $type_id = "";
    public $verbiage = "";
    public $void_date = "";

    /**
     * Captur process.
     * 
     * @params object $objData  
     * @params string $aut_transaction_id  
     * 
     * @return json
     */
    public static function capture($objData, $aut_transaction_id) {
        try {
            $request_data = zeamsterApi::convertObjectToArray($objData);
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $api_url .= "/transactions/{$aut_transaction_id}";

            if (!empty($request_data)) {
                $request = array('transaction' => array_filter($request_data));
            }


            $request = json_encode($request);

            $response = $objzeamsterApi::send_transaction($api_url, 'PUT', null, $request);

            return $response;
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Authorize process.
     * 
     * @params object $objData 
     * 
     * @return json
     */
    public static function charge($objData) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $api_url .= "/transactions";

            $request_data = zeamsterApi::convertObjectToArray($objData);

            if (!empty($request_data)) {
                $request = array('transaction' => array_filter($request_data));

                $request = json_encode($request);

                $response = $objzeamsterApi::send_transaction($api_url, 'POST', null, $request);

                return $response;
            }
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Refund process.
     * 
     * @params object $objData 
     * @params string $originalTransactionId   
     * @params string $charge_type   
     * 
     * @return json
     */
    public static function refund($objData, $originalTransactionId = null, $charge_type = null) {
        try {

            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();

            $request_data = zeamsterApi::convertObjectToArray($objData);

            if (!empty($request_data)) {
                $request = array('transaction' => array_filter($request_data));
                $request = json_encode($request);
                if (!empty($charge_type) && $charge_type == zeamsterApi::REFUND) {
                    $transactionsUrl = $api_url . '/transactions';
                    $method = 'POST';
                } else {
                    if ($charge_type == zeamsterApi::PARTIALREVERSAL) {
                        $transactionsUrl = $api_url . '/transactions';
                        $method = 'POST';
                    }
                }
                $response = $objzeamsterApi::send_transaction($transactionsUrl, $method, null, $request);

                return $response;
            }
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Void process.
     * 
     * @params object $objData
     * @params string $originalTransactionId   
     * @params string $charge_type   
     * 
     * @return json
     */
    public static function void($objData, $originalTransactionId = null, $charge_type = null) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();

            if ($charge_type == zeamsterApi::VOID) {
                $transactionsUrl = $api_url . '/transactions' . "/{$originalTransactionId}";
                $method = 'PUT';
            }
            $request_data = zeamsterApi::convertObjectToArray($objData);
            $request = array('transaction' => array_filter($request_data));
            $request = json_encode($request);
            $response = $objzeamsterApi::send_transaction($transactionsUrl, $method, null, $request);

            return $response;
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

}
