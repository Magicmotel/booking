@extends('layouts.layout')

@section('title')
	Shifts Management
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Shifts Management</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
               	<li class="active"><a href="javascript:void(0);">Shifts Management</a></li>
                <li><a href="{{url('assign-shifts')}}">Assign Shifts</a></li>
                <li><a href="{{url('shifts')}}">Manage Shifts</a></li>
                <li><a href="{{url('shifts/create')}}">Create Shift</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                	{{--*/ $toDate = date("Y-m-d"); /*--}}
                    @if($request->shift_date >= $toDate )
                        {{--*/ $dayTodate = date('D', strtotime($request->shift_date)); /*--}}
                        @if($dayTodate == 'Mon')
                            {{--*/ $startDate = $request->shift_date; /*--}}
                        @else
                            {{--*/ $startDate = date('Y-m-d', strtotime("last Monday")); /*--}}
                        @endif
                    @else
                        {{--*/ $dayTodate = date('D'); /*--}}
                        @if($dayTodate == 'Mon')
                            {{--*/ $startDate = date('Y-m-d'); /*--}}
                        @else
                            {{--*/ $startDate = date('Y-m-d', strtotime("last Monday")); /*--}}
                        @endif
                    @endif
                    
                    {{--*/ $EmpVars = $BtnVars = ""; /*--}}
                    @if($request->job_title)
						{{--*/ $EmpVars .= " AND role_id = ".$request->job_title; /*--}}
                        {{--*/ $BtnVars .= "&job_title=".$request->job_title; /*--}}
                    @endif
                    @if($request->emp_id)
                    	{{--*/ $EmpVars .= " AND emp_id = ".$request->emp_id; /*--}}
                        {{--*/ $BtnVars .= "&emp_id=".$request->emp_id; /*--}}
                    @endif
                	<div class="x_content">
                        <form id="demo-form2" action="{!!url('record-shifts')!!}" method="get" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="form-group">
                                <label for="room_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Job Titles</label>
                                <div class="col-md-4 col-sm-3 col-xs-12 LR_Padd5">
                                    <select name="job_title" id="job_title" class="form-control col-md-3 col-xs-6" onchange="FilterSource(this.value)">
                                        <option value="">Select Job Title</option>
                                        @foreach( App\Role::where([ ['hotel_id', $Hotel_ID] ])->get(['name as RoleType', 'id as RoleId']) as $roles )
                                        <option value="{!!$roles->RoleId!!}"{!!($request->job_title == $roles->RoleId)?" Selected":"";!!}>{!!$roles->RoleType!!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="room_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Employee</label>
                                <div class="col-md-4 col-sm-3 col-xs-12 LR_Padd5">
                                    <select name="emp_id" id="emp_id" class="form-control col-md-3 col-xs-6"  onchange="FilterSource('{{$request->job_title}}', this.value)">
                                        <option value="">Select Employee</option>
                                        @if($request->job_title)
                                        @foreach( App\Employee::where([ ['hotel_id', $Hotel_ID], ['role_id', $request->job_title] ])->orderBy('emp_fname', 'ASC')->get() as $employee )
                                        <option value="{!!$employee->emp_id!!}"{!!($request->emp_id == $employee->emp_id)?" Selected":"";!!}>
                                            {!!$employee->emp_fname!!}{!!($employee->emp_mname)?" $employee->emp_mname":""!!}{!!" ".$employee->emp_lname!!}
                                        </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group form-group-last">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 LR_Padd5">
                                    <button type="submit" class="btn btn-success">SEARCH</button>
                                    <button type="reset" class="btn btn-primary">RESET</button>
                                </div>
                            </div>
                        </form>
                        {{--*/
                            $carryDate_From = $startDate;
                            $carryDate_To   = date('Y-m-d', strtotime($carryDate_From.'+6 days'));
                            
                            $PrevDate_Shift = date('Y-m-d', strtotime($carryDate_To.'-13 days'));
                            $NextDate_Shift = date('Y-m-d', strtotime($carryDate_To.'+1 days'));
                        
                            $room_rate_option_from = $carryDate_From;
                            $room_rate_option_to   = $carryDate_To;
                            $date1 = date_create($carryDate_From);
                            $date2 = date_create($carryDate_To); 
                            $diff = date_diff($date1, $date2);
                            $dayNum = $diff->format("%a");
                        /*--}}
                        <div class="col-md-4 col-sm-12 col-xs-12 marginTop24 TB_Padd10 AlgnLeft">
                            <b><i class="fa fa-calendar-o"></i> <a href="{!!url('record-shifts?shift_date='.$toDate.$BtnVars)!!}" title="Today">{{date('m/d/Y', strtotime($toDate))}}</a></b>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 marginTop24 TB_Padd10 AlgnCenter">
                            <b><i class="fa fa-calendar-o"></i> {{date('m/d/Y', strtotime($carryDate_From))." - ".date('m/d/Y', strtotime($carryDate_To))}}</b>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 TB_Padd10 AlgnRight">
                            <div class="rateAvail-button-group">
                                @if($carryDate_From >= $toDate)
                                    <a href="{!!url('record-shifts?shift_date='.$PrevDate_Shift.$BtnVars)!!}" class="rateAvail-default fc-corner-left" title="Previous">
                                        <span class="rateAvail-left-single-arrow"></span>
                                    </a>
                                @endif
                                <a href="{!!url('record-shifts?shift_date='.$NextDate_Shift.$BtnVars)!!}" class="rateAvail-default fc-corner-right" title="Next">
                                    <span class="rateAvail-right-single-arrow"></span>
                                </a>
                            </div>
                        </div>
                  	</div>
                    <div class="x_content"  id="printableArea">
                        <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th></th>
                            {{--*/ $temp_Date_From = date('Y-m-d', strtotime($carryDate_From)); /*--}}
                            @for($i = 1; $i <= $dayNum+1; $i++)
                                {{--*/ $dayName = date('D', strtotime($temp_Date_From)); /*--}}
                                {{--*/ $dayFormat = date('d M', strtotime($temp_Date_From)); /*--}}
                            <th class="{{$temp_Date_From}}" style="text-align:center;{!!($dayName=='Sat' || $dayName=='Sun')?' background-color:#E9E9E9;':'';!!}">{{$dayName}}<br />{{$dayFormat}}</th>
                                {{--*/ $temp_Date_From = date('Y-m-d', strtotime($temp_Date_From.'+1 days')); /*--}}
                            @endfor
                        </tr>
                        </thead>
                        @foreach(DB::select("SELECT emp_fname as EmpName, emp_id as EmployeeId
                                             FROM employees WHERE hotel_id = ".$Hotel_ID.$EmpVars." ORDER BY role_id ASC, emp_fname ASC
                                            ") as $valRst)
                        <tbody>
                        <tr>
                            <th scope="row" style="color:#FF6600; background-color:#F9F9F9">{{$valRst->EmpName}}</th>
                            {{--*/ $temp_Date_From = date('Y-m-d', strtotime($carryDate_From)); /*--}}
                            @for($i = 1; $i <= $dayNum+1; $i++)
                                <td class="{{$temp_Date_From}} zeroPadd">
                                {{--*/
                                $Sql = DB::select("SELECT emp_shift_id as EmpShift_ID, shift_date as EmpShift_Date, shift_id as ShiftId
                                                    FROM employee_shifts
                                                    WHERE hotel_id = ".$Hotel_ID." AND emp_id = ".$valRst->EmployeeId."
                                                    AND shift_date = '".$temp_Date_From."'
                                                    ORDER BY shift_date ASC
                                                   ");
                                /*--}}
                                @foreach($Sql as $Rst)
                                    <div class="shiftBoxOuter col-md-12 col-xs-12" id="availShiftDown_{{$Rst->EmpShift_ID}}" onclick="getavailShiftUpdate('{{$Rst->EmpShift_ID}}')">
                                   	{{--*/ $shiftArray = array(); /*--}}
                                    @if($Rst->ShiftId)
                                        {{--*/
                                        $shiftSql = DB::select("SELECT shift_id, shift_name, shift_color
                                                                FROM shifts WHERE hotel_id = ".$Hotel_ID." AND shift_id IN( ".$Rst->ShiftId." ) ORDER BY shift_name ASC
                                                               ");
                                        /*--}}
                                        @foreach($shiftSql as $shiftRst)
                                            <span class="shiftBox AlgnCenter col-md-12 col-xs-12" style="background:{{$shiftRst->shift_color}}; color:#FFFFFF;">
                                            {{$shiftRst->shift_name}}
                                            </span>
                                            {{--*/$shiftArray[] = $shiftRst->shift_name;/*--}}
                                        @endforeach
                                   	@endif
                                    </div>
                                    <div class="shiftBoxOuter col-md-12 col-xs-12 displayNone" id="availShiftUp_{{$Rst->EmpShift_ID}}" data-id="{{$Rst->EmpShift_ID}}">
                                    <form name="shiftForm_{{$Rst->EmpShift_ID}}" id="shiftForm_{{$Rst->EmpShift_ID}}">
                                    <input type="hidden" name="emp_shift_id" value="{{$Rst->EmpShift_ID}}" />
                                    <meta name="_token" content="{{ csrf_token() }}"/> 
                                    @foreach( App\Shift::where([ ['hotel_id', $Hotel_ID] ])->orderBy('shift_name', 'ASC')->get() as $shifts )
                                        <div class="shiftBox ">
                                            <input type="checkbox" name="shift_id[]" id="shift_id_{{$Rst->EmpShift_ID}}_{{$shifts->shift_id}}" value="{{$shifts->shift_id}}" class="js-switch"{!!(in_array($shifts['shift_name'], $shiftArray))?" Checked":""!!} />
                                            <div class="labelSwitchShift">{{$shifts->shift_name}}</div>
                                        </div>
                                    @endforeach
                                    <button class="btn btn-success buTTonResize" onclick="setavailShiftUpdate('{{$Rst->EmpShift_ID}}')">Assign Shifts</button>
                                    </form>
                                    </div>
                                @endforeach
                                </td>
                                {{--*/ $temp_Date_From = date('Y-m-d', strtotime($temp_Date_From.'+1 days')); /*--}}
                            @endfor
                        </tr>
                        </tbody>
                        @endforeach
                    </table>  
                    </div>
                    <button class="btn btn-default marginTop20 margin5" onClick="printDiv('printableArea')"><i class="fa fa-print"></i> Print</button> 
                </div>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function FilterSource(str, str1){
		if(str || str1){
			RedirectStr = '?';
			if(str){
				RedirectStr += "job_title="+str+"&";
			}
			if(str1){
				RedirectStr += "emp_id="+str1+"&";
			}
			RedirectStr = RedirectStr.replace(/&+$/,'');
			window.location.href="{!!url('record-shifts')!!}"+RedirectStr;
		}else if(!str && !str1){
			window.location.href="{!!url('record-shifts')!!}";
		}
	}

	function getavailShiftUpdate(str){
		$("#availShiftDown_"+str).hide();
		$("#availShiftUp_"+str).show();
		$("#availShiftUp_"+str).focus();
	}
	function setavailShiftUpdate(str){
		$("#shiftForm_"+str).on('submit',function(e){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
				}
			})
			e.preventDefault(e);
	
			$.ajax({
				type:"POST",
				url:"{{ url('/setassignShift') }}",
				data:$(this).serialize(),
				dataType: 'json',
				success: function(msg){
					if ( msg.status === 'success' ) {
						toastr.success( msg.response );
						setInterval(function() {
							window.location.reload();
						}, 1000);
					}
					if ( msg.status === 'error' ) {
						toastr.error( msg.response );
						setInterval(function() {
							window.location.reload();
						}, 1000);
					}
				},
				error: function(data){
		
				}
			})
		});
	}
	function printDiv(divName) {
		 var printContents = document.getElementById(divName).innerHTML;
		 var originalContents = document.body.innerHTML;
	
		 document.body.innerHTML = printContents;
	
		 window.print();
	
		 document.body.innerHTML = originalContents;
	}
</script>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
@stop 

@section('jQuery')
<script>
$(document).ready(function() {
	$('.availShiftUp').on("keydown", function(t) {
		if(t.which == 13){
			str  = $(this).val()
			str1 = $(this).attr('data-id');
			setavailShiftUpdate(str, str1);
		}
    })	
});
</script>
@stop