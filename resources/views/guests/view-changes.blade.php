@extends('layouts.layout')

<? 	foreach($Results as $val){
		$outPut = $val;
	}
?>

@section('title')
	Change Activity List
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
    	<div class="page-title">
            <div class="title_left"><h3>Change Activity List</h3></div>
            @if($outPut->RsrvSource)
                <div class="col-md-3 col-sm-3 col-xs-6 fltR zeroRightPadd">
                    <h3 class="AlgnCenter">{{$outPut->RsrvSource}}</h3>
                </div>
            @endif
        </div>
        {{--*/ $PayBalance = ($outPut->PayAmount-$outPut->PayDeposit); /*--}}
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('guest-info', $outPut->RsrvId)}}">Guest Info</a></li>
                <li><a href="{{url('payment-info', $outPut->RsrvId)}}">Payment Info</a></li>
                <li><a href="{{url('change-room', $outPut->RsrvId)}}">Room Change</a></li>
                <li><a href="{{url('guest-folio', $outPut->RsrvId)}}">Guest Folio</a></li>
                <li><a href="{{url('change-stay', $outPut->RsrvId)}}">Change Stay</a></li>
                <li class="active"><a href="javascript:void(0);">View Changes</a></li>
                @if($outPut->RsrvStat == '' && $PayBalance == 0)
                <li><a href="{{url('cancel-reservation', $outPut->RsrvId)}}">Cancel Reservation</a></li>
                @endif
            </ul>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <div class="table-responsive">
                        	<div class="col-md-12 col-sm-12 col-xs-12 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Guest Name:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->GuestLname}}, {{$outPut->GuestFname}}
                                </div>
                                @if($outPut->RsrvSource)
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    {{$outPut->RsrvSource}} Reservation#
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    {{$outPut->SourceRsrvId}}
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Company Name:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->GuestCompany}}
                                </div>                        	
                                @if($outPut->RsrvSource)
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Confirmation#
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->SourceRsrvCNFId}}
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Reservation#:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    {{$outPut->RsrvId}}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Arrival Date:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->ArvlDate}}
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Room#
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="RoomAssignId_{!!$outPut->RsrvId!!}">
                                    @if($outPut->RsrvStat == 'Checked Out' || $outPut->RsrvStat == 'Cancelled')
                                        <div class="fltL" style="position:relative">{{$outPut->RoomNumber}}</div>
	                                    <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                    @else
                                        @if($outPut->RoomNumber)
                                            @if($outPut->RsrvLock)
                                                <div class="fltL" style="position:relative">{{$outPut->RoomNumber}}</div>
                                                <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                                <i class="fa fa-lock fltL lockFeaturButton cursorPointer" id="unLockButton" data-id="{!!$outPut->RsrvId!!}" aria-hidden="true"></i>
                                            @else
                                                <div class="fltL" style="position:relative">
                                                <a href="{{url('change-room', $outPut->RsrvId)}}" class="btn-list-link">{{$outPut->RoomNumber}}</a>
                                                @if($outPut->RsrvStat == "")
                                                <i class="closeButton fa fa-times" data-id="{!!$outPut->RsrvId!!}"></i>
                                                @endif
                                                </div>
                                                <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                                <i class="fa fa-unlock fltL lockFeaturButton cursorPointer" id="LockButton" data-id="{!!$outPut->RsrvId!!}" aria-hidden="true"></i>
                                            @endif
                                        @else
                                            <a href="{{url('change-room', $outPut->RsrvId)}}" class="fltL btn-list-link">Select Room</a>
                                            <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Departure Date:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->DprtDate}}
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Estimated Cost:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    $ {{$outPut->EstAmount}} {!!($outPut->StayNights > 1)?'<i class="fa fa-info-circle cursorPointer font16 marginLeft5" id="dayWiseRate" title="Daywise Room Charges" aria-hidden="true"></i>':''!!}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Number of Nights:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->StayNights}}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop24 LR_Padd0">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">Change Date</th>
                                            <th class="column-title">Change User</th>
                                            <th class="column-title">Category</th>
                                            <th class="column-title">Field Name</th>
                                            <th class="column-title">Previous Value</th>
                                            <th class="column-title">New Value</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($AlltRst))
                                        @foreach($AlltRst as $vals)
                                        <tr class="even pointer">
                                            <td>{!!date('m/d/Y h:i A', strtotime($vals->ChangeDate))!!}</td>
                                            <td>{!!$vals->Fname." ".$vals->Lname!!}</td>
                                            <td>Stay Info</td>
                                            <td>Room</td>
                                            <td>{{$vals->PrvsRoomNumber}}</td>
                                            <td>{{$vals->RoomNumber}}</td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr class="even pointer">
                                            <td colspan="6" class="errorMessageTR">Room Number not Assigned</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                           	</div>
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>

<!-- checkedOutModel -->
<div id="checkedOutModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="model_RsrvId"></h4>
            </div>
            <div class="modal-footer AlgnLeft" id="modal-matter"></div>
        </div>
    </div>
</div>
<div id="fc_create" data-toggle="modal" data-target="#checkedOutModel"></div>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!}
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	$(document).on('click', '#dayWiseRate', function(e){
		var rsrvId = '{{$outPut->RsrvId}}';
		$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		$.ajax({
			url: "{{ url('/dayWiseRatePlan') }}",
			type: 'POST',
			data:{"rsrvId":rsrvId, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				if ( msg.status === 'success' ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					$("#model_RsrvId").html("Reservation ID: "+rsrvId);
					$("#modal-matter").html(msg.response);
					$('#fc_create').click();
				}
				if ( msg.status === 'error' ) {
					toastr.error( msg.response );
				}
			},
			error: function( data ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				if ( data.status === 422 ) {
					toastr.error('Try Again.');
				}
			}
		});
		return false;
	});
	
	$(document).on('click', '.closeButton', function(e){
		var x = confirm("Are you sure you want to un assigned this Room Number?");
		if (x){
			var rsrvId = $(this).attr('data-id');
			$("#main_container_Loading").show();
			$("#main_container_overlay").show();
			$.ajax({
				url: "{{ url('/unAssignRoomNumber') }}",
				type: 'POST',
				data:{"rsrvId":rsrvId, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( msg.status === 'success' ) {
						$('#RoomAssignId_'+rsrvId).html("<a href=\"{{url('change-room')}}/"+rsrvId+"\" class=\"btn-list-link\">Select Room</a>");
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( data.status === 422 ) {
						toastr.error('Cannot delete the Assigned Room');
					}
				}
			});
			return true;
		}else{
			return false;
		}
	});
	
	$(document).on('click', '.lockFeaturButton', function(e){
		var selfId = $(this).attr('id');
		if(selfId == 'LockButton'){
			var C_selfId = 'unLockButton';
			var rsrvLock = 1;
			var Msg = "Are you sure you want to Lock this Room Number?";
		}else{
			var C_selfId = 'LockButton';
			var rsrvLock = 0;
			var Msg = "Are you sure you want to Unlock this Room Number?";
		}
		var x = confirm(Msg);
		if (x){
			var rsrvId = $(this).attr('data-id');
			$("#main_container_Loading").show();
			$("#main_container_overlay").show();
			$.ajax({
				url: "{{ url('/lockAssignRoomNumber') }}",
				type: 'POST',
				data:{"rsrvId":rsrvId, "rsrvLock":rsrvLock, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( msg.status === 'success' ) {
						$('#RoomAssignId_'+rsrvId).html(msg.content);
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( data.status === 422 ) {
						toastr.error('Cannot handle Request.');
					}
				}
			});
			return true;
		}else{
			return false;
		}
	});
});
</script>
@stop