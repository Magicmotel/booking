@extends('layouts.layout')

{{--*/ $TitleSubject = "Hotel# MMKiD".$Hotel_ID.", ".$MotelInfo->hotel_name." :: Monthly Financial Report ".$startDate." to ".$endDate;/*--}} 
@section('title')
	{{$TitleSubject}}
@stop

@section('CascadingSheet')
    {!!Html::script('vendors/pdfmake/build/sprintf.js')!!}
    {!!Html::script('vendors/pdfmake/build/jspdf.js')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Financial Report</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('report-daily-financial')}}">Daily Financial Report</a></li>
                <li class="active"><a href="javascript:void(0);">Monthly Financial Report</a></li>
                <li><a href="{{url('report-annual-financial')}}">Annual Financial Report</a></li>
                <li><a href="{{url('report-tax-exemption')}}">Tax Exemption Report</a></li>
            </ul>
        </div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
          	    <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <form id="demo-form2" action="{!!url('report-monthly-financial')!!}" method="get" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                            	<div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group form-group-last">
                                        <label class="control-label col-md-4 col-sm-3 col-xs-12">From Month</label>
                                        <div class="col-md-7 col-sm-3 col-xs-12">
                                            <input type="text" name="start_date" id="start_date" class="form-control has-feedback-left" placeholder="From Month" value="{{$startDate}}" readonly="readonly">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                    </div>
                               	</div>
                                <div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group form-group-last">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <button type="submit" class="btn btn-success"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
                                        </div>
                                    </div>
                               	</div>
                            </div>
                        </form>
                  	</div>
                </div>
          	</div>
            @if($startDate)
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content" id="printableArea">
                        <table class="table table-striped" id="pdfableArea">
                            <tr class="backgroundNone">
                                <td width="20%" class="borderNone height140">
                                	{!!Html::image('motel/'.$MotelInfo->hotel_logo, 'alt', array( 'title' => 'Motel Logo', 'style' => 'max-width:120px; max-height:120px;'))!!}
                              	</td>
                                <td width="60%" align="center" valign="middle" class="zeroPadd borderNone labelText font16">
                                	Monthly Financial Report<br /><br />
                                    From: {{$startDate}} to {{$endDate}}
                                </td>
                                <td width="20%" class="zeroPadd borderNone">
                                	<div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnLeft LneHeight24">Date: {{date('m/d/Y')}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnLeft LneHeight24">Time: {{date('h:i A')}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnLeft LneHeight24">User: {{Helper::getAdminUserName()}}</div>
                               	</td>
                            </tr>
                            {{--*/ 	$TaxVal = App\Taxes::where('hotel_id', $Hotel_ID)->groupBy('tax_type')->get();
                            		$totalRoomCharge = $totalOtherCharge = $totalCashCredit = $totalCCCredit = $totalBillCredit = $totalTotalCharge = 0;/*--}}
                            <tr class="backgroundNone">
                            	<td colspan="3" class="zeroPadd borderNone">
                                	<table id="exelableArea" class="table table-bordered table-striped jambo_table">
                                        <thead>
                                        <tr>
                                        	<th>Date</th>
                                            <th>Room Charge</th>
											@foreach($TaxVal as $TaxValRes)
                                            <th>{{$TaxValRes->tax_type}}</th>
                                            {{--*/ 	$TaxTypeVal = str_replace(" ", "", $TaxValRes->tax_type);
                                            		$totalTax_{"$TaxTypeVal"} = 0;
                                           	/*--}}
                                            @endforeach
                                            <th>Other Charges</th>
                                            <th>Cash/Check</th>
                                            <th>CC Type</th>
                                            <th>CC Amount</th>
                                            <th>Direct Bill</th>
                                            <th>Total</th>
                                        </tr>
                                        </thead>
                                       	<tbody>
                                       	@foreach($Results as $roomVals)
                                        	{{--*/ 
                                            	$totalTax = 0;
                                            	$totalRoomCharge  += $roomVals->RoomCharge;                                           
                                            	$totalOtherCharge += $roomVals->OtherCharge;
                                            	$totalCashCredit  += $roomVals->CashCredit;
                                                $totalCCCredit    += $roomVals->CCCredit;
                                                $totalBillCredit  += $roomVals->DirectCredit;
                                            /*--}}
                                        <tr>
                                        	<td>{{$roomVals->FolioDate}}</td>
                                            <td>{{($roomVals->RoomCharge>0)?"$".number_format($roomVals->RoomCharge, 2, '.', ''):""}}</td>
                                            @foreach($TaxVal as $TaxValRes)
                                            	{{--*/ $TaxTypeVal = str_replace(" ", "", $TaxValRes->tax_type); /*--}}
                                            <td>{!!($roomVals->{"$TaxTypeVal"})?"$".number_format($roomVals->{"$TaxTypeVal"}, 2, '.', ''):""!!}</td>
                                            	{{--*/ $totalTax += $roomVals->{"$TaxTypeVal"};
                                                	   $totalTax_{"$TaxTypeVal"} += $roomVals->{"$TaxTypeVal"};
                                                /*--}}
                                            @endforeach
                                            {{--*/
                                            	$dayTotalCharge = $roomVals->RoomCharge+$roomVals->OtherCharge+$totalTax;
                                                $totalTotalCharge += $dayTotalCharge;
                                           	/*--}}
                                            <td>{{($roomVals->OtherCharge>0)?"$".number_format($roomVals->OtherCharge, 2, '.', ''):""}}</td>
                                            <td>{{($roomVals->CashCredit>0)?"$".number_format($roomVals->CashCredit, 2, '.', ''):""}}</td>
                                            <td>
                                            @if($roomVals->CCType)
                                            	{{--*/ $cardTypeArray = array();
                                                $typeCard = DB::select("SELECT ZL.account_type FROM zeamster_log as ZL
                                                				   		INNER JOIN payment_option as PO ON PO.gateway_transaction_id = ZL.transaction_id
                                                						WHERE PO.transaction_id IN ('".str_replace(", ", "', '", $roomVals->CCType)."')
                                                                        GROUP BY ZL.account_type
                                                                       ");
                                               	foreach($typeCard as $typeCardVal){
                                                	$cardTypeArray[] = $typeCardVal->account_type;
                                                }
                                                echo implode(", ",$cardTypeArray);
                                                /*--}}
                                            @endif
                                           	</td>
                                            <td>{{($roomVals->CCCredit)?"$".number_format($roomVals->CCCredit, 2, '.', ''):""}}</td>
                                            <td>{{($roomVals->DirectCredit)?"$".number_format($roomVals->DirectCredit, 2, '.', ''):""}}</td>
                                            <td class="AlgnRight">{{($dayTotalCharge>0)?"$".number_format($dayTotalCharge, 2, '.', ''):""}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody> 
                                        <tfoot>
                                        	<th>Total</th>
                                            <th>{{($totalRoomCharge>0)?"$".number_format($totalRoomCharge, 2, '.', ''):""}}</th>
											@foreach($TaxVal as $TaxValRes)
                                            	{{--*/$TaxTypeVal = str_replace(" ", "", $TaxValRes->tax_type);/*--}}
                                            <th>{{($totalTax_{"$TaxTypeVal"}>0)?"$".number_format($totalTax_{"$TaxTypeVal"}, 2, '.', ''):""}}</th>
                                            @endforeach
                                            <th>{{($totalOtherCharge>0)?"$".number_format($totalOtherCharge, 2, '.', ''):""}}</th>
                                            <th>{{($totalCashCredit>0)?"$".number_format($totalCashCredit, 2, '.', ''):""}}</th>
                                            <th></th>
                                            <th>{{($totalCCCredit>0)?"$".number_format($totalCCCredit, 2, '.', ''):""}}</th>
                                            <th>{{($totalBillCredit>0)?"$".number_format($totalBillCredit, 2, '.', ''):""}}</th>
                                            <th class="AlgnRight">{{($totalTotalCharge>0)?"$".number_format($totalTotalCharge, 2, '.', ''):""}}</th>
                                        </tfoot>
                                    </table>
                              	</td>
                            </tr>
                        </table>
                    </div>
                    <button class="btn btn-primary marginTop20 margin5" id="btnPrint"><i class="fa fa-print"></i> Print</button>
                    <button class="btn btn-success marginTop20 margin5" id="btnExcel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</button>
                    <button class="btn btn-danger marginTop20 margin5" id="btnPDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</button>
                </div>
         	</div>
            @endif
      	</div>
    </div>
</div>
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	var checkin = $('#start_date').datepicker({
		startDate: moment(),
    	minViewMode:1,
    	viewMode:1,
    	format: 'mm/dd/yyyy'
	}).on('changeDate', function(ev){
		checkin.hide();
	}).data('datepicker');
	
	
	$("#btnPrint").click(function(e){
		var printContents = document.getElementById('printableArea').innerHTML;
		var originalContents = document.body.innerHTML;
		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
		window.location.reload();
	});
	$("#btnExcel").click(function(e){
		e.preventDefault();
		
		var data_type = 'data:application/vnd.ms-excel';
		var table_div = document.getElementById('exelableArea');
		var table_html = table_div.outerHTML.replace(/ /g, '%20');
		
		var a = document.createElement('a');
		a.href = data_type + ', ' + table_html;
		a.download = '{{$TitleSubject}}.xls';
		a.click();
	});
	$("#btnPDF").click(function(e){
		var pdf = new jsPDF('p', 'pt', 'letter');
		source = $('#pdfableArea')[0];
		specialElementHandlers = {
			// element with id of "bypass" - jQuery style selector
			'#bypassme': function(element, renderer) {
				// true = "handled elsewhere, bypass text extraction"
				return true
			}
		};
		margins = {
			top: 40,
			bottom: 40,
			left: 40,
			width:572
		};
		
		pdf.fromHTML(
				source, // HTML string or DOM elem ref.
				margins.left, // x coord
				margins.top,
				{// y coord
					'width': margins.width, // max width of content on PDF
					'elementHandlers': specialElementHandlers
				},
				function(dispose) {
					pdf.save('{{$TitleSubject}}.pdf');
				},
				margins
		);
	});
});
</script>
@stop