@extends('layouts.layout')

@section('title')
	Manage Discounts
@stop

@section('CascadingSheet')
    <!-- Datatables -->
    {!!Html::style('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-dragtable/css/dragtable.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Discounts</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('room/taxes')}}">Manage Taxes</a></li>
                <li class="active"><a href="javascript:void(0);">Manage Discounts</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                        @if ($message = Session::get('update'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @elseif($message = Session::get('danger'))
                            <div class="alert alert-danger">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="col-md-5 col-sm-12 col-xs-12 zeroLeftPadd">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @elseif ($message = Session::get('error'))
                                <div class="alert alert-warning">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-striped jambo_table bulk_action marginTop30">
                                <thead>
                                <tr class="headings">
                                    <th>Create Discount Code</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                        			<td>
                                        <form id="demo-form2" action="{!!url('room/discounts')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                                            {!!csrf_field()!!}
                                            <div class="form-group">
                                                <label class="control-label col-md-5 col-sm-4 col-xs-12">Discount Code <span class="required">*</span></label>
                                                <div class="col-md-7 col-sm-6 col-xs-12">
                                                    <input type="text" name="discount_code" id="discount_code" value="" class="form-control col-md-3 col-xs-3" placeholder="Discount Code" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5 col-sm-4 col-xs-12">Value of Discount <span class="required">*</span></label>
                                                <div class="col-md-5 col-sm-6 col-xs-12">
                                                    <input type="text" name="discount_value" id="discount_value" value="" class="form-control col-md-3 col-xs-3" placeholder="Discount Value" required="required" min=".1">
                                                </div>
                                                <div class="col-md-2 col-sm-6 col-xs-12 zeroLeftPadd">
                                                    <select name="discount_type" id="discount_type" class="form-control col-md-3 col-xs-3 LR_Padd5">
                                                        <option value="1" selected="selected">%</option>
                                                        <option value="2">Flat</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5 col-sm-4 col-xs-12">Start From</label>
                                                <div class="col-md-7 col-sm-6 col-xs-12">
                                                    <input type="text" name="discount_from" id="discount_from" value="" class="form-control col-md-4 col-xs-12 has-feedback-left" placeholder="Start From" readonly="readonly" required="required">
                                        			<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-5 col-sm-4 col-xs-12">Valid upto</label>
                                                <div class="col-md-7 col-sm-6 col-xs-12">
                                                    <div class="fltL control-value marginRight10">
                                                        <input type="radio" name="discount_valid" id="discount_valid" value="1" Checked onclick="getValidityofDiscount(this.value);" />
                                                        <div class="labelSwitchCheck">Unlimited</div>
                                                    </div>
                                                    
                                                    <div class="fltL control-value">
                                                        <input type="radio" name="discount_valid" id="discount_valid" value="2" {!!(old('discount_valid') == 2)?" Checked":"";!!} onclick="getValidityofDiscount(this.value);" />
                                                        <div class="labelSwitchCheck">Specific Date</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group displayNone" id="discountValid">
                                                <div class="col-md-7 col-sm-6 col-xs-12  col-md-offset-5">
                                                    <input type="text" name="discount_to" id="discount_to" value="" class="form-control col-md-4 col-xs-12 has-feedback-left" placeholder="Valid upto" readonly="readonly">
                                        			<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <div class="form-group form-group-last marginTop24">
                                                <div class="col-md-12 col-sm-6 col-xs-12 col-md-offset-5">
                                                    <button type="submit" class="btn btn-success"><i class="fa fa-plus-square-o"></i> Add Discount</button>
                                                    <button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-7 col-sm-12 col-xs-12 zeroPadd">
                            <table id="datatable-responsive" class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th id="inHouse_1">Discount Code</th>
                                        <th id="inHouse_2">Discount</th>
                                        <th id="inHouse_3">Start From</th>
                                        <th id="inHouse_5">Valid Upto</th>
                                        <th id="inHouse_6">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($Results))
                                    @foreach($Results as $vals)
                                    <tr>
                                        <td>{{$vals->discount_code}}</td>
                                        <td>{{($vals->discount_type==1)?$vals->discount_value."%":"$ ".$vals->discount_value}}</td>
                                        <td>{{($vals->discount_from == '0000-00-00')?date('m/d/Y'):date("m/d/Y", strtotime($vals->discount_from))}}</td>
                                        <td>{{($vals->discount_to == '0000-00-00')?"Unlimited":date("m/d/Y", strtotime($vals->discount_to))}}</td>
                                        <td>
                                            <a href="{!!url('room/discounts', $vals->discount_id)!!}/edit" class="left btn btn-info buTTonResize"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                            <form action="{!!url('room/discounts', $vals->discount_id)!!}" method="post" onsubmit="return ConfirmAction('Are you sure you want to delete this Discount Code?')" class="left">
                                            {!!method_field('DELETE')!!}
                                            {!!csrf_field()!!}
                                            {!!Form::hidden('hotel_id', $Hotel_ID)!!}
                                            {!!Form::button('<i class="fa fa-trash-o"></i> Delete', ['class'=>'left btn btn-danger buTTonResize marginLeft5', 'type'=>'submit'])!!}
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                               	@else
                                	<tr class="even pointer">
                                        <td colspan="4" class="errorMessageTR">No Discount Code Found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function getValidityofDiscount(str){
		if(str == 2){
			$('#discountValid').show();
			$("#discount_to").prop("required", true);
			$("#discount_to").val("");
		}else{
			$('#discountValid').hide();
			$("#discount_to").prop("required", false);
			$('#discount_to').val("");
		}
	}
	function ConfirmAction(str)
	{
		var x = confirm(str);
		if (x) return true; else return false;
	}
</script>
@stop

@section('JavascriptSRC')
    <!-- Datatables -->
    {!!Html::script('vendors/datatables.net-dragtable/js/jquery.dragtable.js')!!}
    {!!Html::script('vendors/datatables.net/js/jquery.dataTables.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')!!}
    {!!Html::script('vendors/jszip/dist/jszip.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/pdfmake.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/vfs_fonts.js')!!} 
@stop 

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	
	var discountFrom = $('#discount_from').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		var newDate = new Date(ev.date);
		newDate.setDate(newDate.getDate());
		discountTo.setValue(newDate);
		discountFrom.hide();
	}).data('datepicker');
	
	var discountTo = $('#discount_to').datepicker({
		onRender: function(date) {
			return date.valueOf() < discountFrom.date.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		discountTo.hide();
	}).data('datepicker');

	$('#datatable-responsive').DataTable({
		"aaSorting": [[1, 'asc']],
		"bPaginate": false,
		"bFilter": true,
		"columnDefs": [ { orderable: false, targets: -1 } ]
	});
});
</script>
@stop