<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Hotel;
use App\HouseKeeping;
use App\RoomAssign;
use Helper;
use DB;

class HouseKeepingController extends Controller
{
	public function index(Request $request)
    {
        $Hotel_ID = Helper::getHotelId();
		return view('services.housekeeping')->with('Hotel_ID', $Hotel_ID)->with('request', $request);
    }

	public function store(Request $request)
    {
		$Hotel_ID = Helper::getHotelId();
		$admin_ID = Helper::getAdminUserId();
		
		if($Hotel_ID == $request->hotel_id)
		{
			$EmpId = $request->emp_id;
			$RoomAssingArray = $request->room_assign_id;
			foreach($RoomAssingArray as $RoomAssingId)
			{
				$RequestArray = array();	
				$RequestArray[] = $Hotel_ID;
				$RequestArray[] = $RoomAssingId;
				$RequestArray[] = $EmpId;
				$RequestArray[] = $_SERVER['REMOTE_ADDR'];
				$RequestArray[] = $admin_ID;
				DB::select('CALL HouseKeeping_Insert_SP(?,?,?,?,?)', $RequestArray);
			}
			return redirect('housekeeping-headquarters')->with('success','Dirty Rooms assigned to Housekeeping successfully');
		}else{
			return redirect('housekeeping-headquarters')->with("danger", "Please select your Motel");
		}
    }
	
	public function cleanroom(Request $request)
    {
		$Hotel_ID = Helper::getHotelId();
		$admin_ID = Helper::getAdminUserId();
		if($Hotel_ID == $request->hotel_id)
		{
			$HouseKeepingArray = $request->housekeeping_id;
			foreach($HouseKeepingArray as $HouseKeepingId)
			{
				if($request->form_type == 1)
				{
					$Results = HouseKeeping::where([ ['hotel_id', $Hotel_ID], ['housekeeping_id', $HouseKeepingId] ])->first();
					$Results->status = 1;
					$Results->reg_ip   = $_SERVER['REMOTE_ADDR'];
					$Results->added_by = $admin_ID;
					$Results->save();
				}
				elseif($request->form_type == 2)
				{
					$Results = HouseKeeping::where([ ['hotel_id', $Hotel_ID], ['housekeeping_id', $HouseKeepingId] ])->first();
					$Results->status = 1;
					$Results->reg_ip   = $_SERVER['REMOTE_ADDR'];
					$Results->added_by = $admin_ID;
					$Results->save();
					
					$AssgnRst = RoomAssign::where([ ['hotel_id', $Hotel_ID], ['room_assign_id', $Results->room_assign_id] ])->first();
					$AssgnRst->room_stat = 1;
					$AssgnRst->reg_ip   = $_SERVER['REMOTE_ADDR'];
					$AssgnRst->added_by = $admin_ID;
					$AssgnRst->save();
				}
			}
			return redirect('housekeeping-headquarters')->with('success','Dirty Rooms cleaned successfully');
		}else{
			return redirect('housekeeping-headquarters')->with("danger", "Please select your Motel");
		}
    }

    public function assignment_print(Request $request)
    {	
		$Hotel_ID = Helper::getHotelId();
		$MotelInfo = Hotel::where('hotel_id',$Hotel_ID)->first(['hotel_name', 'hotel_logo']);
		return view('services.housekeeping-print')->with('Hotel_ID', $Hotel_ID)->with('MotelInfo', $MotelInfo)->with("request", $request);		
    }
}
