@extends('layouts.layout')

@section('title')
	Create Shift
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Create Shift</h3></div>
        </div>
        
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
            	<li><a href="{{url('record-shifts')}}">Shifts Management</a></li>
                <li><a href="{{url('assign-shifts')}}">Assign Shifts</a></li>
                <li><a href="{{url('shifts')}}">Manage Shifts</a></li>
                <li class="active"><a href="javascript:void(0);">Create Shift</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	@if (count($errors))
                <ul class="errorFormMessage">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                    {{--*/ $dateArray = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12') /*--}}
                    <form id="demo-form2" action="{!!url('shifts')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                        {!!csrf_field()!!}
                        <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name of Shift <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            	<input type="text" name="shift_name" id="shift_name" value="{!!old('shift_name')!!}" class="form-control col-md-7 col-xs-12" placeholder="Name of Shift" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Shift Timing <span class="required">*</span></label>
                            <div class="col-md-1 col-sm-6 col-xs-12">
                            	<select name="shift_from_time" id="shift_from_time" class="form-control col-md-4 col-xs-12">
                                	@foreach($dateArray as $dateVal)
                                	<option value="{{$dateVal}}">{{$dateVal}}</option>
                                	@endforeach
                                </select>
                            </div>
                            <div class="col-md-1 col-sm-6 col-xs-12">
                            	<select name="shift_from_meridiem" id="shift_from_meridiem" class="form-control col-md-4 col-xs-12">
                                	<option value="AM">AM</option>
                                	<option value="PM">PM</option>
                                </select>
                            </div>
                            <div class="control-value left">
                            	-
                            </div>
                            <div class="col-md-1 col-sm-6 col-xs-12">
                            	<select name="shift_to_time" id="shift_to_time" class="form-control col-md-4 col-xs-12">
                                	@foreach($dateArray as $dateVal)
                                	<option value="{{$dateVal}}">{{$dateVal}}</option>
                                	@endforeach
                                </select>
                            </div>
                            <div class="col-md-1 col-sm-6 col-xs-12">
                            	<select name="shift_to_meridiem" id="shift_to_meridiem" class="form-control col-md-4 col-xs-12">
                                	<option value="AM">AM</option>
                                	<option value="PM">PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Shift Color <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            	<input type="text" name="shift_color" id="shift_color" value="{!!old('shift_color')!!}" class="demo1 form-control col-md-7 col-xs-12" placeholder="Shift Color" required="required">
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop20">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-plus-square-o"></i> Create Shift</button>
                                <button type="reset" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                    	</div>                    
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')!!}
@stop

@section('jQuery')
<script>
	$(document).ready(function() {
		$('.demo1').colorpicker();
	});
</script>
@stop