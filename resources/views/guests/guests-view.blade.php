@extends('layouts.layout')

@section('title')
	View Guest Information
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>View Guest Information</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <div class="form-horizontal form-label-left">
                    	<div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Guest Name:</label>
                            <div class="control-value col-md-3 col-sm-3">
                                {!!($Results->guest_title == 1)?"Mr.":"";!!}
                                {!!($Results->guest_title == 2)?"Mrs.":"";!!}
                                {!!($Results->guest_title == 3)?"Miss":"";!!}
                                {!!$Results->guest_fname!!}
                                {!!$Results->guest_mname!!}
                                {!!$Results->guest_lname!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Date Of Birth:</label>
                            <div class="control-value col-md-6 col-sm-6">
                            {!!$Results->guest_dob!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Address:</label>
                            <div class="control-value col-md-6 col-sm-6">
                            {!!nl2br($Results->guest_address)!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">City:</label>
                            <div class="control-value col-md-6 col-sm-6">
                            {!!$Results->guest_city!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Zip Code:</label>
                            <div class="control-value col-md-6 col-sm-6">
                            {!!$Results->guest_zip!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Email:</label>
                            <div class="control-value col-md-6 col-sm-6">
                            {!!$Results->guest_email!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Cell Phone Number:</label>
                            <div class="control-value col-md-6 col-sm-6">
                            {!!$Results->guest_phone!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Nationality:</label>
                            <div class="control-value col-md-6 col-sm-6">
                            {!!$Results->guest_nation!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">ID Type:</label>
                            <div class="control-value col-md-6 col-sm-6">
                                {!!($Results->guest_idproof == 1)?"Passport":"";!!}
                                {!!($Results->guest_idproof == 2)?"Driving Licence":"";!!}
                                {!!($Results->guest_idproof == 3)?"State issued Id":"";!!}
                                {!!($Results->guest_idproof == 4)?"$Results->guest_idproof_other":"";!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">ID Number:</label>
                            <div class="control-value col-md-6 col-sm-6">
                            {!!$Results->guest_proofno!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Company Name:</label>
                            <div class="control-value col-md-6 col-sm-6">
                            {!!$Results->guest_company!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Group Name:</label>
                            <div class="control-value col-md-6 col-sm-6">
                            {!!$Results->guest_group!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Vehicle Details:</label>
                            <div class="control-value col-md-6 col-sm-6">
                            {!!$Results->guest_vehicle!!}
                            </div>
                        </div>
                    	<div class="form-group form-group-last">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            	<a href="{!!url('guests', $Results->guest_id)!!}/edit" class="btn btn-success">EDIT</a>
                                <a href="{!!url('guests')!!}" class="btn btn-primary">BACK</a>
                            </div>
                    	</div>
                    </div>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop