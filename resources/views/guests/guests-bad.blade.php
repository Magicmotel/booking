@extends('layouts.layout')

@section('title')
	mark Guest as Bad Guest
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>mark Guest as Bad Guest</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <form id="demo-form2" action="{!!url('bad-guest', $Results->guest_id)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                        {!!method_field('PATCH')!!}
                        {!!csrf_field()!!}
                    	<div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Title</label>
                            <div class="control-value col-md-3 col-sm-3">
                                <div class="col-md-12 col-sm-12 col-xs-12 labelText">
                                	{{$Results->guest_lname}}, {{$Results->guest_fname}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Reasons</label>
                            <div class="col-md-6 col-sm-6">
                            	<div class="fltL control-value marginRight10">
                                <input type="checkbox" name="guest_bad_reason[]" id="guest_bad_reason_1" class="flat" value="Reason 1"><div class="labelSwitchCheck">Reason 1</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-md-offset-3">
                            	<div class="fltL control-value marginRight10">
                                <input type="checkbox" name="guest_bad_reason[]" id="guest_bad_reason_2" class="flat" value="Reason 2"><div class="labelSwitchCheck">Reason 2</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-md-offset-3">
                            	<div class="fltL control-value marginRight10">
                                <input type="checkbox" name="guest_bad_reason[]" id="guest_bad_reason_3" class="flat" value="Reason 3"><div class="labelSwitchCheck">Reason 3</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-md-offset-3">
                            	<div class="fltL control-value marginRight10">
                                <input type="checkbox" name="guest_bad_reason[]" id="guest_bad_reason_4" class="flat" value="Reason 4"><div class="labelSwitchCheck">Reason 4</div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-md-offset-3">
                            	<div class="fltL control-value marginRight10">
                                <input type="checkbox" name="guest_bad_reason[]" id="guest_bad_reason_5" class="flat" value="Reason 5"><div class="labelSwitchCheck">Reason 5</div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-md-offset-3 marginTop10">
                                <textarea name="guest_bad_reason[]" id="guest_bad_reason_6" rows="5" placeholder="Other Reasons......" class="form-control col-md-12 col-xs-12"></textarea>
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop24">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-warning">mark as Bad Guest</button>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function getIdProofType(str){
		if(str == 4){
			$('#guest_idproof_Box').show();
		}else{
			$('#guest_idproof_Box').hide();
			$('#guest_idproof_other').val("");
		}
	}
</script>
@stop      
