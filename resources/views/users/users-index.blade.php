@extends('layouts.layout')

@section('title')
	All Admin Users
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Admin Users</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class="active"><a href="javascript:void(0);">Manage Admin Users</a></li>
                <li><a href="{{url('users/create')}}">Create Admin User</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if (count($errors))
                                <ul class="errorFormMessage">
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            @endif
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @elseif ($message = Session::get('danger'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                    	<th class="column-title">No</th>
                                        <th class="column-title">User Name</th>
                                        <th class="column-title">Role</th>
                                        <?php /*?><th class="column-title">Shift</th><?php */?>
                                        <th class="column-title no-link last"><span class="nobr">Action</span></th>
                                    </tr>
                                </thead>
                            
                                <tbody>
                                @foreach($Results as $vals)
                                    <tr class="even pointer">
                                        <td class=" ">{{ ++$i }}</td>
                                        <td class=" ">{!!$vals->UserFname!!}{!!($vals->UserMname)?" $vals->UserMname":""!!}{!!($vals->UserLname)?" $vals->UserLname":""!!}</td>
                                        <td class=" ">{!!$vals->RoleName!!}</td>
                                        <?php /*?><td class=" ">{!!$vals->ShiftType!!}</td><?php */?>
                                        <td class=" last">
                                            <a href="{!!url('users', $vals->UserId)!!}/edit" class="left btn btn-info buTTonResize"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                            @if($vals->UserSuper == 0)
                                            <form action="{!!url('users', $vals->UserId)!!}" method="post" style="float:left;width:auto; margin:0;" onsubmit="return ConfirmAction('Are you sure you want to delete this User?');">
                                            {!!method_field('DELETE')!!}
                                            {!!csrf_field()!!}
                                            {!!Form::button('<i class="fa fa-trash-o"></i> Delete', ['class'=>'left btn btn-danger buTTonResize marginLeft5', 'type'=>'submit'])!!}
                                            </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $Results->render() !!}
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function ConfirmAction(str)
	{
		var x = confirm(str);
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop      
