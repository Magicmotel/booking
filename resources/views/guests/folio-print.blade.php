@extends('layouts.layout')

@section('title')
	Reservation# {{$RsrvRst->room_reservation_id}}, {{$GuestRst->guest_fname." ".$GuestRst->guest_lname}} :: Guest Folio
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Print Guest Folio</h3></div>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  	<div class="x_content" id="printableArea">
                        <table class="table table-striped">
                            <tr class="backgroundNone">
                                <td width="20%" class="borderNone height140">
                                    {!!Html::image('motel/'.$HotelRst->hotel_logo, 'alt', array( 'title' => 'Motel Logo', 'style' => 'max-width:120px; max-height:120px;' ))!!}
                                </td>
                                <td width="50%" class="zeroPadd borderNone AlgnCenter vAlgnTop font16">
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 labelText AlgnLeft">{{$HotelRst->hotel_name}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft">{{nl2br($HotelRst->hotel_address)}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft">{{$HotelRst->hotel_city}}, {{$HotelRst->hotel_state}} {{$HotelRst->hotel_zip}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft">{{$HotelRst->hotel_phone_1}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft">{{$HotelRst->hotel_email}}</div>
                                </td>
                                <td width="20%" class="borderNone">&nbsp;</td>
                            </tr>
                            <tr class="backgroundNone">
                                <td width="100%" colspan="3" class="zeroPadd paddingTop20 borderNone vAlgnTop AlgnRight">
                                	<table width="100%">
                                		<tr class="backgroundNone marginTop10">
                                            <td width="20%" class="zeroPadd borderNone labelText AlgnLeft LneHeight24">Guest Name</td>
                                            <td width="30%" class="zeroPadd borderNone AlgnLeft">{{$GuestRst->guest_fname." ".$GuestRst->guest_lname}}</td>
                                            <td width="50%" class="zeroPadd borderNone vAlgnTop AlgnRight">Date : {{date('m/d/Y')}}</td>
                                        </tr>
                                        <tr class="backgroundNone marginTop10">
                                            <td width="20%" class="zeroPadd borderNone labelText AlgnLeft LneHeight24">Arrival</td>
                                            <td width="30%" colspan="2" class="zeroPadd borderNone AlgnLeft">{{date("m/d/Y", strtotime($RsrvRst->arrival))}}</td>
                                        </tr>
                                        <tr class="backgroundNone marginTop10">
                                            <td width="20%" class="zeroPadd borderNone labelText AlgnLeft LneHeight24">Departure</td>
                                            <td width="30%" colspan="2" class="zeroPadd borderNone AlgnLeft">{{date("m/d/Y", strtotime($RsrvRst->departure))}}</td>
                                        </tr>
                                        <tr class="backgroundNone marginTop10">
                                            <td width="20%" class="zeroPadd borderNone labelText AlgnLeft LneHeight24">Number of Nights</td>
                                            <td width="30%" colspan="2" class="zeroPadd borderNone AlgnLeft">{{$RsrvRst->reserv_night}}</td>
                                        </tr>
                                        <tr class="backgroundNone">
                                            <td width="20%" class="zeroPadd borderNone labelText AlgnLeft LneHeight24">Reservation#</td>
                                            <td width="30%" colspan="2" class="zeroPadd borderNone AlgnLeft">{{$RsrvRst->room_reservation_id}}</td>
                                        </tr>
                                        <tr class="backgroundNone">
                                            <td width="20%" class="zeroPadd borderNone labelText AlgnLeft LneHeight24">Room Type:</td>
                                            <td width="30%" colspan="2" class="zeroPadd borderNone AlgnLeft">{{$RoomTypeRst->room_display}}</td>
                                        </tr>
                                    </table>
                            	</td>
                            </tr>
                            <tr class="backgroundNone">
                                <td width="100%" colspan="3" class="zeroPadd paddingTop20 borderNone vAlgnTop">
                                	<table class="table table-striped jambo_table bulk_action">
                                        <thead>
                                            <tr class="headings">
                                                <th class="column-title">Date</th>
                                                <th class="column-title">Payment/Charge Type</th>
                                                <th class="column-title">Comment</th>
                                                <th class="column-title">Amount</th>
                                            </tr>
                                        </thead>
                                    
                                        <tbody>
                                            {{--*/ 	$amountDeposit = $amountBalance = 0;
                                            $paymentModeArray = array("Cash Payment"=>"success", "CC Payment"=>"success", "CC Payment Auth Only"=>"warning",
                                                                      "Check Payment"=>"success", "Company Payment"=>"warning");
                                            $rsrvQry = DB::select("SELECT * FROM room_reservation_folio_meta
                                                                   WHERE room_reservation_id = ".$RsrvRst->room_reservation_id." AND hotel_id = ".$HotelRst->hotel_id."
                                                                   ORDER BY folio_date ASC, folio_type, folio_id ASC
                                                                  ");
                                            /*--}}
                                            @foreach($rsrvQry as $vals)
                                               @if($vals->folio_order == 0 && $vals->folio_type == 0)
                                                <tr class="even pointer paymentModeColor_{{$paymentModeArray[$vals->folio_pay_type]}}">
                                                    <td class=" ">{!!date('m/d/Y h:i A', strtotime($vals->created_at))!!}</td>
                                                    <td class=" ">{!!$vals->folio_pay_type!!}</td>
                                                    <td class=" ">{!!date('m/d/Y', strtotime($vals->folio_date))!!} {!!$vals->folio_comment!!}</td>
                                                    <td class=" last">( ${!!$vals->folio_amount!!} )</td>
                                                </tr>
                                                @elseif($vals->folio_type == 2)
                                                <tr class="even pointer paymentModeColor_adjustment">
                                                    <td class=" ">{!!date('m/d/Y h:i A', strtotime($vals->created_at))!!}</td>
                                                    <td class=" ">{!!$vals->folio_pay_type!!}</td>
                                                    <td class=" ">{!!date('m/d/Y', strtotime($vals->folio_date))!!} {!!$vals->folio_comment!!}</td>
                                                    <td class=" last">- ${!!$vals->folio_amount!!}</td>
                                                </tr>
                                                @else
                                                <tr class="even pointer{!!($vals->folio_type == 2)?' paymentModeColor_adjustment':''!!}">
                                                    <td class=" ">{!!date('m/d/Y h:i A', strtotime($vals->created_at))!!}</td>
                                                    <td class=" ">{!!$vals->folio_pay_type!!}</td>
                                                    <td class=" ">{!!date('m/d/Y', strtotime($vals->folio_date))!!} {!!$vals->folio_comment!!}</td>
                                                    <td class=" last">{{($vals->folio_pay_type == 'Discount')?'- ':''}}${!!$vals->folio_amount!!}</td>
                                                </tr>
                                                @endif
                                                @if($vals->folio_order == 0 && $vals->folio_type == 0)
                                                {{--*/ $amountDeposit = $amountDeposit+$vals->folio_amount; /*--}}
                                                @elseif($vals->folio_order == 0 && $vals->folio_type == 2)
                                                {{--*/ $amountBalance = $amountBalance+$vals->folio_amount; /*--}}
                                                @elseif($vals->folio_type == 2)
                                                {{--*/ $amountDeposit = $amountDeposit+$vals->folio_amount; /*--}}
                                                @else
                                                {{--*/ $amountBalance = $amountBalance+$vals->folio_amount; /*--}}
                                                @endif
                                            @endforeach
                                            {{--*/ $remainBalance = $amountBalance-$amountDeposit; /*--}}
                                            <tr class="tableFooter">
                                                <td class=" "></td>
                                                <td class=" "></td>
                                                <td class=" ">Balance</td>
                                                <td class=" last">{!!($remainBalance < 0)?'- $'.($remainBalance*-1):'$'.$remainBalance!!}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr class="backgroundNone">
                                <td width="70%" colspan="2" class="zeroPadd borderNone paddingTop10">
                                    Thank you for staying with {{$HotelRst->hotel_name}} !!!<br /> We are looking forward to welcoming you on your next stay with us. :)
                                </td>
                                <td width="30%" class="zeroPadd borderNone AlgnRight" style="position:relative; vertical-align:bottom; opacity:0.4;">
                                    <div style="float:right; width:120px; font-size:10px; position:absolute; right:5px; bottom:20px;">Powered By</div>
                                    {!!Html::image('build/images/logo.jpg', 'MMK Logo', array( 'title' => 'MMK Logo', 'width' => 120, 'height' => 33 ))!!}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <button class="btn btn-primary marginTop20 margin5" onClick="printDiv('printableArea')"><i class="fa fa-print"></i> Print</button>
                    <a class="btn btn-default marginTop20 margin5" href="{{url('guest-folio', $RsrvRst->room_reservation_id)}}"><i class="fa fa-chevron-left"></i> Back</a>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function printDiv(divName) {
		 var printContents = document.getElementById(divName).innerHTML;
		 var originalContents = document.body.innerHTML;
		 document.body.innerHTML = printContents;
		 window.print();
		 document.body.innerHTML = originalContents;
		 window.location.reload();
	}
</script>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/datatables.net/js/jquery.dataTables.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')!!}
    {!!Html::script('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')!!}
    {!!Html::script('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')!!}
    {!!Html::script('vendors/datatables.net-buttons/js/buttons.flash.min.js')!!}
    {!!Html::script('vendors/datatables.net-buttons/js/buttons.html5.min.js')!!}
    {!!Html::script('vendors/jszip/dist/jszip.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/pdfmake.min.js')!!}
   
@stop  

@section('jQuery')
<script>
$(document).ready(function() {
	var handleDataTableButtons = function() {
		if ($("#datatable-buttons").length) {
			$("#datatable-buttons").DataTable({
				"aaSorting": [[2, 'asc']],
				"bLengthChange": false,
				"bFilter": false,
				"bInfo": false,
				"bPaginate": false,
				dom: "Bfrtip",
				buttons: [
					{
					  extend: "copy",
					  className: "btn-sm"
					},
					{
					  extend: "csv",
					  className: "btn-sm"
					},
					{
					  extend: "pdf",
					  className: "btn-sm"
					},
				],
				responsive:true
			});
		}
	};

	TableManageButtons = function() {
		"use strict";
			return {
			init: function() {
			  	handleDataTableButtons();
			}
		};
	}();
	TableManageButtons.init();
});
</script>
@stop