@extends('layouts.layout')

@section('title')
	Edit Guest Information
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Edit Guest Information</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	@if (count($errors))
                <ul class="errorFormMessage">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <form id="demo-form2" action="{!!url('guests', $Results->guest_id)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                    {!!method_field('PATCH')!!}
               		{!!csrf_field()!!}
                    	<div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Title</label>
                            <div class="col-md-3 col-sm-3">
                            <select name="guest_title" class="form-control col-md-2 col-xs-12 zeroPadd">
                                <option value="">Title</option>
                                <option value="1"{!!($Results->guest_title == 1)?" Selected":"";!!}>Mr.</option>
                                <option value="2"{!!($Results->guest_title == 2)?" Selected":"";!!}>Mrs.</option>
                                <option value="3"{!!($Results->guest_title == 3)?" Selected":"";!!}>Miss</option>
                            </select>
                            </div>
                        </div>                                    
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">First Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_fname" id="guest_fname" value="{!!$Results->guest_fname or old('guest_fname')!!}" placeholder="First Name" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Middle Name / Initial</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_mname" id="guest_mname" value="{!!$Results->guest_mname or old('guest_mname')!!}" placeholder="Middle Name" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Last Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_lname" id="guest_lname" value="{!!$Results->guest_lname or old('guest_lname')!!}" placeholder="Last Name" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Date Of Birth <span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="guest_dob" id="guest_dob" value="{!!$Results->guest_dob or old('guest_dob')!!}" placeholder="mm/dd/yyyy" class="date-picker form-control col-md-7 col-xs-12" required="required" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Address</label>
                            <div class="col-md-6 col-sm-6">
                            <textarea name="guest_address" id="guest_address" placeholder="Address" class="form-control col-md-7 col-xs-12">{!!$Results->guest_address or old('guest_address')!!}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Zip Code</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_zip" id="guest_zip" value="{!!$Results->guest_zip or old('guest_zip')!!}" placeholder="Zip Code" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">City</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_city" id="guest_city" value="{!!$Results->guest_city or old('guest_city')!!}" placeholder="City" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>    
                        <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">State</label>
                                <div class="col-md-4 col-sm-6">
                                <select name="guest_state" id="guest_state" class="form-control col-md-2 col-xs-12 zeroPadd">
                                    <option value="">Select State</option>
                                    @foreach(App\ZipCityState::groupBy('zip_state')->get() as $stateVal)
                                    <option value="{{$stateVal->zip_state}}"{!!($Results->guest_state == $stateVal->zip_state)?"Selected":""!!}>{{$stateVal->zip_state}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>                    
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Email</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="email" name="guest_email" id="guest_email" value="{!!$Results->guest_email or old('guest_email')!!}" placeholder="Email" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Cell Phone Number</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_phone" id="guest_phone" value="{!!$Results->guest_phone or old('guest_phone')!!}" placeholder="Cell Phone Number" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Nationality</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_nation" id="guest_nation" value="{!!$Results->guest_nation or old('guest_nation')!!}" placeholder="Nationality" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">ID Type <span class="required">*</span>
                            </label>
                            <div class="col-md-2 col-sm-6">
                            <select name="guest_idproof" id="guest_idproof" class="form-control col-md-2 col-xs-12 zeroPadd" onchange="getIdProofType(this.value);" required="required">
                                <option value="">ID Type</option>
                                <option value="1"{!!($Results->guest_idproof == 1)?" Selected":"";!!}>Passport</option>
                                <option value="2"{!!($Results->guest_idproof == 2)?" Selected":"";!!}>Driving Licence</option>
                                <option value="3"{!!($Results->guest_idproof == 3)?" Selected":"";!!}>State issued Id</option>
                                <option value="4"{!!($Results->guest_idproof == 4)?" Selected":"";!!}>Other</option>
                            </select>
                            </div>
                            <div class="col-md-2 col-sm-6" id="guest_idproof_Box"{!!($Results->guest_idproof == 4)?'':' style="display:none;"';!!}>
                            <input type="text" name="guest_idproof_other" id="guest_idproof_other" value="{!!$Results->guest_idproof_other or old('guest_idproof_other')!!}" placeholder="Other Id Proof" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">ID Number <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_proofno" id="guest_proofno" value="{!!$Results->guest_proofno or old('guest_proofno')!!}" placeholder="ID Number" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Group Name</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_group" id="guest_group" value="{!!$Results->guest_group or old('guest_group')!!}" placeholder="Group Name" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Vehicle Details</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="guest_vehicle" id="guest_vehicle" value="{!!$Results->guest_vehicle or old('guest_vehicle')!!}" placeholder="Vehicle Details" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop24">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">UPDATE CHANGES</button>
                                <button type="reset" class="btn btn-primary">CANCEL CHANGES</button>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function getIdProofType(str){
		if(str == 4){
			$('#guest_idproof_Box').show();
		}else{
			$('#guest_idproof_Box').hide();
			$('#guest_idproof_other').val("");
		}
	}
</script>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
@stop  

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	$('#guest_dob').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() > now.valueOf() ? 'disabled' : '';
		}
	});
	
	$("#guest_zip").on("keyup", function() {
		var zipCode = $(this).val().substring(0, 5);
		
		if (zipCode.length == 5 && /^[0-9]+$/.test(zipCode))
		{
			$("#main_container_Loading").show();
			$("#main_container_overlay").show();
			$("#guest_email").focus();
			$.ajax({
				url: "{{ url('/getCityData') }}",
				type: 'POST',
				data:{"zipCode":zipCode, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					if ( msg.status === 'success' ) {
						$("#guest_city").val(msg.zip_city);
						$("#guest_state").val(msg.zip_state);
						
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						$("#guest_city").val(msg.zip_city);
						$("#guest_state").val(msg.zip_state);
						
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					if ( data.status === 422 ) {
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.error('Cannot find the result');
					}
				}
			});
			return false;
		}
	});
	
	$('#search_phone').on('keydown', function(e) {
		if(e.which == 13){
			findGuestFunction();
		}
	});
	$('#search_name').on('keydown', function(e) {
		if(e.which == 13){
			findGuestFunction();
		}
	});
	$('#search_email').on('keydown', function(e) {
		if(e.which == 13){
			findGuestFunction();
		}
	});
	$('.findButton').on('click', function(e) {
		findGuestFunction();
	});
	
	$(document).on('click', '.guestButton', function(e){
		var guestId  = $(this).attr('data-id');
		$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		$.ajax({
			url: "{{ url('/fillGuest') }}",
			type: 'POST',
			data:{"guestId":guestId, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				if ( msg.status === 'success' ) {
					$("#guest_id").val(msg.guest_id);
					
					var objTitle = document.getElementById("guest_title");
					setTitleValue(objTitle, msg.guest_title);
					function setTitleValue(selectObj, valueToSet) {
						for (var i = 0; i < selectObj.options.length; i++) {
							if (selectObj.options[i].value == valueToSet) {
								selectObj.options[i].selected = true;
								return;
							}
						}
					}
					
					$("#guest_fname").val(msg.guest_fname);
					$("#guest_mname").val(msg.guest_mname);
					$("#guest_lname").val(msg.guest_lname);
					$("#guest_address").val(msg.guest_address);
					$("#guest_zip").val(msg.guest_zip);
					$("#guest_city").val(msg.guest_city);
					
					var objState = document.getElementById("guest_state");
					setStateValue(objState, msg.guest_state);
					function setStateValue(selectObj, valueToSet) {
						for (var i = 0; i < selectObj.options.length; i++) {
							if (selectObj.options[i].value == valueToSet) {
								selectObj.options[i].selected = true;
								return;
							}
						}
					}
					
					$("#guest_email").val(msg.guest_email);
					$("#guest_phone").val(msg.guest_phone);
					$("#guest_dob").val(msg.guest_dob);
					$("#guest_nation").val(msg.guest_nation);
					
					var objSelect = document.getElementById("guest_idproof");
					setSelectedValue(objSelect, msg.guest_idproof);
					function setSelectedValue(selectObj, valueToSet) {
						for (var i = 0; i < selectObj.options.length; i++) {
							if (selectObj.options[i].value == valueToSet) {
								selectObj.options[i].selected = true;
								return;
							}
						}
					}
					
					if(msg.guest_idproof_other){
						$("#guest_idproof_other").val(msg.guest_idproof_other);
						$("#guest_idproof_Box").show();
					}
					$("#guest_proofno").val(msg.guest_proofno);
					
					/*var objCompany = document.getElementById("guest_company");
					setCompanyValue(objCompany, msg.guest_company);
					function setCompanyValue(selectObj, valueToSet) {
						for (var i = 0; i < selectObj.options.length; i++) {
							if (selectObj.options[i].value == valueToSet) {
								selectObj.options[i].selected = true;
								return;
							}
						}
					}*/
					
					$("#guest_group").val(msg.guest_group);
					$("#guest_vehicle").val(msg.guest_vehicle);
					
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.success( msg.response );
				}
				if ( msg.status === 'error' ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.error( msg.response );
				}
			},
			error: function( data ) {
				if ( data.status === 422 ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.error('Cannot find the result');
				}
			}
		});
		return false;
	});
});
function findGuestFunction()
{
	var searchPhone = $("#search_phone").val();
	var searchName  = $("#search_name").val();
	var searchEmail = $("#search_email").val();
	$("#main_container_Loading").show();
	$("#main_container_overlay").show();
	$.ajax({
		url: "{{ url('/findGuest') }}",
		type: 'POST',
		data:{"searchPhone":searchPhone, "searchName":searchName, "searchEmail":searchEmail, "_token": "{{ csrf_token() }}" },
		dataType: 'json',
		success: function( msg ) {
			if ( msg.status === 'success' ) {
				$("#resultGuest").show();
				$("#resultGuest").html(msg.response);
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
			}
			if ( msg.status === 'error' ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				toastr.error( msg.response );
			}
		},
		error: function( data ) {
			if ( data.status === 422 ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				toastr.error('Cannot find the result');
			}
		}
	});
	return false;
}
function getRateBehalfCompany(profile_id)
{
	var reserv_night = $("#reserv_night").val();
	var arrival = $("#arrival").val();
	var departure  = $("#departure").val();
	var reserve_quantitiy = $("#reserve_quantitiy").val();
	var modified_price  = $("#modified_price").val();
	var discount = $("#discount").val();
	$("#main_container_Loading").show();
	$("#main_container_overlay").show();
	$.ajax({
		url: "{{ url('/rate_behalf_company') }}",
		type: 'POST',
		data:{"profile_id":profile_id, "arrival":arrival, "departure":departure, "reserv_night":reserv_night, "reserve_quantitiy":reserve_quantitiy, "modified_price":modified_price, "discount":discount, "_token": "{{ csrf_token() }}" },
		dataType: 'json',
		success: function( msg ) {
			if ( msg.status === 'success' ) {
				$("#behalfCompany").html(msg.response);
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				toastr.success( 'success' );
			}
			if ( msg.status === 'error' ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				toastr.error( msg.response );
			}
		},
		error: function( data ) {
			if ( data.status === 422 ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				toastr.error('Cannot find the result');
			}
		}
	});
	return false;
}
</script>
@stop      
