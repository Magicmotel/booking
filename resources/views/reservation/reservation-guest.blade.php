@extends('layouts.layout')

@section('title')
	Guest Information
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Guest Information</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
						<?	$reserve_QuantitiyArray = $modified_PriceArray = $aArray = $enter_AdultArray = $enter_ChildArray = $enter_RoomArray = $discountArray = array();
							$modified_Price = $reserve_Quantitiy = $enter_Adult = $enter_Child = $enter_Room = $discounT = "";
                            foreach($request->reserve_quantitiy as $key=>$val)
							{
                                if($val){
									$modified_PriceArray[] = $request->modified_rate[$key];
                                    $reserve_QuantitiyArray[] = $val;
									$aArray = explode("-",$val);
									$roomTypeId = $aArray[0];
									
									$enter_AdultVal = $enter_ChildVal = $enter_RoomVal = $discountVal = "";
									if($request->{"enter_adult_".$roomTypeId}){
										$enter_AdultVal = implode("-",$request->{"enter_adult_".$roomTypeId});
									}
									if($request->{"enter_child_".$roomTypeId}){
										$enter_ChildVal = implode("-",$request->{"enter_child_".$roomTypeId});
									}
									if($request->{"enter_room_".$roomTypeId}){
										$enter_RoomVal = implode("-",$request->{"enter_room_".$roomTypeId});
									}
									if($request->{"discount_".$roomTypeId}){
										$discountVal = implode("-",$request->{"discount_".$roomTypeId});
									}
									
									$enter_AdultArray[] = $enter_AdultVal;
                                    $enter_ChildArray[] = $enter_ChildVal;
									$enter_RoomArray[]  = $enter_RoomVal;
									$discountArray[]    = $discountVal;
                                }
                            }
							$modified_Price    = implode(",", $modified_PriceArray);
                            $reserve_Quantitiy = implode(",", $reserve_QuantitiyArray);
							$enter_Adult       = implode(",", $enter_AdultArray);
                            $enter_Child       = implode(",", $enter_ChildArray);
                            $enter_Room        = implode(",", $enter_RoomArray);
                            $discounT          = implode(",", $discountArray);
						?>
                        <form id="demo-form2" action="{!!url('reservation')!!}" method="post" data-parsley-validate="" class="form-horizontal form-label-left">
                            {!!csrf_field()!!}
                            <input type="hidden" name="arrival" id="arrival" value="{!!$request->arrival or date('m/d/Y')!!}">
                            <input type="hidden" name="departure" id="departure" value="{!!$request->departure or  date('m/d/Y', strtotime('+1 days'))!!}">
                            <input type="hidden" name="reserv_night" id="reserv_night" value="{!!$request->reserv_night!!}">
                         	<input type="hidden" name="company_profile" id="company_profile" value="{!!$request->company_profile!!}">
                            <input type="hidden" name="reserve_quantitiy" id="reserve_quantitiy" value="{!!$reserve_Quantitiy!!}">
                            <input type="hidden" name="modified_price" id="modified_price" value="{!!$modified_Price!!}">
                            <input type="hidden" name="company_price" id="company_price" value="">
                            <input type="hidden" name="enter_adult" value="{!!$enter_Adult!!}">
                            <input type="hidden" name="enter_child" value="{!!$enter_Child!!}">
                            <input type="hidden" name="enter_room" value="{!!$enter_Room!!}">
                            <input type="hidden" name="discount" id="discount" value="{!!$discounT!!}">
                            <input type="hidden" name="reserv_type" id="reserv_type" value="{{$request->reserv_type}}">
                            <input type="hidden" name="guest_id" id="guest_id" value="">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Company Profile</label>
                                <div class="col-md-6 col-sm-6">
                                <select name="company_guest" id="company_guest" class="form-control col-md-2 col-xs-12 LR_Padd5" disabled>
                                    <option value="0">None</option>
                                    @foreach(App\CompanyProfile::where('hotel_id', $Hotel_ID)->get() as $companyVal)
                                    <option value="{{$companyVal->profile_id}}"{!!($request->company_profile == $companyVal->profile_id)?' Selected':''!!}>{{$companyVal->company_name}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Title</label>
                                <div class="col-md-3 col-sm-3">
                                <select name="guest_title" id="guest_title" class="form-control col-md-2 col-xs-12 LR_Padd5">
                                    <option value="">Title</option>
                                    <option value="1">Mr.</option>
                                    <option value="2">Mrs.</option>
                                    <option value="3">Miss</option>
                                </select>
                                </div>
                            </div>                                    
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">First Name <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                <input type="text" name="guest_fname" id="guest_fname" placeholder="First Name" class="form-control col-md-7 col-xs-12" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3">Middle Name / Initial</label>
                                <div class="col-md-6 col-sm-6">
                                <input type="text" name="guest_mname" id="guest_mname" placeholder="Middle Name" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Last Name <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                <input type="text" name="guest_lname" id="guest_lname" placeholder="Last Name" class="form-control col-md-7 col-xs-12" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Date Of Birth</label>
                                <div class="col-md-3 col-sm-6">
                                <input type="text" name="guest_dob" id="guest_dob" placeholder="mm/dd/yyyy" class="date-picker form-control col-md-7 col-xs-12" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Address <span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                <textarea name="guest_address" id="guest_address" placeholder="Address" class="form-control col-md-7 col-xs-12" required="required"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Country <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6">
                                <select name="guest_country" id="guest_country" class="form-control col-md-2 col-xs-12 LR_Padd5" required="required">
                                    @foreach(App\ZipCountry::get() as $countryVal)
                                    <option value="{{$countryVal->country_id}}"{{($countryVal->country_id==1)?' Selected':''}}>{{$countryVal->country_name}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">ZIP Code <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6">
                                <input type="text" name="guest_zip" id="guest_zip" placeholder="ZIP Code" maxlength="5" class="form-control col-md-7 col-xs-12" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">City <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6">
                                <input type="text" name="guest_city" id="guest_city" placeholder="City" class="form-control col-md-7 col-xs-12" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">State <span class="required">*</span></label>
                                <div class="col-md-4 col-sm-6">
                                <input type="text" name="guest_state" id="guest_state" placeholder="State" class="form-control col-md-7 col-xs-12" required="required" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Email</label>
                                <div class="col-md-6 col-sm-6">
                                <input type="email" name="guest_email" id="guest_email" placeholder="Email" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Cell Phone Number</label>
                                <div class="col-md-6 col-sm-6">
                                <input type="text" name="guest_phone" id="guest_phone" placeholder="Cell Phone Number" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Nationality</label>
                                <div class="col-md-6 col-sm-6">
                                <input type="text" name="guest_nation" id="guest_nation" placeholder="Nationality" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">ID Type</label>
                                <div class="col-md-2 col-sm-6">
                                <select name="guest_idproof" id="guest_idproof" class="form-control col-md-2 col-xs-12 LR_Padd5" onchange="getIdProofType(this.value);">
                                    <option value="">ID Type</option>
                                    <option value="1">Passport</option>
                                    <option value="2">Driving Licence</option>
                                    <option value="3">State issued Id</option>
                                    <option value="4">Other</option>
                                </select>
                                </div>
                                <div class="col-md-2 col-sm-6 displayNone" id="guest_idproof_Box">
                                <input type="text" name="guest_idproof_other" id="guest_idproof_other" placeholder="Other Id Proof" class="form-control col-md-7 col-xs-12" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">ID Number</label>
                                <div class="col-md-6 col-sm-6">
                                <input type="text" name="guest_proofno" id="guest_proofno" placeholder="ID Number" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Company Name</label>
                                <div class="col-md-6 col-sm-6">
                                <input type="text" name="guest_company" id="guest_company" placeholder="Company Name" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Group Name</label>
                                <div class="col-md-6 col-sm-6">
                                <input type="text" name="guest_group" id="guest_group" placeholder="Group Name" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Vehicle Details</label>
                                <div class="col-md-6 col-sm-6">
                                <input type="text" name="guest_vehicle" id="guest_vehicle" placeholder="Vehicle Details" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>
                            <div class="form-group">
                            	<label class="control-label col-md-3 col-sm-3">Tax Exempt</label> 
                                <div class="col-md-6 col-sm-6">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default btn-toggleYes">
                                          <input type="radio" name="guest_tax" id="guest_tax1" placeholder="" value="1"> Yes
                                        </label>
                                        <label class="btn btn-default btn-toggleNo active">
                                          <input type="radio" name="guest_tax" id="guest_tax2" placeholder="" value="0" checked="checked"> No
                                        </label>
                                    </div>
                                </div>
                            </div>
                           	<div class="form-group form-group-last marginTop24">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-plus-square-o"></i> Create Reservation</button>
                                    <a class="btn btn-default marginLeft5" href="{{URL::previous()}}"><i class="fa fa-chevron-left"></i> Back</a>
                                </div>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
            	<div class="x_panel"><div class="form-horizontal form-label-left">
                    <span class="section">GET REGISTERED USER INFO:</span>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 zeroPadd">
                        <input type="text" name="search_phone" id="search_phone" placeholder="Search by Cell Phone Number" class="form-control col-md-7 col-xs-12">
                        <?php /*?><input type="text" name="country" id="autocomplete-custom-append" class="form-control col-md-10"/><?php */?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 zeroPadd">
                        <input type="text" name="search_name" id="search_name" placeholder="Search by Name" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 zeroPadd">
                        <input type="text" name="search_email" id="search_email" placeholder="Search by Email" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group form-group-last marginTop10">
                       	<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                            <div class="btn btn-success findButton"><i class="fa fa-search"></i> Search Guest</div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 zeroPadd displayNone" id="resultGuest"></div>
                </div></div>
                <div id="behalfCompany">
                {{--*/ $fromDate = date('Y-m-d', strtotime($request->arrival)); $toDate = date('Y-m-d', strtotime($request->departure.'-1 days')); /*--}}
                {{--*/ $profile_ID = $request->company_profile; /*--}}
                {{--*/ $room_type_id = $rsrvTaxBool = $no_room = $totalAmount = $lastAmount = $RoomRates = 0; $valsArray = array(); /*--}}
                {{--*/ $QuantitiyArray = explode(",", $reserve_Quantitiy); /*--}}
                {{--*/ $PriceArray     = explode(",", $modified_Price); /*--}}
                {{--*/ $DiscountArray  = explode(",", $discounT); /*--}}
                @foreach($QuantitiyArray as $key=>$vals)
                    {{--*/
                    $TotalTaxAmount = $TotalRoomAmount = 0;
                    $valsArray = explode("-", $vals);
                    $room_type_id = $valsArray[0];
                    $no_room 	  = $valsArray[1];
                    $room_Rate    = round($PriceArray[$key], 2);
                    
                    $discountIdArray = explode("-", $DiscountArray[$key]);
                    $discountValues = implode(",", $discountIdArray);
                   	
                        
                    $RoomTypeRst = App\Roomtype::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $room_type_id] ])->first();
                    $RoomTypes = $RoomTypeRst->room_type;
                        
                   	if($profile_ID && !$room_Rate)
                    {
						$CompanyPrice = App\CompanyProfileRate::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $room_type_id], ['profile_id', $profile_ID] ])->first();
                        $room_Rate    = $CompanyPrice->company_rate;
                    }
                        
                    if($profile_ID){
                        $companyTaxRst = App\CompanyProfile::where('profile_id', $profile_ID)->first();
                        $rsrvTaxBool   = $companyTaxRst->company_tax;
                    }
                        
                    $folioRateQry = DB::select("SELECT room_rate_price as fRoomRate, room_rate_date as fRoomDate FROM room_rate_option
                                                WHERE room_type_id = ".$room_type_id." AND hotel_id = ".$Hotel_ID." AND
                                                (room_rate_date BETWEEN '".$fromDate."' AND '".$toDate."') ORDER BY room_rate_date ASC
                                               ");
                    foreach($folioRateQry as $folioRateRst)
                    {
                        $DiscountCodeString = ""; $discountWiseRate = 0;
                        $dayWiseRate = ($room_Rate > 0)?$room_Rate:$folioRateRst->fRoomRate;
                        $dayWiseDate = $folioRateRst->fRoomDate;
                        
                        if($discountValues)
                        {
                            $discountQry = DB::select("SELECT discount_id as DiscountId, discount_code as DiscountCode, discount_value as DiscountValue,
                                                       discount_type as DiscountType
                                                       FROM discounts
                                                       WHERE hotel_id = ".$Hotel_ID." AND discount_id IN (".$discountValues.")
                                                       ORDER BY discount_code ASC
                                                      ");
                            $discountWiseRate = 0; $DiscountCodeArray = array();
                            foreach($discountQry as $discountRst)
                            {
                                if($discountRst->DiscountType == 2){
                                    $discountWiseRate = $discountWiseRate+$discountRst->DiscountValue;
                                }else{
                                    $discountWiseRate = $discountWiseRate+(($dayWiseRate*$discountRst->DiscountValue)/100);
                                }
                                $DiscountCodeArray[] = $discountRst->DiscountCode;
                            }
                            $DiscountCodeString = implode(", ",$DiscountCodeArray);
                            $discountWiseRate = round($discountWiseRate, 2);
                        }
                        
                        $FinalRoomRate_wo_Tax = $dayWiseRate - $discountWiseRate;
                        if($FinalRoomRate_wo_Tax<=0){
                            $FinalRoomRate_wo_Tax = 1;
                        }
                        $TotalRoomAmount = $TotalRoomAmount+$FinalRoomRate_wo_Tax;
                        
                        if($rsrvTaxBool==0)
                        {
                            $folioTaxQry = DB::select("SELECT tax_type as fTaxType, tax_value as fTaxValue, tax_value_type as fValueType
                                                       FROM taxes WHERE hotel_id = ".$Hotel_ID." AND status = 0 AND
                                                       NOT( (tax_end != '0000-00-00' AND tax_end < '".$dayWiseDate."') OR
                                                            (tax_start != '0000-00-00' AND tax_start >= '".$dayWiseDate."') OR
                                                            (tax_end = '0000-00-00' AND tax_start > '".$dayWiseDate."'AND tax_start < '".$dayWiseDate."') OR
                                                            (tax_start = '0000-00-00' AND tax_end < '".$dayWiseDate."' AND tax_end > '".$dayWiseDate."')
                                                          )
                                                      ");
                            foreach($folioTaxQry as $folioTaxRst)
                            {
                                if($folioTaxRst->fValueType == 1){
                                    $TotalTaxAmount = $TotalTaxAmount+( round((($FinalRoomRate_wo_Tax*$folioTaxRst->fTaxValue)/100), 2) );
                                }elseif($folioTaxRst->fValueType == 2){
                                    $TotalTaxAmount = $TotalTaxAmount+( round($folioTaxRst->fTaxValue, 2) );
                                }
                            }
                       	}
                 	}
                    $total_Rate = $TotalRoomAmount+$TotalTaxAmount;
                    /*--}}
                    <div class="x_panel">                        
                        <span class="section uPPerLetter">{{$RoomTypes}}</span>
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingBottom5">
                            <div class="col-md-8 zeroPadd">Room Rate ({{$request->reserv_night}} Nights)</div>
                            <div class="col-md-4 zeroPadd alignTxtRight">${{$TotalRoomAmount}}</div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingBottom5">
                            <div class="col-md-8 zeroPadd">Taxes</div>                                
                            <div class="col-md-4 zeroPadd alignTxtRight">${{$TotalTaxAmount}}</div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingBottom5">
                            <div class="col-md-8 zeroPadd">Room Total Rate</div>                                
                            <div class="col-md-4 zeroPadd alignTxtRight">${{$total_Rate}}</div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingBottom5">
                            <div class="col-md-8 zeroPadd">Total Rooms</div>                                
                            <div class="col-md-4 zeroPadd alignTxtRight">{{$no_room}}</div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 priceHighlight">
                            <div class="col-md-8 zeroPadd">Total Amount</div>                                
                            <div class="col-md-4 zeroPadd alignTxtRight">${{$totalAmount = ($total_Rate*$no_room)}}</div>
                        </div>
                        {{--*/ $lastAmount = $lastAmount+$totalAmount /*--}}
                    </div>
                @endforeach
                <div class="x_panel">
                    <div class="col-md-12 col-sm-12 col-xs-12 LstPriceHighlight">
                        <div class="col-md-8 zeroPadd">Payable Amount</div>                                
                        <div class="col-md-4 zeroPadd alignTxtRight">${{$lastAmount}}</div>
                    </div>
               	</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function getIdProofType(str){
		if(str == 4){
			$('#guest_idproof_Box').show();
			$("#guest_idproof_other").prop("required", true);
			$("#guest_idproof_other").val("");
		}else{
			$('#guest_idproof_Box').hide();
			$("#guest_idproof_other").prop("required", false);
			$('#guest_idproof_other').val("");
		}
	}
	function disableBackButton()
	{
		window.history.forward();
	}
	setTimeout("disableBackButton()", 0);
</script>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
@stop  

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var checkin = $('#guest_dob').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() > now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		checkin.hide();
	}).data('datepicker');
	
	$("#guest_zip").on("keyup", function() {
		var zipCode  = $(this).val().substring(0, 5);
		var zipCntry = $('#guest_country').val();
		if (zipCode.length == 5 && /^[0-9]+$/.test(zipCode) && zipCntry == 1)
		{
			$("#main_container_Loading").show();
			$("#main_container_overlay").show();
			$("#guest_email").focus();
			$.ajax({
				url: "{{ url('/getCityData') }}",
				type: 'POST',
				data:{"zipCode":zipCode, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					if ( msg.status === 'success' ) {
						$("#guest_city").val(msg.zip_city);
						$("#guest_state").val(msg.zip_state);
						
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						$("#guest_city").val(msg.zip_city);
						$("#guest_state").val(msg.zip_state);
						
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					if ( data.status === 422 ) {
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.error('Cannot find the result');
					}
				}
			});
			return false;
		}
	});
	
	$('#search_phone').on('keydown', function(e) {
		if(e.which == 13){
			findGuestFunction();
		}
	});
	$('#search_name').on('keydown', function(e) {
		if(e.which == 13){
			findGuestFunction();
		}
	});
	$('#search_email').on('keydown', function(e) {
		if(e.which == 13){
			findGuestFunction();
		}
	});
	$('.findButton').on('click', function(e) {
		findGuestFunction();
	});
	$('#guest_country').on('change', function(e) {
		$('#guest_zip').val();
		$('#guest_city').val();
		$('#guest_state').val();
	});
	$(document).on('click', '.guestButton', function(e){
		var guestId  = $(this).attr('data-id');
		$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		$.ajax({
			url: "{{ url('/fillGuest') }}",
			type: 'POST',
			data:{"guestId":guestId, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				if ( msg.status === 'success' ) {
					$("#guest_id").val(msg.guest_id);
					
					var objTitle = document.getElementById("guest_title");
					setTitleValue(objTitle, msg.guest_title);
					function setTitleValue(selectObj, valueToSet) {
						for (var i = 0; i < selectObj.options.length; i++) {
							if (selectObj.options[i].value == valueToSet) {
								selectObj.options[i].selected = true;
								return;
							}
						}
					}
					
					$("#guest_fname").val(msg.guest_fname);
					$("#guest_mname").val(msg.guest_mname);
					$("#guest_lname").val(msg.guest_lname);
					$("#guest_address").val(msg.guest_address);
					
					var objCoutry = document.getElementById("guest_country");
					setCountryValue(objCoutry, msg.guest_country);
					function setCountryValue(selectObj, valueToSet) {
						for (var i = 0; i < selectObj.options.length; i++) {
							if (selectObj.options[i].value == valueToSet) {
								selectObj.options[i].selected = true;
								return;
							}
						}
					}
					$("#guest_zip").val(msg.guest_zip);
					$("#guest_city").val(msg.guest_city);
					$("#guest_state").val(msg.guest_state);
					
					$("#guest_email").val(msg.guest_email);
					$("#guest_phone").val(msg.guest_phone);
					$("#guest_dob").val(msg.guest_dob);
					$("#guest_nation").val(msg.guest_nation);
					
					var objSelect = document.getElementById("guest_idproof");
					setSelectedValue(objSelect, msg.guest_idproof);
					function setSelectedValue(selectObj, valueToSet) {
						for (var i = 0; i < selectObj.options.length; i++) {
							if (selectObj.options[i].value == valueToSet) {
								selectObj.options[i].selected = true;
								return;
							}
						}
					}
					
					if(msg.guest_idproof_other){
						$("#guest_idproof_other").val(msg.guest_idproof_other);
						$("#guest_idproof_Box").show();
					}
					$("#guest_proofno").val(msg.guest_proofno);
					$("#guest_company").val(msg.guest_company);
					$("#guest_group").val(msg.guest_group);
					$("#guest_vehicle").val(msg.guest_vehicle);
					if(msg.guest_tax == 1){
						$("#guest_tax1").prop("checked", true);
						$("#guest_tax2").prop("checked", false);
						$(".btn-toggleNo").removeClass("active");
						$(".btn-toggleYes").addClass("active");
					}else{
						$("#guest_tax1").prop("checked", false);
						$("#guest_tax2").prop("checked", true);
						$(".btn-toggleYes").removeClass("active");
						$(".btn-toggleNo").addClass("active");
					}
					
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.success( msg.response );
				}
				if ( msg.status === 'error' ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.error( msg.response );
				}
			},
			error: function( data ) {
				if ( data.status === 422 ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.error('Cannot find the result');
				}
			}
		});
		return false;
	});
});
function findGuestFunction()
{
	var searchPhone = $("#search_phone").val();
	var searchName  = $("#search_name").val();
	var searchEmail = $("#search_email").val();
	$("#main_container_Loading").show();
	$("#main_container_overlay").show();
	$.ajax({
		url: "{{ url('/findGuest') }}",
		type: 'POST',
		data:{"searchPhone":searchPhone, "searchName":searchName, "searchEmail":searchEmail, "_token": "{{ csrf_token() }}" },
		dataType: 'json',
		success: function( msg ) {
			if ( msg.status === 'success' ) {
				$("#resultGuest").show();
				$("#resultGuest").html(msg.response);
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
			}
			if ( msg.status === 'error' ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				toastr.error( msg.response );
			}
		},
		error: function( data ) {
			if ( data.status === 422 ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				toastr.error('Cannot find the result');
			}
		}
	});
	return false;
}
function getRateBehalfCompany(profile_id)
{
	var reserv_night = $("#reserv_night").val();
	var arrival = $("#arrival").val();
	var departure  = $("#departure").val();
	var reserve_quantitiy = $("#reserve_quantitiy").val();
	var modified_price  = $("#modified_price").val();
	var discount = $("#discount").val();
	$("#main_container_Loading").show();
	$("#main_container_overlay").show();
	$.ajax({
		url: "{{ url('/rate_behalf_company') }}",
		type: 'POST',
		data:{"profile_id":profile_id, "arrival":arrival, "departure":departure, "reserv_night":reserv_night, "reserve_quantitiy":reserve_quantitiy, "modified_price":modified_price, "discount":discount, "_token": "{{ csrf_token() }}" },
		dataType: 'json',
		success: function( msg ) {
			if ( msg.status === 'success' ) {
				$("#behalfCompany").html(msg.response);
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				toastr.success( 'success' );
			}
			if ( msg.status === 'error' ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				toastr.error( msg.response );
			}
		},
		error: function( data ) {
			if ( data.status === 422 ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				toastr.error('Cannot find the result');
			}
		}
	});
	return false;
}
</script>
@stop