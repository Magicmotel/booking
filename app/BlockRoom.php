<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockRoom extends Model
{
	protected $fillable= [
		'hotel_id', 'room_assign_id', 'block_from', 'block_to', 'reg_ip', 'added_by'
	];
	protected $primaryKey = 'block_room_id';
	protected $table = 'block_rooms';
}
