@extends('layouts.layout')

@section('title')
	Post Charges
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Post Charges</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	@if (count($errors))
                <ul class="errorFormMessage">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <form id="demo-form2" action="{!!url('guest-folio/post-charge')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                    	{!!csrf_field()!!}
                    	<input type="hidden" name="rsrv_id" id="rsrv_id" value="{{$RsrvRst->room_reservation_id}}" />
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Charge Type <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3">
                            <select name="folio_pay_type" id="folio_pay_type" class="form-control col-md-3 col-xs-12 zeroPadd" required>
                                <option value="">Select Charge Type</option>
                                <option value="1">Room Charge</option>
                                <option value="11">Pet Charge</option>
                                <option value="12">Rollaway Bed</option>
                                <option value="13">Damage Charge</option>
                                <option value="15">Other Charge</option>
                            </select>
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Charge Date <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="folio_date" id="folio_date" value="" placeholder="mm/dd/yyyy" class="date-picker form-control col-md-7 col-xs-12" required="required" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Comments <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="folio_comment" id="folio_comment" value="" placeholder="Enter Comments" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Amount <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6">
                                <span class="input-group-btn fltL widthAuto"><div class="btn btn-primary">$</div></span>
                                <input type="text" name="folio_amount" id="folio_amount" value="" placeholder="Enter Amount" class="form-control width40" required="required" min="0.1" maxlength="3">
                          	</div>
                        </div>
                        <div class="form-group displayNone" id="folio_auto_option">
                            <label class="control-label col-md-3 col-sm-3">Don't auto-post tonight</label>
                            <div class="col-md-6 col-sm-6">
                            <div class="fltL control-value paddingRight5"><input type="checkbox" name="folio_auto" id="folio_auto" value="1" class="js-switch" checked="checked" /></div>
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop24">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Post Charges</button>
                                <button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                                <a href="{{url('guest-folio', $RsrvRst->room_reservation_id)}}" class="btn btn-default"><i class="fa fa-chevron-left"></i> Back</a>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var folio_date = $('#folio_date').datepicker({
		startDate: moment()
	}).on('changeDate', function(ev){
		folio_date.hide();
	}).data('datepicker');
	$('#folio_pay_type').on('change', function(){
		if(this.value == 1){
			$('#folio_auto_option').show();
		}else{
			$('#folio_auto_option').hide();
		}
	});
});
</script>
@stop      
