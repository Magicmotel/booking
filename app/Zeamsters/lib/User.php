<?php

/**
 * This file contains functions related to all type of user functionlity.
 * 
 * User class  
 * 
 */

namespace App\Zeamsters\lib;

//include_once BASE_PATH . DS . 'includes/Logging.php';

use App\Zeamsters\lib\ZeamsterPaymentTransaction as zeamsterApi;
use Exception;
use App\Zeamsters\includes\Logging;

class User extends Exception {

    public $id = '';
    public $account_number = '';
    public $address = '';
    public $auth_token_expire_mins = '';
    public $branding_domain_url = '';
    public $city = '';
    public $contact_id = '';
    public $created_ts = '';
    public $current_date_time = '';
    public $current_login = '';
    public $current_login_ip = '';
    public $date_of_birth = '';
    public $domain_id = '';
    public $email = '';
    public $email_trx_receipt = '';
    public $first_name = '';
    public $last_login_ts = '';
    public $last_name = '';
    public $locale = '';
    public $login_attempts = '';
    public $modified_ts = '';
    public $primary_location_id = '';
    public $requires_new_password = '';
    public $state = '';
    public $status_id = '';
    public $tz = '';
    public $username = '';
    public $user_type_id = '';
    public $zip = '';

    /**
     * Create new record
     * @param object $objData
     * 
     * @return json
     */
    public static function createUserRecord($objData) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/users";
            $request_data = zeamsterApi::convertObjectToArray($objData);
            if (!empty($request_data)) {
                $request = array('user' => array_filter($request_data));
                $request = json_encode($request);

                $response = $objzeamsterApi::send_transaction($request_url, 'POST', null, $request);

                return $response;
            }
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Update exiting user record
     * @param object $objData
     * @param string $user_id
     * 
     * @return json
     */
    public static function updateUserRecord($objData, $user_id) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/users/{$user_id}";
            $request_data = zeamsterApi::convertObjectToArray($objData);
            if (!empty($request_data)) {
                $request = array('users' => array_filter($request_data));
                $request = json_encode($request);
                $response = $objzeamsterApi::send_transaction($request_url, 'PUT', null, $request);
                return $response;
            }
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * This function is use to view self user record

     * @return mixed
     */
    public static function viewSelfUserRecord() {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/users/me";
            $response = $objzeamsterApi::send_transaction($request_url, 'GET', null, null);
            return $response;
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Get / View record
     * @param string $user_id
     * 
     * @return json
     */
    public static function getUserRecord($user_id = null, $filter = null, $perPage = null, $sort = null, $pageNo = null) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            if (!empty($user_id)) {
                $request_url = $api_url . "/users/{$user_id}";
            } else {
                if (!empty($perPage) || !empty($filter) || !empty($sort) || !empty($pageNo)) {
                    $request_url = $api_url . "/users?{$filter}&page_size={$perPage}&sort={$sort}&page={$pageNo}";
                } else {
                    $request_url = $api_url . "/users";
                }
            }

            $response = $objzeamsterApi::send_transaction($request_url, 'GET', null, null);
            return $response;
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Delete user record
     * 
     * @param string $user_id
     * 
     * @return json
     */
    public static function deleteUserRecord($user_id) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/users/{$user_id}";
            $response = $objzeamsterApi::send_transaction($request_url, 'DELETE', null, null);
            return $response;
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

}
