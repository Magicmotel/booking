<?php
namespace App\Zeamsters\includes;
/**
 * Log Class
 */
class Logging {

    /**
     * Manage log in file
     * @param string $message
     * @param string $file
     */
    public static function log($message, $file = NULL) {

        try {
            $logDir = BASE_PATH . DS . 'log';

            /* set default log file */
            if (!isset($file)) {
                $file = "message.log";
            }
            $logFile = $logDir . DS . $file;

            /* Create log directory */
            if (!is_dir($logDir)) {
                mkdir($logDir);
                chmod($logDir, 0750);
            }

            $date = date("Y-m-d H:i:s");
            $file = fopen($logFile, 'a+');

            $message = $date . " : " . $message . "\n";
            fwrite($file, $message);
            fclose($file);
            chmod($logFile, 0640);
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
    }
}
 
