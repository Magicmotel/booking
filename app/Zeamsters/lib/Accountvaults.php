<?php

/**
 * This file contains functions related to create,update, view  and delete
 * for Accountvaults endpoints.
 * 
 * Accountvaults class  
 * 
 */
 
namespace App\Zeamsters\lib;

//include_once BASE_PATH . DS . 'includes/Logging.php';

use App\Zeamsters\lib\ZeamsterPaymentTransaction as zeamsterApi;
use Exception;
use App\Zeamsters\includes\Logging;

class Accountvaults extends Exception {

    public $id = "";
    public $account_number = "";
    public $account_holder_name = "";
    public $account_type = "";
    public $account_vault_api_id = "";
    public $billing_address = "";
    public $billing_zip = "";
    public $card_type = "";
    public $contact_id = "";
    public $created_ts = "";
    public $exp_date = "";
    public $first_six = "";
    public $is_company = "";
    public $last_four = "";
    public $location_id = "";
    public $modified_ts = "";
    public $payment_method = "";
    public $routing = "";
    public $ticket = "";
    public $title = "";
    public $token = "";

    /**
     * Creating an accountvaults
     * 
     * @params object $objData   
     * 
     * @return json
     */
    public static function createRecord($objData) {
        try {
            $request_data = zeamsterApi::convertObjectToArray($objData);
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url."/accountvaults";

            if (!empty($request_data)) {
                $request = array('accountvault' => array_filter($request_data));
                $request = json_encode($request);
                $response = $objzeamsterApi::send_transaction($request_url, 'POST', null, $request);

                return $response;
            }
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Update Account Record.
     * 
     * @params object $objData   
     * @params string $account_id  
     *   
     * @return json
     */
    public static function updateAccountVaultsRecords($objData, $account_id) {
        try {

            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url ."/accountvaults/{$account_id}";
            $request_data = zeamsterApi::convertObjectToArray($objData);
            if (!empty($request_data) && !empty($account_id)) {
                $request = array('accountvault' => array_filter($request_data));

                $request = json_encode($request);

                $method = 'PUT';

                $response = $objzeamsterApi::send_transaction($request_url, $method, null, $request);

                return $response;
            } else {
                throw new Exception("Please check request data??");
            }
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Get / View Exiting Account Record.

     * @params string $account_id   
     *  
     * @return json
     */
    public static function getAccountVaultsRecords($account_id = null,$filter = null, $perPage = null,$sort = null,$pageNo =null) {
        try {
                  
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            if (!empty($account_id)) {
                $request_url = $api_url."/accountvaults/{$account_id}";
            } else {
                if(!empty($perPage) || !empty($filter) || !empty($sort) || !empty($pageNo) ){
                    $request_url = $api_url . "/accountvaults?{$filter}&page_size={$perPage}&sort={$sort}&page={$pageNo}";
                }else{
                    $request_url=  $api_url . "/accountvaults";
                }
               
            }
           
            $method = 'GET';
            $response = $objzeamsterApi::send_transaction($request_url, $method, null, null);

            return $response;
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Delete existing account record.

     * @params string $account_id   
     *  
     * @return json
     */
    public static function deleteAccountVaultsRecord($account_id) {
        try {

            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url."/accountvaults/{$account_id}";

            $method = 'DELETE';
            $response = $objzeamsterApi::send_transaction($request_url, $method, null, null);

            return $response;
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

}
