<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelSupport extends Model
{
    protected $table = 'hotel_support';
	protected $primaryKey = 'support_id';
	protected $fillable = [
        'hotel_id', 'support_body', 'status', 'reg_ip', 'added_by', 'updated_by'
    ];
}
