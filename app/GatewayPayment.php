<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GatewayPayment extends Model
{
    
    protected $fillable = [
        'gateway_name', 'reg_ip', 'added_by'
    ];
	protected $primaryKey = 'gateway_id';	
	protected $table = 'gateway_payments';
	
}
