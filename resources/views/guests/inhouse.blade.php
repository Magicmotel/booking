@extends('layouts.layout')

@section('title')
	In House : {{date('F j, Y', strtotime($searchDate))}}
@stop

@section('CascadingSheet')
    <!-- Datatables -->
    {!!Html::style('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-dragtable/css/dragtable.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>In House : {{date('F j, Y', strtotime($searchDate))}}</h3></div>
            <div class="col-md-2 col-sm-2 col-xs-2 fltL zeroRightPadd marginTop5 marginBottom5" id="calenderBox">
            <input type="text" name="search_date" id="search_date" value="{{$searchDate}}" placeholder="mm/dd/yyyy" class="date-picker form-control col-md-7 col-xs-12  has-feedback-left" readonly="readonly">
            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @elseif ($message = Session::get('warning'))
                                <div class="alert alert-warning">
                                    <p>{{ $message }}</p>
                                </div>
                            @elseif ($message = Session::get('danger'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <?	//table table-striped table-bordered dt-responsive nowrap sar-table   cellspacing="0" width="100%"> ?>
                            <table id="datatable-responsive" class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th id="inHouse_1"><div class="some-handle">Name</div></th>
                                    <th id="inHouse_2"><div class="some-handle">City</div></th>
                                    <th id="inHouse_3"><div class="some-handle">Room#</div></th>
                                    <th id="inHouse_4"><div class="some-handle">Arrival</div></th>
                                    <th id="inHouse_5"><div class="some-handle">Departure</div></th>
                                    <th id="inHouse_6"><div class="some-handle">Type</div></th>
                                    <th id="inHouse_7"><div class="some-handle">Condition</div></th>
                                    <th id="inHouse_8"><div class="some-handle">Reservation#</div></th>
                                    <th id="inHouse_9"><div class="some-handle">Source</div></th>
                                    <th id="inHouse_10"><div class="some-handle">Balance</div></th>
                                    <th id="inHouse_11"><div class="some-handle">Action</div></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Results as $vals)
                                <tr>
                                    <td class="uPPerLetter">
                                    	<a href="{{url('guest-info', $vals->RsrvId)}}" class="btn-list-link">{{$vals->GuestLname}}, {{$vals->GuestFname}}</a>
                                        @if($vals->GuestBadStatus == 1)
                                    	<i class="fa fa-user activeGuest"></i>
                                        @else
                                        <i class="fa fa-user-times badGuest"></i>
                                        @endif
                                  	</td>
                                  	<td>{{$vals->GuestCity}}</td>
                                  	<td>
                                    	@if($vals->RoomNumber)
                                            @if($vals->RsrvLock)
                                                <div class="fltL" style="position:relative; padding-top:2px;">{{$vals->RoomNumber}}</div>
                                                <i class="fa fa-lock lockFeaturButton" aria-hidden="true"></i>
                                            @else
                                                <a href="{{url('change-room', $vals->RsrvId)}}" class="btn-list-link">{{$vals->RoomNumber}}</a>
                                            @endif
                                      	@else
                                            <a href="{{url('change-room', $vals->RsrvId)}}" class="btn-list-link">Select Room</a>
                                        @endif
                                   	</td>
                                  	<td>{{$vals->ArvlDate}}</td>
                                  	<td>{{$vals->DprtDate}}</td>
                                  	<td>{{$vals->RoomTypes}}</td>
                                  	<td>
                                    	@if($vals->RoomNumber)
                                    	{{($vals->RoomStat==1)?"Clean":"Dirty"}}
                                   		@endif
                                	</td>
                                    <td>{{($vals->RsrvId)}}</td>
                                    <td>{{($vals->RsrvSource)?$vals->RsrvSource:"Local"}}</td>
                                    {{--*/ $PayBalance = ($vals->PayAmount-$vals->PayDeposit) /*--}}
                                  	<td>{!!($PayBalance < 0)?'- $'.($PayBalance*-1):'$'.$PayBalance!!}</td>
                                    <td>
                                        <a href="javascript:void(0);" data-id="{{$vals->RsrvId}}" data-value="{{$vals->GuestLname.', '.$vals->GuestFname}}~{{$vals->ArvlDate}}~{{$vals->DprtDate}}~{{$PayBalance}}" class="checkOutButton left btn btn-success btn-xs buTTonResize">Check Out</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                           	</table>
                            
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<!-- checkedOutModel -->
<div id="checkedOutModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="model_RsrvId"></h4>
            </div>
            <div class="modal-matter">
            	<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                    <div class="col-md-3 col-sm-12 col-xs-12 labelText zeroPadd">Guest Name:</div>
                    <div class="col-md-8 col-sm-12 col-xs-12 UpperLetter zeroPadd" id="model_GuestName"></div>
                </div>
            	<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingTop15">
                    <div class="col-md-3 col-sm-12 col-xs-12 labelText zeroPadd">Arrival Date:</div>
                    <div class="col-md-8 col-sm-12 col-xs-12 UpperLetter zeroPadd" id="model_Arrival"></div>
                </div>
            	<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingTop15">
                    <div class="col-md-3 col-sm-12 col-xs-12 labelText zeroPadd">Departure Date:</div>
                    <div class="col-md-8 col-sm-12 col-xs-12 UpperLetter zeroPadd" id="model_Depart"></div>
                </div>
            	<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingTop40">
                    <div class="col-md-3 col-sm-12 col-xs-12 labelText zeroPadd">Balance:</div>
                    <div class="col-md-8 col-sm-12 col-xs-12 UpperLetter zeroPadd" id="model_Balance"></div>
                </div>
            </div>
            <div class="modal-footer AlgnLeft">
                <button type="button" class="btn btn-info pnsubmit" data-dismiss="modal">Pay Now</button>
                <button type="button" class="btn btn-success cosubmit" data-dismiss="modal">Check Out</button>
                <button type="button" class="btn btn-warning prsubmit" data-dismiss="modal">Payment Reversal</button>
                <button type="button" class="btn btn-primary gfsubmit" data-dismiss="modal">Guest Folio</button>
                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div id="fc_create" data-toggle="modal" data-target="#checkedOutModel"></div>
<script>
	function ConfirmAction()
	{
		var x = confirm("Are you sure this Guest want to Check-Out?");
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop     

@section('JavascriptSRC')
    <!-- Datatables -->
    {!!Html::script('vendors/datatables.net-dragtable/js/jquery.dragtable.js')!!}
    {!!Html::script('vendors/datatables.net/js/jquery.dataTables.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')!!}
    {!!Html::script('vendors/jszip/dist/jszip.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/pdfmake.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/vfs_fonts.js')!!} 
@stop  

@section('jQuery')
<script type="text/javascript">
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	$('#search_date').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		var newDate = new Date(ev.date).toString("MM/dd/yyyy");
		window.location.href="{!!url('in-house?search_date=')!!}"+newDate;
	}).data('datepicker');
	
	$('#datatable-responsive').DataTable({
		"aaSorting": [[3, 'desc']],
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"bPaginate": false,
		"columnDefs": [ { orderable: false, targets: -1 } ]
	});
	$('#datatable-responsive').dragtable({
		dragHandle:'.some-handle',
		persistState: function(table) {
			if (!window.sessionStorage) return;
			var ss = window.sessionStorage;
			table.el.find('th').each(function(i) {
				if(this.id != '') {table.sortOrder[this.id]=i;}
			});
			ss.setItem('tableorder',JSON.stringify(table.sortOrder));
		},
		restoreState: eval('(' + window.sessionStorage.getItem('tableorder') + ')')
	});
	$('.checkOutButton').on('click', function(){
		var RsrvId = $(this).attr('data-id');
		var RsrvArray = $(this).attr('data-value').split("~");
		var GuestName = RsrvArray[0];
		var Arrival = RsrvArray[1];
		var Depart = RsrvArray[2];
		var Balance = RsrvArray[3];
		$("#model_RsrvId").html("Reservation ID: "+RsrvId);
		$("#model_GuestName").html(GuestName);
		$("#model_Arrival").html(Arrival);
		$("#model_Depart").html(Depart);
		$("#model_Balance").html("$ "+Balance);
		if(Balance == 0){
			$(".pnsubmit").hide();
			$(".cosubmit").show();
			$(".prsubmit").hide();
		}else if(Balance < 0){
			$(".pnsubmit").hide();
			$(".cosubmit").hide();
			$(".prsubmit").show();
		}else{
			$(".pnsubmit").show();
			$(".cosubmit").hide();
			$(".prsubmit").hide();
		}
		$('#fc_create').click();
		$(".pnsubmit").on("click", function() {
			window.location.href="{!!url('payment-pay')!!}/"+RsrvId;
			return true;
		});
		$(".cosubmit").on("click", function() {
			window.location.href="{!!url('checked-out-reservation')!!}/"+RsrvId;
			return true;
		});
		$(".prsubmit").on("click", function() {
			window.location.href="{!!url('payment-info')!!}/"+RsrvId;
			return true;
		});
		$(".gfsubmit").on("click", function() {
			window.location.href="{!!url('guest-info')!!}/"+RsrvId;
			return true;
		});
	});
});
</script>
@stop
