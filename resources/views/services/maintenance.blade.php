@extends('layouts.layout')

@section('title')
	Manage Maintenance Queries
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Maintenance Queries</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class="active"><a href="javascript:void(0);">Manage Maintenance Queries</a></li>
                <li><a href="{{url('maintenance/create')}}">Create Maintenance Query</a></li>
            </ul>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @elseif ($message = Session::get('danger'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                    	<th>No</th>
                                        <th>Room Number</th>
                                        <th>Type</th>
                                        <th>Maintenance For</th>
                                        <th>Reply</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($Results))
                                	@foreach($Results as $vals)
                                    <tr class="even pointer">
                                        <td>{{ ++$i }}</td>
                                        <td>{!!$vals->room_number!!}</td>
                                        <td>{{$vals->room_type}} &nbsp; ({{($vals->room_smoking_type != 1)?"":"N"}}{!!$vals->room_bed_size!!})</td>
                                        <td>{!!$vals->maintenance_for!!}</td>
                                        <td>{!!$vals->maintenance_reply!!}</td>
                                        <td>
                                        	@if($vals->status == 0)
                                            <a href="{!!url('maintenance', $vals->maintenance_id)!!}/reply" class="left btn btn-info buTTonResize"><i class="fa fa-reply"></i> Reply</a>
                                            @else
                                            <form action="{!!url('maintenance', $vals->maintenance_id)!!}" method="post" onsubmit="return ConfirmAction('Are you sure you want to delete this Query?')">
                                            {!!method_field('DELETE')!!}
                                            {!!csrf_field()!!}
                                            {!!Form::hidden('hotel_id', $Hotel_ID)!!}
                                            {!!Form::button('<i class="fa fa-trash-o"></i> Delete', ['class'=>'left btn btn-danger buTTonResize', 'type'=>'submit'])!!}
                                            </form>
                                            @endif
                                        </td>
                                    </tr>
                                	@endforeach
                                @else
                                	<tr>
                                        <td colspan="6" class="errorMessageTR">No Record Found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $Results->render() !!}
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function ConfirmAction(str)
	{
		var x = confirm(str);
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop      
