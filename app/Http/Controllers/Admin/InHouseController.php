<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Hotel;
use App\Guest;
use App\Roomtype;
use App\RoomAssign;
use App\RoomReservation;
use App\RoomReservationFolio;
use App\RoomReservationFolioMeta;
use App\RoomReservationFolioExempt;
use App\ReservationRoomAllot;
use App\ReservationRoomAllotPast;
use App\RoomReservationDiscount;
use App\CompanyProfile;
use App\CompanyProfileRate;
use App\PaymentOption;
use Helper;
use DB;
use Validator;

class InHouseController extends Controller
{
    public function index(Request $request)
    {
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		
		if($request->search_date){
			$toDay = date("Y-m-d", strtotime($request->search_date));
			$searchDate = $request->search_date;
		}else{
			$toDay = date("Y-m-d");
			$searchDate = date("m/d/Y");
		}
		
		$Results = DB::select("SELECT GST.guest_id as GuestId, GST.guest_fname as GuestFname, GST.guest_lname as GuestLname, GST.guest_city as GuestCity,
							   GST.guest_zip as GuestPostal, GST.guest_group as GuestGroup, GST.bad_status as GuestBadStatus, RSV.reservation_locked as RsrvLock,
							   RSV.room_reservation_id as RsrvId, RSV.reserv_night as RsrvNights, RSV.status as RsrvStat, RSV.reserv_source as RsrvSource,
							   DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate,
							   (SELECT CP.company_name FROM company_profile as CP WHERE CP.profile_id = RSV.guest_profile_id) as GuestCompany,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
									)
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit,
							   (SELECT RT.room_type
							   	FROM roomtypes as RT WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomTypes,
							   (SELECT R.room_number
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber,
							   (SELECT R.room_stat
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomStat
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.arrival <= '".$toDay."' AND RSV.departure >= '".$toDay."' AND RSV.status = 'Checked In'
							   ORDER BY ArvlDate DESC, DprtDate ASC
							  ");
		return view('guests.inhouse')->with("Results", $Results)->with("toDay", $toDay)->with("searchDate", $searchDate);
    }
	
	public function departure(Request $request)
    {
        $Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		
		if($request->search_date){
			$toDay = date("Y-m-d", strtotime($request->search_date));
			$searchDate = $request->search_date;
		}else{
			$toDay = date("Y-m-d");
			$searchDate = date("m/d/Y");
		}
		
		$Results = DB::select("SELECT GST.guest_id as GuestId, GST.guest_fname as GuestFname, GST.guest_lname as GuestLname, GST.guest_city as GuestCity,
							   GST.guest_zip as GuestPostal, GST.guest_group as GuestGroup, GST.bad_status as GuestBadStatus,
							   RSV.room_reservation_id as RsrvId, RSV.reserv_night as RsrvNights, RSV.status as RsrvStat,
							   DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate,
							   (SELECT CP.company_name FROM company_profile as CP WHERE CP.profile_id = RSV.guest_profile_id) as GuestCompany,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
									)
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit,
							   (SELECT RT.room_type
							   	FROM roomtypes as RT WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomTypes,
							   (SELECT R.room_number
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber,
							   (SELECT R.room_stat
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomStat
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.departure = '".$toDay."' AND RSV.status = 'Checked In'
							   ORDER BY ArvlDate DESC, DprtDate ASC
							  ");
							  
		return view('guests.departure')->with("Results", $Results)->with("toDay", $toDay)->with("searchDate", $searchDate);
    }
	
	public function checkout_reservation($id)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$toDay = date("Y-m-d");
		
		try{
			DB::beginTransaction();
			
			$RoomAllotRst = ReservationRoomAllot::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $id] ])->first();
			$RoomId       = $RoomAllotRst->room_assign_id;
			
			$RoomNoRst = RoomAssign::where([ ['hotel_id', $Hotel_ID], ['room_assign_id', $RoomId] ])->first();
			$RoomNoRst->room_stat = 2;
			$RoomNoRst->reg_ip 	  = $_SERVER['REMOTE_ADDR'];
			$RoomNoRst->added_by  = $admin_ID;
			$RoomNoRst->save();
			
			DB::select("DELETE FROM room_reservation_folio WHERE folio_date >= '".$toDay."' AND folio_order > 0 AND room_reservation_id = ".$id);
			
			$Results  = RoomReservation::find($id);
			$DepartureDate = $Results->departure;
			$cRoomTypeId   = $Results->room_type_id;
			
			$Results->status	= "Checked Out";
			$Results->departure = $toDay;
			$Results->reg_ip 	= $_SERVER['REMOTE_ADDR'];
			$Results->added_by 	= $admin_ID;
			$Results->save();
			
			if($DepartureDate > $toDay){
				$Helper->ChannelPartnerRequest($Hotel_ID, $cRoomTypeId, $toDay, $DepartureDate);
			}
			
			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
		}
		return redirect('checked-out-guests');
    }
	
	public function checkout_guest(Request $request)
    {
        $Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		
		if($request->search_date){
			$toDay = date("Y-m-d", strtotime($request->search_date));
			$searchDate = $request->search_date;
		}else{
			$toDay = date("Y-m-d");
			$searchDate = date("m/d/Y");
		}
			
		$Results = DB::select("SELECT GST.guest_id as GuestId, GST.guest_fname as GuestFname, GST.guest_lname as GuestLname, GST.guest_city as GuestCity,
							   GST.guest_zip as GuestPostal, GST.guest_group as GuestGroup, GST.bad_status as GuestBadStatus,
							   RSV.room_reservation_id as RsrvId, RSV.reserv_night as RsrvNights, RSV.status as RsrvStat,
							   DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate,
							   (SELECT CP.company_name FROM company_profile as CP WHERE CP.profile_id = RSV.guest_profile_id) as GuestCompany,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
									)
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit,
							   (SELECT RT.room_type
							   	FROM roomtypes as RT WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomTypes,
							   (SELECT R.room_number
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber,
							   (SELECT R.room_stat
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomStat
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.departure = '".$toDay."' AND RSV.status = 'Checked Out'
							   ORDER BY ArvlDate ASC, DprtDate ASC
							  ");
							  
		return view('guests.checked-out-guests')->with("Results", $Results)->with("toDay", $toDay)->with("searchDate", $searchDate);
    }
	
	public function guestinfo(Request $request, $id)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$Results = DB::select("SELECT GST.guest_id as GuestId, GST.guest_fname as GuestFname, GST.guest_mname as GuestMname, GST.guest_lname as GuestLname,
							   GST.guest_address as GuestAddress, GST.guest_city as GuestCity, GST.guest_state as GuestState, GST.guest_zip as GuestPostal,
							   GST.guest_phone as GuestContact, GST.guest_email as GuestEmail, GST.guest_dob as GuestDOB, GST.guest_vehicle as GuestVehicle,
							   RSV.room_reservation_id as RsrvId, DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate,
							   RSV.reserv_night as StayNights, RSV.status as RsrvStat, RSV.reservation_locked as RsrvLock, RSV.special_request as SpecialRequest,
							   RSV.reserv_source as RsrvSource, RSV.source_reserv_id as SourceRsrvId, RSV.source_cnf_id as SourceRsrvCNFId,
							   (SELECT CP.company_name FROM company_profile as CP WHERE CP.profile_id = RSV.guest_profile_id) as GuestCompany,
							   (SELECT SUM(RF.folio_amount) FROM room_reservation_folio as RF
								WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0
							   ) as EstAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
									)
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit,
							   (SELECT R.room_number
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber,
							   (SELECT RT.room_type
							   	FROM roomtypes as RT
								WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomType
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.room_reservation_id = ".$id." AND RSV.hotel_id = ".$Hotel_ID."
							   ORDER BY ArvlDate ASC, DprtDate ASC
							  ");
		if(count($Results)){
			return view('guests.guest-info')->with("Results", $Results);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
    }
	
	public function paymentinfo(Request $request, $id)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$Results = DB::select("SELECT GST.guest_id as GuestId, GST.guest_fname as GuestFname, GST.guest_mname as GuestMname, GST.guest_lname as GuestLname,
							   GST.guest_address as GuestAddress, GST.guest_city as GuestCity, GST.guest_state as GuestState, GST.guest_zip as GuestPostal,
							   GST.guest_phone as GuestContact, GST.guest_email as GuestEmail, GST.guest_dob as GuestDOB, GST.guest_vehicle as GuestVehicle,
							   RSV.room_reservation_id as RsrvId, DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate,
							   RSV.reserv_night as StayNights, RSV.status as RsrvStat, RSV.reservation_locked as RsrvLock, RSV.special_request as SpecialRequest,
							   RSV.reserv_source as RsrvSource, RSV.source_reserv_id as SourceRsrvId, RSV.source_cnf_id as SourceRsrvCNFId,
							   (SELECT CP.company_name FROM company_profile as CP WHERE CP.profile_id = RSV.guest_profile_id) as GuestCompany,
							   (SELECT SUM(RF.folio_amount) FROM room_reservation_folio as RF
								WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0
							   ) as EstAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
									)
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit,
							   (SELECT R.room_number
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber,
							   (SELECT RT.room_type
							   	FROM roomtypes as RT
								WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomType
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.room_reservation_id = ".$id." AND RSV.hotel_id = ".$Hotel_ID."
							   ORDER BY ArvlDate ASC, DprtDate ASC
							  ");
		if(count($Results))
		{
			$PaymentRst = DB::select("SELECT RRFM.folio_type, RRFM.folio_pay_type, RRFM.folio_amount, RRFM.folio_comment, PO.transaction_id, PO.created_at, PO.log_id
								   	  FROM room_reservation_folio_meta as RRFM RIGHT JOIN payment_option as PO ON PO.log_id = RRFM.log_id AND RRFM.folio_order = 0
								   	  WHERE PO.hotel_id = ".$Hotel_ID." AND PO.room_reservation_id = ".$id."
								   	  ORDER BY RRFM.folio_date ASC
								  ");
			$CardDtlRst = DB::select("SELECT SUM(RRFM.folio_amount) as cc_amt, PO.log_id, PO.cc_number,
									  CONCAT(PO.cc_holder_f, ' ', PO.cc_holder_l) as cc_holder, CONCAT(PO.cc_exp_mm, '/', PO.cc_exp_yy) as cc_exp
									  FROM room_reservation_folio_meta as RRFM
									  RIGHT JOIN payment_option as PO ON PO.log_id = RRFM.log_id AND RRFM.folio_order = 0 AND RRFM.folio_type = 0
								   	  WHERE PO.hotel_id = ".$Hotel_ID." AND PO.room_reservation_id = ".$id." AND (PO.payment_mode = 2 OR PO.payment_mode = 3)
									  GROUP BY PO.cc_number ORDER BY PO.cc_number ASC
								     ");

			return view('guests.payment-info')->with("Results", $Results)->with("PaymentRst", $PaymentRst)->with("CardDtlRst", $CardDtlRst);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
    }
	
	public function payment_reversal(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID   = $Helper->getHotelId();
		$admin_ID   = $Helper->getAdminUserId();
		$Gateway_ID = 1;
		
		$rsrv_Id = $request->rsrv_id;
		$Result  = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $rsrv_Id] ])->first();
		if(count($Result))
		{
			$paymentMode   = $request->payment_mode;
			$refund_Amount = $request->folio_amount;
			
			$bank_name    = $request->bank_name;
			$holder_name  = $request->holder_name;
			$check_number = $request->check_number;
			$check_date   = $request->check_date;
			
			$CardLog = $request->card_log;
			$cc_number = $cc_holder = $cc_exp_mm = $cc_exp_yy = "";
			if($CardLog){
				$CardLogQry = PaymentOption::where('log_id', $CardLog)->first();
				$cc_number = $CardLogQry->cc_number;
				$cc_holder_f = $CardLogQry->cc_holder_f;
				$cc_holder_l = $CardLogQry->cc_holder_l;
				$cc_exp_mm = $CardLogQry->cc_exp_mm;
				$cc_exp_yy = $CardLogQry->cc_exp_yy;
			}
			
			$folio_Comment = $request->folio_comment;
			
			if($paymentMode == 1)
			{
				$unique_ID = "CSH".$Helper->getUniqueId().$rsrv_Id;
				
				$PaymentOptionOBJ = new PaymentOption;
				$PaymentOptionOBJ->transaction_id = $unique_ID;
				$PaymentOptionOBJ->payment_mode   = $paymentMode;
				$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
				$PaymentOptionOBJ->room_reservation_id = $rsrv_Id;
				$PaymentOptionOBJ->reg_ip   	  = $_SERVER['REMOTE_ADDR'];
				$PaymentOptionOBJ->added_by 	  = $admin_ID;
				$PaymentOptionOBJ->save();
				
				$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
				$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
				$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
				$RoomReservationFolioMetaOBJ->folio_type 	= 2;
				$RoomReservationFolioMetaOBJ->folio_date 	= date("Y-m-d");
				$RoomReservationFolioMetaOBJ->folio_pay_type= 'Cash Refund';
				$RoomReservationFolioMetaOBJ->folio_amount 	= $refund_Amount;
				$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
				$RoomReservationFolioMetaOBJ->folio_auto  	= 0;
				$RoomReservationFolioMetaOBJ->folio_order 	= 0;
				$RoomReservationFolioMetaOBJ->log_id 		= $PaymentOptionOBJ->log_id;
				$RoomReservationFolioMetaOBJ->reg_ip 		= $_SERVER['REMOTE_ADDR'];
				$RoomReservationFolioMetaOBJ->added_by 		= $admin_ID;
				$RoomReservationFolioMetaOBJ->save();
			}
			elseif($paymentMode == 2)
			{
				$unique_ID = "CRD".$Helper->getUniqueId().$rsrv_Id;
				
				$PaymentOptionOBJ = new PaymentOption;
				$PaymentOptionOBJ->transaction_id = $unique_ID;
				$PaymentOptionOBJ->payment_mode   = $paymentMode;
				$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
				$PaymentOptionOBJ->room_reservation_id = $rsrv_Id;
				$PaymentOptionOBJ->cc_number 	  = $cc_number;
				$PaymentOptionOBJ->cc_holder_f 	  = $cc_holder_f;
				$PaymentOptionOBJ->cc_holder_l 	  = $cc_holder_l;
				$PaymentOptionOBJ->cc_exp_mm 	  = $cc_exp_mm;
				$PaymentOptionOBJ->cc_exp_yy 	  = $cc_exp_yy;
				$PaymentOptionOBJ->reg_ip    	  = $_SERVER['REMOTE_ADDR'];
				$PaymentOptionOBJ->added_by  	  = $admin_ID;
				$PaymentOptionOBJ->save();
				
				$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
				$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
				$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
				$RoomReservationFolioMetaOBJ->folio_type 	= 2;
				$RoomReservationFolioMetaOBJ->folio_date 	= date("Y-m-d");
				$RoomReservationFolioMetaOBJ->folio_pay_type= 'CC Refund';
				$RoomReservationFolioMetaOBJ->folio_amount  = $refund_Amount;
				$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
				$RoomReservationFolioMetaOBJ->folio_auto    = 0;
				$RoomReservationFolioMetaOBJ->folio_order   = 0;
				$RoomReservationFolioMetaOBJ->log_id 		= $PaymentOptionOBJ->log_id;
				$RoomReservationFolioMetaOBJ->reg_ip 		= $_SERVER['REMOTE_ADDR'];
				$RoomReservationFolioMetaOBJ->added_by 		= $admin_ID;
				$RoomReservationFolioMetaOBJ->save();
			}
			elseif($paymentMode == 4)
			{
				$unique_ID = "CHK".$Helper->getUniqueId().$rsrv_Id;
				
				$PaymentOptionOBJ = new PaymentOption;
				$PaymentOptionOBJ->transaction_id = $unique_ID;
				$PaymentOptionOBJ->payment_mode   = $paymentMode;
				$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
				$PaymentOptionOBJ->room_reservation_id = $rsrv_Id;
				$PaymentOptionOBJ->bank_name   	  = $bank_name;
				$PaymentOptionOBJ->holder_name    = $holder_name;
				$PaymentOptionOBJ->check_number   = $check_number;
				$PaymentOptionOBJ->check_date 	  = $check_date;
				$PaymentOptionOBJ->reg_ip   	  = $_SERVER['REMOTE_ADDR'];
				$PaymentOptionOBJ->added_by 	  = $admin_ID;
				$PaymentOptionOBJ->save();
				
				$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
				$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
				$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
				$RoomReservationFolioMetaOBJ->folio_type 	= 2;
				$RoomReservationFolioMetaOBJ->folio_date 	= date("Y-m-d");
				$RoomReservationFolioMetaOBJ->folio_pay_type= 'Check Refund';
				$RoomReservationFolioMetaOBJ->folio_amount 	= $refund_Amount;
				$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
				$RoomReservationFolioMetaOBJ->folio_auto  	= 0;
				$RoomReservationFolioMetaOBJ->folio_order 	= 0;
				$RoomReservationFolioMetaOBJ->log_id 		= $PaymentOptionOBJ->log_id;
				$RoomReservationFolioMetaOBJ->reg_ip 		= $_SERVER['REMOTE_ADDR'];
				$RoomReservationFolioMetaOBJ->added_by 		= $admin_ID;
				$RoomReservationFolioMetaOBJ->save();
			}
			elseif($paymentMode == 5)
			{
				$unique_ID = "CMP".$Helper->getUniqueId().$rsrv_Id;
				
				$PaymentOptionOBJ = new PaymentOption;
				$PaymentOptionOBJ->transaction_id = $unique_ID;
				$PaymentOptionOBJ->payment_mode   = $paymentMode;
				$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
				$PaymentOptionOBJ->room_reservation_id = $rsrv_Id;
				$PaymentOptionOBJ->reg_ip   	  = $_SERVER['REMOTE_ADDR'];
				$PaymentOptionOBJ->added_by 	  = $admin_ID;
				$PaymentOptionOBJ->save();
				
				$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
				$RoomReservationFolioMetaOBJ->hotel_id 		= $Hotel_ID;
				$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
				$RoomReservationFolioMetaOBJ->folio_type	= 2;
				$RoomReservationFolioMetaOBJ->folio_date 	= date("Y-m-d");
				$RoomReservationFolioMetaOBJ->folio_pay_type= 'Company Refund';
				$RoomReservationFolioMetaOBJ->folio_amount 	= $refund_Amount;
				$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
				$RoomReservationFolioMetaOBJ->folio_auto  	= 0;
				$RoomReservationFolioMetaOBJ->folio_order 	= 0;
				$RoomReservationFolioMetaOBJ->log_id 		= $PaymentOptionOBJ->log_id;
				$RoomReservationFolioMetaOBJ->reg_ip 		= $_SERVER['REMOTE_ADDR'];
				$RoomReservationFolioMetaOBJ->added_by 		= $admin_ID;
				$RoomReservationFolioMetaOBJ->save();
			}
			return redirect('payment-info/'.$rsrv_Id)->with("success", "Payment reverse successfully Done");
		}else{
			return redirect('in-house')->with("warning", "Reservation# ".$rsrv_Id." not Found");
		}
	}
	
	public function changeroom(Request $request, $id)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$RsrvRst = RoomReservation::find($id);
		if(!count($RsrvRst)){
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
		$ArrivalDate = $RsrvRst->arrival; $DepartureDate = $RsrvRst->departure;
		$fromDate = ($toDay >= $ArrivalDate)?$toDay:$ArrivalDate;
		$toDate   = $DepartureDate;
		
		if($toDate == $fromDate){
			$varQry = " AND ((RSV.departure = '".$toDate."' AND RSV.arrival <= '".$fromDate."'))";
		}else{
			$varQry = " AND NOT ((RSV.departure <= '".$fromDate."') OR (RSV.arrival >= '".$toDate."'))";
		}
		
		$valueType = $RsrvRst->room_type_id;
		$TypeSql   = Roomtype::where([ ['hotel_id', $Hotel_ID], ['room_type_id', '!=', $RsrvRst->room_type_id] ])->orderBy('room_order', 'ASC')->get(['room_type_id']);
		foreach($TypeSql as $TypeRst){
			$valueType .= ", ".$TypeRst->room_type_id;
		}
		
		$Results = DB::select("SELECT GST.guest_fname as GuestFname, GST.guest_lname as GuestLname, RSV.room_reservation_id as RsrvId,
							   RSV.reserv_source as RsrvSource, RSV.source_reserv_id as SourceRsrvId, RSV.source_cnf_id as SourceRsrvCNFId,
							   RSV.room_type_id as RoomTypeId, RSV.status as RsrvStat, RSV.reservation_locked as RsrvLock, RRA.allot_id as AllotId,
							   ( SELECT SUM(RF.folio_amount) FROM room_reservation_folio as RF
								 WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0
							   ) as EstAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
									)
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit,
							   ( SELECT RT.room_type
							   	 FROM roomtypes as RT WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomTypes,
							   ( SELECT CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size)
							   	 FROM roomtypes as RT WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomType,
							   ( SELECT R.room_number
							   	 FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								 WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
							   WHERE RSV.room_reservation_id = ".$id." AND RSV.hotel_id = ".$Hotel_ID."							   
							  ");
							  
	  	$vcntRoom_Rst = DB::select("SELECT R1.room_assign_id as RoomId, R1.room_number as RoomNumber, R1.room_stat as RoomStat, R1.room_type_id as RoomTypeId,
									RT.room_type as RoomTypes, RT.room_display as DisplayType, CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType
									FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
									WHERE R1.status = 1 AND R1.room_number != '' AND R1.trash = 0 AND R1.hotel_id = ".$Hotel_ID." AND
									R1.room_assign_id NOT IN(
										SELECT R.room_assign_id
										FROM room_reservation as RSV
										INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
										INNER JOIN room_assign as R ON R.room_assign_id = RRA.room_assign_id
										WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.status != 'Checked out' AND RSV.status != 'Cancelled' ".$varQry."
									) ORDER BY FIELD(R1.room_type_id, ".$valueType."), R1.room_number ASC
								   ");
		if(count($Results)){
			return view('guests.change-room')->with("Results", $Results)->with("request", $request)->with("vcntRoom_Rst", $vcntRoom_Rst);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
    }
	
	public function allotroom(Request $request, $id)
    {
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		
		$rsrvId = $request->rsrv_id;
		$roomId = $request->room_id;
		$rtypeId  = $request->room_type_id;
		$CrtypeId = $request->c_room_type_id;
		$antiType = $request->antitype;
		
		$RsrvAllot = ReservationRoomAllot::find($id);
		if($RsrvAllot->hotel_id == $Hotel_ID)
		{
			try
			{
				DB::beginTransaction();
				
				if($rtypeId != $CrtypeId)
				{
					$RsrvRst = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $rsrvId] ])->first();
					$ArrivalDate   = $RsrvRst->arrival;
					$DepartureDate = $RsrvRst->departure;
					$RsrvGuest     = $RsrvRst->guest_id;
					$profile_ID    = $RsrvRst->guest_profile_id;
					$rsrvTaxBool   = $RsrvRst->reservation_tax_exmpt;
					
					$RsrvRst->room_type_id = $rtypeId;
					$RsrvRst->reg_ip = $_SERVER['REMOTE_ADDR'];
					$RsrvRst->added_by = $admin_ID;
					$RsrvRst->save();
					
					DB::select("UPDATE room_reservation_folio SET
								room_type_id = '".$rtypeId."', reg_ip = '".$_SERVER['REMOTE_ADDR']."', added_by = '".$admin_ID."'
								WHERE room_reservation_id = ".$rsrvId." AND hotel_id = ".$Hotel_ID."");				
					
					$cRoomTypeId = $CrtypeId.",".$rtypeId;
					$toDay = date("Y-m-d");
					$Helper->ChannelPartnerRequest($Hotel_ID, $cRoomTypeId, $toDay, $DepartureDate);
				}
				
				if($rtypeId != $CrtypeId && $antiType == 1)
				{ 
					$toDay = date("Y-m-d");
					$fromDate = ($toDay >= $ArrivalDate)?$toDay:$ArrivalDate;
					$toDate   = date('Y-m-d', strtotime($DepartureDate.'-1 days'));
					
					$room_Rate = 0;
					if($profile_ID)
					{
						$CompanyPrice = CompanyProfileRate::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $rtypeId], ['profile_id', $profile_ID] ])->first();
						$room_Rate = $CompanyPrice->company_rate;
					}
					
					$discountValues = "";
					$discountIdArray = RoomReservationDiscount::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $rsrvId] ])->get();
					if(count($discountIdArray))
					{
						$discountValuesArray = array(); 
						foreach($discountIdArray as $discountId){
							$discountValuesArray[] = $discountId->discount_id;
						}
						$discountValues = implode(",", $discountValuesArray);
					}
					
					$folioRateQry = DB::select("SELECT room_rate_price as fRoomRate, room_rate_date as fRoomDate FROM room_rate_option
												WHERE room_type_id = ".$rtypeId." AND hotel_id = ".$Hotel_ID." AND
												(room_rate_date BETWEEN '".$fromDate."' AND '".$toDate."')
												ORDER BY room_rate_date ASC
											   ");
					foreach($folioRateQry as $folioRateRst)
					{
						$folio_Data  = $DiscountCodeString = ""; $discountWiseRate = 0;
						$dayWiseRate = ($room_Rate > 0)?$room_Rate:$folioRateRst->fRoomRate;
						$folio_Data  = $dayWiseRate;
						$dayWiseDate = $folioRateRst->fRoomDate;
						
						if($discountValues)
						{
							$discountQry = DB::select("SELECT discount_id as DiscountId, discount_code as DiscountCode, discount_value as DiscountValue,
													   discount_type as DiscountType
													   FROM discounts
													   WHERE hotel_id = ".$Hotel_ID." AND discount_id IN (".$discountValues.")
													   ORDER BY discount_code ASC
													  ");
							$discountWiseRate = 0; $DiscountCodeArray = array();
							foreach($discountQry as $discountRst)
							{
								if($discountRst->DiscountType == 2){
									$discountWiseRate = $discountWiseRate+$discountRst->DiscountValue;
								}else{
									$discountWiseRate = $discountWiseRate+(($dayWiseRate*$discountRst->DiscountValue)/100);
								}
								$DiscountCodeArray[] = $discountRst->DiscountCode;
							}
							$DiscountCodeString = implode(", ",$DiscountCodeArray);
							$discountWiseRate = round($discountWiseRate, 2);
						}
						$FinalRoomRate_wo_Tax = $dayWiseRate - $discountWiseRate;
						if($FinalRoomRate_wo_Tax<=0){
							$FinalRoomRate_wo_Tax = 1;
						}
						
						$FolioRst = RoomReservationFolio::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $rsrvId],
																  ['folio_date', $dayWiseDate], ['folio_pay_type', 'Room Charge'] ])->first();
						$FolioRst->folio_amount	 = $FinalRoomRate_wo_Tax;
						$FolioRst->folio_data	 = $folio_Data;
						$FolioRst->folio_comment = $DiscountCodeString;
						$FolioRst->reg_ip 		 = $_SERVER['REMOTE_ADDR'];
						$FolioRst->added_by 	 = $admin_ID;
						$FolioRst->save();
						
						if($rsrvTaxBool == 0)
						{
							$folioTaxQry = DB::select("SELECT tax_type as fTaxType, tax_value as fTaxValue, tax_value_type as fValueType
													   FROM taxes WHERE hotel_id = ".$Hotel_ID." AND status = 0 AND
													   NOT( (tax_end != '0000-00-00' AND tax_end < '".$dayWiseDate."') OR
															(tax_start != '0000-00-00' AND tax_start >= '".$dayWiseDate."') OR
															(tax_end = '0000-00-00' AND tax_start > '".$dayWiseDate."'AND tax_start < '".$dayWiseDate."') OR
															(tax_start = '0000-00-00' AND tax_end < '".$dayWiseDate."' AND tax_end > '".$dayWiseDate."')
														  )
													  ");
							foreach($folioTaxQry as $folioTaxRst){
								$dayTaxAmount = "";
								if($folioTaxRst->fValueType == 1){
									$dayTaxAmount = round((($FinalRoomRate_wo_Tax*$folioTaxRst->fTaxValue)/100), 2);
									$folio_Data = $folioTaxRst->fTaxValue."%";
								}elseif($folioTaxRst->fValueType == 2){
									$dayTaxAmount = $dayTaxAmount+( round($folioTaxRst->fTaxValue, 2) );
									$folio_Data = $folioTaxRst->fTaxValue;
								}
								
								$FoliosRst = RoomReservationFolio::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $rsrvId],
																		  ['folio_date', $dayWiseDate], ['folio_pay_type', $folioTaxRst->fTaxType] ])->first();
																	  
								$FoliosRst->folio_amount = $dayTaxAmount;
								$FoliosRst->folio_data	 = $folio_Data;
								$FoliosRst->reg_ip 		 = $_SERVER['REMOTE_ADDR'];
								$FoliosRst->added_by 	 = $admin_ID;
								$FoliosRst->save();
							}
						}
					}
				}
				
				$pastAllot = new ReservationRoomAllotPast;
				$pastAllot->hotel_id = $Hotel_ID;
				$pastAllot->room_reservation_id = $rsrvId;
				$pastAllot->p_room_assign_id = $RsrvAllot->room_assign_id;
				$pastAllot->room_assign_id   = $roomId;
				$pastAllot->reg_ip = $_SERVER['REMOTE_ADDR'];
				$pastAllot->added_by = $admin_ID;
				$pastAllot->save();
					
				$RsrvAllot->room_assign_id = $roomId;
				$RsrvAllot->reg_ip   = $_SERVER['REMOTE_ADDR'];
				$RsrvAllot->added_by = $admin_ID;
				$RsrvAllot->save();
				
				if($request->type == 'check-in')
				{
					$Results  = RoomReservation::find($rsrvId); // return result in json format
					$Results->status	= "Checked IN";
					$Results->reg_ip 	= $_SERVER['REMOTE_ADDR'];
					$Results->added_by 	= $admin_ID;
					$Results->save();
				}
				DB::commit();
			} catch (\Exception $e) {
				DB::rollback();
			}
			if($request->type == 'check-in')
			{
				$Results  = RoomReservation::find($rsrvId); // return result in json format
				$Results->status	= "Checked IN";
				$Results->reg_ip 	= $_SERVER['REMOTE_ADDR'];
				$Results->added_by 	= $admin_ID;
				$Results->save();
				return redirect('in-house');
			}elseif($request->type == 'income-wo-check-in'){
				return redirect('incoming-reservation');
			}
			return redirect('change-room/'.$rsrvId)->with('success','Room Number assigned successfully');
		}else{
			return redirect('change-room/'.$rsrvId)->with("danger", "Please select your Motel");
		}
    }
	
	public function guestfolio(Request $request, $id)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$toDay = date("Y-m-d");
			
		$Results = DB::select("SELECT GST.guest_fname as GuestFname, GST.guest_mname as GuestMname, GST.guest_lname as GuestLname,
							   RSV.room_reservation_id as RsrvId, DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate,
							   RSV.reserv_night as StayNights, RSV.status as RsrvStat, RSV.reservation_locked as RsrvLock,
							   RSV.reserv_source as RsrvSource, RSV.source_reserv_id as SourceRsrvId, RSV.source_cnf_id as SourceRsrvCNFId,
							   (SELECT CP.company_name FROM company_profile as CP WHERE CP.profile_id = RSV.guest_profile_id) as GuestCompany,
							   (SELECT SUM(RF.folio_amount) FROM room_reservation_folio as RF
								WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0
							   ) as EstAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
									)
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit,
							   (SELECT R.room_number
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber,
							   (SELECT RT.room_type
							   	FROM roomtypes as RT
								WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomType
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.room_reservation_id = ".$id." AND RSV.hotel_id = ".$Hotel_ID."
							   ORDER BY ArvlDate ASC, DprtDate ASC
							  ");
		if(count($Results)){
			return view('guests.guest-folio')->with("Results", $Results)->with("Hotel_ID", $Hotel_ID);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
    }
	
	public function changestay(Request $request, $id)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$Results = DB::select("SELECT GST.guest_fname as GuestFname, GST.guest_mname as GuestMname, GST.guest_lname as GuestLname,
							   RSV.room_reservation_id as RsrvId, DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate,
							   RSV.reserv_night as StayNights, RSV.status as RsrvStat, RSV.reservation_locked as RsrvLock, RSV.guest_profile_id as Profile_ID,
							   RSV.reserv_source as RsrvSource, RSV.source_reserv_id as SourceRsrvId, RSV.source_cnf_id as SourceRsrvCNFId,
							   (SELECT CP.company_name FROM company_profile as CP WHERE CP.profile_id = RSV.guest_profile_id) as GuestCompany,
							   (SELECT SUM(RF.folio_amount) FROM room_reservation_folio as RF
								WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0
							   ) as EstAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
									)
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit,
							   (SELECT R.room_number
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber,
							   (SELECT RT.room_type
							   	FROM roomtypes as RT
								WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomType
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.room_reservation_id = ".$id." AND RSV.hotel_id = ".$Hotel_ID."
							   ORDER BY ArvlDate ASC, DprtDate ASC
							  ");
		if(count($Results)){
			return view('guests.change-stay')->with("Results", $Results)->with("Hotel_ID", $Hotel_ID);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
    }
	
	public function modify_stay(Request $request, $id)
    {
		$Hotel_ID = Helper::getHotelId();
		$admin_ID = Helper::getAdminUserId();
		
		$Crnt_ArrivalDate   = date("Y-m-d", strtotime($request->current_from));
		$Crnt_DepartureDate = date("Y-m-d", strtotime($request->current_to));
		
		$ArrivalDate   = date("Y-m-d", strtotime($request->from));
		$DepartureDate = date("Y-m-d", strtotime($request->to));
		$fromDate = date('Y-m-d', strtotime($ArrivalDate));
		$toDate = date('Y-m-d', strtotime($DepartureDate.'-1 days'));
		$NightStay = $request->reserv_night;
		
		$RoomReservationRst = RoomReservation::where([ ['room_reservation_id', $id], ['hotel_id', $Hotel_ID] ])->first();
		if(count($RoomReservationRst))
		{
			if($ArrivalDate == $Crnt_ArrivalDate && $DepartureDate == $Crnt_DepartureDate){
				return redirect('change-stay/'.$id)->with("danger", "No changes found in Request.");
			}
			else
			{
				$Reservation_ID = $RoomReservationRst->room_reservation_id;
				$room_typeId    = $RoomReservationRst->room_type_id;
				$profile_ID     = $RoomReservationRst->guest_profile_id;
				$rsrvTaxBool    = $RoomReservationRst->reservation_tax_exmpt;
				
				$RoomAssignRst = ReservationRoomAllot::where([ ['room_reservation_id', $Reservation_ID], ['hotel_id', $Hotel_ID] ])->first();
				
				$varQry  = " AND NOT ((RSV.departure <= '".$ArrivalDate."') OR (RSV.arrival >= '".$DepartureDate."'))";
				$varQry1 = " AND NOT ((BR.block_to <= '".$ArrivalDate."') OR (BR.block_from >= '".$DepartureDate."'))";
				
				$vcntRoom_Qry = DB::select("SELECT R1.room_assign_id as RoomId, R1.room_number as RoomNumber, R1.room_stat as RoomStat,
											R1.room_type_id as RoomTypeId, CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType
											FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
											WHERE R1.hotel_id = ".$Hotel_ID." AND R1.status = 1 AND R1.trash = 0 AND R1.room_type_id = ".$room_typeId." AND
											R1.room_assign_id = ".$RoomAssignRst->room_assign_id." AND 
											R1.room_assign_id NOT IN(
												SELECT R.room_assign_id
												FROM room_reservation as RSV
												INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
												INNER JOIN room_assign as R ON R.room_assign_id = RRA.room_assign_id
												WHERE R.room_type_id = ".$room_typeId." AND RSV.hotel_id = ".$Hotel_ID.$varQry." AND
												RSV.room_reservation_id != ".$Reservation_ID." AND RSV.status != 'Checked out' AND RSV.status != 'Cancelled'
											) AND
											R1.room_assign_id NOT IN(
												SELECT BR.room_assign_id
												FROM block_rooms as BR
												WHERE BR.room_type_id = ".$room_typeId." AND BR.hotel_id = ".$Hotel_ID.$varQry1."
											)
											ORDER BY R1.room_number ASC");
				if(count($vcntRoom_Qry))
				{
				
					// RESERVATION MODIFICATION
					if($ArrivalDate > $Crnt_ArrivalDate)
					{
						DB::select("DELETE FROM room_reservation_folio
									WHERE folio_date < '".$ArrivalDate."' AND hotel_id = ".$Hotel_ID." AND room_reservation_id = ".$Reservation_ID." AND folio_order > 0");
					}
					if($DepartureDate < $Crnt_DepartureDate)
					{
						DB::select("DELETE FROM room_reservation_folio
									WHERE folio_date >= '".$DepartureDate."' AND hotel_id = ".$Hotel_ID." AND room_reservation_id = ".$Reservation_ID." AND folio_order > 0");
					}
	
					$room_Rate = 0;
					if($profile_ID)
					{
						$CompanyPrice = CompanyProfileRate::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $room_typeId], ['profile_id', $profile_ID] ])->first();
						$room_Rate = $CompanyPrice->company_rate;
					}
					
					$discountValues = "";
					$discountIdArray = RoomReservationDiscount::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $Reservation_ID] ])->get();
					if(count($discountIdArray))
					{
						$discountValuesArray = array(); 
						foreach($discountIdArray as $discountId){
							$discountValuesArray[] = $discountId->discount_id;
						}
						$discountValues = implode(",", $discountValuesArray);
					}
					
					$folioRateQry = DB::select("SELECT room_rate_price as fRoomRate, room_rate_date as fRoomDate FROM room_rate_option
												WHERE room_type_id = ".$room_typeId." AND hotel_id = ".$Hotel_ID." AND
												(room_rate_date BETWEEN '".$fromDate."' AND '".$toDate."') ORDER BY room_rate_date ASC
											   ");
					foreach($folioRateQry as $folioRateRst)
					{
						$folio_Data  = $DiscountCodeString = ""; $discountWiseRate = 0;
						$dayWiseRate = ($room_Rate > 0)?$room_Rate:$folioRateRst->fRoomRate;
						$folio_Data  = $dayWiseRate;
						$dayWiseDate = $folioRateRst->fRoomDate;
						
						$RsrvFolioCnt = RoomReservationFolio::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $Reservation_ID], ['folio_date', $dayWiseDate] ])->count();
						if(!$RsrvFolioCnt)
						{
							if($discountValues)
							{
								$discountQry = DB::select("SELECT discount_id as DiscountId, discount_code as DiscountCode, discount_value as DiscountValue,
														   discount_type as DiscountType
														   FROM discounts
														   WHERE hotel_id = ".$Hotel_ID." AND discount_id IN (".$discountValues.")
														   ORDER BY discount_code ASC
														  ");
								$discountWiseRate = 0; $DiscountCodeArray = array();
								foreach($discountQry as $discountRst){
									if($discountRst->DiscountType == 2){
										$discountWiseRate = $discountWiseRate+$discountRst->DiscountValue;
									}else{
										$discountWiseRate = $discountWiseRate+(($dayWiseRate*$discountRst->DiscountValue)/100);
									}
									$DiscountCodeArray[] = $discountRst->DiscountCode;
								}
								$DiscountCodeString = implode(", ",$DiscountCodeArray);
								$discountWiseRate = round($discountWiseRate, 2);
							}
							
							$FinalRoomRate_wo_Tax = $dayWiseRate - $discountWiseRate;
							if($FinalRoomRate_wo_Tax<=0){
								$FinalRoomRate_wo_Tax = 1;
							}
							
							$ReservationFolioArray   = array();
							$ReservationFolioArray[] = $Hotel_ID;
							$ReservationFolioArray[] = $room_typeId;
							$ReservationFolioArray[] = $Reservation_ID;
							$ReservationFolioArray[] = $dayWiseDate;
							$ReservationFolioArray[] = "Room Charge";
							$ReservationFolioArray[] = $FinalRoomRate_wo_Tax;
							$ReservationFolioArray[] = $folio_Data;
							$ReservationFolioArray[] = $DiscountCodeString;
							$ReservationFolioArray[] = 1;
							$ReservationFolioArray[] = $_SERVER['REMOTE_ADDR'];
							$ReservationFolioArray[] = $admin_ID;
							
							DB::select('CALL Reservation_Folio_Insert_SP(?,?,?,?,?,?,?,?,?,?,?)', $ReservationFolioArray);
							
							if($rsrvTaxBool == 0)
							{
								$folioTaxQry = DB::select("SELECT tax_type as fTaxType, tax_value as fTaxValue, tax_value_type as fValueType
														   FROM taxes WHERE hotel_id = ".$Hotel_ID." AND status = 0 AND
														   NOT( (tax_end != '0000-00-00' AND tax_end < '".$dayWiseDate."') OR
																(tax_start != '0000-00-00' AND tax_start >= '".$dayWiseDate."') OR
																(tax_end = '0000-00-00' AND tax_start > '".$dayWiseDate."'AND tax_start < '".$dayWiseDate."') OR
																(tax_start = '0000-00-00' AND tax_end < '".$dayWiseDate."' AND tax_end > '".$dayWiseDate."')
															  )
														  ");
								$SJ = 2;
								foreach($folioTaxQry as $folioTaxRst){
									$folio_Data = "";
									$dayTaxAmount = "";
									if($folioTaxRst->fValueType == 1){
										$dayTaxAmount = round((($FinalRoomRate_wo_Tax*$folioTaxRst->fTaxValue)/100), 2);
										$folio_Data = $folioTaxRst->fTaxValue."%";
									}elseif($folioTaxRst->fValueType == 2){
										$dayTaxAmount = $dayTaxAmount+( round($folioTaxRst->fTaxValue, 2) );
										$folio_Data = $folioTaxRst->fTaxValue;
									}
									
									$ReservationFolioTArray   = array();
									$ReservationFolioTArray[] = $Hotel_ID;
									$ReservationFolioTArray[] = $room_typeId;
									$ReservationFolioTArray[] = $Reservation_ID;
									$ReservationFolioTArray[] = $dayWiseDate;
									$ReservationFolioTArray[] = $folioTaxRst->fTaxType;
									$ReservationFolioTArray[] = $dayTaxAmount;
									$ReservationFolioTArray[] = $folio_Data;
									$ReservationFolioTArray[] = "";
									$ReservationFolioTArray[] = $SJ;
									$ReservationFolioTArray[] = $_SERVER['REMOTE_ADDR'];
									$ReservationFolioTArray[] = $admin_ID;
									
									DB::select('CALL Reservation_Folio_Insert_SP(?,?,?,?,?,?,?,?,?,?,?)', $ReservationFolioTArray);
									
									$SJ++;
								}
							}
						}
					}
					
					$RoomReservationRst->arrival = $ArrivalDate;
					$RoomReservationRst->departure = $DepartureDate;
					$RoomReservationRst->reserv_night = $NightStay;
					$RoomReservationRst->reg_ip = $_SERVER['REMOTE_ADDR'];
					$RoomReservationRst->added_by = $admin_ID;
					$RoomReservationRst->save();
					return redirect('change-stay/'.$id)->with("success", "Reservation updated Successfully.");
				}else{
					return redirect('change-stay/'.$id)->with("danger", "Room not available for Booking.");
				}
			}
		}else{
			return redirect('change-stay/'.$id)->with("danger", "Reservation not found in this Motel.");
		}
	}
	
	public function cancelreserv(Request $request, $id)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$Results = DB::select("SELECT GST.guest_fname as GuestFname, GST.guest_mname as GuestMname, GST.guest_lname as GuestLname,
							   RSV.room_reservation_id as RsrvId, RSV.reserv_night as StayNights, RSV.status as RsrvStat, RSV.reservation_locked as RsrvLock,
							   DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate,
							   RSV.reserv_source as RsrvSource, RSV.source_reserv_id as SourceRsrvId, RSV.source_cnf_id as SourceRsrvCNFId,
							   (SELECT SUM(RF.folio_amount) FROM room_reservation_folio as RF
								WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0
							   ) as EstAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
									)
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.room_reservation_id = ".$id." AND RSV.hotel_id = ".$Hotel_ID."
							   ORDER BY ArvlDate ASC, DprtDate ASC
							  ");
		if(count($Results)){
			return view('guests.cancel-reservation')->with("Results", $Results)->with("Hotel_ID", $Hotel_ID);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
    }
	
	public function cancelreserv_reason(Request $request)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		$RsrvId = $request->room_reservation_id;
		
		if($Hotel_ID == $request->hotel_id){
			$Results = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $RsrvId] ])->first();
			$ArrivalDate   = $Results->arrival;
			$DepartureDate = $Results->departure;
			$cRoomTypeId   = $Results->room_type_id;
		
			$Results->status = 'Cancelled';
			$Results->reserv_reason = $request->rsrv_comment;
			$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
			$Results->added_by = $admin_ID;
			$Results->save(); 
			
			$Helper->ChannelPartnerRequest($Hotel_ID, $cRoomTypeId, $ArrivalDate, $DepartureDate);
			
			return redirect('cancel-reservation/'.$RsrvId)->with("success", "Reservation# {$RsrvId} cancelled successfully");	
		}else{
			return redirect('cancel-reservation/{{$RsrvId}}')->with("danger", "Please select your Motel");
		}
    }
	
	public function view_change(Request $request, $id)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$toDay = date("Y-m-d");
		
		$Results = DB::select("SELECT GST.guest_id as GuestId, GST.guest_fname as GuestFname, GST.guest_mname as GuestMname, GST.guest_lname as GuestLname,
							   GST.guest_address as GuestAddress, GST.guest_city as GuestCity, GST.guest_state as GuestState, GST.guest_zip as GuestPostal,
							   GST.guest_phone as GuestContact, GST.guest_email as GuestEmail, GST.guest_dob as GuestDOB, GST.guest_vehicle as GuestVehicle,
							   RSV.room_reservation_id as RsrvId, DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate,
							   RSV.reserv_night as StayNights, RSV.status as RsrvStat, RSV.reservation_locked as RsrvLock, RSV.special_request as SpecialRequest,
							   RSV.reserv_source as RsrvSource, RSV.source_reserv_id as SourceRsrvId, RSV.source_cnf_id as SourceRsrvCNFId,
							   (SELECT CP.company_name FROM company_profile as CP WHERE CP.profile_id = RSV.guest_profile_id) as GuestCompany,
							   (SELECT SUM(RF.folio_amount) FROM room_reservation_folio as RF
								WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0
							   ) as EstAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
									)
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit,
							   (SELECT R.room_number
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber,
							   (SELECT RT.room_type
							   	FROM roomtypes as RT
								WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomType
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.room_reservation_id = ".$id." AND RSV.hotel_id = ".$Hotel_ID."
							   ORDER BY ArvlDate ASC, DprtDate ASC
							  ");
		if(count($Results))
		{
			$AlltRst = DB::select("SELECT U.fname as Fname, U.mname as Mname, U.lname as Lname, RRAP.created_at as ChangeDate,
								   ( SELECT R.room_number
									 FROM room_assign as R WHERE R.room_assign_id = RRAP.p_room_assign_id
								   ) as PrvsRoomNumber,
								   ( SELECT R.room_number
									 FROM room_assign as R WHERE R.room_assign_id = RRAP.room_assign_id
								   ) as RoomNumber
								   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
								   INNER JOIN reservation_room_allot_past as RRAP ON RRAP.room_reservation_id = RSV.room_reservation_id
								   INNER JOIN users as U ON U.id = RRAP.added_by
								   WHERE RSV.room_reservation_id = ".$id." AND RSV.hotel_id = ".$Hotel_ID."
								   ORDER BY p_allot_id ASC
								  ");
			return view('guests.view-changes')->with("Results", $Results)->with("AlltRst", $AlltRst);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
    }
	
	public function noshow_reserv(Request $request, $id)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();

		$Results = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $id] ])->first();
		if(count($Results))
		{
			$Results->status = 'Cancelled';
			$Results->cancel_type = 'No Show';
			$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
			$Results->added_by = $admin_ID;
			$Results->save(); 
			
			return redirect('guest-info/'.$id)->with("success", "No Show Labaled successfully");	
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
    }
	
	public function undo_check_in(Request $request, $id)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();

		$Results = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $id] ])->first();
		if(count($Results))
		{
			$Results->status = '';
			$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
			$Results->added_by = $admin_ID;
			$Results->save(); 
			
			return redirect('guest-info/'.$id)->with("success", "Undo Check-In successfully");	
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
    }
	
   	public function guestinfo_edit($id)
    {	
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		
		$RsrvRst = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $id] ])->first();
		if(!count($RsrvRst)){
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
		
		$Results = Guest::where('guest_id', $RsrvRst->guest_id)->first();
		if(count($Results)){
			$RsrvRst = DB::select("SELECT GST.guest_id as GuestId, GST.guest_fname as GuestFname, GST.guest_mname as GuestMname, GST.guest_lname as GuestLname,
								   GST.guest_address as GuestAddress, GST.guest_city as GuestCity, GST.guest_state as GuestState, GST.guest_zip as GuestPostal,
								   GST.guest_phone as GuestContact, GST.guest_email as GuestEmail, GST.guest_dob as GuestDOB, GST.guest_vehicle as GuestVehicle,
								   RSV.room_reservation_id as RsrvId, DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate,
								   RSV.reserv_night as StayNights, RSV.status as RsrvStat, RSV.reservation_locked as RsrvLock, RSV.special_request as SpecialRequest,
								   RSV.reserv_source as RsrvSource, RSV.source_reserv_id as SourceRsrvId, RSV.source_cnf_id as SourceRsrvCNFId,
								   (SELECT CP.company_name FROM company_profile as CP WHERE CP.profile_id = RSV.guest_profile_id) as GuestCompany,
								   (SELECT SUM(RF.folio_amount) FROM room_reservation_folio as RF
									WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0
								   ) as EstAmount,
								   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
										  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
										) - 
										( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
										  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
										)
								   ) as PayAmount,
								   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
										  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
										) - 
										( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
										  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
										)
								   ) as PayDeposit,
								   (SELECT R.room_number
									FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
									WHERE RRA.room_reservation_id = RSV.room_reservation_id
								   ) as RoomNumber,
								   (SELECT RT.room_type
									FROM roomtypes as RT
									WHERE RT.room_type_id = RSV.room_type_id
								   ) as RoomType
								   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
								   WHERE RSV.room_reservation_id = ".$id." AND RSV.hotel_id = ".$Hotel_ID."
								   ORDER BY ArvlDate ASC, DprtDate ASC
								  ");
			return view('guests.guest-info-edit')->with("Results", $Results)->with("RsrvRst", $RsrvRst);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
    }

    public function guestinfo_update(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$Rsrv_Id = $request->room_reservation_id;
		
		$validator = Validator::make($request->all(), [
           	'guest_fname' => 'Required|Min:3|Max:80|Alpha',
			'guest_lname' => 'Required|Min:3|Max:80|Alpha',
			'guest_address' => 'Required'
        ]);
		
		if ($validator->fails()) {
            return redirect('guest-info/'.$Rsrv_Id.'/edit')->withErrors($validator)->withInput();
        }
		else
		{
			$Guests = Guest::where('guest_id', $id)->first();
			if($Guests){
				$Guests->guest_title  = $request->guest_title;
				$Guests->guest_fname  = $request->guest_fname;
				$Guests->guest_mname = $request->guest_mname;
				$Guests->guest_lname = $request->guest_lname;
				$Guests->guest_address = $request->guest_address;
				$Guests->guest_country = $request->guest_country;
				$Guests->guest_zip = $request->guest_zip;
				$Guests->guest_city = $request->guest_city;
				$Guests->guest_state = $request->guest_state;
				$Guests->guest_email = $request->guest_email;
				$Guests->guest_phone = $request->guest_phone;
				$Guests->guest_dob = $request->guest_dob;
				$Guests->guest_nation = $request->guest_nation;
				$Guests->guest_idproof = $request->guest_idproof;
				$Guests->guest_idproof_other = $request->guest_idproof_other;
				$Guests->guest_proofno = $request->guest_proofno;
				$Guests->guest_group = $request->guest_group;
				$Guests->guest_vehicle = $request->guest_vehicle;
				$Guests->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Guests->added_by = $admin_ID;
				$Guests->save();
			}
			return redirect('guest-info/'.$Rsrv_Id)->with('success', 'Guest Information updated successfully');
		}
    }
	
	public function confirmation_letter(Request $request, $id)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		
		$RsrvRst = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $id] ])->first();
		if(count($RsrvRst))
		{
			$FolioRst = DB::select("SELECT folio_date, SUM(folio_amount) as dayTotal FROM room_reservation_folio
									WHERE hotel_id = ".$RsrvRst->hotel_id." AND room_reservation_id = ".$RsrvRst->room_reservation_id." AND folio_order > 0
									GROUP BY folio_date ORDER BY folio_date ASC");
			$RoomTypeRst = Roomtype::where([ ['hotel_id', $RsrvRst->hotel_id], ['room_type_id', $RsrvRst->room_type_id] ])->first();
			$HotelRst = Hotel::where('hotel_id', $RsrvRst->hotel_id)->first();
			$GuestRst = Guest::where('guest_id', $RsrvRst->guest_id)->first();
			return view('guests.confirmation-letter')->with("RsrvRst", $RsrvRst)->with("FolioRst", $FolioRst)
													 ->with("HotelRst", $HotelRst)->with("GuestRst", $GuestRst)->with("RoomTypeRst", $RoomTypeRst);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
	}
	
	public function registration_card(Request $request, $id)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		
		$RsrvRst = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $id] ])->first();
		if(count($RsrvRst))
		{
			$RoomNumber = "";
			$FolioRst = DB::select("SELECT folio_date, SUM(folio_amount) as dayTotal FROM room_reservation_folio
									WHERE hotel_id = ".$RsrvRst->hotel_id." AND room_reservation_id = ".$RsrvRst->room_reservation_id." AND folio_order > 0
									GROUP BY folio_date ORDER BY folio_date ASC");
			$RoomTypeRst = Roomtype::where([ ['hotel_id', $RsrvRst->hotel_id], ['room_type_id', $RsrvRst->room_type_id] ])->first();
			$RoomNoRst = DB::select("SELECT RA.room_number
									 FROM reservation_room_allot as RRA INNER JOIN room_assign as RA ON RA.room_assign_id = RRA.room_assign_id
									 WHERE RRA.hotel_id = ".$RsrvRst->hotel_id." AND RRA.room_reservation_id = ".$RsrvRst->room_reservation_id);
			if(count($RoomNoRst))
			{
				foreach($RoomNoRst as $RoomNoVal){
					$RoomNumber = $RoomNoVal->room_number;
				}
			}
			$HotelRst = Hotel::where('hotel_id', $RsrvRst->hotel_id)->first();
			$GuestRst = Guest::where('guest_id', $RsrvRst->guest_id)->first();
			return view('guests.registration-card')->with("RsrvRst", $RsrvRst)->with("FolioRst", $FolioRst)->with("RoomNumber", $RoomNumber)
												   ->with("HotelRst", $HotelRst)->with("GuestRst", $GuestRst)->with("RoomTypeRst", $RoomTypeRst);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
	}
	
	public function confirmation_mail(Request $request)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		
		$RsrvId       = $request->rsrv_id;
		$emailContent = $request->emailContent;
		$emailAddress = $request->emailAddress;
		$emailFrom    = $request->emailFrom;
		$emailFromName= str_replace("&amp;", "&", $request->emailFromName);
		
		if($request->ajax())
		{
			if(trim($emailAddress) == '')
			{
				return response()->json(['response' => 'Please enter Email Address', 'status' => 'error']);
			}
			
			$MailTo = $emailAddress;
			$MailSubject = 'Email Confirmation - Reservation# '.$RsrvId;
			$MailContent = $emailContent;
			
			$MailHeader  = "MIME-Version: 1.0" . "\r\n";
			$MailHeader .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$MailHeader .= 'From: '.$emailFromName.'<'.$emailFrom.'>' . "\r\n";
			if(mail($MailTo, $MailSubject, $MailContent, $MailHeader))
			{
				return response()->json(['response' => "Mail Send Successfully", 'status' => 'success']);
			}else{
				return response()->json(['response' => "Mail Failed", 'status' => 'error']);
			}
		}else{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
	}
	
	public function folio_print(Request $request, $id)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		
		$RsrvRst = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $id] ])->first();
		if(count($RsrvRst))
		{
			$RoomNumber = "";
			$FolioRst = DB::select("SELECT folio_date, SUM(folio_amount) as dayTotal FROM room_reservation_folio
									WHERE hotel_id = ".$RsrvRst->hotel_id." AND room_reservation_id = ".$RsrvRst->room_reservation_id." AND folio_order > 0
									GROUP BY folio_date ORDER BY folio_date ASC");
			$RoomTypeRst = Roomtype::where([ ['hotel_id', $RsrvRst->hotel_id], ['room_type_id', $RsrvRst->room_type_id] ])->first();
			$RoomNoRst = DB::select("SELECT RA.room_number
									 FROM reservation_room_allot as RRA INNER JOIN room_assign as RA ON RA.room_assign_id = RRA.room_assign_id
									 WHERE RRA.hotel_id = ".$RsrvRst->hotel_id." AND RRA.room_reservation_id = ".$RsrvRst->room_reservation_id);
			if(count($RoomNoRst))
			{
				foreach($RoomNoRst as $RoomNoVal){
					$RoomNumber = $RoomNoVal->room_number;
				}
			}
			$HotelRst = Hotel::where('hotel_id', $RsrvRst->hotel_id)->first();
			$GuestRst = Guest::where('guest_id', $RsrvRst->guest_id)->first();
			return view('guests.folio-print')->with("RsrvRst", $RsrvRst)->with("FolioRst", $FolioRst)->with("RoomNumber", $RoomNumber)
										     ->with("HotelRst", $HotelRst)->with("GuestRst", $GuestRst)->with("RoomTypeRst", $RoomTypeRst);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
	}
	
	public function payment_pay(Request $request, $id)
    {
        $Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		
		$rsrv_Id   = $id;
		$rsrv_Type = $request->type;
		
		$Results = DB::select("SELECT RSV.guest_profile_id, RSV.arrival,
							   ( SELECT SUM(RF.folio_amount) FROM room_reservation_folio as RF
								 WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0
							   ) as EstAmount,
							   ( ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
								 ) - 
								 ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
								 )
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.room_reservation_id = ".$rsrv_Id." AND RSV.hotel_id = ".$Hotel_ID."
							  ");
		if(count($Results))
		{
			foreach($Results as $Value){
				$search_Date= date('m/d/Y', strtotime($Value->arrival));
				$esmt_Rate  = $Value->EstAmount;
				$pay_Rate   = $Value->PayAmount;
				$dpst_Rate  = $Value->PayDeposit;
				$Profile_Id = $Value->guest_profile_id;
			}
			$rsrv_Min_Rate = "1";
			$rsrv_Rate = $pay_Rate-$dpst_Rate;
			if($pay_Rate<=0){
				$rsrv_Rate = $esmt_Rate-$dpst_Rate;
			}
		
			return view('guests.payment-pay')->with("Hotel_ID", $Hotel_ID)->with("rsrv_Id", $rsrv_Id)->with("rsrv_Rate", $rsrv_Rate)->with("rsrv_Min_Rate", $rsrv_Min_Rate)->with("Profile_Id", $Profile_Id)->with("rsrv_Type", $rsrv_Type)->with("esmt_Rate", $esmt_Rate)->with("pay_Rate", $pay_Rate)->with("dpst_Rate", $dpst_Rate)->with("search_Date", $search_Date);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$rsrv_Id." not Found");
		}
	}
	
	public function payment_pay_store(Request $request, $id)
    {
        $Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		
		$rsrv_Id     = $id;
		$rsrv_Type   = $request->rsrv_Type;
		$paymentMode = $request->payment_mode;
		
		$amount_Type = $request->rsrv_amount;
		if($amount_Type == 1){
			$pay_Amount = $request->rsrv_rate;
		}elseif($amount_Type == 2){
			$pay_Amount = $request->rsrv_min_rate;
		}elseif($amount_Type == 3){
			$pay_Amount = $request->rsrv_custom;
		}
		
		$bank_name    = $request->bank_name;
		$holder_name  = $request->holder_name;
		$check_number = $request->check_number;
		$check_date   = $request->check_date;
		
		$cc_number   = str_replace("-","",$request->cc_number);
		$cc_holder_f = $request->cc_holder_fname;
		$cc_holder_l = $request->cc_holder_lname;
		$cc_exp_mm   = $request->cc_exp_mm;
		$cc_exp_yy   = $request->cc_exp_yy;
		
		$folio_Comment = $request->folio_comment;
		
		$Result = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $rsrv_Id] ])->first();
		$RoomTypeId = $Result->room_type_id;
		$GuestId    = $Result->guest_id;
		if($paymentMode == 1)
		{
			$unique_ID = "CSH".$Helper->getUniqueId().$rsrv_Id;
			
			$PaymentOptionOBJ = new PaymentOption;
			$PaymentOptionOBJ->transaction_id = $unique_ID;
			$PaymentOptionOBJ->payment_mode   = $paymentMode;
			$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
			$PaymentOptionOBJ->room_reservation_id = $rsrv_Id;
			$PaymentOptionOBJ->reg_ip   = $_SERVER['REMOTE_ADDR'];
			$PaymentOptionOBJ->added_by = $admin_ID;
			$PaymentOptionOBJ->save();
			
			$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
			$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
			$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
			$RoomReservationFolioMetaOBJ->folio_type = 0;
			$RoomReservationFolioMetaOBJ->folio_date = date("Y-m-d");
			$RoomReservationFolioMetaOBJ->folio_pay_type = 'Cash Payment';
			$RoomReservationFolioMetaOBJ->folio_amount = $pay_Amount;
			$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
			$RoomReservationFolioMetaOBJ->folio_auto  = 0;
			$RoomReservationFolioMetaOBJ->folio_order = 0;
			$RoomReservationFolioMetaOBJ->log_id = $PaymentOptionOBJ->log_id;
			$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
			$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
			$RoomReservationFolioMetaOBJ->save();
			
			return redirect('guest-folio/'.$rsrv_Id);
		}
		elseif($paymentMode == 2)
		{
			$unique_ID = "CRD".$Helper->getUniqueId().$rsrv_Id;
			
			$PaymentOptionOBJ = new PaymentOption;
			$PaymentOptionOBJ->transaction_id = $unique_ID;
			$PaymentOptionOBJ->payment_mode   = $paymentMode;
			$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
			$PaymentOptionOBJ->room_reservation_id = $rsrv_Id;
			$PaymentOptionOBJ->cc_number = $cc_number;
			$PaymentOptionOBJ->cc_holder_f = $cc_holder_f;
			$PaymentOptionOBJ->cc_holder_l = $cc_holder_l;
			$PaymentOptionOBJ->cc_exp_mm = $cc_exp_mm;
			$PaymentOptionOBJ->cc_exp_yy = $cc_exp_yy;
			$PaymentOptionOBJ->reg_ip    = $_SERVER['REMOTE_ADDR'];
			$PaymentOptionOBJ->added_by  = $admin_ID;
			$PaymentOptionOBJ->save();
			
			$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
			$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
			$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
			$RoomReservationFolioMetaOBJ->folio_type = 0;
			$RoomReservationFolioMetaOBJ->folio_date = date("Y-m-d");
			$RoomReservationFolioMetaOBJ->folio_pay_type = 'CC Payment';
			$RoomReservationFolioMetaOBJ->folio_amount = $pay_Amount;
			$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
			$RoomReservationFolioMetaOBJ->folio_auto  = 0;
			$RoomReservationFolioMetaOBJ->folio_order = 0;
			$RoomReservationFolioMetaOBJ->log_id = $PaymentOptionOBJ->log_id;
			$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
			$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
			$RoomReservationFolioMetaOBJ->save();
			
			return redirect('guest-folio/'.$rsrv_Id);
		}
		elseif($paymentMode == 3)
		{
			$unique_ID = "CRD".$Helper->getUniqueId().$rsrv_Id;
			
			$PaymentOptionOBJ = new PaymentOption;
			$PaymentOptionOBJ->transaction_id = $unique_ID;
			$PaymentOptionOBJ->payment_mode   = $paymentMode;
			$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
			$PaymentOptionOBJ->room_reservation_id = $rsrv_Id;
			$PaymentOptionOBJ->cc_number = $cc_number;
			$PaymentOptionOBJ->cc_holder_f = $cc_holder_f;
			$PaymentOptionOBJ->cc_holder_l = $cc_holder_l;
			$PaymentOptionOBJ->cc_exp_mm = $cc_exp_mm;
			$PaymentOptionOBJ->cc_exp_yy = $cc_exp_yy;
			$PaymentOptionOBJ->reg_ip    = $_SERVER['REMOTE_ADDR'];
			$PaymentOptionOBJ->added_by  = $admin_ID;
			$PaymentOptionOBJ->save();
			
			return redirect('guest-info/'.$rsrv_Id);
		}
		elseif($paymentMode == 4)
		{
			$unique_ID = "CHK".$Helper->getUniqueId().$rsrv_Id;
			
			$PaymentOptionOBJ = new PaymentOption;
			$PaymentOptionOBJ->transaction_id = $unique_ID;
			$PaymentOptionOBJ->payment_mode   = $paymentMode;
			$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
			$PaymentOptionOBJ->room_reservation_id = $rsrv_Id;
			$PaymentOptionOBJ->bank_name   	= $bank_name;
			$PaymentOptionOBJ->holder_name  = $holder_name;
			$PaymentOptionOBJ->check_number = $check_number;
			$PaymentOptionOBJ->check_date 	= $check_date;
			$PaymentOptionOBJ->reg_ip   = $_SERVER['REMOTE_ADDR'];
			$PaymentOptionOBJ->added_by = $admin_ID;
			$PaymentOptionOBJ->save();
			
			$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
			$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
			$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
			$RoomReservationFolioMetaOBJ->folio_type = 0;
			$RoomReservationFolioMetaOBJ->folio_date = date("Y-m-d");
			$RoomReservationFolioMetaOBJ->folio_pay_type = 'Check Payment';
			$RoomReservationFolioMetaOBJ->folio_amount = $pay_Amount;
			$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
			$RoomReservationFolioMetaOBJ->folio_auto  = 0;
			$RoomReservationFolioMetaOBJ->folio_order = 0;
			$RoomReservationFolioMetaOBJ->log_id = $PaymentOptionOBJ->log_id;
			$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
			$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
			$RoomReservationFolioMetaOBJ->save();
			
			return redirect('guest-folio/'.$rsrv_Id);
		}
		elseif($paymentMode == 5)
		{
			$unique_ID = "CMP".$Helper->getUniqueId().$rsrv_Id;
			
			$PaymentOptionOBJ = new PaymentOption;
			$PaymentOptionOBJ->transaction_id = $unique_ID;
			$PaymentOptionOBJ->payment_mode   = $paymentMode;
			$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
			$PaymentOptionOBJ->room_reservation_id    = $rsrv_Id;
			$PaymentOptionOBJ->reg_ip   = $_SERVER['REMOTE_ADDR'];
			$PaymentOptionOBJ->added_by = $admin_ID;
			$PaymentOptionOBJ->save();
			
			$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
			$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
			$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
			$RoomReservationFolioMetaOBJ->folio_type = 0;
			$RoomReservationFolioMetaOBJ->folio_date = date("Y-m-d");
			$RoomReservationFolioMetaOBJ->folio_pay_type = 'Company Payment';
			$RoomReservationFolioMetaOBJ->folio_amount = $pay_Amount;
			$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
			$RoomReservationFolioMetaOBJ->folio_auto  = 0;
			$RoomReservationFolioMetaOBJ->folio_order = 0;
			$RoomReservationFolioMetaOBJ->log_id = $PaymentOptionOBJ->log_id;
			$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
			$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
			$RoomReservationFolioMetaOBJ->save();
			
			return redirect('guest-folio/'.$rsrv_Id);
		}
		
	}
	
	public function postcharges(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$RsrvRst = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $id] ])->first();
		if(count($RsrvRst)){
			return view('guests.folio-post-charge')->with("Hotel_ID", $Hotel_ID)->with("RsrvRst", $RsrvRst);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
    }
	
	public function postcharges_update(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$RsrvId     = $request->rsrv_id;
		$chargeType = $request->folio_pay_type;
		$chargeDate = date('Y-m-d', strtotime($request->folio_date));
		$chargeComment = $request->folio_comment;
		$chargeAmount  = $request->folio_amount;
		$chargeAuto = ($chargeType ==  1)?$request->folio_auto:0;
		
		$RsrvRst = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $RsrvId] ])->first();
		if(count($RsrvRst))
		{
			$rsrvTaxBool = $RsrvRst->reservation_tax_exmpt;
			
			$chargeArray = array(1=>'Room Charge', 11=>'Pet Charge', 12=>'Rollaway Charge', 13=>'Damage Charge', 14=>'Other Charge');
			
			$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
			$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
			$RoomReservationFolioMetaOBJ->room_reservation_id = $RsrvRst->room_reservation_id;
			$RoomReservationFolioMetaOBJ->folio_type = 1;
			$RoomReservationFolioMetaOBJ->folio_date = $chargeDate;
			$RoomReservationFolioMetaOBJ->folio_pay_type = $chargeArray[$chargeType];
			$RoomReservationFolioMetaOBJ->folio_amount = $chargeAmount;
			$RoomReservationFolioMetaOBJ->folio_comment = $chargeComment;
			$RoomReservationFolioMetaOBJ->folio_auto = $chargeAuto;
			$RoomReservationFolioMetaOBJ->folio_order = $chargeType;
			$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
			$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
			$RoomReservationFolioMetaOBJ->save();
			
			if($chargeType == 1)
			{
				if($rsrvTaxBool == 1)
				{
					$TaxExemptFolio = new RoomReservationFolioExempt;
					$TaxExemptFolio->hotel_id 		= $Hotel_ID;
					$TaxExemptFolio->room_reservation_id = $RsrvRst->room_reservation_id;
					$TaxExemptFolio->folio_type 	= 1;
					$TaxExemptFolio->folio_date 	= $chargeDate;
					$TaxExemptFolio->folio_pay_type = $chargeArray[$chargeType];
					$TaxExemptFolio->folio_amount 	= $chargeAmount;
					$TaxExemptFolio->folio_data 	= $chargeAmount;
					$TaxExemptFolio->folio_order 	= $chargeType;
					$TaxExemptFolio->folio_from 	= 0;
					$TaxExemptFolio->save();
				}
				$folioTaxQry = DB::select("SELECT tax_type as fTaxType, tax_value as fTaxValue, tax_value_type as fValueType
										   FROM taxes WHERE hotel_id = ".$Hotel_ID." AND status = 0 AND
										   NOT( (tax_end != '0000-00-00' AND tax_end < '".$chargeDate."') OR
												(tax_start != '0000-00-00' AND tax_start >= '".$chargeDate."') OR
												(tax_end = '0000-00-00' AND tax_start > '".$chargeDate."'AND tax_start < '".$chargeDate."') OR
												(tax_start = '0000-00-00' AND tax_end < '".$chargeDate."' AND tax_end > '".$chargeDate."')
											  )
										  ");
				$SJ = 2;
				foreach($folioTaxQry as $folioTaxRst)
				{
					$folio_Data = "";
					$dayTaxAmount = "";
					if($folioTaxRst->fValueType == 1)
					{
						$dayTaxAmount = round((($chargeAmount*$folioTaxRst->fTaxValue)/100), 2);
						$folio_Data = $folioTaxRst->fTaxValue."%";
					}
					elseif($folioTaxRst->fValueType == 2)
					{
						$dayTaxAmount = $dayTaxAmount+( round($folioTaxRst->fTaxValue, 2) );
						$folio_Data = $folioTaxRst->fTaxValue;
					}
					if($rsrvTaxBool == 0)
					{
						$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
						$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
						$RoomReservationFolioMetaOBJ->room_reservation_id = $RsrvRst->room_reservation_id;
						$RoomReservationFolioMetaOBJ->folio_type = 1;
						$RoomReservationFolioMetaOBJ->folio_date = $chargeDate;
						$RoomReservationFolioMetaOBJ->folio_pay_type = $folioTaxRst->fTaxType;
						$RoomReservationFolioMetaOBJ->folio_amount = $dayTaxAmount;
						$RoomReservationFolioMetaOBJ->folio_data = $folio_Data;
						$RoomReservationFolioMetaOBJ->folio_comment = "";
						$RoomReservationFolioMetaOBJ->folio_auto = 0;
						$RoomReservationFolioMetaOBJ->folio_order = $SJ;
						$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
						$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
						$RoomReservationFolioMetaOBJ->save();
					}
					else
					{
						$TaxExemptFolio = new RoomReservationFolioExempt;
						$TaxExemptFolio->hotel_id 		= $Hotel_ID;
						$TaxExemptFolio->room_reservation_id = $RsrvRst->room_reservation_id;
						$TaxExemptFolio->folio_type 	= 1;
						$TaxExemptFolio->folio_date 	= $chargeDate;
						$TaxExemptFolio->folio_pay_type = $folioTaxRst->fTaxType;
						$TaxExemptFolio->folio_amount 	= $dayTaxAmount;
						$TaxExemptFolio->folio_data 	= $folio_Data;
						$TaxExemptFolio->folio_order 	= $SJ;
						$TaxExemptFolio->folio_from 	= 0;
						$TaxExemptFolio->save();
					}
					$SJ++;
				}
			}
			return redirect('guest-folio/'.$RsrvRst->room_reservation_id)->with("success", "Charges added Successfully.");
		}
		else
		{
			return redirect('guest-info/{{$RsrvId}}')->with("danger", "Reservation not Found!");
		}
    }
	
	public function postadjustment(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$RsrvRst = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $id] ])->first();
		if(count($RsrvRst)){
			return view('guests.folio-post-adjustment')->with("Hotel_ID", $Hotel_ID)->with("RsrvRst", $RsrvRst);
		}else{
			return redirect('in-house')->with("danger", "Reservation# ".$id." not Found");
		}
    }
	
	public function adjustAmount(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		if($request->ajax())
		{	
			$chargeArray = array(1=>'Room Charge', 11=>'Pet Charge', 12=>'Rollaway Charge', 13=>'Damage Charge', 14=>'Other Charge');
			$RsrvId    = $request->rsrv_id;	
			$PayType   = $request->folio_pay_type;
			$FolioDate = date('Y-m-d', strtotime($request->folio_date));
			$FolioType = $chargeArray[$PayType];
			$Results = RoomReservationFolioMeta::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $RsrvId],
														 ['folio_pay_type', $FolioType], ['folio_date', $FolioDate], ['folio_type', 1] ])
												->orderBy('folio_id', 'DESC')
												->first();
			if(count($Results))
			{
				return response()->json(['response' => $Results, 'status' => 'success']);
			}else{
				return response()->json(['response' => "Charges not Found", 'status' => 'error']);
			}
		}else{
			return response()->json(['response' => 'Try Again.', 'status' => 'error']);
		}
    }
	
	
	public function postadjustment_update(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$RsrvId     = $request->rsrv_id;
		$chargeType = $request->folio_pay_type;
		$chargeDate = date('Y-m-d', strtotime($request->folio_date));
		$chargeComment = $request->folio_comment;
		$chargeAmount  = $request->folio_amount;
		$chargeAuto = 0;
		
		$RsrvRst = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $RsrvId] ])->first();
		if(count($RsrvRst))
		{
			$rsrvTaxBool = $RsrvRst->reservation_tax_exmpt;
			
			$chargeArray = array(1=>'Room Charge', 11=>'Pet Charge', 12=>'Rollaway Charge', 13=>'Damage Charge', 14=>'Other Charge');
			
			$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
			$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
			$RoomReservationFolioMetaOBJ->room_reservation_id = $RsrvRst->room_reservation_id;
			$RoomReservationFolioMetaOBJ->folio_type = 2;
			$RoomReservationFolioMetaOBJ->folio_date = $chargeDate;
			$RoomReservationFolioMetaOBJ->folio_pay_type = $chargeArray[$chargeType];
			$RoomReservationFolioMetaOBJ->folio_amount = $chargeAmount;
			$RoomReservationFolioMetaOBJ->folio_comment = $chargeComment;
			$RoomReservationFolioMetaOBJ->folio_auto = $chargeAuto;
			$RoomReservationFolioMetaOBJ->folio_order = $chargeType;
			$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
			$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
			$RoomReservationFolioMetaOBJ->save();
			
			if($chargeType == 1)
			{
				if($rsrvTaxBool == 1)
				{
					$TaxExemptFolio = new RoomReservationFolioExempt;
					$TaxExemptFolio->hotel_id 		= $Hotel_ID;
					$TaxExemptFolio->room_reservation_id = $RsrvRst->room_reservation_id;
					$TaxExemptFolio->folio_type 	= 2;
					$TaxExemptFolio->folio_date 	= $chargeDate;
					$TaxExemptFolio->folio_pay_type = $chargeArray[$chargeType];
					$TaxExemptFolio->folio_amount 	= $chargeAmount;
					$TaxExemptFolio->folio_data 	= $chargeAmount;
					$TaxExemptFolio->folio_order 	= $chargeType;
					$TaxExemptFolio->folio_from 	= 0;
					$TaxExemptFolio->save();
				}
				
				$folioTaxQry = DB::select("SELECT tax_type as fTaxType, tax_value as fTaxValue, tax_value_type as fValueType
										   FROM taxes WHERE hotel_id = ".$Hotel_ID." AND status = 0 AND
										   NOT( (tax_end != '0000-00-00' AND tax_end < '".$chargeDate."') OR
												(tax_start != '0000-00-00' AND tax_start >= '".$chargeDate."') OR
												(tax_end = '0000-00-00' AND tax_start > '".$chargeDate."'AND tax_start < '".$chargeDate."') OR
												(tax_start = '0000-00-00' AND tax_end < '".$chargeDate."' AND tax_end > '".$chargeDate."')
											  )
										  ");
				$SJ = 2;
				foreach($folioTaxQry as $folioTaxRst)
				{
					$folio_Data = "";
					$dayTaxAmount = "";
					if($folioTaxRst->fValueType == 1)
					{
						$dayTaxAmount = round((($chargeAmount*$folioTaxRst->fTaxValue)/100), 2);
						$folio_Data = $folioTaxRst->fTaxValue."%";
					}
					elseif($folioTaxRst->fValueType == 2)
					{
						$dayTaxAmount = $dayTaxAmount+( round($folioTaxRst->fTaxValue, 2) );
						$folio_Data = $folioTaxRst->fTaxValue;
					}
					
					if($rsrvTaxBool == 0)
					{
						$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
						$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
						$RoomReservationFolioMetaOBJ->room_reservation_id = $RsrvRst->room_reservation_id;
						$RoomReservationFolioMetaOBJ->folio_type = 2;
						$RoomReservationFolioMetaOBJ->folio_date = $chargeDate;
						$RoomReservationFolioMetaOBJ->folio_pay_type = $folioTaxRst->fTaxType;
						$RoomReservationFolioMetaOBJ->folio_amount = $dayTaxAmount;
						$RoomReservationFolioMetaOBJ->folio_data = $folio_Data;
						$RoomReservationFolioMetaOBJ->folio_comment = "";
						$RoomReservationFolioMetaOBJ->folio_auto = 0;
						$RoomReservationFolioMetaOBJ->folio_order = $SJ;
						$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
						$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
						$RoomReservationFolioMetaOBJ->save();
					}
					else
					{
						$TaxExemptFolio = new RoomReservationFolioExempt;
						$TaxExemptFolio->hotel_id 		= $Hotel_ID;
						$TaxExemptFolio->room_reservation_id = $RsrvRst->room_reservation_id;
						$TaxExemptFolio->folio_type 	= 2;
						$TaxExemptFolio->folio_date 	= $chargeDate;
						$TaxExemptFolio->folio_pay_type = $folioTaxRst->fTaxType;
						$TaxExemptFolio->folio_amount 	= $dayTaxAmount;
						$TaxExemptFolio->folio_data 	= $folio_Data;
						$TaxExemptFolio->folio_order 	= $SJ;
						$TaxExemptFolio->folio_from 	= 0;
						$TaxExemptFolio->save();
					}
					$SJ++;
				}
			}
			return redirect('guest-folio/'.$RsrvRst->room_reservation_id)->with("success", "Charges adjusted Successfully.");
		}
		else
		{
			return redirect('guest-info/{{$RsrvId}}')->with("danger", "Reservation not Found!");
		}
    }
	
	public function dayWiseRatePlan(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$RsrvId = $request->rsrvId;
		
		if($request->ajax())
		{	
			$Results = RoomReservationFolio::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $RsrvId], ['folio_pay_type', 'Room Charge'] ])->get();
			if(count($Results))
			{
				$outPut =  '<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginBottom10">
								<div class="col-md-12 col-sm-12 col-xs-12 labelText zeroPadd">Day-wise Room Rate Charges (Excluding Tax):</div>
							</div>';
				foreach($Results as $Value)
				{
					$outPut .= '<div class="col-md-6 col-sm-12 col-xs-12 zeroPadd LneHeight30">
									<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">'.date("m/d/Y", strtotime($Value->folio_date)).'</div>
									<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd" id="model_GuestName">$ '.$Value->folio_amount.'</div>
								</div>';
				}
			
				return response()->json(['response' => $outPut, 'status' => 'success']);
			}else{
				return response()->json(['response' => "Charges not Found", 'status' => 'error']);
			}
		}else{
			return response()->json(['response' => 'Try Again.', 'status' => 'error']);
		}
    }
	
	public function transactionDetailPayment(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$RsrvId = $request->rsrvId;
		$LogId = $request->logId;
		
		if($request->ajax())
		{	
			$Results = DB::select("SELECT RRFM.folio_pay_type, RRFM.folio_amount, RRFM.folio_comment,
								   PO.transaction_id, PO.room_reservation_id, PO.payment_mode, PO.created_at,
								   PO.bank_name, PO.holder_name, PO.check_number, PO.check_date,
								   PO.cc_number, CONCAT(PO.cc_holder_f, ' ', PO.cc_holder_l) as cc_holder,  PO.cc_exp_mm, PO.cc_exp_yy
								   FROM room_reservation_folio_meta as RRFM RIGHT JOIN payment_option as PO ON PO.log_id = RRFM.log_id
								   WHERE PO.hotel_id = ".$Hotel_ID." AND PO.room_reservation_id = ".$RsrvId." AND PO.log_id = ".$LogId." 
								  ");
			if(count($Results))
			{
				$Value = json_decode(json_encode($Results[0]), true);
				
				if(isset($Value['folio_pay_type']) && $Value['folio_pay_type']){ $payType = $Value['folio_pay_type']; }else{ $payType = "Card Auth"; }
				if(isset($Value['folio_amount']) && $Value['folio_amount']){ $payAmount = "$ ".$Value['folio_amount']; }else{ $payAmount = ""; }
				if(isset($Value['folio_comment']) && $Value['folio_comment']){ $payComment = $Value['folio_comment']; }else{ $payComment = ""; }
				$outPut =  '<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd LneHeight30">
								<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">Transaction Date</div>
								<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd">'.date('m/d/Y h:i A', strtotime($Value['created_at'])).'</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd LneHeight30">
								<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">Reservation Id</div>
								<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd">'.$Value['room_reservation_id'].'</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd LneHeight30">
								<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">Payment Mode</div>
								<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd">'.$payType.'</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd LneHeight30">
								<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">Amount</div>
								<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd">'.$payAmount.'</div>
							</div>
							';
				if($Value['payment_mode'] == 4){
				$outPut .= '<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd LneHeight30">
								<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">Name of Bank</div>
								<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd">'.$Value['bank_name'].'</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd LneHeight30">
								<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">Account Holder Name</div>
								<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd">'.$Value['holder_name'].'</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd LneHeight30">
								<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">Check Number</div>
								<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd">'.$Value['check_number'].'</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd LneHeight30">
								<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">Check Date</div>
								<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd">'.date("m/d/Y", strtotime($Value['check_date'])).'</div>
							</div>';
				}
				if($Value['payment_mode'] == 2 || $Value['payment_mode'] == 3){
				$outPut .= '<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd LneHeight30">
								<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">Card Number</div>
								<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd">'.$Value['cc_number'].'</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd LneHeight30">
								<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">Account Holder Name</div>
								<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd">'.$Value['cc_holder'].'</div>
							</div>
							<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd LneHeight30">
								<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">Expiration Date</div>
								<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd">'.$Value['cc_exp_mm'].'/'.$Value['cc_exp_yy'].'</div>
							</div>';
				}
				$outPut .= '<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd LneHeight30">
								<div class="col-md-5 col-sm-12 col-xs-12 labelText zeroPadd">Comment</div>
								<div class="col-md-7 col-sm-12 col-xs-12 UpperLetter zeroPadd">'.$payComment.'</div>
							</div>';
				$transId = $Value['transaction_id'];
				
				
				return response()->json(['response' => $outPut, 'transId' => $transId, 'status' => 'success']);
			}else{
				return response()->json(['response' => "Charges not Found", 'status' => 'error']);
			}
		}else{
			return response()->json(['response' => 'Try Again.', 'status' => 'error']);
		}
    }
}