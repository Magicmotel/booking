<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Roomtype;
use App\RoomAssign;
use App\Guest;
use App\RoomReservation;
use App\RoomReservationFolio;
use App\RoomReservationDiscount;
use App\ReservationRoomAllot;
use App\ReservationRoomAllotPast;
use App\CompanyProfile;
use App\CompanyProfileRate;
use App\ZipCountry;
use Helper;
use Auth;
use DB;

class RoomReservationController extends Controller
{
    public function index(Request $request)
    {
        $Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		
		$cntValue = 0;
				
		$varQry = $rquestFromDate = $rquestToDate = ""; $cntSearch = 0;
		if($request->guest_fname){ $varQry .= " AND GST.guest_fname LIKE '%".$request->guest_fname."%'"; $cntValue = 1; }
		if($request->guest_lname){ $varQry .= " AND GST.guest_lname LIKE '%".$request->guest_lname."%'"; $cntValue = 1; }
		if($request->guest_phone){ $varQry .= " AND GST.guest_phone LIKE '%".$request->guest_phone."%'"; $cntValue = 1; }
		if($request->guest_postal){ $varQry .= " AND GST.guest_zip = '".$request->guest_postal."'"; $cntSearch = 1; $cntValue = 1; }
		if($request->company_profile){ $varQry .= " AND RSV.guest_profile_id = ".$request->company_profile; $cntSearch = 1; $cntValue = 1; }
		if($request->guest_group){ $varQry .= " AND GST.guest_group LIKE '%".$request->guest_group."%'"; $cntSearch = 1; $cntValue = 1; }
		if($request->start_date){ $rquestFromDate = date("Y-m-d", strtotime($request->start_date)); $cntSearch = 1; $cntValue = 1;	}
		if($request->end_date){
			$rquestToDate = date("Y-m-d", strtotime($request->end_date)); $cntSearch = 1; $cntValue = 1;
		}else{
			$rquestToDate = date("Y-m-d");
		}
		
		if($rquestFromDate && $rquestToDate){
			$varQry .= " AND ( (RSV.arrival >= '$rquestFromDate' AND RSV.arrival < '$rquestToDate') OR 
							   (RSV.departure > '$rquestFromDate' AND RSV.departure <= '$rquestToDate') OR 
							   (RSV.arrival < '$rquestFromDate' AND RSV.departure > '$rquestToDate')
			  				 )";
		}elseif($rquestFromDate){
			$varQry .= " AND RSV.departure > '".$rquestFromDate."')";
		}
		
		if($request->room_number){
			$RoomAssignRst = RoomAssign::where([ ['hotel_id', $Hotel_ID], ['room_number', $request->room_number] ])->first();
			if(count($RoomAssignRst)){
				$varQry .= " AND RALT.room_assign_id = ".$RoomAssignRst->room_assign_id; 
			}
			$cntSearch = 1; $cntValue = 1;
		}
		
		
		$Results = DB::select("SELECT GST.guest_id as GuestId, GST.guest_fname as GuestFname, GST.guest_lname as GuestLname, GST.guest_phone as GuestPhone,
							   DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate, GST.bad_status as GuestBadStatus,
							   RSV.room_reservation_id as RsrvId, RSV.guest_child as gChild, RSV.status as RsrvStat, RSV.cancel_type as RsrvNoShow,
							   RSV.reservation_locked as RsrvLock, RSV.reserv_source as RsrvSource,
							   (SELECT RT.room_type
							   	FROM roomtypes as RT WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomTypes,
							   (SELECT R.room_number
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   INNER JOIN reservation_room_allot as RALT ON RALT.room_reservation_id = RSV.room_reservation_id
							   WHERE RSV.hotel_id = ".$Hotel_ID.$varQry." ORDER BY ArvlDate ASC, DprtDate ASC
							  ");
		return view('reservation.reservation')->with("Hotel_ID", $Hotel_ID)->with("request", $request)->with("Results", $Results)->with("cntSearch", $cntSearch)->with("cntValue", $cntValue)->with("rstNum", count($Results));
    }

    public function create(Request $request)
    {	
		if($request->input() && !$request->guest_adult){
			$request->guest_adult = 1;
		}
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
        return view('reservation.reservation-create')->with("Hotel_ID", $Hotel_ID)->with("request", $request);
    }
	
	public function walkin(Request $request)
    {	
		if($request->input() && !$request->guest_adult){
			$request->guest_adult = 1;
		}
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
        return view('reservation.reservation-walkin')->with("Hotel_ID", $Hotel_ID)->with("request", $request);
    }
	
	public function guest(Request $request)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
        return view('reservation.reservation-guest')->with("Hotel_ID", $Hotel_ID)->with("request", $request);
    }
	
	public function payment_gateway(Request $request)
    {	
		$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
        return view('reservation.reservation-payment')->with("Hotel_ID", $Hotel_ID)->with("request", $request);
    }

    public function store(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$unique_ID = $Helper->getUniqueId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$rsrvType = $request->reserv_type;
		
		$ArrivalDate   = date("Y-m-d", strtotime($request->arrival));
		$DepartureDate = date("Y-m-d", strtotime($request->departure));
		$fromDate = date('Y-m-d', strtotime($ArrivalDate)); $toDate = date('Y-m-d', strtotime($DepartureDate.'-1 days'));
		
		$profile_ID = $request->company_profile;
		$guest_Tax  = $request->guest_tax;
		if($request->guest_id)
		{
			$Guest_ID = $request->guest_id;
			$Guests = Guest::where('guest_id', $Guest_ID)->first();
			$Guests->guest_id_unq = $unique_ID;
			$Guests->guest_title  = $request->guest_title;
			$Guests->guest_fname  = $request->guest_fname;
			$Guests->guest_mname = $request->guest_mname;
			$Guests->guest_lname = $request->guest_lname;
			$Guests->guest_dob = $request->guest_dob;
			$Guests->guest_address = $request->guest_address;
			$Guests->guest_country = $request->guest_country;
			$Guests->guest_city = $request->guest_city;
			$Guests->guest_state = $request->guest_state;
			$Guests->guest_zip = $request->guest_zip;
			$Guests->guest_email = $request->guest_email;
			$Guests->guest_phone = $request->guest_phone;
			$Guests->guest_nation = $request->guest_nation;
			$Guests->guest_idproof = $request->guest_idproof;
			$Guests->guest_idproof_other = $request->guest_idproof_other;
			$Guests->guest_proofno = $request->guest_proofno;
			$Guests->guest_company = $request->guest_company;
			$Guests->guest_group = $request->guest_group;
			$Guests->guest_vehicle = $request->guest_vehicle;
			$Guests->guest_tax = $request->guest_tax;
			$Guests->reg_ip = $_SERVER['REMOTE_ADDR'];
			$Guests->added_by = $admin_ID;
			$Guests->save();
		}else{
			$GuestsArray   = array();
			$GuestsArray[] = $unique_ID;
			$GuestsArray[] = $request->guest_title;
			$GuestsArray[] = $request->guest_fname;
			$GuestsArray[] = $request->guest_mname;
			$GuestsArray[] = $request->guest_lname;
			$GuestsArray[] = $request->guest_dob;
			$GuestsArray[] = $request->guest_address;
			$GuestsArray[] = $request->guest_country;
			$GuestsArray[] = $request->guest_city;
			$GuestsArray[] = $request->guest_state;
			$GuestsArray[] = $request->guest_zip;
			$GuestsArray[] = $request->guest_email;
			$GuestsArray[] = $request->guest_phone;
			$GuestsArray[] = $request->guest_nation;
			$GuestsArray[] = $request->guest_idproof;
			$GuestsArray[] = $request->guest_idproof_other;
			$GuestsArray[] = $request->guest_proofno;
			$GuestsArray[] = $request->guest_company;
			$GuestsArray[] = $request->guest_group;
			$GuestsArray[] = $request->guest_vehicle;
			$GuestsArray[] = $request->guest_tax;
			$GuestsArray[] = $_SERVER['REMOTE_ADDR'];
			$GuestsArray[] = $admin_ID;

			$GuestsResult = DB::select('CALL Reservation_Guest_Insert_SP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $GuestsArray);
			$GuestIdArray = DB::select('SELECT LAST_INSERT_ID()');
			foreach(json_decode(json_encode($GuestIdArray), true)[0] as $val){
				$Guest_ID = $val;
			}
		}
		
		// Room Reservation
		$modified_PriceArray = explode(",", $request->modified_price);
		$enter_AdultArray    = explode(",", $request->enter_adult);
		$enter_ChildArray    = explode(",", $request->enter_child);
		$enter_RoomArray     = explode(",", $request->enter_room);
		$discountArray 		 = explode(",", $request->discount);
		
		$rsrvTaxBool = $room_typeId = $qty_reserve = $RoomRates = $rsvDayWisePrice = $lastAmount = $totalAmount = 0;
		$valsArray = array(); $cRoomTypeIdArray = array();
		foreach(explode(",", $request->reserve_quantitiy) as $key=>$value)
		{
			$TotalTaxAmount = 0;
			$valsArray = explode("-", $value);
			$room_typeId = $valsArray[0];
			$qty_reserve = $valsArray[1];
			$cRoomTypeIdArray[] = $room_typeId;
			
			
			$RoomListQry = DB::select("SELECT RT.room_type_id, RT.room_type, RT.room_count, RT.room_pic_1, RT.guest_adult, RT.guest_child,
									   (    room_count - 
											( IF( ( SELECT SUM(RSV.qty_reserve) FROM room_reservation as RSV WHERE RSV.room_type_id = RT.room_type_id AND RSV.status != 'Cancelled'
													AND NOT ((RSV.departure <= '".$ArrivalDate."') OR (RSV.arrival >= '".$DepartureDate."')) GROUP BY RSV.room_type_id
												  ) != 0, 
												  ( SELECT SUM(RSV.qty_reserve) FROM room_reservation as RSV WHERE RSV.room_type_id = RT.room_type_id AND RSV.status != 'Cancelled'
													AND NOT ((RSV.departure <= '".$ArrivalDate."') OR (RSV.arrival >= '".$DepartureDate."')) GROUP BY RSV.room_type_id
												  ), 0              
												)
											) - 
											( IF( ( SELECT count(R.room_type_id) FROM room_assign as R
													WHERE R.room_type_id = RT.room_type_id AND (R.status = 0 OR R.trash = 1 OR R.room_number = '') GROUP BY R.room_type_id
												  ) != 0, 
												  ( SELECT count(R.room_type_id) FROM room_assign as R
													WHERE R.room_type_id = RT.room_type_id AND (R.status = 0 OR R.trash = 1 OR R.room_number = '') GROUP BY R.room_type_id
												  ), 0
												)
											) - 
											( IF( ( SELECT count(DISTINCT(BR.room_assign_id)) FROM block_rooms as BR WHERE BR.room_type_id = RT.room_type_id
													AND NOT ((BR.block_to <= '".$ArrivalDate."') OR (BR.block_from >= '".$DepartureDate."')) GROUP BY BR.room_type_id
												  ) != 0, 
												  ( SELECT count(DISTINCT(BR.room_assign_id)) FROM block_rooms as BR WHERE BR.room_type_id = RT.room_type_id
													AND NOT ((BR.block_to <= '".$ArrivalDate."') OR (BR.block_from >= '".$DepartureDate."')) GROUP BY BR.room_type_id
												  ), 0              
												)
											)
										) as CNT
										FROM roomtypes as RT INNER JOIN room_assign as RA ON RA.room_type_id = RT.room_type_id
										WHERE RT.hotel_id = ".$Hotel_ID." AND RT.room_type_id = ".$room_typeId." AND RA.room_number != '' GROUP BY RT.room_type_id");
			if(count($RoomListQry))
			{
				foreach($RoomListQry as $val)
				{
					if($val->CNT >= $qty_reserve)
					{ 
						$room_Rate = round($modified_PriceArray[$key], 2);
						
						$gst_AdultArray = explode("-", $enter_AdultArray[$key]);
						$gst_ChildArray = explode("-", $enter_ChildArray[$key]);
						$gst_RoomArray  = explode("-", $enter_RoomArray[$key]);
						$discountIdArray = explode("-", $discountArray[$key]);
						$discountValues  = implode(",", $discountIdArray);
						
						if($profile_ID && !$room_Rate){
							$CompanyPrice = CompanyProfileRate::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $room_typeId], ['profile_id', $profile_ID] ])->first();
							$room_Rate = $CompanyPrice->company_rate;
						}
						if($profile_ID){
							$companyTaxRst = CompanyProfile::where('profile_id', $profile_ID)->first();
							$rsrvTaxBool   = $companyTaxRst->company_tax;
						}else{
							$rsrvTaxBool = $guest_Tax;
						}
						
						for($PKJ = 1; $PKJ <= $qty_reserve; $PKJ++)
						{
							$valOfAdult = $valOfChild = $valOfRoom = 0;
							if(isset($gst_AdultArray[$PKJ-1]) && !empty($gst_AdultArray[$PKJ-1])){
								$valOfAdult = $gst_AdultArray[$PKJ-1];
							}
							if(isset($gst_ChildArray[$PKJ-1]) && !empty($gst_ChildArray[$PKJ-1])){
								$valOfChild = $gst_ChildArray[$PKJ-1];
							}
							if(isset($gst_RoomArray[$PKJ-1]) && !empty($gst_RoomArray[$PKJ-1])){
								$valOfRoom = $gst_RoomArray[$PKJ-1];
								if($valOfRoom>0)
								{
									$vcntRoom_Rst = DB::select("SELECT R1.room_assign_id as RoomId
																FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
																WHERE R1.status = 1 AND R1.room_number != '' AND R1.trash = 0 AND R1.hotel_id = ".$Hotel_ID."
																AND RT.room_type_id = ".$room_typeId." AND R1.room_assign_id = ".$valOfRoom." AND
																R1.room_assign_id NOT IN(
																	SELECT R.room_assign_id
																	FROM room_reservation as RSV
																	INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
																	INNER JOIN room_assign as R ON R.room_assign_id = RRA.room_assign_id
																	WHERE RSV.hotel_id = ".$Hotel_ID." AND R.room_type_id = ".$room_typeId." AND
																	RSV.status != 'Checked out' AND RSV.status != 'Cancelled' AND
																	NOT ((RSV.departure <= '".$ArrivalDate."') OR (RSV.arrival >= '".$DepartureDate."'))
																)
															   ");
									if(!count($vcntRoom_Rst)){
										$valOfRoom = 0;
									}
								}
							}
							
							if($rsrvType == 'walkin'){
								$rsrvStatus = "Checked In";
							}else{
								$rsrvStatus = "";
							}
							
							$unique_ID = $Helper->getUniqueId();
							
							$ReservationArray   = array();
							$ReservationArray[] = $unique_ID;
							$ReservationArray[] = $Hotel_ID;
							$ReservationArray[] = $Guest_ID;
							$ReservationArray[] = $room_typeId;
							$ReservationArray[] = $ArrivalDate;
							$ReservationArray[] = $DepartureDate;
							$ReservationArray[] = $request->reserv_night;
							$ReservationArray[] = $valOfAdult;
							$ReservationArray[] = $valOfChild;
							$ReservationArray[] = 1;
							$ReservationArray[] = $profile_ID;
							$ReservationArray[] = $rsrvTaxBool;
							$ReservationArray[] = $rsrvStatus;
							$ReservationArray[] = "";
							$ReservationArray[] = "";
							$ReservationArray[] = "";
							$ReservationArray[] = $_SERVER['REMOTE_ADDR'];
							$ReservationArray[] = $admin_ID;
							
							$ReservationResult  = DB::select('CALL Reservation_Room_Insert_SP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $ReservationArray);
							$ReservationIdArray = DB::select('SELECT LAST_INSERT_ID()');
							foreach(json_decode(json_encode($ReservationIdArray), true)[0] as $val){
								$Reservation_ID = $val;
							}
						
							$discountValuesArray = array(); $discountValues = "";
							foreach($discountIdArray as $discountId){
								if($discountId>0){
									$ReservationDiscountArray   = array();
									$ReservationDiscountArray[] = $Hotel_ID;
									$ReservationDiscountArray[] = $Reservation_ID;
									$ReservationDiscountArray[] = $discountId;
									$ReservationDiscountArray[] = $_SERVER['REMOTE_ADDR'];
									$ReservationDiscountArray[] = $admin_ID;
									DB::select('CALL Reservation_Discount_Insert_SP(?,?,?,?,?)', $ReservationDiscountArray);
									$discountValuesArray[] = $discountId;
								}
							}
							$discountValues = implode(",", $discountValuesArray);
							
							if($valOfRoom > 0){
								$pastAllot = new ReservationRoomAllotPast;
								$pastAllot->hotel_id = $Hotel_ID;
								$pastAllot->room_reservation_id = $Reservation_ID;
								$pastAllot->p_room_assign_id = 0;
								$pastAllot->room_assign_id   = $valOfRoom;
								$pastAllot->reg_ip = $_SERVER['REMOTE_ADDR'];
								$pastAllot->added_by = $admin_ID;
								$pastAllot->save();
							}
							
							$ReservationRoomAllotArray   = array();
							$ReservationRoomAllotArray[] = $Hotel_ID;
							$ReservationRoomAllotArray[] = $Reservation_ID;
							$ReservationRoomAllotArray[] = $valOfRoom;
							$ReservationRoomAllotArray[] = $_SERVER['REMOTE_ADDR'];
							$ReservationRoomAllotArray[] = $admin_ID;
							DB::select('CALL Reservation_RoomAllot_Insert_SP(?,?,?,?,?)', $ReservationRoomAllotArray);
							
							$folioRateQry = DB::select("SELECT room_rate_price as fRoomRate, room_rate_date as fRoomDate FROM room_rate_option
														WHERE room_type_id = ".$room_typeId." AND hotel_id = ".$Hotel_ID." AND
														(room_rate_date BETWEEN '".$fromDate."' AND '".$toDate."') ORDER BY room_rate_date ASC
													   ");
							foreach($folioRateQry as $folioRateRst)
							{
								$folio_Data  = $DiscountCodeString = ""; $discountWiseRate = 0;
								$dayWiseRate = ($room_Rate > 0)?$room_Rate:$folioRateRst->fRoomRate;
								$folio_Data  = $dayWiseRate;
								$dayWiseDate = $folioRateRst->fRoomDate;
								
								if($discountValues)
								{
									$discountQry = DB::select("SELECT discount_id as DiscountId, discount_code as DiscountCode, discount_value as DiscountValue,
															   discount_type as DiscountType
															   FROM discounts
															   WHERE hotel_id = ".$Hotel_ID." AND discount_id IN (".$discountValues.")
															   ORDER BY discount_code ASC
															  ");
									$discountWiseRate = 0; $DiscountCodeArray = array();
									foreach($discountQry as $discountRst)
									{
										if($discountRst->DiscountType == 2){
											$discountWiseRate = $discountWiseRate+$discountRst->DiscountValue;
										}else{
											$discountWiseRate = $discountWiseRate+(($dayWiseRate*$discountRst->DiscountValue)/100);
										}
										$DiscountCodeArray[] = $discountRst->DiscountCode;
									}
									$DiscountCodeString = implode(", ",$DiscountCodeArray);
									$discountWiseRate = round($discountWiseRate, 2);
								}
								
								$FinalRoomRate_wo_Tax = $dayWiseRate - $discountWiseRate;
								if($FinalRoomRate_wo_Tax<=0){
									$FinalRoomRate_wo_Tax = 1;
								}
								
								$ReservationFolioArray   = array();
								$ReservationFolioArray[] = $Hotel_ID;
								$ReservationFolioArray[] = $room_typeId;
								$ReservationFolioArray[] = $Reservation_ID;
								$ReservationFolioArray[] = $dayWiseDate;
								$ReservationFolioArray[] = "Room Charge";
								$ReservationFolioArray[] = $FinalRoomRate_wo_Tax;
								$ReservationFolioArray[] = $folio_Data;
								$ReservationFolioArray[] = $DiscountCodeString;;
								$ReservationFolioArray[] = 1;
								$ReservationFolioArray[] = $_SERVER['REMOTE_ADDR'];
								$ReservationFolioArray[] = $admin_ID;
								
								DB::select('CALL Reservation_Folio_Insert_SP(?,?,?,?,?,?,?,?,?,?,?)', $ReservationFolioArray);
								
								if($rsrvTaxBool==0)
								{
									$folioTaxQry = DB::select("SELECT tax_type as fTaxType, tax_value as fTaxValue, tax_value_type as fValueType
															   FROM taxes WHERE hotel_id = ".$Hotel_ID." AND status = 0 AND
															   NOT( (tax_end != '0000-00-00' AND tax_end < '".$dayWiseDate."') OR
															   		(tax_start != '0000-00-00' AND tax_start >= '".$dayWiseDate."') OR
																	(tax_end = '0000-00-00' AND tax_start > '".$dayWiseDate."'AND tax_start < '".$dayWiseDate."') OR
																	(tax_start = '0000-00-00' AND tax_end < '".$dayWiseDate."' AND tax_end > '".$dayWiseDate."')
																  )
															  ");
									$SJ = 2;
									foreach($folioTaxQry as $folioTaxRst){
										$folio_Data = "";
										$dayTaxAmount = "";
										if($folioTaxRst->fValueType == 1){
											$dayTaxAmount = round((($FinalRoomRate_wo_Tax*$folioTaxRst->fTaxValue)/100), 2);
											$folio_Data = $folioTaxRst->fTaxValue."%";
										}elseif($folioTaxRst->fValueType == 2){
											$dayTaxAmount = $dayTaxAmount+( round($folioTaxRst->fTaxValue, 2) );
											$folio_Data = $folioTaxRst->fTaxValue;
										}
										
										$ReservationFolioTArray   = array();
										$ReservationFolioTArray[] = $Hotel_ID;
										$ReservationFolioTArray[] = $room_typeId;
										$ReservationFolioTArray[] = $Reservation_ID;
										$ReservationFolioTArray[] = $dayWiseDate;
										$ReservationFolioTArray[] = $folioTaxRst->fTaxType;
										$ReservationFolioTArray[] = $dayTaxAmount;
										$ReservationFolioTArray[] = $folio_Data;
										$ReservationFolioTArray[] = "";
										$ReservationFolioTArray[] = $SJ;
										$ReservationFolioTArray[] = $_SERVER['REMOTE_ADDR'];
										$ReservationFolioTArray[] = $admin_ID;
										
										DB::select('CALL Reservation_Folio_Insert_SP(?,?,?,?,?,?,?,?,?,?,?)', $ReservationFolioTArray);
										
										$SJ++;
									}
								}
							}
						}
						
					}
				}
			}
		}
		
		$cRoomTypeId = implode(",",$cRoomTypeIdArray);
		$Helper->ChannelPartnerRequest($Hotel_ID, $cRoomTypeId, $ArrivalDate, $DepartureDate);
		
		if($rsrvType == 'walkin'){
			return redirect('payment-pay/'.$Reservation_ID.'?type=walkin');
		}elseif(isset($Reservation_ID) && !empty($Reservation_ID)){	
			return redirect('payment-pay/'.$Reservation_ID.'?type=create');
			//return redirect('reservation/payment?rsrv_id='.$Reservation_ID.'&type='.$lastAmount);
		}else{	
			return redirect('reservation/create');
		}
    }
	
	public function incoming_reservation(Request $request)
    {
		
        $Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		if($request->search_date){
			$toDay = date("Y-m-d", strtotime($request->search_date));
			$searchDate = $request->search_date;
		}else{
			$toDay = date("Y-m-d");
			$searchDate = date("m/d/Y");
		}
		
		$Results = DB::select("SELECT GST.guest_id as GuestId, GST.guest_fname as GuestFname, GST.guest_lname as GuestLname, GST.guest_city as GuestCity,
							   GST.guest_zip as GuestPostal, GST.guest_company as GuestCompany, GST.guest_group as GuestGroup, GST.bad_status as GuestBadStatus,
							   RSV.room_reservation_id as RsrvId, RSV.reserv_night as RsrvNights, RSV.status as RsrvStat, RSV.reservation_locked as RsrvLock,
							   DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate, RSV.reserv_source as RsrvSource,
							   (SELECT RT.room_type
							   	FROM roomtypes as RT WHERE RT.room_type_id = RSV.room_type_id
							   ) as RoomTypes,
							   (SELECT R.room_number
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomNumber,
							   (SELECT R.room_stat
							   	FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								WHERE RRA.room_reservation_id = RSV.room_reservation_id
							   ) as RoomStat
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.arrival = '".$toDay."' AND RSV.status = ''
							   ORDER BY ArvlDate ASC, DprtDate ASC
							  ");
		
		$toDay = date("Y-m-d");					  
		$pResults = DB::select("SELECT GST.guest_id as GuestId, GST.guest_fname as GuestFname, GST.guest_lname as GuestLname, GST.guest_city as GuestCity,
							   	GST.guest_zip as GuestPostal, GST.guest_company as GuestCompany, GST.guest_group as GuestGroup, GST.bad_status as GuestBadStatus,
							    RSV.room_reservation_id as RsrvId, RSV.reserv_night as RsrvNights, RSV.status as RsrvStat, RSV.reservation_locked as RsrvLock,
							    DATE_FORMAT(RSV.arrival, '%m/%d/%Y') as ArvlDate, DATE_FORMAT(RSV.departure, '%m/%d/%Y') as DprtDate, RSV.reserv_source as RsrvSource,
							    (SELECT RT.room_type
								 FROM roomtypes as RT WHERE RT.room_type_id = RSV.room_type_id
							    ) as RoomTypes,
							    (SELECT R.room_number
								 FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								 WHERE RRA.room_reservation_id = RSV.room_reservation_id
							    ) as RoomNumber,
							    (SELECT R.room_stat
								 FROM room_assign as R INNER JOIN reservation_room_allot as RRA ON RRA.room_assign_id = R.room_assign_id
								 WHERE RRA.room_reservation_id = RSV.room_reservation_id
							    ) as RoomStat
							    FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							    WHERE RSV.hotel_id = ".$Hotel_ID." AND RSV.arrival < '".$toDay."' AND RSV.status = ''
							    ORDER BY ArvlDate ASC, DprtDate ASC
							   ");
							  
		return view('reservation.reservation-incoming')->with("Results", $Results)->with("pResults", $pResults)->with("searchDate", $searchDate);
    }

    public function checkin_reservation($id)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$Results  = RoomReservation::find($id); // return result in json format
		$Results->status	= "Checked In";
		$Results->reg_ip 	= $_SERVER['REMOTE_ADDR'];
		$Results->added_by 	= $admin_ID;
		$Results->save();
		
		return redirect('in-house');
    }
	
	public function special_request(Request $request, $id)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$Results  = RoomReservation::find($id); // return result in json format
		$Results->special_request	= $request->special_request;
		$Results->reg_ip 	= $_SERVER['REMOTE_ADDR'];
		$Results->added_by 	= $admin_ID;
		$Results->save();
		
		$url = 'guest-info/'.$id;
		return redirect($url)->with('success','Special request updated successfully');
    }
   
	public function rateBehalfCompany(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$RsltHtml = "";
		
		$profile_id = $request->profile_id;
		
		$fromDate = date('Y-m-d', strtotime($request->arrival));
		$toDate = date('Y-m-d', strtotime($request->departure.'-1 days'));
		
		$fromDate = date('Y-m-d', strtotime($request->arrival));
		$toDate = date('Y-m-d', strtotime($request->departure.'-1 days'));
		$room_type_id = $no_room = $totalAmount = $lastAmount = $RoomRates = $company_rate = 0;
		$valsArray = array();
		$QuantitiyArray = explode(",", $request->reserve_quantitiy);
		$PriceArray     = explode(",", $request->modified_price);
		$DiscountArray  = explode(",", $request->discount);
		foreach($QuantitiyArray as $key=>$vals)
		{	
			$TotalTaxAmount = 0;
			$valsArray = explode("-", $vals);
			$room_type_id = $valsArray[0];
			$no_room 	  = $valsArray[1];
			$room_Rate    = round($PriceArray[$key], 2);
			
			$discountIdArray = explode("-", $DiscountArray[$key]);
			$discountValues = implode(",", $discountIdArray);
			
			if($profile_id){
				$CompanyPrice = CompanyProfileRate::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $room_type_id], ['profile_id', $profile_id] ])->first();
				$company_rate = $CompanyPrice->company_rate;
			}
			$Qry = DB::select("SELECT RT.room_type_id, RT.room_type, 
								(SELECT SUM(PRC.room_rate_price) FROM room_rate_option as PRC
									WHERE PRC.room_type_id = RT.room_type_id AND PRC.hotel_id = ".$Hotel_ID." AND
									(PRC.room_rate_date between '$fromDate' AND '$toDate')) as room_Rate
							   FROM roomtypes as RT WHERE RT.hotel_id = ".$Hotel_ID." AND RT.room_type_id = ".$room_type_id."
							  ");
			$TaxQry = DB::select("SELECT tax_type as fTaxType, tax_value as fTaxValue, tax_value_type as fValueType
								  FROM taxes WHERE hotel_id = ".$Hotel_ID." AND status = 0 AND
								  NOT( 	(tax_end != '0000-00-00' AND tax_end < '".$chargeDate."') OR
										(tax_start != '0000-00-00' AND tax_start >= '".$chargeDate."') OR
										(tax_end = '0000-00-00' AND tax_start > '".$chargeDate."'AND tax_start < '".$chargeDate."') OR
										(tax_start = '0000-00-00' AND tax_end < '".$chargeDate."' AND tax_end > '".$chargeDate."')
									 )
								  ");
			
			foreach($Qry as $Rst)
			{
				$RoomRates = $Rst->room_Rate;
			    $RoomTypes = $Rst->room_type;
			}
			if($company_rate > 0){
				$RoomRate_wo_Tax = ($company_rate*$request->reserv_night);
			}elseif($room_Rate > 0){
				$RoomRate_wo_Tax = ($room_Rate*$request->reserv_night);
			}else{
				$RoomRate_wo_Tax = $RoomRates;
			}
			
			if($discountValues)
			{
				$discountQry = DB::select("SELECT discount_id as DiscountId, discount_code as DiscountCode, discount_value as DiscountValue,
										   discount_type as DiscountType
										   FROM discounts
										   WHERE hotel_id = ".$Hotel_ID." AND discount_id IN (".$discountValues.")
										   ORDER BY discount_code ASC
										  "); 
				$discountWiseRate = 0;
				foreach($discountQry as $discountRst)
				{
					if($discountRst->DiscountType == 2)
					{
						$discountWiseRate = $discountWiseRate+$discountRst->DiscountValue;
					}else{
						$discountWiseRate = $discountWiseRate+(($RoomRate_wo_Tax*$discountRst->DiscountValue)/100);
					}
				}
				$RoomRate_wo_Tax = $RoomRate_wo_Tax - round($discountWiseRate, 2);
			}
			if($RoomRate_wo_Tax<=0)
			{
				$RoomRate_wo_Tax = 1;
			}
			
			foreach($TaxQry as $TaxRst)
			{
				if($TaxRst->TaxType == 1)
				{
					$TotalTaxAmount = $TotalTaxAmount+( round((($RoomRate_wo_Tax*$TaxRst->TaxValue)/100), 2) );
				}elseif($TaxRst->TaxType == 2){
					$TotalTaxAmount = $TotalTaxAmount+( round($TaxRst->TaxValue*$request->reserv_night, 2) );
				}
			}
			$total_Rate = $RoomRate_wo_Tax+$TotalTaxAmount;
			$totalAmount = ($total_Rate*$no_room);
			$RsltHtml .= '<div class="x_panel">                        
				<span class="section uPPerLetter">'.$RoomTypes.'</span>
				<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingBottom5">
					<div class="col-md-6 zeroPadd">Room Rate ('.$request->reserv_night.' Nights)</div>                                
					<div class="col-md-6 zeroPadd alignTxtRight">$'.$RoomRate_wo_Tax.'</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingBottom5">
					<div class="col-md-6 zeroPadd">Taxes</div>                                
					<div class="col-md-6 zeroPadd alignTxtRight">$'.$TotalTaxAmount.'</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingBottom5">
					<div class="col-md-6 zeroPadd">Room Total Rate</div>                                
					<div class="col-md-6 zeroPadd alignTxtRight">$'.$total_Rate.'</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingBottom5">
					<div class="col-md-6 zeroPadd">Total Rooms</div>                                
					<div class="col-md-6 zeroPadd alignTxtRight">'.$no_room.'</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 priceHighlight">
					<div class="col-md-6 zeroPadd">Total Amount</div>                                
					<div class="col-md-6 zeroPadd alignTxtRight">$'.$totalAmount.'</div>
				</div>
			</div>';
			$lastAmount = $lastAmount+$totalAmount;
		}
		$RsltHtml .= '<div class="x_panel">
						<div class="col-md-12 col-sm-12 col-xs-12 LstPriceHighlight">
							<div class="col-md-6 zeroPadd">Payable Amount</div>                                
							<div class="col-md-6 zeroPadd alignTxtRight">$'.$lastAmount.'</div>
						</div>
					</div>';
		
		if($request->ajax()) {
			return response()->json(['response' => $RsltHtml, 'status' => 'success']);
		}else{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
}