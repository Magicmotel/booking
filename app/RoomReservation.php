<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomReservation extends Model
{
	protected $fillable= [
		'room_reservation_id_unq', 'hotel_id', 'guest_id', 'room_type_id',
		'arrival', 'departure', 'reserv_night', 'guest_adult', 'guest_child', 'qty_reserve', 'status',
		'confirmation', 'reserv_reason', 'special_request', 'reserv_source', 'source_reserv_id', 'source_cnf_id',
		'trash', 'reg_ip', 'added_by'
	];
	protected $primaryKey = 'room_reservation_id';
	protected $table = 'room_reservation';
}
