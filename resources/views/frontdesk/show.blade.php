{!!Html::style('vendors/bootstrap/dist/css/bootstrap.min.css')!!}
{!!Html::style('vendors/font-awesome/css/font-awesome.min.css')!!}
{!!Html::style('vendors/bootstrap-daterangepicker/datepicker.css')!!}
{!!Html::style('build/css/custom.min.css')!!}
{!!Html::script('vendors/daypilot/jquery-1.9.1.min.js')!!}
<body style="background:transparent; font-size:13px">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <meta name="_token" content="{{csrf_token()}}"/>
        <div class="x_content">
        	<div class="alert alert-danger marginZero marginTop20" id="ErrorMessage" style="display:none">
                <p>Error</p>
            </div>
            <div class="form-horizontal form-label-left">
                <div class="page-title" style="height:auto">
                    <div class="title_left LR_Margin10"><h3>Reservation Information</h3></div>
                </div>
                <div class="form-group col-xs-12 zeroPadd">
                    <label class="control-label col-md-4 col-sm-4">Guest Name</label>
                    <div class="control-value col-md-8 col-sm-8">
                        {{$GstRst->guest_fname.' '.$GstRst->guest_lname}}
                    </div>
                </div>
                <div class="form-group col-xs-12 zeroPadd">
                    <label class="control-label col-md-4 col-sm-4">Arrival Date</label>
                    <div class="control-value col-md-8 col-sm-8">
                        {{date('m/d/Y', strtotime($RsrvRst->arrival))}}
                    </div>
                </div>
                <div class="form-group col-xs-12 zeroPadd">
                    <label class="control-label col-md-4 col-sm-4">Departure Date</label>
                    <div class="control-value col-md-8 col-sm-8">
                        {{date('m/d/Y', strtotime($RsrvRst->departure))}}
                    </div>
                </div>
                <div class="form-group col-xs-12 zeroPadd">
                    <label class="control-label col-md-4 col-sm-4">Status</label>
                    <div class="control-value col-md-8 col-sm-8">
                        {{--*/	$options = array(""=>"Reserved", "Checked In"=>"Checked In", "Checked Out"=>"Checked Out"); /*--}}
                        {!!$options[$RsrvRst->status]!!}
                    </div>
                </div>
                <div class="form-group form-group-last marginTop20">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 marginBottom10">
                        <a href="javascript:redirectguest();" class="btn btn-primary btn-xs">GUEST INFO</a>
                        @if($RsrvRst->status == "")
                        	<a href="javascript:redirectincoming();" class="btn btn-success btn-xs">CHECK-IN</a>
                       		<a href="javascript:redirectcancel();" class="btn btn-warning btn-xs">CANCEL RESERVATION</a>
                        @endif
                        <a href="javascript:close();" class="btn btn-danger btn-xs">CLOSE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
	{!!Html::script('vendors/jquery/dist/jquery.min.js')!!}
    {!!Html::script('vendors/jquery/dist/jquery-ui.min.js')!!}
    {!!Html::script('vendors/bootstrap/dist/js/bootstrap.min.js')!!}
    <!-- bootstrap-daterangepicker -->
    {!!Html::script('vendors/moment/min/moment.min.js')!!}
    {!!Html::script('vendors/bootstrap-daterangepicker/datepicker.js')!!}
    
    <!-- Custom Theme Scripts -->
    {!!Html::script('build/js/custom.min.js')!!}
	<script type="text/javascript">
    function close(result) {
        if (parent && parent.DayPilot && parent.DayPilot.ModalStatic) {
            parent.DayPilot.ModalStatic.close(result);
        }
    }
	
	function redirectguest(result) {
		close("{{url('guest-info', $RsrvRst->room_reservation_id)}}");
	}
	
	function redirectincoming(result) {
		close("{{url('incoming-reservation')}}");
	}
	
	function redirectcancel(result) {
		close("{{url('cancel-reservation', $RsrvRst->room_reservation_id)}}");
	}
	
	$(document).ready(function () {
        $("#guest_name").focus();
    });
    </script>
    
</body>