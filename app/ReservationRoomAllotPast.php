<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationRoomAllotPast extends Model
{
	protected $fillable= [
		'hotel_id', 'room_reservation_id', 'p_room_assign_id', 'room_assign_id',
		'reg_ip', 'added_by'
	];
	protected $primaryKey = 'p_allot_id';
	protected $table = 'reservation_room_allot_past';
}