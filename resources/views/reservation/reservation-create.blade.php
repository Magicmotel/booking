@extends('layouts.layout')

@section('title')
	Create Reservation
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Create Reservation</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <form id="demo-form1" action="{!!url('reservation/create')!!}" method="get" class="form-horizontal form-label-left">
                    <div class="well" style="overflow:auto">
                        <div class="col-md-3 col-sm-3 col-xs-12 zeroPadd TB_Padd5">
                            <div class="col-md-12 col-sm-12 col-xs-12">Check In:</div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="from" id="from" class="form-control has-feedback-left" placeholder="Check In" value="{!!$request->from or date('m/d/Y')!!}" required="required" readonly="readonly" >
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-12 zeroPadd TB_Padd5">
                            <div class="col-md-12 col-sm-12 col-xs-12">Check Out:</div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="to" id="to" class="form-control has-feedback-left" placeholder="Check Out" value="{!!$request->to or  date('m/d/Y', strtotime('+1 days'))!!}" required="required" readonly="readonly">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-12 zeroPadd TB_Padd5">
                        	<div class="col-md-12 col-sm-12 col-xs-12">Nights</div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                            	<input type="number" name="reserv_night" id="reserv_night" value="{!!$request->reserv_night or 1!!}" class="form-control col-md-3 col-xs-6" min="1" max="30">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12 zeroPadd TB_Padd5">
                        	<div class="col-md-12 col-sm-12 col-xs-12">Company Profile</div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                            	<select name="company_profile" id="company_profile" class="form-control col-md-2 col-xs-12 zeroPadd">
                                    <option value="0">Select Company</option>
                                    @foreach(App\CompanyProfile::where('hotel_id', $Hotel_ID)->get() as $companyVal)
                                    <option value="{{$companyVal->profile_id}}"{!!($request->company_profile == $companyVal->profile_id)?' Selected':''!!}>{{$companyVal->company_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-12  zeroPadd TB_Padd5">
                            <div class="col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                            	<button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Search{{($request->from)?" Again...":""}}</button>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
                @if($request->from && $request->to)
                {{--*/
                $rquestFromDate = date('Y-m-d', strtotime($request->from)); $rquestToDate = date('Y-m-d', strtotime($request->to));
                $fromDate = date('Y-m-d', strtotime($rquestFromDate)); $toDate = date('Y-m-d', strtotime($rquestToDate.'-1 days'));
                $profile_ID = $request->company_profile;
                
                if($request->company_profile){
                	$queryPart = "SELECT CPR.company_rate FROM company_profile_rate as CPR
                    			  WHERE CPR.room_type_id = RT.room_type_id AND CPR.hotel_id = ".$Hotel_ID." AND CPR.profile_id = ".$profile_ID." LIMIT 0, 1";
                }else{
                	$queryPart = "SELECT PRC.room_rate_price FROM room_rate_option as PRC
                                  WHERE PRC.room_type_id = RT.room_type_id AND PRC.hotel_id = ".$Hotel_ID." AND (PRC.room_rate_date between '".$fromDate."' AND '".$toDate."')
                                  ORDER BY PRC.room_rate_date ASC LIMIT 0, 1";
                }
                                                    
                $RoomListQry = DB::select("SELECT RT.room_type_id, RT.room_type, RT.room_count, RT.room_pic_1, RT.guest_adult, RT.guest_child,
                    CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as BedSize,
                    ( ".$queryPart." ) as room_rate,
                    (   room_count - 
                    	( IF(	( SELECT SUM(RSV.qty_reserve) FROM room_reservation as RSV WHERE RSV.room_type_id = RT.room_type_id AND RSV.status != 'Cancelled'
                                  AND NOT ((RSV.departure <= '".$rquestFromDate."') OR (RSV.arrival >= '".$rquestToDate."')) GROUP BY RSV.room_type_id
                             	) != 0, 
                                ( SELECT SUM(RSV.qty_reserve) FROM room_reservation as RSV WHERE RSV.room_type_id = RT.room_type_id AND RSV.status != 'Cancelled'
                                  AND NOT ((RSV.departure <= '".$rquestFromDate."') OR (RSV.arrival >= '".$rquestToDate."')) GROUP BY RSV.room_type_id
                                ), 0              
                            )
                       	) - 
                    	( IF(	( SELECT count(R.room_type_id) FROM room_assign as R
                                  WHERE R.room_type_id = RT.room_type_id AND (R.status = 0 OR R.trash = 1 OR R.room_number = '') GROUP BY R.room_type_id
                             	) != 0, 
                                ( SELECT count(R.room_type_id) FROM room_assign as R
                                  WHERE R.room_type_id = RT.room_type_id AND (R.status = 0 OR R.trash = 1 OR R.room_number = '') GROUP BY R.room_type_id
                                ), 0
                            )
                       	) - 
                    	( IF(
                        	 	( SELECT count(DISTINCT(BR.room_assign_id)) FROM block_rooms as BR WHERE BR.room_type_id = RT.room_type_id
                                  AND NOT ((BR.block_to <= '".$rquestFromDate."') OR (BR.block_from >= '".$rquestToDate."')) GROUP BY BR.room_type_id
                             	) != 0, 
                                ( SELECT count(DISTINCT(BR.room_assign_id)) FROM block_rooms as BR WHERE BR.room_type_id = RT.room_type_id
                                  AND NOT ((BR.block_to <= '".$rquestFromDate."') OR (BR.block_from >= '".$rquestToDate."')) GROUP BY BR.room_type_id
                                ), 0              
                            )
                       	)
                    ) as CNT
                    FROM roomtypes as RT INNER JOIN room_assign as RA ON RA.room_type_id = RT.room_type_id
                    WHERE RT.hotel_id = ".$Hotel_ID." AND RA.room_number != '' GROUP BY RT.room_type_id ORDER BY RT.room_order ASC, CNT DESC");
                /*--}}
                @if(count($RoomListQry))
                <form id="demo-form2" action="{!!url('reservation/guest')!!}" method="post" class="form-horizontal form-label-left" data-parsley-validate>
                	{!!csrf_field()!!}
                    <input type="hidden" name="arrival" value="{!!$request->from or date('m/d/Y')!!}">
                    <input type="hidden" name="departure" value="{!!$request->to or  date('m/d/Y', strtotime('+1 days'))!!}">
                    <input type="hidden" name="reserv_night" value="{!!$request->reserv_night!!}">
                    <input type="hidden" name="company_profile" id="company_profile" value="{!!$request->company_profile!!}">
                    @foreach($RoomListQry as $val)
                  		@if($val->CNT > 0)
                        	{{--*/
                            $varQry  = " AND NOT ((RSV.departure <= '".$rquestFromDate."') OR (RSV.arrival >= '".$rquestToDate."'))";
                            $varQry1 = " AND NOT ((BR.block_to <= '".$rquestFromDate."') OR (BR.block_from >= '".$rquestToDate."'))";
                            $vcntRoom_Qry = DB::select("SELECT R1.room_assign_id as RoomId, R1.room_number as RoomNumber, R1.room_stat as RoomStat,
									R1.room_type_id as RoomTypeId, CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType
									FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
									WHERE R1.hotel_id = ".$Hotel_ID." AND R1.status = 1 AND R1.room_number != '' AND
                                    R1.trash = 0 AND R1.room_type_id = ".$val->room_type_id." AND
									R1.room_assign_id NOT IN(
										SELECT R.room_assign_id
										FROM room_reservation as RSV
										INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
										INNER JOIN room_assign as R ON R.room_assign_id = RRA.room_assign_id
										WHERE R.room_type_id = ".$val->room_type_id." AND RSV.hotel_id = ".$Hotel_ID.$varQry."
                                        AND RSV.status != 'Checked out' AND RSV.status != 'Cancelled'
									) AND
                                    R1.room_assign_id NOT IN(
										SELECT BR.room_assign_id
										FROM block_rooms as BR
										WHERE BR.room_type_id = ".$val->room_type_id." AND BR.hotel_id = ".$Hotel_ID.$varQry1."
									)
                                    ORDER BY R1.room_number ASC");
                            /*--}}
                        <div class="x_panel padding10">
                            <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                                <div class="col-md-3 col-sm-3 col-xs-12 padding5">
                                	<div style="display:table-cell; width:200px; height:150px; vertical-align:middle;">
                                    @if($val->room_pic_1)
                                    {{Html::image('room_type/'.$val->room_pic_1, 'alt', array( 'style' => 'max-width:200px; max-height:150px;' ))}}
                                    @else
                                    {{Html::image('room_type/demo.jpg', 'alt', array( 'width' => 200, 'height' => 150 ))}}
                                    @endif
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 zeroPadd">
                                    <div class="col-md-8 col-sm-8 col-xs-12 padding5">
                                        <h4>{{$val->room_type}} ({{$val->BedSize}})</h4>
                                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                                        No of Guests: {{$val->guest_adult}} Adult / {{$val->guest_child}} Children
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
                                        Total Rooms: {{$val->room_count}} / Available: {{$val->CNT}}
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12 padding5 AlgnCenter">
                                        <b>Rooms</b><br /><br />
                                        <select name="reserve_quantitiy[]" id="reserve_quantitiy_{{$val->room_type_id}}" data-parsley-type="1" required onchange="openRoomChoose(this.value, '{{$val->CNT}}', '{{$val->room_type_id}}')" class="padding5" >
                                            <option value="">0</option>
                                            @for($pk=1; $pk <= $val->CNT; $pk++)
                                                <option value="{{$val->room_type_id.'-'.$pk}}">{{$pk}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding5 RoomOptionBox marginTop10 paddingBottom10 displayNone" id="roomChoose_{{$val->room_type_id}}">
                                    @for($pk=1; $pk <= $val->CNT; $pk++)
                                        <div class="col-md-12 zeroPadd AlgnCenter marginTop10 displayNone" id="roomChoose_{{$val->room_type_id}}_{{$pk}}">
                                            <div class="col-md-2 col-sm-2 col-xs-6 zeroRightPadd AlgnLeft">
                                                <b class="col-md-12 zeroPadd marginTop28">Room {{$pk}}:</b>
                                            </div>
                                            <div class="{!!($val->guest_child)?'col-md-3':'col-md-6'!!} col-sm-3 col-xs-6 AlgnLeft">
                                                <b class="col-md-12 zeroPadd">Adult</b>
                                                <input type="number" name="enter_adult_{{$val->room_type_id}}[]" id="enter_adult_{{$val->room_type_id}}_{{$pk}}" value="0" class="form-control col-md-3 col-xs-6" min="1" max="{{$val->guest_adult}}" placeholder="Adults">
                                            </div>
                                            @if($val->guest_child)
                                            <div class="col-md-3 col-sm-3 col-xs-6 AlgnLeft">
                                                <b class="col-md-12 zeroPadd">Children (0-12)</b>
                                                <input type="number" name="enter_child_{{$val->room_type_id}}[]" id="enter_child_{{$val->room_type_id}}_{{$pk}}" value="0" class="form-control col-md-3 col-xs-6" min="0" max="{{$val->guest_child}}" placeholder="Children">
                                            </div>
                                            @endif
                                            <div class="col-md-4 col-sm-3 col-xs-6 AlgnLeft">
                                            	<b class="col-md-12 zeroPadd">Room Number</b>
                                                <select name="enter_room_{{$val->room_type_id}}[]" id="enter_room_{{$val->room_type_id}}_{{$pk}}" class="form-control col-md-3 col-xs-6 positionTypes">
                                                    <option value="">Select Room</option>
                                                    @foreach($vcntRoom_Qry as $vcntRoom_Rst)
                                                        <option{!!($vcntRoom_Rst->RoomStat == 1)?'':' style="background:#FF0000; color:#FFFFFF;"'!!} value="{{$vcntRoom_Rst->RoomId}}">{{$vcntRoom_Rst->RoomNumber}} ({{($vcntRoom_Rst->RoomStat == 1)?"Clean":"Dirty"}})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endfor
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 padding5 AlgnCenter">
                                    <b>Price</b><br />
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 AlgnCenter col-xs-12 zeroPadd marginBottom5">
                                        <span class="displayNone" id="modified_backup{{$val->room_type_id}}">{{$val->room_rate}}</span>
                                        <span class="displayNone" id="discount_bool_{{$val->room_type_id}}">0</span>
                                        <span class="col-md-12 col-sm-12 col-xs-12 marginTop5" id="modified_show{{$val->room_type_id}}">$ {{$val->room_rate}}</span><br />
                                        per room/night
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-12 zeroPadd">
                                        <div class="modifierButton" id="modified_button{{$val->room_type_id}}" onclick="modifyPriceFunct('{{$val->room_type_id}}')"><i class="fa fa-pencil-square-o"></i> </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 displayNone" id="modified_rateBox{{$val->room_type_id}}">
                                        <input type="text" name="modified_rate[]" id="modified_rate{{$val->room_type_id}}" value="" class="modifiedRate col-md-6 col-md-offset-3 LR_Padd5">
                                        <div class="fltL rateUpdateButton zeroPadd" onclick="setModifiedPrice('{{$val->room_type_id}}')"><i class="fa fa-check-square"></i></div>
                                    </div>
                                    {{--*/	$toDay = date('Y-m-d');
                                    $discountSql = DB::select("SELECT discount_id as DiscountId, discount_code as DiscountCode, discount_value as DiscountValue,
                                                               discount_type as DiscountType
                                                               FROM discounts
                                                               WHERE hotel_id = ".$Hotel_ID." AND
                                                               discount_from <= '".$toDay."' AND (discount_to >= '".$toDay."' OR discount_to = '0000-00-00')
                                                               ORDER BY discount_code ASC
                                                              ");
                                    $discountNum = count($discountSql);
                                    $PSJ=1;
                                    /*--}}
                                    @if($discountNum)
                                    <b class="col-md-12 col-sm-12 col-xs-12 btn-list-link coupon_Button" id="coupon_Button_{{$val->room_type_id}}" data-id="{{$val->room_type_id}}">Discount Code</b>
                                    <div class="col-md-10 col-md-offset-1 zeroPadd CouponOptionBox marginTop6 paddingBottom10 displayNone" id="coupon_Box_{{$val->room_type_id}}">
                                        <i class="closeButton_RsrvCreate fa fa-times" data-id="{{$val->room_type_id}}"></i>
                                        @foreach($discountSql as $dsctVals)
                                        <div class="col-md-12 col-sm-12 col-xs-12 AlgnLeft marginTop10">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="checkbox" name="discount_{{$val->room_type_id}}[]" id="discount_{{$val->room_type_id}}_{{$PSJ}}"
                                                	class="flat discountBox_{{$val->room_type_id}}" value="{{$dsctVals->DiscountId}}"
                                                    data-id="{{$dsctVals->DiscountValue}}" data-title="{{$dsctVals->DiscountType}}">
                                                <div class="labelSwitchCheck">{{$dsctVals->DiscountCode}}</div>
                                            </div>
                                        </div>
                                        {{--*/ $PSJ++; /*--}}
                                        @endforeach
                                        <div class="col-md-12 AlgnLeft marginTop10">
                                            <div type="submit" class="btn btn-success btn-xs discount_Button" data-id="{{$val->room_type_id}}" data-title="{{$discountNum}}">Apply Discount</div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                
                            </div>
                        </div>
                    	@endif
                    @endforeach
                    <div class="form-group form-group-last">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-6">
                            <button type="submit" class="btn btn-success">Book Rooms</button>
                        </div>
                    </div>
                </form>
                @endif
                @endif
        	</div>
        </div>
    </div>
</div>
<input type="hidden" id="refreshed" value="no">
<script type="text/javascript" language="javascript">
	function modifyPriceFunct(str)
	{
		$("#modified_button"+str).hide();
		$("#modified_rateBox"+str).show();
	}
	function setModifiedPrice(str){
		var newRate = $("#modified_rate"+str).val();
		if(newRate>0){
			$("#modified_button"+str).show();
			$("#modified_rateBox"+str).hide();
			$("#modified_show"+str).html("$ "+newRate);
			$("#modified_backup"+str).html(newRate);
			$("#modified_show"+str).show();
		}else{
			$("#modified_button"+str).show();
			$("#modified_rateBox"+str).hide();
			$("#modified_show"+str).show();
		}
	}
	function openRoomChoose(selectedRoom, totalRooms, roomTypeId){
		if(selectedRoom ==""){
			var roomSelect = 0;
			$("#roomChoose_"+roomTypeId).hide();
		}else{
			var res = selectedRoom.split("-");
			var roomSelect = res[1];
			$("#roomChoose_"+roomTypeId).show();			
		}
		for(i = 1; i <= totalRooms; i++ ){
			if(i <= roomSelect){
				$("#roomChoose_"+roomTypeId+"_"+i).show();
				$("#enter_adult_"+roomTypeId+"_"+i).val("1");
				$("#enter_child_"+roomTypeId+"_"+i).val("0");
			}else{
				$("#roomChoose_"+roomTypeId+"_"+i).hide();
				$("#enter_adult_"+roomTypeId+"_"+i).val("0");
				$("#enter_child_"+roomTypeId+"_"+i).val("0");
			}
		}
	}
</script>
@stop

@section('jQuery')
<script>
window.onload=function(){
	var e=document.getElementById("refreshed");
	console.log(e.value);
	if(e.value=="no"){e.value="yes";}else{e.value="no";window.location.reload();}
	
}
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var checkin = $('#from').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		var newDate = new Date(ev.date);
		newDate.setDate(newDate.getDate() + 1);
		checkout.setValue(newDate);
		checkin.hide();
		var startVal = $('#from').val();
		var endVal = $('#to').val();
		if(startVal && endVal){
			$('#reserv_night').val(daydiff(parseDate(startVal), parseDate(endVal)));
		}
		$('#to')[0].focus();
	}).data('datepicker');
	
	var checkout = $('#to').datepicker({
		onRender: function(date) {
			return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		checkout.hide();
		var startVal = $('#from').val();
		var endVal = $('#to').val();
		if(startVal && endVal){
			$('#reserv_night').val(daydiff(parseDate(startVal), parseDate(endVal)));
		}
	}).data('datepicker');
	
	$('#reserv_night').on('keyup mouseup', function(){
		var HmD = $('#reserv_night').val();
		if(HmD ==0){
			HmD = 1;
			$('#reserv_night').val('1')
		}
		var newDate = new Date(checkin.date);
		newDate.setDate(newDate.getDate() + parseInt(HmD));
		checkout.setValue(newDate);
	});
	
	function parseDate(str) {
		var mdy = str.split('/');
		return new Date(mdy[2], mdy[0]-1, mdy[1]);
	}
	
	function daydiff(first, second) {
		return Math.round((second-first)/(1000*60*60*24));
	}
	
	$(".coupon_Button").click(function(){
		roomId = $(this).attr('data-id');
		$("#coupon_Box_"+roomId).show();
	});
	$(".discount_Button").click(function(){
		var roomId = $(this).attr('data-id');
		var dTotal = $(this).attr('data-title');
		var roomAmount = $("#modified_backup"+roomId).html();
		
		var priceAmountis = roomAmount;
		var discountAmountis = 0;
		var discountBool = 0;
		for(i = 1; i <= dTotal; i++ ){
			if(i <= dTotal){
				if(document.getElementById("discount_"+roomId+"_"+i).checked == true){
					discountValue = $("#discount_"+roomId+"_"+i).attr("data-id");
					discountType  = $("#discount_"+roomId+"_"+i).attr("data-title");
					if(discountType == 2){
						discountAmountis = Number(discountAmountis)+Number(discountValue);
					}else{
						discountAmountis = Number(discountAmountis)+Number((priceAmountis*discountValue)/100);
					}
					discountBool++;
				}
			}
		}
		priceAmountis = priceAmountis - (Math.round(discountAmountis*100)/100);
		
		if(priceAmountis<=0){
			priceAmountis = 1;
		}
		$("#modified_show"+roomId).html("$ "+priceAmountis);
		$("#discount_bool_"+roomId).html(discountBool);
		$("#coupon_Box_"+roomId).hide();
	});
	$("select.positionTypes").change(function () {
		$("select.positionTypes option[value='" + $(this).data('index') + "']").show();
		$(this).data('index', this.value);
		$("select.positionTypes option[value='" + this.value + "']:not([value=''])").hide();
		$(this).find("option[value='" + this.value + "']:not([value=''])").show();
    });
	$(".closeButton_RsrvCreate").click( function(e) {
		var roomId = $(this).attr('data-id');
		var discountBool = $("#discount_bool_"+roomId).html();
		if(discountBool == 0){
			$("input.discountBox_"+roomId+":checkbox").prop('checked', false);
		}
		$("#coupon_Box_"+roomId).hide();
	});
	$('form').bind("keypress", function(e) {
	 	if (e.keyCode == 13) {               
			e.preventDefault();
			return false;
	  	}
	});
});
</script>
@stop