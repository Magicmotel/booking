<?php

/**
 * This file used for testing different functionlity to consume Zeamster API.
 */
include_once 'config.php';
include_once BASE_PATH . DS . 'lib/ZeamsterPaymentAPI.php';
include_once BASE_PATH . DS . 'lib/GetTransaction.php';
include_once BASE_PATH . DS . 'lib/TransactionProcess.php';
include_once BASE_PATH . DS . 'lib/CustomerContact.php';
include_once BASE_PATH . DS . 'lib/LocationProcess.php';
include_once BASE_PATH . DS . 'lib/TagsProcess.php';
include_once BASE_PATH . DS . 'lib/NotesProcess.php';
include_once BASE_PATH . DS . 'lib/AccountvaultsProcess.php';
include_once BASE_PATH . DS . 'lib/User.php';
include_once BASE_PATH . DS . 'lib/RecurringProcess.php';
include_once BASE_PATH . DS . 'lib/TerminalProcess.php';

use lib\getTransactionDetails\getTransactionDetails as getTransaction;
use lib\transactionProcess\transactionProcess;
use lib\zeamsterApi\ZeamsterPaymentTransaction as zeamsterApi;
use lib\customerContact\Contact;
use lib\locationProcess\Locations;
use lib\tagsProcess\Tags;
use lib\notesProcess\Notes;
use lib\accountvaultsProcess\Accountvaults;
use lib\user\User;
use lib\RecurringProcess\Recurring;
use lib\terminalProcess\Terminal;

/* * ********************************************************************************** */
/* Capture,Authorize, Void , Refund and Get functionlity with Transaction service       */
/* * ********************************************************************************* */

echo 'Test Result for Zeamster API with PHP SDK: <br /><br />';
/** Get Single transaction details on basis of original transaction Id * */
if (!empty($_GET['action']) && $_GET['action'] == 'get_transaction') {

    $originalTransactionId = '111111111111111111111111';
    $result = getTransaction::getTransaction($originalTransactionId);
    echo '<pre>';
    print_r($result);
}
/** Get all transaction details  * */
if (!empty($_GET['action']) && $_GET['action'] == 'get_transaction_list') {
    $perPage = 1;
    $sort = 'auth_amount';
    $filter = "auth_amount=505.00";
    $result = getTransaction::getTransaction(null, $filter, $perPage, $sort);
    echo '<pre>';
    print_r($result);
}

$objTransaction = new transactionProcess();    // Instantiate transaction class

/**
 * Test for sale transaction functionlity.
 */
if (!empty($_GET['action']) && strtolower($_GET['action']) == 'sale' && !empty($_GET['token'])) {
    $transaction_amount = 50;
    $order_id = sprintf("%06d", mt_rand(1, 999999));
    $ticket = $_GET['token'];
    $objTransaction->account_holder_name = 'Smith';
    $objTransaction->billing_street = '123';
    $objTransaction->billing_phone = '9811673671';
    $objTransaction->payment_method = 'cc';
    $objTransaction->account_vault_id = '';
    $objTransaction->transaction_amount = $transaction_amount;
    $objTransaction->checkin_date = '2017-03-03';
    $objTransaction->checkout_date = '2017-03-05';
    $objTransaction->room_rate = '1';
    $objTransaction->action = zeamsterApi::CHARGE;
    $objTransaction->location_id = LOCATION_ID;
    $objTransaction->order_num = $order_id;
    $objTransaction->contact_id = '';
    $objTransaction->ticket = $ticket;
    $result = transactionProcess::charge($objTransaction);
    echo '<pre>';
    print_r($result);
}

/**
 * Test for authorize transaction functionlity.
 */
if (!empty($_GET['action']) && strtolower($_GET['action']) == 'auth' && !empty($_GET['token'])) {
    $transaction_amount = 50;
    $order_id = sprintf("%06d", mt_rand(1, 999999));
    $ticket = $_GET['token'];
    $objTransaction->account_holder_name = 'Smith';
    $objTransaction->billing_street = '123';
    $objTransaction->billing_phone = '9811673671';
    $objTransaction->payment_method = 'cc';
    $objTransaction->account_vault_id = '';
    $objTransaction->transaction_amount = $transaction_amount;
    $objTransaction->action = zeamsterApi::AUTHONLY;
    $objTransaction->location_id = LOCATION_ID;
    $objTransaction->order_num = $order_id;
    $objTransaction->contact_id = '';
    $objTransaction->ticket = $ticket;

    $result = transactionProcess::charge($objTransaction);

    echo '<pre>';
    print_r($result);
}

/**
 * Test for capture after authorized transaction.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'capture') {
    $objTransaction->action = zeamsterApi::AUTHCOMPLETE;
    $aut_transaction_id = '111111111111111111111111';
    $result = transactionProcess::capture($objTransaction, $aut_transaction_id);
    echo '<pre>';
    print_r($result);
}

/**
 * Test void and refund functionlity.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'refund') {

    $originalTransactionId = '111111111111111111111111'; // Use original transaction ID

    $transactionDetails = getTransaction::getTransaction($originalTransactionId);

    if (empty($transactionDetails['transaction_amount']) && $transactionDetails['transaction_amount'] == '0.00') {
        echo 'Transaction amount should be greater than zero.';
        exit;
    }

    $transaction_amount = $transactionDetails['transaction_amount'];
    $order_id = (!empty($transactionDetails['order_num']) ? $transactionDetails['order_num'] : '');

    $refundAmount = 0;
    if (!empty($refundAmount) && $refundAmount > 0) {
        $transaction_amount -= $refundAmount;
    }

    if (!empty($transactionDetails['is_refundable'])) {

        $objTransaction->payment_method = 'cc';
        $objTransaction->transaction_amount = number_format($transaction_amount, 2, '.', '');
        $objTransaction->action = zeamsterApi::REFUND;
        $objTransaction->location_id = LOCATION_ID;
        $objTransaction->order_num = $order_id;
        $objTransaction->previous_transaction_id = $originalTransactionId;

        $result = transactionProcess::refund($objTransaction, $originalTransactionId, 'refund');
    } else {

        $objTransaction->action = zeamsterApi::VOID;
        if ($transaction_amount > 0) {

            $objTransaction->payment_method = 'cc';
            $objTransaction->transaction_amount = number_format($transaction_amount, 2, '.', '');
            $objTransaction->action = zeamsterApi::CHARGE;
            $objTransaction->location_id = LOCATION_ID;
            $objTransaction->order_num = $order_id;
            $objTransaction->previous_transaction_id = $originalTransactionId;

            transactionProcess::void($objTransaction, $originalTransactionId, 'void');

            $result = transactionProcess::refund($objTransaction, $originalTransactionId, 'partialreversal');
        } else {
            $result = transactionProcess::void($objTransaction, $originalTransactionId, 'void');
        }
    }

    echo '<pre>';
    print_r($result);
}

/* * ********************************************************************************** */
/*                      Create , Get, Update and Delete Contact Service                 */
/* * ********************************************************************************* */
// 
$objContact = new Contact;      // Instantiate Contact class

/**
 * Test Create new Customer Contact.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'create_contact') {

    $objContact->location_id = LOCATION_ID;
    $objContact->first_name = 'Test';
    $objContact->last_name = 'Team';
    $objContact->cell_phone = '8112455445';

    $result = Contact::createCustomerContact($objContact);
    echo '<pre>';
    print_r($result);
}
/**
 * Test get single Customer Contact.
 *
 */
if (!empty($_GET['action']) && $_GET['action'] == 'get_contact') {

    $contact_id = '111111111111111111111111';

    $result = Contact::getCustomerContact($contact_id);

    echo '<pre>';
    print_r($result);
}
/**
 * Test get single Customer Contact.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'get_contact_list') {
    $perPage = 2;
    $sort = 'created_ts,first_name';
    $filter = array("first_name" => "Smith",
        "last_name" => "tom");
    $filterData = zeamsterApi::convertArrayToUrlStr($filter);
    $pageNo = null;

    $result = Contact::getCustomerContact(null, $filterData, $perPage, $sort, $pageNo);

    echo '<pre>';
    print_r($result);
}
/**
 * Test Update Customer Contact Functionlity.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'update_contact') {

    $objContact->cell_phone = '8250017121';
    // $objContact->first_name = 'Smith';
    $objContact->last_name = 'tom';

    $contact_id = '111111111111111111111111';
    $result = Contact::updateCustomerContact($objContact, $contact_id);
    echo '<pre>';
    print_r($result);
}

/**
 * Test Delete Customer Contact Functionlity.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'delete_contact') {

    $contact_id = '111111111111111111111111';
    $result = Contact::deleteCustomerContact($contact_id);
    echo '<pre>';
    print_r($result);
}
/* * ********************************************************************************** */
/*                      Create , Get, Update and Delete Location Service                 */
/* * ********************************************************************************* */

$objLocation = new Locations();         // Instantiate Locations class


/**
 * Test Create Location.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'create_location') {

    $objLocation->id = '111111111111111111111111';
    $objLocation->account_number = '1234';
    $objLocation->location_api_id = '137';
    $objLocation->name = 'OLUTest';
    $objLocation->branding_domain_url = 'est.zeamster.com';
    $objLocation->address = 'grand brook rd';
    $objLocation->cell_phone = '2485553434';

    $result = Locations::createLocation($objLocation);
    echo '<pre>';
    print_r($result);
}
/**
 * Test Update Location.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'update_location') {

    $objLocation->office_phone = '2485553434';
    $location_id = LOCATION_ID;
    $result = Locations::updateLocation($objLocation, $location_id);
    echo '<pre>';
    print_r($result);
}

/**
 * Test Get Location.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'get_location') {

    $location_id = LOCATION_ID;
    $result = Locations::getLocation($location_id);
    echo '<pre>';
    print_r($result);
}
/**
 * Test Get Location List.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'get_location_list') {
    $perPage = 1;
    $sort = 'name,created_ts';
    $filter = array(
        "name" => "Ecommerce",
    );
    $pageNo = null;
    $filterData = zeamsterApi::convertArrayToUrlStr($filter);
    $result = Locations::getLocation(null, $filterData, $perPage, $sort, $pageNo);
    echo '<pre>';
    print_r($result);
}
/**
 * Test Get Location List.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'delete_location') {
    $location_id = '111111111111111111111111';
    $result = Locations::deleteLocation($location_id);
    echo '<pre>';
    print_r($result);
}

/* * ********************************************************************************** */
/*                      Create , Get, Update and Delete Tag Service                    */
/* * ********************************************************************************* */
$objTags = new Tags();                    // Instantiate Tag class
/**
 * Test Create New tag.                             
 */
if (!empty($_GET['action']) && $_GET['action'] == 'create_tags') {

    $objTags->title = 'Test 6 From Team';
    $objTags->location_id = LOCATION_ID;
    $result = Tags::createTags($objTags);
    echo '<pre>';
    print_r($result);
}
/**
 * Test Get tag uisng tag_id.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'get_tags') {
    $tag_id = '111111111111111111111111';
    $result = Tags::getTagsDetails($tag_id);
    echo '<pre>';
    print_r($result);
}
/**
 * Test Get Tags List.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'get_tags_list') {
    $perPage = 2;
    $sort = 'created_ts,title';
    $filter = array(
        "title" => "test1",
    );
    $pageNo = null;
    $filterData = null; //zeamsterApi::convertArrayToUrlStr($filter);
    $result = Tags::getTagsDetails(null, $filterData, $perPage, $sort, $pageNo);
    echo '<pre>';
    print_r($result);
}
/**
 * Test Update Tags Data.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'update_tags') {

    $objTags->title = 'Update Tag1';
    $objTags->location_id = LOCATION_ID;

    $tag_id = '111111111111111111111111';
    $result = Tags::updateTags($objTags, $tag_id);
    echo '<pre>';
    print_r($result);
}
/**
 * Test Delete existing Tags using Zeamster API.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'delete_tags') {

    $tag_id = '111111111111111111111111';
    $result = Tags::deleteTags($tag_id);
    echo '<pre>';
    echo $result;
    print_r($result);
}

/* * ********************************************************************************** */
/*                      Create , Get, Update and Delete Notes Service                    */
/* * ********************************************************************************* */
$objNotes = new Notes();                       // Instantiate Tag class
/**
 * Test Create New notes.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'create_notes') {

    $objNotes->resource_id = '111111111111111111111111';
    $objNotes->resource = 'Contact';
    $objNotes->note = 'test note for contact deatil';

    $result = Notes::createNotes($objNotes);
    echo '<pre>';
    print_r($result);
}

/**
 * Test Get Single notes uisng $notes_id.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'get_notes') {

    $notes_id = '111111111111111111111111';
    $result = Notes::getNotesDetails($notes_id);
    echo '<pre>';
    print_r($result);
}

/**
 * Test Get notes list.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'get_notes_list') {
    $perPage = 2;
    $sort = 'created_ts,note';
    $filter = array(
        "note" => "This is test content for notes",
    );
    $pageNo = null;
    $filterData = null; // zeamsterApi::convertArrayToUrlStr($filter);
    $result = Notes::getNotesDetails(null, $filterData, $perPage, $sort, $pageNo);
    echo '<pre>';
    print_r($result);
}

/**
 * Test Update Notes Data.
 */

if (!empty($_GET['action']) && $_GET['action'] == 'update_notes') {

    $objNotes->title = 'Update Tag123';
    $objNotes->location_id = LOCATION_ID;

    $notes_id = '111111111111111111111111';

    $result = Notes::updateNotes($objNotes, $notes_id);
    echo '<pre>';
    print_r($result);
}
/**
 * Test Delete Notes Data.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'delete_notes') {

    $notes_id = '111111111111111111111111';
    $result = Notes::deleteNotes($notes_id);
    echo '<pre>';
    print_r($result);
}
// To DO   

/* * ********************************************************************************** */
/*                      Create , Get, Update and Delete Acountvaults Service             */
/* * ********************************************************************************* */

$objAccountVaults = new Accountvaults();    // Instantiate Acountvaults class

/**
 * Test Create new Accountvaults.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'create_account') {

    $objAccountVaults->payment_method = "cc";
    $objAccountVaults->ticket = "111111111111111111111111";
    $objAccountVaults->location_id = LOCATION_ID;
    $objAccountVaults->contact_id = "111111111111111111111111";

    $result = Accountvaults::createRecord($objAccountVaults);
    echo '<pre>';
    print_r($result);
}

/**
 * Test Create new Accountvaults Record.
 */
if (!empty($_GET['action']) && $_GET['action'] == 'create_account_record') {

    $objAccountVaults->title = "Test new Record1";
    $objAccountVaults->payment_method = "cc";
    $objAccountVaults->account_holder_name = "Test Team";
    $objAccountVaults->account_number = "5454545454545454";
    $objAccountVaults->exp_date = "0917";
    $objAccountVaults->contact_id = "111111111111111111111111";
    $objAccountVaults->location_id = LOCATION_ID;
    $result = Accountvaults::createRecord($objAccountVaults);
    echo '<pre>';
    print_r($result);
}

/**
 * Test Update account vault .
 */
if (!empty($_GET['action']) && $_GET['action'] == 'update_account_record') {

    $objAccountVaults->exp_date = "1018";
    $account_id = '111111111111111111111111';
    $result = Accountvaults::updateAccountVaultsRecords($objAccountVaults, $account_id);
    echo '<pre>';
    print_r($result);
}

/**
 * Test Get account vault record
 */
if (!empty($_GET['action']) && $_GET['action'] == 'get_account_record') {

    $account_id = '111111111111111111111111';
    $result = Accountvaults::getAccountVaultsRecords($account_id);
    echo '<pre>';
    print_r($result);
}

/**
 * Test Get account vault record list
 */
if (!empty($_GET['action']) && $_GET['action'] == 'get_account_list') {
    $perPage = 1;
    $sort = 'account_holder_name,created_ts';
    $filter = array(
        "account_holder_name" => "Link Black",
    );
    $pageNo = null;
    $filterData = zeamsterApi::convertArrayToUrlStr($filter);

    $result = Accountvaults::getAccountVaultsRecords(null, $filterData, $perPage, $sort, $pageNo);
    echo '<pre>';
    print_r($result);
}

/**
 * Test Delete existing account vault record
 */
if (!empty($_GET['action']) && $_GET['action'] == 'delete_account_record') {
    $account_id = '111111111111111111111111';
    $result = Accountvaults::deleteAccountVaultsRecord($account_id);
    echo '<pre>';
    print_r($result);
}

/* * ********************************************************************************** */
/*                      Create , Get, Update and Delete User Service                   */
/* * ********************************************************************************* */

$objUser = new User();                 // Instantiate User class
/* Need to verify for Domain ID (To DO) */
if (!empty($_GET['action']) && $_GET['action'] == 'create_user') {

    $objUser->id = "111111111111111111111111";
   
    $objUser->primary_location_id = LOCATION_ID;
   
    $objUser->username = "test@test123.com";
    $objUser->user_type_id = "250";
    $objUser->first_name = "tom";
    $objUser->last_name = "test smith";

    $result = User::createUserRecord($objUser);
    echo '<pre>';
    print_r($result);
}
/* Update user record */
if (!empty($_GET['action']) && $_GET['action'] == 'update_user') {

    $objUser->id = "111111111111111111111111";
    $objUser->domain_id = "111111111111111111111111";
    $objUser->contact_id = "111111111111111111111111";
    $objUser->username = "test@test123.com";
    $objUser->last_name = "Smith";

    $user_id = '111111111111111111111111';
    $result = User::updateUserRecord($objUser, $user_id);
    echo '<pre>';
    print_r($result);
}
/* View self user record */
if (!empty($_GET['action']) && $_GET['action'] == 'view_self_user') {

    $result = User::viewSelfUserRecord();
    echo '<pre>';
    print_r($result);
}

/* View single user record */
if (!empty($_GET['action']) && $_GET['action'] == 'get_user') {

    $user_id = '111111111111111111111111';
    $result = User::getUserRecord($user_id);

    echo '<pre>';
    print_r($result);
}

/* View user list */
if (!empty($_GET['action']) && $_GET['action'] == 'get_user_list') {
    $perPage = 1;
    $sort = 'account_holder_name,created_ts';
    $filter = array(
        "username" => "test@test123.com",
    );
    $pageNo = null;
    $filterData = zeamsterApi::convertArrayToUrlStr($filter);
    $result = User::getUserRecord(null, $filterData, $perPage, $sort, $pageNo);
    echo '<pre>';
    print_r($result);
}
/* Delete user data */
if (!empty($_GET['action']) && $_GET['action'] == 'delete_user') {
    $user_id = '';  // To Do
    $result = User::deleteUserRecord();
    echo '<pre>';
    print_r($result);
}


/* * ********************************************************************************** */
/*                      Create , Get, Update and Delete Recurring Service               */
/* * ********************************************************************************* */
$objRecurring = new Recurring();
/* Create Recurring data */
if (!empty($_GET['action']) && $_GET['action'] == 'create_recurring_record') {

    $objRecurring->interval = "1";
    $objRecurring->interval_type = "m";
    $objRecurring->start_date = "2018-01-01";
    $objRecurring->description = "Test Recurring from Team";
    $objRecurring->transaction_amount = "5.50";
    $objRecurring->location_id = LOCATION_ID;
    $objRecurring->account_vault_id = "111111111111111111111111";
    $objRecurring->product_transaction_id = "111111111111111111111111";

    $result = Recurring::createRecurringRecord($objRecurring);
    echo '<pre>';
    print_r($result);
}
/* Create Recurring data */
/* Note : Update information is not available on Zeamste Site */

if (!empty($_GET['action']) && $_GET['action'] == 'update_recurring_record') {
   
    $recurring_id = '';
    $objRecurring = '';
    $result = Recurring::updateRecurringRecord($objRecurring);
    echo '<pre>';
    print_r($result);
}
/* Get single recurring record */
if (!empty($_GET['action']) && $_GET['action'] == 'get_recurring_record') {

    $recurring_id = "111111111111111111111111";
    $result = Recurring::getRecurringRecord($recurring_id);
    echo '<pre>';
    print_r($result);
}
/* Get recurring record list */
if (!empty($_GET['action']) && $_GET['action'] == 'get_recurring_list') {
    $perPage = 1;
    $sort = 'transaction_amount,created_ts';
    $filter = array(
        "account_vault_id" => "111111111111111111111111",
    );
    $filterData = zeamsterApi::convertArrayToUrlStr($filter);
    $pageNo = null;
    $result = Recurring::getRecurringRecord(null, $filterData, $perPage, $sort, $pageNo);
    echo '<pre>';
    print_r($result);
}

/* * ********************************************************************************** */
/*                      Create , Get, Update and Delete Terminal Service               */
/* * ********************************************************************************* */
/* Get single Terminal record */
if (!empty($_GET['action']) && $_GET['action'] == 'get_terminal_record') {

    $terminal_id = "111111111111111111111111";
    $result = Terminal::getTerminalsRecords($terminal_id);
    echo '<pre>';
    print_r($result);
}
/* Get Terminal record list */
if (!empty($_GET['action']) && $_GET['action'] == 'get_terminal_list') {

    $perPage = 1;
    $sort = 'created_ts';
    $filter = array(
        "account_holder_name" => "Link Black",
    );
    $filterData =  null; // zeamsterApi::convertArrayToUrlStr($filter);
    $pageNo = null;
    $result = Terminal::getTerminalsRecords(null, $filterData, $perPage, $sort, $pageNo);
    echo '<pre>';
    print_r($result);
}


echo '<br><br>' . '******************** END ****************************';

