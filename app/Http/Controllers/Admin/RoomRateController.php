<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Roomtype;
use App\RoomRate;
use App\RoomRateOption;
use App\ChannelMotel;
use App\ChannelMapping;
use Helper;
use DB;

class RoomRateController extends Controller
{
    public function index(Request $request)
    {
		$Hotel_ID  = Helper::getHotelId();
		return view('rooms.room-rate')->with("Hotel_ID", $Hotel_ID)->with("request", $request);
    }
	
	public function setavailRate(Request $request)
    {
       	$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		
		if($request->ajax())
		{
			$room_rate = $request->room_rate_price;
			$RoomRateRst = RoomRateOption::where([ ['hotel_id', $Hotel_ID], ['room_rate_option_id', $request->room_rate_option_id] ])->first();
			if(count($RoomRateRst))
			{
				$RoomTypeId   = $RoomRateRst->room_type_id;
				$Channel_From = $Channel_To = $RoomRateRst->room_rate_date;
				$RoomRateRst->room_rate_price = $room_rate;
				$RoomRateRst->save();
			
				$channelSql = DB::select("SELECT CMM.channel_room_type_id, CMM.channel_rate_plan_id, CM.channel_hotel_id, CM.channel_username, CM.channel_password
										  FROM channel_motel_mapping as CMM INNER JOIN channel_motels as CM ON CM.channel_id = CMM.channel_id
										  WHERE CMM.room_type_id = ".$RoomTypeId." AND CM.hotel_id = ".$Hotel_ID." AND CM.channel_status = 1 AND CM.channel_id = 1
										 ");
				if(count($channelSql))
				{
					foreach($channelSql as $channelRst){
						$Channel_RoomTypeId = $channelRst->channel_room_type_id;
						$Channel_RatePlanId = $channelRst->channel_rate_plan_id;
						$Channel_HotelId    = $channelRst->channel_hotel_id;
						$Channel_Username   = $channelRst->channel_username;
						$Channel_Password   = $channelRst->channel_password;
					}
					
					$requestXML = '<?xml version="1.0" encoding="UTF-8"?>
							<AvailRateUpdateRQ xmlns="http://www.expediaconnect.com/EQC/AR/2011/06">
								<Authentication username="'.$Channel_Username.'" password="'.$Channel_Password.'"/>
								<Hotel id="'.$Channel_HotelId.'"/>
								<AvailRateUpdate>
									<DateRange from="'.$Channel_From.'" to="'.$Channel_To.'"/>
									<RoomType id="'.$Channel_RoomTypeId.'" closed="false">
										<RatePlan id="'.$Channel_RatePlanId.'">
											<Rate currency="USD">
												<PerDay rate="'.$room_rate.'"/>
											</Rate>
										</RatePlan>
									</RoomType>
								</AvailRateUpdate>
							</AvailRateUpdateRQ>';
					
					$URL = "https://services.expediapartnercentral.com/eqc/ar";
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $URL);
					curl_setopt($ch, CURLOPT_VERBOSE, 1);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
					curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
					$responseXML = curl_exec($ch);
					$headerResponse = curl_getinfo($ch);
					curl_close($ch);
					
					if(isset($headerResponse['http_code']) && $headerResponse['http_code']==200)
					{
						$responseString = simplexml_load_string($responseXML);
						$responseJSON   = json_decode(json_encode($responseString), true);
						if(isset($responseJSON['Success'])){
							if(count($responseJSON['Success']) > 0){
								$ResponseStat = 'Success with Warning';
							}else{
								$ResponseStat = 'Success';
							}
						}
						elseif(isset($responseJSON['Error'])){
							$ResponseStat = 'Error';
						}
					}else{
						$ResponseStat = 'Error';
					}
					DB::select("CALL ChannelCommunication_SP ('".$Hotel_ID."', 'MMK', '".$URL."', '".$requestXML."', 'Expedia', '".$responseXML."', '".$ResponseStat."', '".$_SERVER['REMOTE_ADDR']."')");
				}
				return response()->json(['response' => 'Room Rate updated successfully', 'status' => 'success']);
			}else{
				return response()->json(['response' => 'Selected Room Rate record not Found!', 'status' => 'error']);
			}
		}else{
			return response()->json(['response' => 'Try Again!!', 'status' => 'error']);
		}
    }

    public function store(Request $request)
    {
		$admin_ID  = Helper::getAdminUserId();
		$Hotel_ID  = Helper::getHotelId();
		
		$room_type_id   = $request->room_type_id;
		$room_rate_from = $request->room_rate_from;
		$room_rate_to   = $request->room_rate_to;
		$room_rate      = $request->room_rate;
		
		//  Monday
		if($request->room_rate_mon == 1){
			$room_rate_mon = $room_rate;
			$room_chck_mon = 1;
			$Channel_mon   = 'true';
		}else{
			$room_rate_mon = 0;
			$room_chck_mon = 0;
			$Channel_mon   = 'false';
		}
		//  Tuesday
		if($request->room_rate_tue == 1){
			$room_rate_tue = $room_rate;
			$room_chck_tue = 1;
			$Channel_tue   = 'true';
		}else{
			$room_rate_tue = 0;
			$room_chck_tue = 0;
			$Channel_tue   = 'false';
		}
		//  Wednesday
		if($request->room_rate_wed == 1){
			$room_rate_wed = $room_rate;
			$room_chck_wed = 1;
			$Channel_wed   = 'true';
		}else{
			$room_rate_wed = 0;
			$room_chck_wed = 0;
			$Channel_wed   = 'false';
		}
		//  Thursday
		if($request->room_rate_thu == 1){
			$room_rate_thu = $room_rate;
			$room_chck_thu = 1;
			$Channel_thu   = 'true';
		}else{
			$room_rate_thu = 0;
			$room_chck_thu = 0;
			$Channel_thu   = 'false';
		}
		//  Friday
		if($request->room_rate_fri == 1){
			$room_rate_fri = $room_rate;
			$room_chck_fri = 1;
			$Channel_fri   = 'true';
		}else{
			$room_rate_fri = 0;
			$room_chck_fri = 0;
			$Channel_fri   = 'false';
		}
		//  Saturday
		if($request->room_rate_sat == 1){
			$room_rate_sat = $room_rate;
			$room_chck_sat = 1;
			$Channel_sat   = 'true';
		}else{
			$room_rate_sat = 0;
			$room_chck_sat = 0;
			$Channel_sat   = 'false';
		}
		//  Sunday
		if($request->room_rate_sun == 1){
			$room_rate_sun = $room_rate;
			$room_chck_sun = 1;
			$Channel_sun   = 'true';
		}else{
			$room_rate_sun = 0;
			$room_chck_sun = 0;
			$Channel_sun   = 'false';
		}
		
		if($room_rate_from && $room_rate_to)
		{
			$room_rate_option_from = $room_rate_from = date('Y-m-d', strtotime($room_rate_from));
			$room_rate_option_to   = $room_rate_to   = date('Y-m-d', strtotime($room_rate_to."+1 day"));
		}
		elseif(!$room_rate_from && $room_rate_to)
		{
			$room_rate_from = date('m/d/Y');
			$room_rate_option_from = $room_rate_from = date('Y-m-d', strtotime($room_rate_from));
			$room_rate_option_to   = $room_rate_to   = date('Y-m-d', strtotime($room_rate_to."+1 day"));
			
		}
		elseif($room_rate_from && !$room_rate_to)
		{
			$room_rate_option_from = $room_rate_from = date('Y-m-d', strtotime($room_rate_from));
			$room_rate_option_to   = $room_rate_to   = date('Y-m-d', strtotime($room_rate_from."+1 day"));
		}
		else
		{
			$room_rate_option_from = date('Y-m-d');
			$room_rate_option_to   = date('Y-m-d', strtotime($room_rate_option_from.'+12 months'));
			$room_rate_from = "";
			$room_rate_to   = "";
		}
		
		$channelSql = DB::select("SELECT CMM.channel_room_type_id, CMM.channel_rate_plan_id, CM.channel_hotel_id, CM.channel_username, CM.channel_password
								  FROM channel_motel_mapping as CMM INNER JOIN channel_motels as CM ON CM.channel_id = CMM.channel_id
								  WHERE CMM.room_type_id = ".$room_type_id." AND CM.hotel_id = ".$Hotel_ID." AND CM.channel_status = 1 AND CM.channel_id = 1
								 ");
		if(count($channelSql))
		{
			foreach($channelSql as $channelRst){
				$Channel_RoomTypeId = $channelRst->channel_room_type_id;
				$Channel_RatePlanId = $channelRst->channel_rate_plan_id;
				$Channel_HotelId    = $channelRst->channel_hotel_id;
				$Channel_Username   = $channelRst->channel_username;
				$Channel_Password   = $channelRst->channel_password;
			}
			$Channel_From = $room_rate_option_from;
			$Channel_To   = date('Y-m-d', strtotime($room_rate_option_to.'-1 day'));
			
			$requestXML = '<?xml version="1.0" encoding="UTF-8"?>
					<AvailRateUpdateRQ xmlns="http://www.expediaconnect.com/EQC/AR/2011/06">
						<Authentication username="'.$Channel_Username.'" password="'.$Channel_Password.'"/>
						<Hotel id="'.$Channel_HotelId.'"/>
						<AvailRateUpdate>
							<DateRange from="'.$Channel_From.'" to="'.$Channel_To.'" sun="'.$Channel_sun.'" mon="'.$Channel_mon.'" tue="'.$Channel_tue.'" wed="'.$Channel_wed.'" thu="'.$Channel_thu.'" fri="'.$Channel_fri.'" sat="'.$Channel_sat.'"/>
							<RoomType id="'.$Channel_RoomTypeId.'" closed="false">
								<RatePlan id="'.$Channel_RatePlanId.'">
									<Rate currency="USD">
										<PerDay rate="'.$room_rate.'"/>
									</Rate>
								</RatePlan>
							</RoomType>
						</AvailRateUpdate>
					</AvailRateUpdateRQ>';
			$URL = "https://services.expediapartnercentral.com/eqc/ar";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $URL);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
			$responseXML = curl_exec($ch);
			$headerResponse = curl_getinfo($ch);
			curl_close($ch);
			
			if(isset($headerResponse['http_code']) && $headerResponse['http_code']==200)
			{
				$responseString = simplexml_load_string($responseXML);
				$responseJSON   = json_decode(json_encode($responseString), true);
				if(isset($responseJSON['Success'])){
					if(count($responseJSON['Success']) > 0){
						$ResponseStat = 'Success with Warning';
					}else{
						$ResponseStat = 'Success';
					}
				}
				elseif(isset($responseJSON['Error'])){
					$ResponseStat = 'Error';
				}
			}else{
				$ResponseStat = 'Error';
			}
			DB::select("CALL ChannelCommunication_SP ('".$Hotel_ID."', 'MMK', '".$URL."', '".$requestXML."', 'Expedia', '".$responseXML."', '".$ResponseStat."', '".$_SERVER['REMOTE_ADDR']."')");
		}
		
		$RoomTypeRst = Roomtype::find($room_type_id);
		$RoomTypeValue = $RoomTypeRst->room_type;
		
		$RoomRateArray = array();
		$RoomRateArray[] = $Hotel_ID;
		$RoomRateArray[] = $room_type_id;
		$RoomRateArray[] = $room_rate_from;
		$RoomRateArray[] = $room_rate_to;
		$RoomRateArray[] = $room_rate;
		$RoomRateArray[] = $room_chck_mon;
		$RoomRateArray[] = $room_chck_tue;
		$RoomRateArray[] = $room_chck_wed;
		$RoomRateArray[] = $room_chck_thu;
		$RoomRateArray[] = $room_chck_fri;
		$RoomRateArray[] = $room_chck_sat;
		$RoomRateArray[] = $room_chck_sun;
		$RoomRateArray[] = $_SERVER['REMOTE_ADDR'];
		$RoomRateArray[] = $admin_ID;
		
		DB::select('CALL RoomRate_Insert_SP(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $RoomRateArray);
				
		$date1 = date_create($room_rate_option_from);
		$date2 = date_create($room_rate_option_to);
		$diff = date_diff($date1, $date2);
		if($date1 && $date2)
		{
			$diff = date_diff($date1, $date2);
			$dayNum = $diff->format("%a");
			for($i = 1; $i <= $dayNum; $i++)
			{
				if(('$room_rate_option_from') <= ('$room_rate_option_to'))
				{
					$dayWisePrice = ${'room_rate_'.strtolower(date('D', strtotime($room_rate_option_from)))};
					if($dayWisePrice){
						$RoomRateRst = RoomRateOption::where([ ['room_type_id', '=', $room_type_id], ['hotel_id', '=', $Hotel_ID], ['room_rate_date', '=', $room_rate_option_from] ])->first();
						if(!count($RoomRateRst)){
							$RoomRateOptionArray = array();
							$RoomRateOptionArray[] = $Hotel_ID;
							$RoomRateOptionArray[] = $room_type_id;
							$RoomRateOptionArray[] = $room_rate_option_from;
							$RoomRateOptionArray[] = $dayWisePrice;
							$RoomRateOptionArray[] = $_SERVER['REMOTE_ADDR'];
							$RoomRateOptionArray[] = $admin_ID;
							
							DB::select('CALL RoomRateOption_Insert_SP(?,?,?,?,?,?)', $RoomRateOptionArray);
						}else{
							$RoomRateRst->hotel_id = $Hotel_ID;
							$RoomRateRst->room_type_id = $room_type_id;
							$RoomRateRst->room_rate_date = $room_rate_option_from;
							$RoomRateRst->room_rate_price = $dayWisePrice;
							$RoomRateRst->reg_ip = $_SERVER['REMOTE_ADDR'];
							$RoomRateRst->added_by = $admin_ID;
							$RoomRateRst->save();
						}
					}
				} 
				$room_rate_option_from = date('Y-m-d', strtotime($room_rate_option_from.'+1 days'));
			}
		}
		return redirect('room/room-rate?type=room_price&RoomTypeSelectedId='.$room_type_id)->with('success','Rate added successfully for '.$RoomTypeValue.'.');
    }
}