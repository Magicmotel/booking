<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardGuarantee extends Model
{
    
    protected $fillable = [
        'hotel_id', 'room_reservation_id', 'card_type', 'card_code', 'card_number', 'card_expiry', 'card_cvv',
		'card_holder', 'address_line', 'city_name', 'postal_code', 'state_prov', 'country_name', 'reg_ip', 'added_by'
	];
	protected $primaryKey = 'guarantee_id';	
	protected $table = 'reservation_card_guarantee';
	
}
