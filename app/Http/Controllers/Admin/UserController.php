<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Auth;
use Helper;
use DB;

class UserController extends Controller
{
	public function index(Request $request)
    {
		$Hotel_ID = Helper::getHotelId();
		$Results = DB::table('users as U')
				 	 ->select('R.name as RoleName', 'U.fname as UserFname', 'U.mname as UserMname', 'U.lname as UserLname', 'U.id as UserId', 'U.super_admin as UserSuper')
				 	 ->join('roles as R', 'R.id', '=', 'U.role_id')
				 	 ->where([ ['U.hotel_id', $Hotel_ID] ])
				 	 ->orderBy('R.id', 'ASC')
				 	 ->paginate(10);
		return view('users.users-index')->with("Results", $Results)->with('i', ($request->input('page', 1) - 1) * 10);
	}

    public function create()
    {	
		$Hotel_ID = Helper::getHotelId();
        return view('users.users-create')->with('Hotel_ID', $Hotel_ID);
    }

    public function store(Request $request)
    {
		$unique_ID = Helper::getUniqueId();
		$admin_ID  = Helper::getAdminUserId();
		
        $validator = Validator::make($request->all(), [
            'fname'    => 'Required|Min:3|Max:80|Alpha',
			'lname'    => 'Required|Min:3|Max:80|Alpha',
			'email'    => 'Required|Between:3,64|Email|Unique:users',
			'password' => 'Required|AlphaNum|Min:7|Confirmed',
			'password_confirmation'=>'Required|AlphaNum|Min:7',
			'role_id'  => 'Required'
			//'shift_id' => 'Required'
        ]);
		
		if ($validator->fails()) {
            return redirect('users/create')->withErrors($validator)->withInput();
        }
		else
		{		
			$RequestArray = array();	
			$RequestArray[] = $unique_ID;
			$RequestArray[] = $request->hotel_id;
			$RequestArray[] = $request->role_id;
			//$RequestArray[] = $request->shift_id;
			$RequestArray[] = $request->fname;
			$RequestArray[] = $request->mname;
			$RequestArray[] = $request->lname;
			$RequestArray[] = $request->email;
			$RequestArray[] = bcrypt($request->password);
			$RequestArray[] = $_SERVER['REMOTE_ADDR'];
			$RequestArray[] = $admin_ID;
			
			DB::select('CALL User_Insert_SP(?,?,?,?,?,?,?,?,?,?)', $RequestArray);
			
			return redirect('users')->with('success','User created successfully');
		}
    }

    public function edit($id)
    {	
		$Hotel_ID = Helper::getHotelId();
		$Results  = User::where([ ['id', $id], ['hotel_id', $Hotel_ID] ])->first();
		if(count($Results)){
			return view('users.users-edit')->with("Results", $Results);
		}else{
			return redirect('users')->with('danger','User# '.$id.' not Found');
		}
    }

    public function update(Request $request, $id)
    {
		$Hotel_ID = Helper::getHotelId();
		$admin_ID = Helper::getAdminUserId();
		
		$validator = Validator::make($request->all(), [
           	//'name' 	 => 'Required|Min:3|Max:80|Alpha',
			//'email'    => 'Required|Between:3,64|Email|Unique:users',
			//'password' => 'Required|AlphaNum|Min:7|Confirmed',
			//'password_confirmation'=>'Required|AlphaNum|Min:7',
			'role_id'  => 'Required'
			//'shift_id' => 'Required'
        ]);
		
		if ($validator->fails()) {
            return redirect('users/'.$id.'/edit')->withErrors($validator)->withInput();
        }
		else
		{
			$users = User::where([ ['id', '=', $id], ['hotel_id', '=', $Hotel_ID] ])->first();
			if($users){
				//$users->name = $request->name;
				//$users->email = $request->email;
				//$users->password = bcrypt($request->password);
				$users->role_id = $request->role_id;
				//$users->shift_id = $request->shift_id;
				$users->reg_ip = $_SERVER['REMOTE_ADDR'];
				$users->added_by = $admin_ID;
				$users->save();
			}
			return redirect('users')->with('success','User updated successfully');
		}
    }
	
	public function destroy($id)
    {
		User::destroy($id);
		return redirect('users')->with('success','User deleted permanently');	
    }
}
