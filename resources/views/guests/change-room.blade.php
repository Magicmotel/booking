@extends('layouts.layout')
<? 	foreach($Results as $val){
		$outPut = $val;
	}
?>
@section('title')
	Guest : {{$outPut->GuestLname}}, {{$outPut->GuestFname}}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Guest : {{$outPut->GuestLname}}, {{$outPut->GuestFname}}</h3></div>
            @if($outPut->RsrvSource)
                <div class="col-md-3 col-sm-3 col-xs-6 fltR zeroRightPadd">
                    <h3 class="AlgnCenter">{{$outPut->RsrvSource}}</h3>
                </div>
            @endif
        </div>
        {{--*/ $PayBalance = ($outPut->PayAmount-$outPut->PayDeposit); /*--}}
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('guest-info', $outPut->RsrvId)}}">Guest Info</a></li>
                <li><a href="{{url('payment-info', $outPut->RsrvId)}}">Payment Info</a></li>
                <li class="active"><a href="javascript:void(0);">Room Change</a></li>
                <li><a href="{{url('guest-folio', $outPut->RsrvId)}}">Guest Folio</a></li>
                <li><a href="{{url('change-stay', $outPut->RsrvId)}}">Change Stay</a></li>
                <li><a href="{{url('view-changes', $outPut->RsrvId)}}">View Changes</a></li>
               	@if($outPut->RsrvStat == '' && $PayBalance == 0)
                <li><a href="{{url('cancel-reservation', $outPut->RsrvId)}}">Cancel Reservation</a></li>
                @endif
            </ul>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                    	@if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @elseif ($message = Session::get('danger'))
                            <div class="alert alert-danger">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                    	@if($outPut->RoomNumber)
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        	<div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	Assigned Room#
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                            	<div class="fltL" style="position:relative; padding-top:2px;">{{$outPut->RoomNumber}} ({{$outPut->RoomTypes}})</div>
                                @if($outPut->RsrvLock)
                                <i class="fa fa-lock lockFeaturButton" aria-hidden="true"></i>
                                @endif
                            </div>
                        </div>
                        @else
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        	<div class="col-md-6 col-sm-12 col-xs-12 labelText">
                            	Not assigned any Room ({{$outPut->RoomType}}) to Reservation# {{$outPut->RsrvId}}
                            </div>
                        </div>
                        @endif
                        @if($outPut->RsrvStat != 'Cancelled' && $outPut->RsrvStat != 'Checked Out' && $outPut->RsrvLock != 1)
                        <div class="col-md-12 col-sm-12 col-xs-12 marginTop24">
                        	<table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th class="column-title">Room Number</th>
                                        <th class="column-title">Type</th>
                                        <th class="column-title">Status</th>
                                        <th class="column-title">Action</th>
                                    </tr>
                                </thead>
                            
                                <tbody>
                                	@foreach($vcntRoom_Rst as $vals)
                                        <tr class="even pointer">
                                            <td class=" ">{!!$vals->RoomNumber!!}</td>
                                            <td class=" ">{!!$vals->RoomTypes!!} &nbsp; ({!!$vals->RoomType!!})</td>
                                            <td class=" ">{{($vals->RoomStat==1)?"Clean":"Dirty"}}</td>
                                            <td class=" last">
                                            
                                            	<form action="{!!url('allot-room', $outPut->AllotId)!!}" id="room_assignId_{{$vals->RoomId}}" method="post" style="float:left;width:auto; margin:0;">
                                                {!!method_field('PATCH')!!}
                                                {!!csrf_field()!!}
                                                {!!Form::input('hidden', 'type', $request->type)!!}
                                                {!!Form::input('hidden', 'antitype', '', ['id'=>'antitype'.$vals->RoomId])!!}
                                                {!!Form::input('hidden', 'rsrv_id', $outPut->RsrvId)!!}
                                                {!!Form::input('hidden', 'room_id', $vals->RoomId)!!}
                                                {!!Form::input('hidden', 'room_type_id', $vals->RoomTypeId)!!}
                                                {!!Form::input('hidden', 'c_room_type_id', $outPut->RoomTypeId)!!}
                                                @if($vals->RoomStat==1)
                                                	@if($vals->RoomTypeId != $outPut->RoomTypeId)
                                                		<div class="btn btn-success buTTonResize" onclick="return otherConfirmAllot('{{$vals->RoomId}}')"><i class="fa fa-check-square-o"></i> Assign Room</div>
                                                    @else
		                                                {!!Form::button('<i class="fa fa-check-square-o"></i> Assign Room', ['class'=>'btn btn-success buTTonResize', 'type'=>'submit'])!!}
                                                    @endif
                                               	@else
                                                	@if($vals->RoomTypeId != $outPut->RoomTypeId)
                                                		<div class="btn btn-success buTTonResize" onclick="return dirtyOtherConfirmAllot('{{$vals->RoomId}}')"><i class="fa fa-check-square-o"></i> Assign Room</div>
                                                    @else
		                                                {!!Form::button('<i class="fa fa-check-square-o"></i> Assign Room', ['class'=>'btn btn-success buTTonResize', 'type'=>'submit', 'onclick' => 'return dirtyConfirmAllot()' ])!!}
                                                    @endif
                                               	@endif
                                                </form>
                                          	</td>
                                        </tr>
                                   	@endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>

<!-- calendar modal -->
<div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="myModalLabel">Confirm</h4>
            </div>
            <div class="modal-footer AlgnCenter">
                <button type="button" class="btn btn-success antosubmit">Change Room with same Rate</button>
                <button type="button" class="btn btn-warning antisubmit">Change Room with new Rate</button>
                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
<script>
	function dirtyConfirmAllot()
	{
		var x = confirm("Are you sure you assigning dirty Room to this Guest?");
		if (x){
			return true;
		}else{
			return false;
		}
	}
	function otherConfirmAllot(str)
	{
		$('#fc_create').click();
		$(".antosubmit").on("click", function() {
				$("#antitype"+str).val("");
			$('.antoclose').click();
			$( "#room_assignId_"+str ).submit();
			return true;
		});
		$(".antisubmit").on("click", function() {
			$("#antitype"+str).val("1");
			$('.antoclose').click();
			$( "#room_assignId_"+str ).submit();
			return true;
		});
		return false;
	}
	function dirtyOtherConfirmAllot(str)
	{
		var x = confirm("Are you sure you assing dirty Room to this Guest?");
		if (x){
			$('#fc_create').click();
			$(".antosubmit").on("click", function() {
				$("#antitype"+str).val("");
				$('.antoclose').click();
				$( "#room_assignId_"+str ).submit();
				return true;
			});
			$(".antisubmit").on("click", function() {
				$("#antitype"+str).val("1");
				$('.antoclose').click();
				$( "#room_assignId_"+str ).submit();
				return true;
			});
			return false;
		}else{
			return false;
		}
	}
</script>
@stop