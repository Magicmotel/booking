<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Roomtype;
use App\GatewayPayment;
use App\GatewayCredential;
use Helper;
use DB;

class GatewayPaymentController extends Controller
{
	public function zeamster(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID   = $Helper->getHotelId();
		$Gateway_ID = 1;
		$credentialNum = 1;
		$credentialRst = GatewayCredential::where([ ['hotel_id', $Hotel_ID], ['gateway_id', $Gateway_ID] ])->first();
		if(!count($credentialRst)){
			return redirect('zeamster/credentials');
		}
		return view('gateways.zeamster')->with("credentialRst", $credentialRst)->with("Hotel_ID", $Hotel_ID);
    }
	
	public function zeamster_status($id)
    {	
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$Results = GatewayCredential::where([ ['gc_id', '=', $id], ['hotel_id', '=', $Hotel_ID] ])->first();
		if(count($Results))
		{
			if($Results->gateway_status == 1){
				$Results->gateway_status = 0;
				$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Results->added_by = $admin_ID;
				$Results->save();
				return redirect('zeamster')->with('success','Zeamster paused successfully');
			}else{
				$Results->gateway_status = 1;
				$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Results->added_by = $admin_ID;
				$Results->save();
				return redirect('zeamster')->with('success','Zeamster activated successfully');
			}
		}else{
			return redirect('zeamster')->with("danger", "Please select your Motel");
		}
    }
	
	public function zeamster_credential(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID   = $Helper->getHotelId();
		$Gateway_ID = 1;
		$Num = 1;
		$Results = GatewayCredential::where([ ['hotel_id', $Hotel_ID], ['gateway_id', $Gateway_ID] ])->first();
		return view('gateways.zeamster-credential')->with("Results", $Results)->with("Hotel_ID", $Hotel_ID);
    }
	
	public function zeamster_credential_store(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		$Gateway_ID = 1;
		
		if($Hotel_ID == $request->hotel_id)
		{
			if($request->gc_id){
				$gc_id = $request->gc_id;
				$Rst = GatewayCredential::where('gc_id', $gc_id)->first();
				$Rst->hotel_id   = $Hotel_ID;
				$Rst->gateway_id = $Gateway_ID;
				$Rst->user_id    = $request->user_id;
				$Rst->user_api_key = $request->user_api_key;
				$Rst->developer_id = $request->developer_id;
				$Rst->location_id  = $request->location_id;
				$Rst->ticket_hash_key = $request->ticket_hash_key;
				$Rst->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Rst->added_by = $admin_ID;
				$Rst->save();
			}else{
				$RequestArray = array();
				$RequestArray[] = $Hotel_ID;
				$RequestArray[] = $Gateway_ID;
				$RequestArray[] = $request->user_id;
				$RequestArray[] = $request->user_api_key;
				$RequestArray[] = $request->developer_id;
				$RequestArray[] = $request->location_id;
				$RequestArray[] = $request->ticket_hash_key;
				$RequestArray[] = $_SERVER['REMOTE_ADDR'];
				$RequestArray[] = $admin_ID;
				
				DB::select('CALL GatewayCredential_Insert_SP(?,?,?,?,?,?,?,?,?)', $RequestArray);
			}
			return redirect('zeamster')->with('success','Zeamster Credentials updated successfully');
		}
		else
		{
			return redirect('zeamster')->with("danger", "Please select your Motel");
		}
    }
}
