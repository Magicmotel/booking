<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CompanyProfile;
use App\CompanyProfileRate;
use App\ZipCityState;
use Helper;
use DB;

class CompanyProfileController extends Controller
{
    public function index(Request $request)
    {
		$Hotel_ID = Helper::getHotelId();
		$Results  = CompanyProfile::where('hotel_id', '=', $Hotel_ID)->paginate(10);
		return view('company.company-index')->with("Hotel_ID", $Hotel_ID)->with("Results", $Results)->with('PSJ', ($request->input('page', 1) - 1) * 10);
	}
	
	public function create()
	{
		$Hotel_ID  = Helper::getHotelId();
		return view('company.company-create')->with("Hotel_ID", $Hotel_ID);
	}

    public function store(Request $request)
    {
		$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		if($Hotel_ID == $request->hotel_id)
		{
			try{
				DB::beginTransaction();
				
				$RequestArray = array();
				$RequestArray[] = $Hotel_ID;
				$RequestArray[] = $request->company_name;
				$RequestArray[] = $request->company_address;
				$RequestArray[] = $request->company_zip;
				$RequestArray[] = $request->company_city;
				$RequestArray[] = $request->company_state;
				$RequestArray[] = $request->company_lang;
				$RequestArray[] = $request->company_email;
				$RequestArray[] = $request->company_phone;
				$RequestArray[] = $request->company_fax;
				$RequestArray[] = $request->contact_name;
				$RequestArray[] = $request->contact_title;
				$RequestArray[] = $request->company_notes;
				$RequestArray[] = ($request->company_tax)?$request->company_tax:0;
				$RequestArray[] = $_SERVER['REMOTE_ADDR'];
				$RequestArray[] = $admin_ID;
				
				$Results = DB::select('CALL CompanyProfile_Insert_SP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $RequestArray);
				$CPIdArray = DB::select('SELECT LAST_INSERT_ID()');
				foreach(json_decode(json_encode($CPIdArray), true)[0] as $val){
					$CompanyProfileId = $val;
				}
				
				foreach($request->room_type_id as $key=>$vals)
				{
					$RequestArray = array();	
					$RequestArray[] = $Hotel_ID;
					$RequestArray[] = $CompanyProfileId;
					$RequestArray[] = $vals;
					$RequestArray[] = $request->company_rate[$key];
					$RequestArray[] = $_SERVER['REMOTE_ADDR'];
					$RequestArray[] = $admin_ID;
					DB::select('CALL CompanyProfileRate_Insert_SP (?,?,?,?,?,?)', $RequestArray);
				}
				DB::commit();
				return redirect('company-profile')->with('success','Company Profile added successfully.');
			} catch (\Exception $e) {
				DB::rollback();
				return redirect('company-profile')->with('success','Try Again!!');
			}
		}else{
			return redirect('company-profile')->with("danger", "Please select your Motel");
		}
    }
	
	public function edit($id)
    {
		$Hotel_ID = Helper::getHotelId();
		$Results  = CompanyProfile::where([ ['hotel_id', $Hotel_ID], ['profile_id', $id] ])->first();
		if(count($Results))
		{
			$rateResults = CompanyProfileRate::where([ ['hotel_id', $Hotel_ID], ['profile_id', $id] ])->get();
			$rateOutPut  = array();
			foreach($rateResults as $rateVal){
				$rateOutPut[$rateVal->room_type_id] = $rateVal->company_rate;
			}
			return view('company.company-edit')->with("Hotel_ID", $Hotel_ID)->with("Results", $Results)->with("rateOutPut", $rateOutPut);
		}else{
			return redirect('company-profile')->with('danger', 'Selected Company Profile# '.$id.' not Found.');
		}
    }
	
	public function update(Request $request, $id)
    {
		$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		$profileOBJ = CompanyProfile::where([ ['hotel_id', $Hotel_ID], ['profile_id', $id] ])->first();
		if(count($profileOBJ))
		{
			$profileOBJ->hotel_id = $Hotel_ID;
			$profileOBJ->company_name = $request->company_name;
			$profileOBJ->company_address = $request->company_address;
			$profileOBJ->company_zip = $request->company_zip;
			$profileOBJ->company_city = $request->company_city;
			$profileOBJ->company_state = $request->company_state;
			$profileOBJ->company_lang = $request->company_lang;
			$profileOBJ->company_email = $request->company_email;
			$profileOBJ->company_phone = $request->company_phone;
			$profileOBJ->company_fax = $request->company_fax;
			$profileOBJ->contact_name = $request->contact_name;
			$profileOBJ->contact_title = $request->contact_title;
			$profileOBJ->company_notes = $request->company_notes;
			$profileOBJ->company_tax = ($request->company_tax)?$request->company_tax:0;
			$profileOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
			$profileOBJ->added_by = $admin_ID;
			$profileOBJ->save();
			
			foreach($request->room_type_id as $key=>$vals)
			{
				$rateOBJ = CompanyProfileRate::where([ ['hotel_id', $Hotel_ID], ['profile_id', '=', $id], ['room_type_id', '=', $vals] ])->first();
				if(count($rateOBJ)){
					$rateOBJ->hotel_id = $Hotel_ID;
					$rateOBJ->profile_id = $id;
					$rateOBJ->room_type_id = $vals;
					$rateOBJ->company_rate = $request->company_rate[$key];
					$rateOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
					$rateOBJ->added_by = $admin_ID;
					$rateOBJ->save();
				}else{
					$RequestArray = array();	
					$RequestArray[] = $Hotel_ID;
					$RequestArray[] = $id;
					$RequestArray[] = $vals;
					$RequestArray[] = $request->company_rate[$key];
					$RequestArray[] = $_SERVER['REMOTE_ADDR'];
					$RequestArray[] = $admin_ID;
					DB::select('CALL CompanyProfileRate_Insert_SP (?,?,?,?,?,?)', $RequestArray);
				}
			}		
			return redirect('company-profile')->with('success', 'Company Profile updated Successfully.');
		}else{
			return redirect('company-profile')->with('danger', 'Selected Company Profile# '.$id.' not Found.');
		}
    }
	
	public function getCityData(Request $request)
    {
		$Hotel_ID = Helper::getHotelId();
		$admin_ID = Helper::getAdminUserId();
		
		if($request->ajax())
		{
			$zipCode = $request->zipCode;
			$Results = ZipCityState::where('zip_code', $zipCode)->first();
			if(count($Results)){
				return response()->json(['response' => "City, State Inserted.", 'status' => 'success', 'zip_city' => $Results->zip_city, 'zip_state' => $Results->zip_state]);
			}else{
				return response()->json(['response' => "Zip Code not Found.", 'status' => 'error', 'zip_city' => '', 'zip_state' => '']);
			}
		}else{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
}