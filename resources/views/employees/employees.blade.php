@extends('layouts.layout')

@section('title')
	Manage Employees
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Employees</h3></div>
        </div>
        <?php /*?><div class="col-md-2 col-sm-2 col-xs-2 fltR zeroRightPadd marginBottom5">
        <select name="shift_type" id="shift_type" class="form-control col-md-3 col-xs-6" onchange="FilterSource('{{$request->job_title}}', this.value)">
            <option value="" >Select Shift</option>
            @foreach( App\Shift::where([ ['hotel_id', $Hotel_ID] ])->get(['shift_name as ShiftType', 'shift_id as ShiftTypeId']) as $val )
            <option value="{!!$val->ShiftTypeId!!}"{!!($request->shift_type == $val->ShiftTypeId)?" Selected":""!!}>{!!$val->ShiftType!!}</option>
            @endforeach
        </select>
        </div><?php */?>
        <div class="col-md-2 col-sm-3 col-xs-6 fltR zeroRightPadd marginBottom5">
        {{--*/
        $roleQry = DB::select("SELECT name as JobTitle, id as JobTitleId FROM roles
                               WHERE (hotel_id = ".$Hotel_ID." OR hotel_id = 0) AND id != 1 ORDER BY hotel_id ASC, id ASC, name ASC");
        /*--}}
        <select name="job_title" id="job_title" class="form-control col-md-3 col-xs-6" onchange="FilterSource(this.value)">
            <option value="">Select Job Title</option>
            @foreach(  $roleQry as $val )
            <option value="{!!$val->JobTitleId!!}"{!!($request->job_title == $val->JobTitleId)?" Selected":""!!}>{!!$val->JobTitle!!}</option>
            @endforeach
        </select>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class="active"><a href="javascript:void(0);">Manage Employees</a></li>
                <li><a href="{{url('employees/create')}}">Create Employee</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                           	@elseif ($message = Session::get('danger'))
                            	<div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                    	<th>No</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Job Title</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            
                                <tbody>
                                @if(count($Results))
                                	@foreach($Results as $vals)
                                    <tr class="even pointer">
                                        <td>{{ ++$i }}</td>
                                        <td>{!!$vals->Fname!!}{!!($vals->Mname)?" $vals->Mname":""!!}{!!" ".$vals->Lname!!}</td>
                                        <td>{!!$vals->EmpPhone!!}</td>
                                        <td>{!!$vals->EmpEmail!!}</td>
                                        <td class="UpperLetter">{!!$vals->RoleName!!}</td>
                                        <td>
                                            <a href="{!!url('employees', $vals->EmpId)!!}/edit" class="left btn btn-info buTTonResize"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                            <form action="{!!url('employees', $vals->EmpId)!!}" method="post" onsubmit="return ConfirmAction('Are you sure you want to delete this Employee?')" class="left">
                                            {!!method_field('DELETE')!!}
                                            {!!csrf_field()!!}
                                            {!!Form::hidden('hotel_id', $Hotel_ID)!!}
                                            {!!Form::button('<i class="fa fa-trash-o"></i> Delete', ['class'=>'left btn btn-danger buTTonResize marginLeft5', 'type'=>'submit'])!!}
                                            </form>
                                        </td>
                                    </tr>
                                   	@endforeach
                               	@else
                                	<tr class="even pointer">
                                        <td colspan="6" class="errorMessageTR">No Employee Found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $Results->render() !!}
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function FilterSource(str){
		if(str){
			RedirectStr = '?';
			if(str){
				RedirectStr += "job_title="+str+"&";
			}
			RedirectStr = RedirectStr.replace(/&+$/,'');
			window.location.href="{!!url('employees')!!}"+RedirectStr;
		}else if(!str){
			window.location.href="{!!url('employees')!!}";
		}
	}
	function ConfirmAction(str)
	{
		var x = confirm(str);
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop      
