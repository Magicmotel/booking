@extends('layouts.layout')

@section('title')
	Manage Taxes
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Taxes</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class="active"><a href="javascript:void(0);">Manage Taxes</a></li>
                <li><a href="{{url('room/discounts')}}">Manage Discounts</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th>Tax Name</th>
                                    <th>Type</th>
                                    <th>Value</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($Results))
                                @foreach($Results as $vals)
                                <tr{!!($vals->status==1)?' class="unMask"':''!!}>
                                    <td>{{$vals->tax_type}}</td>
                                    <td>{!!($vals->tax_value_type==1)?"%":"Flat"!!}</td>
                                    <td>{{$vals->tax_value}}</td>
                                    <td>{{($vals->tax_start == '0000-00-00')?'':date("m/d/Y", strtotime($vals->tax_start))}}</td>
                                    <td>{{($vals->tax_end == '0000-00-00')?'':date("m/d/Y", strtotime($vals->tax_end))}}</td>
                                </tr>
                                @endforeach
                           	@else
                                <tr class="even pointer">
                                    <td colspan="5" class="errorMessageTR">No Taxes Found</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    	<div class="form-group form-group-last marginTop24">
                            <div class="col-md-6 col-sm-6 col-xs-12 zeroPadd">
                                <a href="{{url('support')}}" class="btn btn-primary"><i class="fa fa-envelope"></i> Contact to Support</a>
                            </div>
                    	</div>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop