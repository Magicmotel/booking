@extends('layouts.layout')

@section('title')
	Assign Shifts
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Assign Shifts</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('record-shifts')}}">Shifts Management</a></li>
                <li class="active"><a href="javascript:void(0);">Assign Shifts</a></li>
                <li><a href="{{url('shifts')}}">Manage Shifts</a></li>
                <li><a href="{{url('shifts/create')}}">Create Shift</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @elseif ($message = Session::get('danger'))
                            <div class="alert alert-danger">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <form id="demo-form2" action="{!!url('assign-shifts')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                            {!!csrf_field()!!}
                            <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Job Titles <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-3 col-xs-12 LR_Padd5">
                                    <select name="job_title" id="job_title" class="form-control col-md-3 col-xs-6" required="required" onchange="FilterSource(this.value)">
                                        <option value="">Select Job Title</option>
                                        @foreach( App\Role::where([ ['hotel_id', $Hotel_ID] ])->get(['name as RoleType', 'id as RoleId']) as $roles )
                                        <option value="{!!$roles->RoleId!!}"{!!($request->job_title == $roles->RoleId)?" Selected":"";!!}>{!!$roles->RoleType!!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Employees <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-3 col-xs-12 LR_Padd5">
                                    <select name="emp_id" id="emp_id" class="form-control col-md-3 col-xs-6" required="required">
                                        <option value="">Select Employee</option>
                                        @if($request->job_title)
                                        @foreach( App\Employee::where([ ['hotel_id', $Hotel_ID], ['role_id', $request->job_title] ])->orderBy('emp_fname', 'ASC')->get() as $employee )
                                        <option value="{!!$employee->emp_id!!}"{!!($request->emp_id == $employee->emp_id)?" Selected":"";!!}>
                                        	{!!$employee->emp_fname!!}{!!($employee->emp_mname)?" $employee->emp_mname":""!!}{!!" ".$employee->emp_lname!!}
                                       	</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Date</label>
                                <div class="col-md-2 LR_Padd5">
                                    <input type="text" name="shift_from" id="shift_from" class="form-control has-feedback-left" placeholder="From" value="">
                                    <span class="fa fa-calendar-o form-control-feedback left" style="left:5px;" aria-hidden="true"></span>
                                </div>
                                <div class="fltL control-value LR_Padd5">to</div>
                                <div class="col-md-2 LR_Padd5">
                                    <input type="text" name="shift_to" id="shift_to" class="form-control has-feedback-left" placeholder="To" value="">
                                    <span class="fa fa-calendar-o form-control-feedback left" style="left:5px;" aria-hidden="true"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Shifts <span class="required">*</span></label>
                                <div class="col-md-9 col-sm-1 col-xs-12 LR_Padd5">
                                    {{--*/$PKJ=1;/*--}}
                                    @foreach( App\Shift::where([ ['hotel_id', $Hotel_ID] ])->orderBy('shift_name', 'ASC')->get() as $shifts )
                                    	<div class="fltL control-value paddingRight10">
                                        	<input type="checkbox" name="shift_id[]" id="shift_id_{{$shifts->shift_id}}" value="{{$shifts->shift_id}}" class="js-switch"{!!($PKJ==1)?' checked':''!!} />
                                            <div class="labelSwitchCheck">{{$shifts->shift_name}}</div>
                                       	</div>
                                        {{--*/$PKJ++;/*--}}
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group form-group-last marginTop24">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">ADD</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function FilterSource(str){
		if(str){
			RedirectStr = '?';
			if(str){
				RedirectStr += "job_title="+str+"&";
			}
			RedirectStr = RedirectStr.replace(/&+$/,'');
			window.location.href="{!!url('assign-shifts')!!}"+RedirectStr;
		}else if(!str){
			window.location.href="{!!url('assign-shifts')!!}";
		}
	}
</script>
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var checkin = $('#shift_from').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		var newDate = new Date(ev.date);
		newDate.setDate(newDate.getDate() + 13);
		checkout.setValue(newDate);
		checkin.hide();
		var startVal = $('#shift_from').val();
		var endVal = $('#to').val();
		if(startVal && endVal){
			$('#reserv_night').val(daydiff(parseDate(startVal), parseDate(endVal)));
		}
		$('#shift_to')[0].focus();
	}).data('datepicker');
	
	var checkout = $('#shift_to').datepicker({
		onRender: function(date) {
			return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		checkout.hide();
		var startVal = $('#shift_from').val();
		var endVal = $('#shift_to').val();
		if(startVal && endVal){
			$('#reserv_night').val(daydiff(parseDate(startVal), parseDate(endVal)));
		}
	}).data('datepicker');
	
	function parseDate(str) {
		var mdy = str.split('/');
		return new Date(mdy[2], mdy[0]-1, mdy[1]);
	}
	
	function daydiff(first, second) {
		return Math.round((second-first)/(1000*60*60*24));
	}
	
	$('#guest_dob').datepicker({ });
	$('.availRateUp').on("keydown", function(t) {
		if(t.which == 13){
			str  = $(this).val()
			str1 = $(this).attr('data-id');
			setavailRateUpdate(str, str1);
		}
    })
	
});
</script>
@stop