<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use Helper;
use DB;

class RoleController extends Controller
{
	public function index(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		
		$Results = DB::select("SELECT * FROM roles WHERE (hotel_id = ".$Hotel_ID." OR hotel_id = 0) AND id != 1
							   ORDER BY hotel_id ASC, id ASC, name ASC");
		return view('employees.roles')->with("Results", $Results)->with("Hotel_ID", $Hotel_ID)->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function create()
    {	
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
        return view('employees.roles-create')->with('Hotel_ID', $Hotel_ID);
    }

    public function store(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		if($Hotel_ID == $request->hotel_id)
		{
			$RequestArray = array();	
			$RequestArray[] = $Hotel_ID;
			$RequestArray[] = $request->name;
			$RequestArray[] = $_SERVER['REMOTE_ADDR'];
			$RequestArray[] = $admin_ID;
			DB::select('CALL Role_Insert_SP(?,?,?,?)', $RequestArray);
			
			return redirect('job-titles')->with('success','Job Title created successfully');
		}else{
			return redirect('job-titles')->with("danger", "Please select your Motel");
		}
    }

    public function edit($id)
    {	
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$Results  = Role::where([ ['id', '=', $id], ['hotel_id', '=', $Hotel_ID] ])->first();
		if(count($Results))
		{
			if($Results->hotel_id == $Hotel_ID)
			{
				return view('employees.roles-edit')->with("Results", $Results);
			}
		}
		return redirect('job-titles')->with("danger", "Please select your Motel");
    }

    public function update(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$Results = Role::where([ ['id', $id], ['hotel_id', $Hotel_ID] ])->first();
		if(count($Results)){
			$Results->name = $request->name;
			$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
			$Results->added_by = $admin_ID;
			$Results->save();
			return redirect('job-titles')->with('success','Job Title updated successfully');
		}else{
			return redirect('job-titles')->with("danger", "Please select your Motel");
		}
		
    }
	
	public function destroy(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		if($request->hotel_id == $Hotel_ID)
		{
			Role::destroy($id);
			return redirect('job-titles')->with('success','Job Title deleted successfully');
		}
		else
		{
			return redirect('job-titles')->with('danger','Please select your Motel');
		}
	}
}
