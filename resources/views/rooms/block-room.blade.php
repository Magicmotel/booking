@extends('layouts.layout')

@section('title')
	Manage Blocked Rooms
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Blocked Rooms</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class="active"><a href="javascript:void(0);">Manage Blocked Rooms</a></li>
                <li><a href="{{url('block-room/create')}}">Create Room Block</a></li>
            </ul>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @elseif ($message = Session::get('danger'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th>Room Number</th>
                                        <th>Type</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($Results))
                                	@foreach($Results as $vals)
                                    <tr class="even pointer">
                                        <td>{!!$vals->RoomNumber!!}</td>
                                        <td>{{$vals->RoomTypes}} &nbsp; ({{($vals->SmokeType != 1)?"":"N"}}{!!$vals->BedSize!!})</td>
                                        <td>{!!$vals->BlockFrom!!}</td>
                                        <td>{!!$vals->BlockTO!!}</td>
                                        <td>
                                        	<form action="{!!url('block-room', $vals->BlockRoomId)!!}" method="post" onsubmit="return ConfirmDelete()">
                                            {!!method_field('DELETE')!!}
                                            {!!csrf_field()!!}
                                            {!!Form::hidden('hotel_id', $Hotel_ID)!!}
                                            {!!Form::button('<i class="fa fa-check-square-o"></i> UnBlock', ['class'=>'left btn btn-success buTTonResize', 'type'=>'submit'])!!}
                                            </form>
                                        </td>
                                    </tr>
                                	@endforeach
                                @else
                                	<tr>
                                        <td colspan="5" class="errorMessageTR">No Record Found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $Results->render() !!}
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function ConfirmDelete()
	{
		var x = confirm("Are you sure you want to unblock this Room?");
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop      
