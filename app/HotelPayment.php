<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelPayment extends Model
{
	protected $fillable= [
		'payment_id_unq', 'hotel_id',
		'cc_fname', 'cc_mname', 'cc_lname', 'cc_card_no', 'cc_expiry', 'cc_cvv', 'cc_address', 'cc_city', 'cc_zip',
		'ba_fname', 'ba_mname', 'ba_lname', 'ba_bankname', 'ba_account', 'ba_routing', 'ba_address', 'ba_city', 'ba_zip',
		'paypal_link',
		'trash', 'reg_ip', 'added_by'		
	];
	protected $primaryKey = 'payment_id';
	protected $table = 'hotel_payment';
}
