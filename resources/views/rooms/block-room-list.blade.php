<label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_count">Room Number <span class="required">*</span></label>
<div class="col-md-6 col-sm-3 col-xs-12 LR_Padd5">
    <table id="datatable-checkbox" class="table table-striped jambo_table bulk_action checkboxdiv">
        @foreach($Results as $RoomVal)
            <tr>
                <td><input type="checkbox" class="flat" name="room_assign_id[]" value="{{$RoomVal->RoomId}}" data-parsley-type="1" required></td>
                <td>{{$RoomVal->RoomNumber}}</td>
                <td>{{$RoomVal->RoomTypes}} &nbsp; ({{$RoomVal->RoomType}})</td>
                <td>{{$RoomVal->ExistBlock}}</td>
            </tr>
        @endforeach
        </>
    </table>
</div>

<script type="text/javascript">
 	limit = 0; //set limit
	checkboxes = document.querySelectorAll('.checkboxdiv input[type="checkbox"]'); //select all checkboxes
	function checker(elem) {
	  if (elem.checked) { //if checked, increment counter
		limit++;
	  } else {
		limit--; //else, decrement counter
	  }
	  for (i = 0; i < checkboxes.length; i++) { // loop through all 
		if (limit == '<?=$PSJ?>') {
		  if (!checkboxes[i].checked) {
			checkboxes[i].disabled = true; // and disable unchecked checkboxes
		  }
		} else { //if limit is less than two
		  if (!checkboxes[i].checked) {
			checkboxes[i].disabled = false; // enable unchecked checkboxes
		  }
		}
	  }
	}
	for (i = 0; i < checkboxes.length; i++) {
	  checkboxes[i].onclick = function() { //call function on click and send current element as param
		checker(this);
	  }
	}
 </script>