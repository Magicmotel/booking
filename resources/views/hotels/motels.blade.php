@extends('layouts.layout')

@section('title')
	View Motel Information
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>View Motel Information</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	@if (count($errors))
                <ul class="errorFormMessage">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @elseif($message = Session::get('danger'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif

        	<div class="col-md-8 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <div class="form-horizontal form-label-left">
                    	<span class="section">MOTEL INFO:</span>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">Motel Name:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_name!!}</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">Number of Rooms:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_rooms!!} Rooms</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">Website:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_website!!}</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">Email Address:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_email!!}</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">Address:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!nl2br($Results->hotel_address)!!}</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">City:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_city!!}</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">County:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_county!!}</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">State:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_state!!}</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">ZIP:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_zip!!}</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">Country:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_country!!}</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">Phone Number:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">
                            {!!$Results->hotel_phone_1!!}{!!($Results->hotel_phone_2)?", ".$Results->hotel_phone_2:""!!}{!!($Results->hotel_phone_3)?", ".$Results->hotel_phone_3:""!!}{!!($Results->hotel_phone_4)?", ".$Results->hotel_phone_4:""!!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4">Fax Number:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">
                            {!!($Results->hotel_fax_1 && $Results->hotel_fax_2)?$Results->hotel_fax_1.", ".$Results->hotel_fax_2:$Results->hotel_fax_1.$Results->hotel_fax_2!!}
                            </div>
                        </div>
                        <div class="form-group form-group-last">
                            <label class="control-label col-md-4 col-sm-4">Motel Plan:</label>
                            <div class="control-value col-md-8 col-sm-8 UpperLetter">
                            {!!$Results->hotel_plan!!} <a href="#" class="AddLink marginLeft50">Upgrade Plan</a>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
        	</div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <span class="section">OWNER INFO:</span>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4">Name:</label>
                                <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_o_lname!!}, {!!$Results->hotel_o_fname!!} {!!$Results->hotel_o_mname!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4">Phone No:</label>
                                <div class="control-value col-md-8 col-sm-8 UpperLetter">
                                {!!($Results->hotel_o_phone_1 && $Results->hotel_o_phone_2)?$Results->hotel_o_phone_1.", ".$Results->hotel_o_phone_2:$Results->hotel_o_phone_1.$Results->hotel_o_phone_2!!}
                                </div>
                            </div>
                            <div class="form-group form-group-last">
                                <label class="control-label col-md-4 col-sm-4">Email:</label>
                                <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_o_email!!}</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="x_panel">
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <span class="section">MANAGER INFO:</span>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4">Name:</label>
                                <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_m_lname!!}, {!!$Results->hotel_m_fname!!} {!!$Results->hotel_m_mname!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4">Phone No:</label>
                                <div class="control-value col-md-8 col-sm-8 UpperLetter">
                                {!!($Results->hotel_m_phone_1 && $Results->hotel_m_phone_2)?$Results->hotel_m_phone_1.", ".$Results->hotel_m_phone_2:$Results->hotel_m_phone_1.$Results->hotel_m_phone_2!!}
                                </div>
                            </div>
                            <div class="form-group form-group-last">
                                <label class="control-label col-md-4 col-sm-4">Email:</label>
                                <div class="control-value col-md-8 col-sm-8 UpperLetter">{!!$Results->hotel_m_email!!}</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="x_panel">
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <span class="section">MOTEL GALLERY:</span>
                            {{--*/ $fileFolderPath = $folderImage_Path."/motel/"; /*--}}
                            <div class="col-md-3 col-sm-6 col-xs-12 AlgnCenter">
                                @if($Results->hotel_logo)
                                    {!!Html::image($fileFolderPath.$Results->hotel_logo, 'alt', array( 'title' => 'Motel Logo', 'width' => 50, 'height' => 50 ))!!}
                                @endif
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 AlgnCenter">
                                @if($Results->hotel_pic_1)
                                    {!!Html::image($fileFolderPath.$Results->hotel_pic_1, 'alt', array( 'title' => 'Motel Image', 'width' => 50, 'height' => 50 ))!!}
                                @endif
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 AlgnCenter">
                                @if($Results->hotel_pic_2)
                                    {!!Html::image($fileFolderPath.$Results->hotel_pic_2, 'alt', array( 'title' => 'Motel Image', 'width' => 50, 'height' => 50 ))!!}
                                @endif
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 AlgnCenter">
                                @if($Results->hotel_pic_3)
                                    {!!Html::image($fileFolderPath.$Results->hotel_pic_3, 'alt', array( 'title' => 'Motel Image', 'width' => 50, 'height' => 50 ))!!}
                                @endif
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 AlgnCenter">
                                @if($Results->hotel_pic_4)
                                    {!!Html::image($fileFolderPath.$Results->hotel_pic_4, 'alt', array( 'title' => 'Motel Image', 'width' => 50, 'height' => 50 ))!!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
            
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <div class="form-horizontal form-label-left">
                    	<div class="form-group form-group-last">
                            <div class="col-md-2 col-sm-8 col-xs-12 col-md-offset-5">
                            	<a href="{!!url('motels', $Results->hotel_id)!!}/edit" class="btn btn-info buTTonResize"><i class="fa fa-pencil-square-o"></i> Edit Settings</a>
                            </div>
                    	</div>
                   	</div>
                    </div>
             	</div>
          	</div>
        </div>
    </div>
</div>
@stop