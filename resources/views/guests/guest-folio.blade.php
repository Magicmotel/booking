@extends('layouts.layout')
<? 	foreach($Results as $val){
		$outPut = $val;
	}
?>
@section('title')
	Guest Invoice
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Guest Invoice</h3></div>
            @if($outPut->RsrvSource)
                <div class="col-md-3 col-sm-3 col-xs-6 fltR zeroRightPadd">
                    <h3 class="AlgnCenter">{{$outPut->RsrvSource}}</h3>
                </div>
            @endif
        </div>
        {{--*/ $PayBalance = ($outPut->PayAmount-$outPut->PayDeposit); /*--}}
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('guest-info', $outPut->RsrvId)}}">Guest Info</a></li>
                <li><a href="{{url('payment-info', $outPut->RsrvId)}}">Payment Info</a></li>
                <li><a href="{{url('change-room', $outPut->RsrvId)}}">Room Change</a></li>
                <li class="active"><a href="javascript:void(0);">Guest Folio</a></li>
                <li><a href="{{url('change-stay', $outPut->RsrvId)}}">Change Stay</a></li>
                <li><a href="{{url('view-changes', $outPut->RsrvId)}}">View Changes</a></li>
                @if($outPut->RsrvStat == '' && $PayBalance == 0)
                <li><a href="{{url('cancel-reservation', $outPut->RsrvId)}}">Cancel Reservation</a></li>
                @endif
            </ul>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content" id="printableArea">
                    	<div class="col-md-9 col-sm-12 col-xs-12 zeroPadd">
                            <div class="col-md-12 col-sm-12 col-xs-12 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Guest Name:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->GuestLname}}, {{$outPut->GuestFname}}
                                </div>
                                @if($outPut->RsrvSource)
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    {{$outPut->RsrvSource}} Reservation#
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    {{$outPut->SourceRsrvId}}
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Company Name:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->GuestCompany}}
                                </div>                        	
                                @if($outPut->RsrvSource)
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Confirmation#
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->SourceRsrvCNFId}}
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Reservation#:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    {{$outPut->RsrvId}}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Arrival Date:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->ArvlDate}}
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Room#
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12" id="RoomAssignId_{!!$outPut->RsrvId!!}">
                                    @if($outPut->RsrvStat == 'Checked Out' || $outPut->RsrvStat == 'Cancelled')
                                        <div class="fltL" style="position:relative">{{$outPut->RoomNumber}}</div>
                                        <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                    @else
                                        @if($outPut->RoomNumber)
                                            @if($outPut->RsrvLock)
                                                <div class="fltL" style="position:relative">{{$outPut->RoomNumber}}</div>
                                                <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                                <i class="fa fa-lock lockFeaturButton cursorPointer" id="unLockButton" data-id="{!!$outPut->RsrvId!!}" aria-hidden="true"></i>
                                            @else
                                                <div class="fltL" style="position:relative">
                                                <a href="{{url('change-room', $outPut->RsrvId)}}" class="btn-list-link">{{$outPut->RoomNumber}}</a>
                                                @if($outPut->RsrvStat == "")
                                                <i class="closeButton fa fa-times" data-id="{!!$outPut->RsrvId!!}"></i>
                                                @endif
                                                </div>
                                                <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                                <i class="fa fa-unlock lockFeaturButton cursorPointer" id="LockButton" data-id="{!!$outPut->RsrvId!!}" aria-hidden="true"></i> 
                                            @endif
                                        @else
                                            <a href="{{url('change-room', $outPut->RsrvId)}}" class="fltL btn-list-link">Select Room</a>
                                            <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Departure Date:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->DprtDate}}
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Estimated Cost:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    $ {{$outPut->EstAmount}} {!!($outPut->StayNights > 1)?'<i class="fa fa-info-circle cursorPointer font16 marginLeft5" id="dayWiseRate" title="Daywise Room Charges" aria-hidden="true"></i>':''!!}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 LR_Padd0">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText LR_Padd0">
                                    Number of Nights:
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->StayNights}}
                                </div>
                            </div>
                            
                            <div class="col-md-11 col-sm-12 col-xs-12 marginTop24 LR_Padd0">
                            	@if ($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <p>{{ $message }}</p>
                                    </div>
                                @elseif ($message = Session::get('danger'))
                                    <div class="alert alert-danger">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title" style="min-width:150px">Date</th>
                                            <th class="column-title" style="min-width:150px">Charge Type</th>
                                            <th class="column-title" style="min-width:150px">Comment</th>
                                            <th class="column-title" style="min-width:100px">Amount</th>
                                        </tr>
                                    </thead>
                                
                                    <tbody>
                                        {{--*/ $amountDeposit = $amountBalance = 0; $nowTime = date("H:i"); /*--}}
                                        
                                        @if($nowTime >= '12:00')
                                            {{--*/ $tillDate = date("Y-m-d"); /*--}}
                                        @else
                                            {{--*/ $tillDate = date("Y-m-d", strtotime('-1 days')); /*--}}
                                        @endif
                                        {{--*/
                                        $paymentModeArray = array("Cash Payment"=>"success", "CC Payment"=>"success", "CC Payment Auth Only"=>"warning",
                                                                  "Check Payment"=>"success", "Company Payment"=>"warning");
                                        $rsrvQry = DB::select("SELECT * FROM room_reservation_folio_meta
                                                               WHERE room_reservation_id = ".$outPut->RsrvId." AND hotel_id = ".$Hotel_ID."
                                                               ORDER BY folio_date ASC, folio_type, folio_id ASC
                                                              ");
                                        /*--}}
                                        @foreach($rsrvQry as $vals)
                                            @if($vals->folio_order == 0 && $vals->folio_type == 0)
                                            <tr class="even pointer paymentModeColor_{{$paymentModeArray[$vals->folio_pay_type]}}">
                                                <td class=" ">{!!date('m/d/Y h:i A', strtotime($vals->created_at))!!}</td>
                                                <td class=" ">{!!$vals->folio_pay_type!!} <i class="fa fa-info-circle cursorPointer font16 marginLeft5" id="transactionDetail" title="Transaction Detail" data-id="{{$vals->log_id}}" aria-hidden="true"></i></td>
                                                <td class=" ">{!!date('m/d/Y', strtotime($vals->folio_date))!!} {!!$vals->folio_comment!!}</td>
                                                <td class=" last">( ${!!$vals->folio_amount!!} )</td>
                                            </tr>
                                            @elseif($vals->folio_type == 2)
                                            <tr class="even pointer paymentModeColor_adjustment">
                                                <td class=" ">{!!date('m/d/Y h:i A', strtotime($vals->created_at))!!}</td>
                                                <td class=" ">{!!$vals->folio_pay_type!!} <i class="fa fa-info-circle cursorPointer font16 marginLeft5" id="transactionDetail" title="Transaction Detail" data-id="{{$vals->log_id}}" aria-hidden="true"></i></td>
                                                <td class=" ">{!!date('m/d/Y', strtotime($vals->folio_date))!!} {!!$vals->folio_comment!!}</td>
                                                <td class=" last">- ${!!$vals->folio_amount!!}</td>
                                            </tr>
                                            @else
                                            <tr class="even pointer{!!($vals->folio_type == 2)?' paymentModeColor_adjustment':''!!}">
                                                <td class=" ">{!!date('m/d/Y h:i A', strtotime($vals->created_at))!!}</td>
                                                <td class=" ">{!!$vals->folio_pay_type!!}</td>
                                                <td class=" ">{!!date('m/d/Y', strtotime($vals->folio_date))!!} {!!$vals->folio_comment!!}</td>
                                                <td class=" last">{{($vals->folio_pay_type == 'Discount')?'- ':''}}${!!$vals->folio_amount!!}</td>
                                            </tr>
                                            @endif
                                            @if($vals->folio_order == 0 && $vals->folio_type == 0)
                                            {{--*/ $amountDeposit = $amountDeposit+$vals->folio_amount; /*--}}
                                            @elseif($vals->folio_order == 0 && $vals->folio_type == 2)
                                            {{--*/ $amountBalance = $amountBalance+$vals->folio_amount; /*--}}
                                            @elseif($vals->folio_type == 2)
                                            {{--*/ $amountDeposit = $amountDeposit+$vals->folio_amount; /*--}}
                                            @else
                                            {{--*/ $amountBalance = $amountBalance+$vals->folio_amount; /*--}}
                                            @endif
                                        @endforeach
                                        {{--*/ $remainBalance = $amountBalance-$amountDeposit; /*--}}
                                        <tr class="tableFooter">
                                            <td class=" ">Balance</td>
                                            <td class=" "></td>
                                            <td class=" "></td>
                                            <td class=" last">{!!($remainBalance < 0)?'- $'.($remainBalance*-1):'$'.$remainBalance!!}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                @if($remainBalance>0)
                                <div class="form-group form-group-last marginTop10">
                                    <div class="col-md-12 col-sm-12 col-xs-12 AlgnRight">
                                        <a href="{!!url('payment-pay', $outPut->RsrvId)!!}" class="btn btn-info"><i class="fa fa-check-circle-o"></i> Pay Now</a>
                                    </div>
                                </div>
                                @elseif($remainBalance<0)
                                <div class="form-group form-group-last marginTop10">
                                    <div class="col-md-12 col-sm-12 col-xs-12 AlgnRight">
                                        <a href="{!!url('payment-info', $outPut->RsrvId)!!}" class="btn btn-warning"><i class="fa fa-adjust"></i> Payment Reversal</a>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        
                        <div class="col-md-3 col-sm-12 col-xs-12 marginTop190 LR_Padd0">
                        	<div class="col-md-12 col-sm-12 col-xs-12 LR_Padd0 marginBottom10">
                            	<a href="{{url('guest-folio/post-charge', $outPut->RsrvId)}}" class="btn btn-success"><i class="fa fa-money"></i> Post Charges</a>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 LR_Padd0 marginBottom10">
                            	<a href="{{url('guest-folio/post-adjustment', $outPut->RsrvId)}}" class="btn btn-warning"><i class="fa fa-adjust"></i> Post Adjustment</a>
                           	</div>
                            <div class="col-md-12 col-sm-12 col-xs-12 LR_Padd0 marginBottom10">
                            	<a href="{!!url('payment-pay', $outPut->RsrvId)!!}" class="btn btn-success"><i class="fa fa-money"></i> Pre Payment</a>
                           	</div>
                            <div class="col-md-12 col-sm-12 col-xs-12 LR_Padd0 marginBottom10">
                            	<a href="{{url('folio-print', $outPut->RsrvId)}}" class="btn btn-primary"><i class="fa fa-print"></i> Print Guest Folio</a>
                           	</div>
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<!-- checkedOutModel -->
<div id="checkedOutModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="model_RsrvId"></h4>
            </div>
            <div class="modal-footer AlgnLeft" id="modal-matter"></div>
        </div>
    </div>
</div>
<div id="fc_create" data-toggle="modal" data-target="#checkedOutModel"></div>

<!-- transactionDetailPayment -->
<div id="transactionDetailPayment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:400px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="transactionDetail_Title"></h4>
            </div>
            <div class="modal-footer AlgnLeft" id="transactionDetail_Content"></div>
        </div>
    </div>
</div>
<div id="popup_transactionDetailPayment" data-toggle="modal" data-target="#transactionDetailPayment"></div>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!}
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	$(document).on('click', '#dayWiseRate', function(e){
		var rsrvId = '{{$outPut->RsrvId}}';
		$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		$.ajax({
			url: "{{ url('/dayWiseRatePlan') }}",
			type: 'POST',
			data:{"rsrvId":rsrvId, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				if ( msg.status === 'success' ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					$("#model_RsrvId").html("Reservation ID: "+rsrvId);
					$("#modal-matter").html(msg.response);
					$('#fc_create').click();
				}
				if ( msg.status === 'error' ) {
					toastr.error( msg.response );
				}
			},
			error: function( data ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				if ( data.status === 422 ) {
					toastr.error('Try Again.');
				}
			}
		});
		return false;
	});
	
	$(document).on('click', '#transactionDetail', function(e){
		var rsrvId = '{{$outPut->RsrvId}}';
		var logId  = $(this).attr('data-id');
		$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		$.ajax({
			url: "{{ url('/transactionDetailPayment') }}",
			type: 'POST',
			data:{"rsrvId":rsrvId, "logId":logId, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				if ( msg.status === 'success' ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					$("#transactionDetail_Title").html("Transaction ID: "+msg.transId);
					$("#transactionDetail_Content").html(msg.response);
					$('#popup_transactionDetailPayment').click();
				}
				if ( msg.status === 'error' ) {
					toastr.error( msg.response );
				}
			},
			error: function( data ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				if ( data.status === 422 ) {
					toastr.error('Try Again.');
				}
			}
		});
		return false;
	});
	
	$(document).on('click', '.lockFeaturButton', function(e){
		var selfId = $(this).attr('id');
		if(selfId == 'LockButton'){
			var C_selfId = 'unLockButton';
			var rsrvLock = 1;
			var Msg = "Are you sure you want to Lock this Room Number?";
		}else{
			var C_selfId = 'LockButton';
			var rsrvLock = 0;
			var Msg = "Are you sure you want to Unlock this Room Number?";
		}
		var x = confirm(Msg);
		if (x){
			var rsrvId = $(this).attr('data-id');
			$("#main_container_Loading").show();
			$("#main_container_overlay").show();
			$.ajax({
				url: "{{ url('/lockAssignRoomNumber') }}",
				type: 'POST',
				data:{"rsrvId":rsrvId, "rsrvLock":rsrvLock, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( msg.status === 'success' ) {
						$('#RoomAssignId_'+rsrvId).html(msg.content);
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( data.status === 422 ) {
						toastr.error('Cannot handle Request.');
					}
				}
			});
			return true;
		}else{
			return false;
		}
	});
});
</script>
@stop
