@extends('layouts.layout')

@section('title')
	Manage Room Rate
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Room Rate & Availability</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class='{!!(!$request->type)?"active":""!!}'><a href="{{url('room/room-rate')}}">Room Availability</a></li>
                <li class='{!!($request->type)?"active":""!!}'><a href="{{url('room/room-rate?type=room_price')}}">Change Room Rate</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
            	{{--*/ $itemSchema = array(); /*--}} 
            	@if($request->type)
                	<div class="x_panel borderTopNone">
                    {{--*/ $roomRst = DB::select("SELECT RT.room_type_id as RoomTypeId, RT.room_type as RoomType,
                                                  CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomCode
                                                  FROM roomtypes as RT
                                                  WHERE RT.hotel_id = ".$Hotel_ID." AND RT.trash = 0
                                                  ORDER BY RT.room_type ASC");
                    /*--}}
                    @if(count($roomRst))
                    	<div class="x_content">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <form id="demo-form2" action="{!!url('room/room-rate')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                                {!!csrf_field()!!}
                                <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                                <div class="form-group">
                                    <label for="room_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Room Type <span class="required">*</span></label>
                                    <div class="col-md-3 col-sm-3 col-xs-12 LR_Padd5">
                                    	<select name="room_type_id" id="room_type_id" class="form-control col-md-3 col-xs-6 LR_Padd5 uPPerLetter" required="required" onchange="FilterSource(this.value, '{!!$request->type!!}')">
                                            <option value="">Select Room Type</option>
                                            @foreach( $roomRst as $val )
                                            <option value="{!!$val->RoomTypeId!!}" class="uPPerLetter"{!!($request->RoomTypeSelectedId == $val->RoomTypeId)?" Selected":""!!}>{!!$val->RoomType!!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="room_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Date</label>
                                    <div class="col-md-2 LR_Padd5">
                                        <input type="text" name="room_rate_from" id="room_rate_from" class="form-control has-feedback-left" placeholder="From" value="" readonly="readonly">
                                        <span class="fa fa-calendar-o form-control-feedback left" style="left:5px;" aria-hidden="true"></span>
                                    </div>
                                    <div class="fltL control-value LR_Padd5">to</div>
                                    <div class="col-md-2 LR_Padd5">
                                        <input type="text" name="room_rate_to" id="room_rate_to" class="form-control has-feedback-left" placeholder="To" value="" readonly="readonly">
                                        <span class="fa fa-calendar-o form-control-feedback left" style="left:5px;" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12 " for="room_type">Room Rate <span class="required">*</span></label>
                                    <div class="col-md-4 col-sm-1 col-xs-12 LR_Padd5">
                                        <span class="input-group-btn fltL widthAuto"><div class="btn btn-primary cursorInitial">$</div></span>
                                        <input type="text" name="room_rate" id="room_rate" value="" placeholder="Room Rate" class="form-control width30 LR_Padd5" required="required" min="15" max="999.99" maxlength="6">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12 " for="room_type">Days <span class="required">*</span></label>
                                    <div class="col-md-9 col-sm-1 col-xs-12 LR_Padd5">
                              			<div class="fltL control-value paddingRight5"><input type="checkbox" name="room_rate_mon" id="room_rate_mon" value="1" class="js-switch" checked="checked" />  <div class="labelSwitchCheck">Monday</div></div>
                                        <div class="fltL control-value LR_Padd5"><input type="checkbox" name="room_rate_tue" id="room_rate_tue" value="1" class="js-switch" checked="checked" /> <div class="labelSwitchCheck">Tuesday</div></div>
                                        <div class="fltL control-value LR_Padd5"><input type="checkbox" name="room_rate_wed" id="room_rate_wed" value="1" class="js-switch" checked="checked" />  <div class="labelSwitchCheck">Wednesday</div></div>
                                        <div class="fltL control-value LR_Padd5"><input type="checkbox" name="room_rate_thu" id="room_rate_thu" value="1" class="js-switch" checked="checked" />  <div class="labelSwitchCheck">Thursday</div></div>
                                        <div class="fltL control-value LR_Padd5"><input type="checkbox" name="room_rate_fri" id="room_rate_fri" value="1" class="js-switch" checked="checked" />  <div class="labelSwitchCheck">Friday</div></div>
                                        <div class="fltL control-value LR_Padd5"><input type="checkbox" name="room_rate_sat" id="room_rate_sat" value="1" class="js-switch" checked="checked" />  <div class="labelSwitchCheck">Saturday</div></div>
                                        <div class="fltL control-value LR_Padd5"><input type="checkbox" name="room_rate_sun" id="room_rate_sun" value="1" class="js-switch" checked="checked" />  <div class="labelSwitchCheck">Sunday</div></div>
                                 	</div>
                                </div>
                                <div class="form-group form-group-last marginTop20">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Change Room Rate</button>
                                		<button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                   	@else
                      	<div class="errorMessageTR">No Room Type Found</div>
                    @endif  
                    </div>
                    @if($request->RoomTypeSelectedId)
                    <div class="x_panel">
                        <div class="x_content">
                        {{--*/	$nowDay = date('Y-m-d');
                        	$minMaxSql= DB::select("SELECT min(CAST(room_rate_price as DECIMAL(10))) as minRate, max(CAST(room_rate_price as DECIMAL(10))) as maxRate
                                                    FROM room_rate_option
                                                    WHERE hotel_id = ".$Hotel_ID." AND room_type_id = ".$request->RoomTypeSelectedId." AND room_rate_date >= '".$nowDay."'
                                                    ORDER BY room_rate_date ASC
                                                   ");
                            $Sql = DB::select("SELECT room_rate_date as RoomRate_Date, room_rate_price as RoomRate_Price
                                                FROM room_rate_option
                                                WHERE hotel_id = ".$Hotel_ID." AND room_type_id = ".$request->RoomTypeSelectedId." AND room_rate_date >= '".$nowDay."'
                                                ORDER BY room_rate_date ASC
                                               ");
                        /*--}}
                        @foreach($minMaxSql as $minMaxRst)
                            {{--*/ $minPrice = $minMaxRst->minRate; $maxPrice = $minMaxRst->maxRate; /*--}}
                        @endforeach
                        
                        @if(count($Sql))
                        	<div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                            	<div id='calendar'></div>
                            </div>
                            @foreach($Sql as $Rst)
                            	@if($Rst->RoomRate_Price == $minPrice)
                            		{{--*/ $itemSchema[] = array('title' => '$ '.$Rst->RoomRate_Price, 'start' => $Rst->RoomRate_Date, 'color'=> '#26B99A', 'textColor'=> 'white' ); /*--}}
                               	@elseif($Rst->RoomRate_Price == $maxPrice)
                                	{{--*/ $itemSchema[] = array('title' => '$ '.$Rst->RoomRate_Price, 'start' => $Rst->RoomRate_Date, 'color'=> '#EC4444', 'textColor'=> 'white' ); /*--}}
                                @else
                                	{{--*/ $itemSchema[] = array('title' => '$ '.$Rst->RoomRate_Price, 'start' => $Rst->RoomRate_Date, 'color'=> '#337AB7', 'textColor'=> 'white' ); /*--}}
                                @endif
                            @endforeach
                        @else
                            <div class="col-md-12 col-sm-12 col-xs-12">No Rate Available</div>
                        @endif
                        </div>
                    </div>
                    @endif
                @else
                	<div class="x_panel borderTopNone">
                    @if($request->room_rate_from && $request->room_rate_to)
                        {{--*/
                        $room_rate_from = date('Y-m-d', strtotime($request->room_rate_from));
                        $room_rate_to   = date('Y-m-d', strtotime($request->room_rate_to));
                        /*--}}
                    @elseif(!$request->room_rate_from && $request->room_rate_to)
                        {{--*/
                        $room_rate_from = date('Y-m-d');
                        $room_rate_to   = date('Y-m-d', strtotime($request->room_rate_to));
                        /*--}}
                    @elseif($request->room_rate_from && !$request->room_rate_to)
                        {{--*/
                        $room_rate_from = date('Y-m-d', strtotime($request->room_rate_from));
                        $room_rate_to   = $room_rate_from;
                        /*--}}
                    @else
                        {{--*/
                        $room_rate_from = date('Y-m-d');
                        $room_rate_to   = date('Y-m-d', strtotime($room_rate_from.'+13 days'));
                        /*--}}
                    @endif
                    {{--*/ $RT_Sql = DB::select("SELECT DISTINCT(CONCAT(IF(room_smoking_type != 1, '', 'N'), room_bed_size)) as BedSize,
                                                 room_smoking_type, room_bed_size
                                                 FROM roomtypes WHERE hotel_id = ".$Hotel_ID." AND trash = 0 ORDER BY BedSize ASC");
                    /*--}}
                    @if(count($RT_Sql))
                        <div class="x_content">
                        	<form id="demo-form2" action="{!!url('room/room-rate')!!}" method="get" data-parsley-validate class="form-horizontal form-label-left">
                                <div class="form-group">
                                    <label for="room_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Room Type</label>
                                    <div class="col-md-9 col-sm-3 col-xs-12 LR_Padd5">
                                    {{--*/	
                                        if(isset($request->room_fltr)){
                                        	$requestRoom_fltr = $request->room_fltr;
                                      	}else{
                                        	$requestRoom_fltr = array();
                                        }
                                        $RoomTypeVars = $nextPageVars = "";
                                        $RoomTypeArrayVars = array();
                                  	/*--}}    
                                    @foreach($RT_Sql  as $val )
                                        @if(in_array($val->BedSize, $requestRoom_fltr))
                                            {{--*/
                                                $flag = " Checked";
                                                $RoomTypeArrayVars[] = "(room_smoking_type = ".$val->room_smoking_type." AND room_bed_size = '".$val->room_bed_size."' )";
                                                $nextPageVars .= "&room_fltr[]=".$val->BedSize;
                                            /*--}}
                                        @else
                                            {{--*/$flag = "";/*--}}
                                        @endif
                                        <div class="fltL control-value marginRight10">
                                        <input type="checkbox" name="room_fltr[]" id="room_fltr_{!!$val->BedSize!!}" class="flat" value="{!!$val->BedSize!!}"{!!$flag!!}><div class="labelSwitchCheck">{!!$val->BedSize!!}</div>
                                        </div>
                                    @endforeach
                                    </div>
                                </div>
                                {{--*/	$RoomTypeVars = implode(" OR ", $RoomTypeArrayVars); /*--}}
                                @if($RoomTypeVars)
                                	{{--*/ 	$RoomTypeVars = " AND ($RoomTypeVars)"; /*--}}
                                @endif
                                <div class="form-group">
                                    <label for="room_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Date Range</label>
                                    <div class="col-md-2 LR_Padd5">
                                        <input type="text" name="room_rate_from" id="room_rate_from" value="{{$request->room_rate_from}}" class="form-control has-feedback-left" placeholder="From" readonly="readonly">
                                        <span class="fa fa-calendar-o form-control-feedback left" style="left:5px;" aria-hidden="true"></span>
                                    </div>
                                    <div class="fltL control-value LR_Padd5">to</div>
                                    <div class="col-md-2 LR_Padd5">
                                        <input type="text" name="room_rate_to" id="room_rate_to" value="{{$request->room_rate_to}}" class="form-control has-feedback-left" placeholder="To" readonly="readonly">
                                        <span class="fa fa-calendar-o form-control-feedback left" style="left:5px;" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="form-group form-group-last marginTop20">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 LR_Padd5">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                                		<button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                                    </div>
                                </div>
                            </form>
                       	</div>
                        {{--*/
                        	$exactSearch_From = $room_rate_from;
                            $exactSearch_To   = $room_rate_to;
                            $exactSearch_Pg   = ($request->level)?$request->level:1;
                            
                            $Tot_days = ($exactSearch_Pg-1)*14;
                            
                            $carryDate_From = date('Y-m-d', strtotime($exactSearch_From.'+'.$Tot_days.' days'));
                            $carryDate_To   = date('Y-m-d', strtotime($carryDate_From.'+13 days'));
                       	/*--}}
                        @if($carryDate_To > $exactSearch_To)
                            {{--*/ $carryDate_To = $exactSearch_To; /*--}}
                        @endif
                     	{{--*/    
                        	$room_rate_option_from = $carryDate_From;
                            $room_rate_option_to   = $carryDate_To;
                            $date1 = date_create($carryDate_From);
                            $date2 = date_create($carryDate_To); 
                            $diff = date_diff($date1, $date2);
                            $dayNum = $diff->format("%a");
                        /*--}}
                        <div class="x_content">
                        	<div class="col-md-8 col-sm-12 col-xs-12 marginTop24 TB_Padd10 AlgnLeft">
                            	<b><i class="fa fa-calendar-o"></i> {{date('m/d/Y', strtotime($carryDate_From))." - ".date('m/d/Y', strtotime($carryDate_To))}}</b>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 TB_Padd10 AlgnRight">
                            	<div class="rateAvail-button-group">
                                	@if($carryDate_From > $exactSearch_From)
                                        <a href="{!!url('room/room-rate?room_rate_from=')!!}{!!$request->room_rate_from!!}&room_rate_to={!!$request->room_rate_to!!}&level={!!$exactSearch_Pg-1!!}{{$nextPageVars}}" class="rateAvail-default fc-corner-left" title="Previous">
                                            <span class="rateAvail-left-single-arrow"></span>
                                        </a>
                                    @endif
                                    @if($dayNum==13 && $carryDate_To < $exactSearch_To)
                                    <a href="{!!url('room/room-rate?room_rate_from=')!!}{!!$request->room_rate_from!!}&room_rate_to={!!$request->room_rate_to!!}&level={!!$exactSearch_Pg+1!!}{{$nextPageVars}}" class="rateAvail-default fc-corner-right" title="Next">
                                    	<span class="rateAvail-right-single-arrow"></span>
                                   	</a>
                                    @endif
                              	</div>
                            </div>
                          	<table class="table table-bordered">
                              	<thead>
                              	<tr>
                                	<th></th>
                                  	{{--*/ $room_rate_option_from = date('Y-m-d', strtotime($carryDate_From)); /*--}}
                                    @for($i = 1; $i <= $dayNum+1; $i++)
                                  		{{--*/ $dayName = date('D', strtotime($room_rate_option_from)); /*--}}
                                        {{--*/ $dayFormat = date('d M', strtotime($room_rate_option_from)); /*--}}
                                    <th class="{{$room_rate_option_from}}" style="text-align:center;{!!($dayName=='Sat' || $dayName=='Sun')?' background-color:#E9E9E9;':'';!!}">{{$dayName}}<br />{{$dayFormat}}</th>
                                 		{{--*/ $room_rate_option_from = date('Y-m-d', strtotime($room_rate_option_from.'+1 days')); /*--}}
                                  	@endfor
                                </tr>
                                </thead>
                                @foreach(DB::select("SELECT room_display as RoomDisplay, room_type as RoomType, room_type_id as RoomTypeId, room_count as RoomAvail,
                                					 CONCAT(IF(room_smoking_type != 1, '', 'N'), room_bed_size) as RoomTypes
                                					 FROM roomtypes WHERE hotel_id = ".$Hotel_ID.$RoomTypeVars." ORDER BY room_type ASC
                                                    ") as $valRst)
                                <tbody>
                                <tr style="color:#EEA236; background-color:#F9F9F9">
                                    <th scope="row">{{($valRst->RoomType)?$valRst->RoomType:$valRst->RoomTypes}}</th>
                                    {{--*/ $room_rate_option_from = date('Y-m-d', strtotime($carryDate_From)); /*--}}
                                    @for($i = 1; $i <= $dayNum+1; $i++)
                                  		{{--*/ $dayName = date('D', strtotime($room_rate_option_from)); /*--}}
                                    <th class="{{$room_rate_option_from}}" style="text-align:center;{!!($dayName=='Sat' || $dayName=='Sun')?' background-color:#E9E9E9;':'';!!}">Rate</th>
                                 		{{--*/ $room_rate_option_from = date('Y-m-d', strtotime($room_rate_option_from.'+1 days')); /*--}}
                                  	@endfor
                                </tr>
                                <tr>
                                    <th scope="row"></th>
                                    {{--*/
                                    $Sql = DB::select("SELECT RR.room_rate_price as RoomRate_Price, RR.room_rate_option_id as RoomRate_ID, RR.room_rate_date as RoomRate_Date
                                                        FROM roomtypes as RT INNER JOIN room_rate_option as RR ON RR.room_type_id = RT.room_type_id
                                                        WHERE RT.hotel_id = ".$Hotel_ID." AND RR.hotel_id = ".$Hotel_ID." AND RR.room_type_id = ".$valRst->RoomTypeId."
                                                        AND (RR.room_rate_date BETWEEN '".$carryDate_From."' AND '".$carryDate_To."')  
                                                        ORDER BY RR.room_rate_date ASC
                                                       ");
                                    /*--}}
                                    @foreach($Sql as $Rst)
                                        <th class="{{$Rst->RoomRate_Date}} roomAvailRate zeroPadd">
                                        	<span class="padding8 AlgnCenter col-md-12 col-xs-12" id="availRateDown_{{$Rst->RoomRate_ID}}" onclick="getavailRateUpdate('{{$Rst->RoomRate_ID}}')">$ {{$Rst->RoomRate_Price}}</span>
                                            
                                            <input type="text" id="availRateUp_{{$Rst->RoomRate_ID}}" value="{{$Rst->RoomRate_Price}}"  class="form-control col-md-7 col-xs-12 displayNone availRateUp" data-id="{{$Rst->RoomRate_ID}}" min="15" max="999.99" maxlength="6"/></th>
                                    @endforeach
                               	</tr>
                                <tr>
                                    <th scope="row"></th>
                                    {{--*/ $room_rate_option_from = date('Y-m-d', strtotime($carryDate_From)); /*--}}
                                    @for($i = 1; $i <= $dayNum+1; $i++)
                                  		{{--*/ $dayName = date('D', strtotime($room_rate_option_from)); /*--}}
                                    <td class="{{$room_rate_option_from}} font11" style="text-align:center;{!!($dayName=='Sat' || $dayName=='Sun')?' background-color:#E9E9E9;':'';!!}">Availability</td>
                                 		{{--*/ $room_rate_option_from = date('Y-m-d', strtotime($room_rate_option_from.'+1 days')); /*--}}
                                  	@endfor
                                </tr>
                                <tr>
                                    <th scope="row"></th>
                                    {{--*/ $room_rate_option_from = date('Y-m-d', strtotime($carryDate_From)); /*--}}
                                    @for($i = 1; $i <= $dayNum+1; $i++)
                                  		{{--*/ $dayName = date('D', strtotime($room_rate_option_from)); /*--}}
                                        {{--*/
                                        $availSql = DB::select("SELECT
                                                                (	RT.room_count -
                                                                    ( IF(	(SELECT SUM(RSV.qty_reserve) FROM room_reservation as RSV
                                                                             WHERE RSV.room_type_id = RT.room_type_id AND RSV.status != 'Cancelled' AND
                                                                             (RSV.arrival <= '".$room_rate_option_from."' AND RSV.departure > '".$room_rate_option_from."')
                                                                             GROUP BY RSV.room_type_id
                                                                            ) != 0, 
                                                                            (SELECT SUM(RSV.qty_reserve) FROM room_reservation as RSV
                                                                             WHERE RSV.room_type_id = RT.room_type_id AND RSV.status != 'Cancelled' AND
                                                                             (RSV.arrival <= '".$room_rate_option_from."' AND RSV.departure > '".$room_rate_option_from."')
                                                                             GROUP BY RSV.room_type_id
                                                                            ), 0              
                                                                         )
                                                                    ) - 
                                                                    ( IF(	(SELECT count(R.room_type_id) FROM room_assign as R
                                                                             WHERE R.room_type_id = RT.room_type_id AND (R.status = 0 OR R.trash = 1)
                                                                             GROUP BY R.room_type_id
                                                                            ) != 0, 
                                                                            (SELECT count(R.room_type_id) FROM room_assign as R
                                                                             WHERE R.room_type_id = RT.room_type_id AND (R.status = 0 OR R.trash = 1)
                                                                             GROUP BY R.room_type_id
                                                                            ), 0              
                                                                         )
                                                                    ) - 
                                                                    ( IF(
                                                                            (SELECT count(DISTINCT(BR.room_assign_id)) FROM block_rooms as BR
                                                                             WHERE BR.room_type_id = RT.room_type_id AND
                                                                             (BR.block_from <= '".$room_rate_option_from."' AND BR.block_to > '".$room_rate_option_from."')
                                                                             GROUP BY BR.room_type_id
                                                                            ) != 0, 
                                                                            (SELECT count(DISTINCT(BR.room_assign_id)) FROM block_rooms as BR
                                                                             WHERE BR.room_type_id = RT.room_type_id AND
                                                                             (BR.block_from <= '".$room_rate_option_from."' AND BR.block_to > '".$room_rate_option_from."')
                                                                             GROUP BY BR.room_type_id
                                                                            ), 0              
                                                                         )
                                                                    )
                                                                ) as availRooms FROM roomtypes as RT
                                                                WHERE RT.hotel_id = ".$Hotel_ID." AND RT.room_type_id = ".$valRst->RoomTypeId."
                                                             ");					
                                        /*--}}
                                        @foreach($availSql as $availRst)
                                       		{{--*/ $availRooms = $availRst->availRooms; /*--}}
                                       	@endforeach
                                    <td class="AlgnCenter {{$room_rate_option_from}}{{($availRooms==0)?' RB_WF':''}}{!!($dayName=='Sat' || $dayName=='Sun')?' WeekEnd':'';!!}">{{$availRooms}}</td>
                                    	{{--*/ $room_rate_option_from = date('Y-m-d', strtotime($room_rate_option_from.'+1 days')); /*--}}
                                  	@endfor
                                </tr>
                                </tbody>
                               	@endforeach
                            </table>                        
                    	</div>
                   	@else
                      	<div class="errorMessageTR">No Room Type Found</div>
                    @endif
                	</div>
                @endif 
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function FilterSource(str, str1){
		if(str || str1){
			RedirectStr = '?';
			if(str1){
				RedirectStr += "type="+str1+"&";
			}
			if(str){
				RedirectStr += "RoomTypeSelectedId="+str+"&";
			}
			RedirectStr = RedirectStr.replace(/&+$/,'');
			window.location.href="{!!url('room/room-rate')!!}"+RedirectStr;
		}else if(!str && !str1){
			window.location.href="{!!url('room/room-rate')!!}";
		}
	}
	function getavailRateUpdate(str){
		$("#availRateDown_"+str).hide();
		$("#availRateUp_"+str).show();
		$("#availRateUp_"+str).focus();
	}
	function setavailRateUpdate(str, str1){
		$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		if(str>=15){
			$.get("{{ url('/room/setavailRate') }}"+"?room_rate_price="+str+"&room_rate_option_id="+str1, function(msg){ 
				$("#availRateUp_"+str1).hide();
				$("#availRateDown_"+str1).html("$ "+str);
				$("#availRateDown_"+str1).show();
				if ( msg.status === 'success' ) {
					//console.log(msg.response);
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.success( msg.response );
				}
				if ( msg.status === 'error' ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.error( msg.response );
				}
			});	
		}else{
			$("#main_container_Loading").hide();
			$("#main_container_overlay").hide();
			toastr.error( "Please add valid Room Rate<br> Start from $15");
		}	
	}
</script>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
    {!!Html::script('vendors/fullcalendar/dist/fullcalendar.min.js')!!}
@stop  

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var checkin = $('#room_rate_from').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		var newDate = new Date(ev.date);
		newDate.setDate(newDate.getDate() + 13);
		checkout.setValue(newDate);
		checkin.hide();
		var startVal = $('#room_rate_from').val();
		var endVal = $('#to').val();
		if(startVal && endVal){
			$('#reserv_night').val(daydiff(parseDate(startVal), parseDate(endVal)));
		}
		$('#room_rate_to')[0].focus();
	}).data('datepicker');
	
	var checkout = $('#room_rate_to').datepicker({
		onRender: function(date) {
			return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		checkout.hide();
		var startVal = $('#room_rate_from').val();
		var endVal = $('#room_rate_to').val();
		if(startVal && endVal){
			$('#reserv_night').val(daydiff(parseDate(startVal), parseDate(endVal)));
		}
	}).data('datepicker');
	
	function parseDate(str) {
		var mdy = str.split('/');
		return new Date(mdy[2], mdy[0]-1, mdy[1]);
	}
	
	function daydiff(first, second) {
		return Math.round((second-first)/(1000*60*60*24));
	}
	
	$('#guest_dob').datepicker({ });
	
	$(".availRateUp").on("blur",function (event) {
		if(!$(this).hasClass('keyupping')){
			str  = $(this).val()
			str1 = $(this).attr('data-id');
			setavailRateUpdate(str, str1);
		}
	});
	
	$(".availRateUp").on("keyup",function (event) {
		$(this).addClass('keyupping');
		if(event.keyCode == 13){ // Detect Enter
			str  = $(this).val()
			str1 = $(this).attr('data-id');
			setavailRateUpdate(str, str1);
		}else{
			$(this).removeClass('keyupping');
		}
	});
});

$(window).load(function() {
	var date = new Date(),
		d = date.getDate(),
		m = date.getMonth(),
		y = date.getFullYear(),
		started,
		categoryClass;

	var calendar = $('#calendar').fullCalendar({
		header: {
			left: 'today',
			center: 'title',
			right: 'prev,next'
		},
		height: 490,
		firstDay:1,
		selectable: true,
		selectHelper: false,
		editable: false,
		events: <?=json_encode($itemSchema);?>,
		eventRender: function (event, element) {
						var eventDate = event.start;
						var calendarDate = $('#calendar').fullCalendar('getDate');
						if (eventDate.get('month') !== calendarDate.get('month')) {
							return false;
						}
					},
		viewRender: function(currentView){
						var minDate = moment();
						if(minDate >= currentView.start && minDate <= currentView.end){
							$(".fc-prev-button").prop('disabled', true); 
							$(".fc-prev-button").addClass('fc-state-disabled'); 
						}
						else{
							$(".fc-prev-button").removeClass('fc-state-disabled'); 
							$(".fc-prev-button").prop('disabled', false); 
						}		
					}
	});
});
</script>
@stop