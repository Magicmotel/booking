<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Roomtype;
use App\RoomAssign;
use App\RoomReservation;
use App\ReservationRoomAllot;
use App\ReservationRoomAllotPast;
use Helper;
use Auth;
use DB;

class RoomAssignController extends Controller
{
    public function index(Request $request)
    {
		$Hotel_ID = Helper::getHotelId();
		
		if($request->RoomAssignId == 2){
			$RoomAssignStatus = 0;
		}else{
			$RoomAssignStatus = $request->RoomAssignId;
		}
		
		if($request->RoomTypeSelectedId && $request->RoomAssignId)
		{
			$Results = DB::table('room_assign as RA')
						->select('RT.room_type_id as RoomTypeId', 'RT.room_type as RoomType', 'RT.room_smoking_type as SmokeType', 'RT.room_bed_size as BedSize',
								 'RA.room_number as RoomNumber', 'RA.room_assign_id as RoomId', 'RA.status as RoomStatus')
						->join('roomtypes as RT', 'RT.room_type_id', '=', 'RA.room_type_id')
						->where([ ['RT.hotel_id', $Hotel_ID], ['RT.room_type_id', $request->RoomTypeSelectedId], ['RA.status', $RoomAssignStatus], ['RA.trash', 0] ])
						->orderBy('RA.room_number', 'ASC')
						->paginate(10);
		}
		elseif($request->RoomTypeSelectedId)
		{
			$Results = DB::table('room_assign as RA')
						->select('RT.room_type_id as RoomTypeId', 'RT.room_type as RoomType', 'RT.room_smoking_type as SmokeType', 'RT.room_bed_size as BedSize',
								 'RA.room_number as RoomNumber', 'RA.room_assign_id as RoomId', 'RA.status as RoomStatus')
						->join('roomtypes as RT', 'RT.room_type_id', '=', 'RA.room_type_id')
						->where([ ['RT.hotel_id', $Hotel_ID], ['RT.room_type_id', $request->RoomTypeSelectedId], ['RA.trash', 0] ])
						->orderBy('RA.room_number', 'ASC')
						->paginate(10);
		}
		elseif($request->RoomAssignId)
		{
			$Results = DB::table('room_assign as RA')
						->select('RT.room_type_id as RoomTypeId', 'RT.room_type as RoomType', 'RT.room_smoking_type as SmokeType', 'RT.room_bed_size as BedSize',
								 'RA.room_number as RoomNumber', 'RA.room_assign_id as RoomId', 'RA.status as RoomStatus')
						->join('roomtypes as RT', 'RT.room_type_id', '=', 'RA.room_type_id')
						->where([ ['RT.hotel_id', $Hotel_ID], ['RA.status', $RoomAssignStatus], ['RA.trash', 0] ])
						->orderBy('RA.room_number', 'ASC')
						->paginate(10);
		}
		else
		{
			$Results = DB::table('room_assign as RA')
						->select('RT.room_type_id as RoomTypeId', 'RT.room_type as RoomType', 'RT.room_smoking_type as SmokeType', 'RT.room_bed_size as BedSize',
								 'RA.room_number as RoomNumber', 'RA.room_assign_id as RoomId', 'RA.status as RoomStatus')
						->join('roomtypes as RT', 'RT.room_type_id', '=', 'RA.room_type_id')
						->where([ ['RT.hotel_id', $Hotel_ID], ['RA.trash', 0] ])
						->orderBy('RA.room_number', 'ASC')
						->paginate(10);
		}
		$Results->appends(request()->input())->links();
		return view('rooms.assign-room')->with("Results", $Results)->with("request", $request)->with("Hotel_ID", $Hotel_ID);
    }

    public function create(Request $request, $id)
    {	
		$Hotel_ID = Helper::getHotelId();
		$admin_ID = Helper::getAdminUserId();
        
		$TotalCount = $request->add_room;
		
		for($i = 1; $i <= $TotalCount; $i++)
		{
			$RoomAssign = new RoomAssign;
			$RoomAssign->hotel_id     = $Hotel_ID;
			$RoomAssign->room_type_id = $id;
			$RoomAssign->reg_ip   = $_SERVER['REMOTE_ADDR'];
			$RoomAssign->added_by = $admin_ID;
			$RoomAssign->save();
		}
		$url = "room/assign-room/".$id."/edit";
		return redirect($url);		
    }

    public function store(Request $request)
    {
		/*
        $Results = new Product;
		$Results->name = $request->name;
		$Results->price = $request->price;
		$Results->save();
		*/
		$Inputs  = $request->all();
		$Results = RoomAssign::Create($Inputs);
		return redirect('room/assign-room');
    }
	
	public function statusUpdate($RoomAssignId = 0)
    {
		$Hotel_ID = Helper::getHotelId();
		$admin_ID = Helper::getAdminUserId();
		
		$Results     = RoomAssign::where([ ['room_assign_id', $RoomAssignId], ['hotel_id', $Hotel_ID] ])->first(); 
		$RoomTypeRst = Roomtype::where([ ['room_type_id', $Results->room_type_id], ['hotel_id', $Hotel_ID] ])->first();
		
		if($Results->status == 1)
		{
			$Results->status = 0;
			$RoomTypeRst->room_count = $RoomTypeRst->room_count-1;
		}
		else
		{
			$Results->status = 1;
			$RoomTypeRst->room_count = $RoomTypeRst->room_count+1;
		}
		$Results->reg_ip   = $_SERVER['REMOTE_ADDR'];
		$Results->added_by = $admin_ID;
		$Results->save();
		
		$RoomTypeRst->reg_ip   = $_SERVER['REMOTE_ADDR'];
		$RoomTypeRst->added_by = $admin_ID;
		$RoomTypeRst->save();
		
		return redirect('room/assign-room');
	}

   

    public function edit($id)
    {
		$Hotel_ID = Helper::getHotelId();
		
		$Rst  	  = Roomtype::find($id);
		$allRooms = RoomAssign::where('hotel_id', $Hotel_ID)->get();
		$Results  = RoomAssign::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $id] ])->orderBy('room_number', 'ASC')->get();
		if($Rst->hotel_id == $Hotel_ID)
		{
			return view('rooms.assign-room-edit')->with("Results", $Results)->with("Rst", $Rst)->with("allRooms", $allRooms);
		}
		else
		{
			return redirect('room/room-type');
		}
    }

    public function update(Request $request, $id)
    {
		$admin_ID = Helper::getAdminUserId();
		$Hotel_ID = Helper::getHotelId();

		foreach($request->room_assign_id as $key=>$vals)
		{
			$Results = RoomAssign::where([ ['room_assign_id', $vals], ['hotel_id', $Hotel_ID] ])->first();
			$Results->room_number = $request->room_number[$key];
			$Results->reg_ip 	  = $_SERVER['REMOTE_ADDR'];
			$Results->added_by 	  = $admin_ID;
			$Results->save();
		}
		return redirect('room/room-type');
    }
	
	public function trash($id)
    {	
		$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		
		$Results = RoomAssign::where([ ['room_assign_id', $id], ['hotel_id', $Hotel_ID] ])->first();
		if(count($Results))
		{
			if($Results->trash == 1){
				$Results->trash = 0;
			}else{
				$Results->trash = 1;
			}
			
			$Results->reg_ip   = $_SERVER['REMOTE_ADDR'];
			$Results->added_by = $admin_ID;
			$Results->save();
			return redirect('room/assign-room')->with('success','Room deleted successfully');
		}
		else
		{
			return redirect('room/assign-room')->with("danger", "Please select your Motel");
		}
    }

    public function destroy($id)
    {
		$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		
		$Results = RoomAssign::where([ ['room_assign_id', $id], ['hotel_id', $Hotel_ID] ])->first();
		if(count($Results)){
			$Results->delete();
			return redirect('room/assign-room')->with('success','Room deleted successfully');
		}else{
			return redirect('room/assign-room')->with("danger", "Room# ".$id." not Found");
		}
    }
	
	public function existsRoomNumber(Request $request)
    {
       	$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		
		$RoomIdArray    = explode(",", $request->RoomId);
		$RoomValueArray = explode(",", $request->RoomValue);
		
		if($request->ajax())
		{
			$errorVariable = $errorMessage = $errorRoomId = array();
			$errorCount = 0;
			
			foreach($RoomIdArray as $key => $RoomId)
			{
				$RoomNumberValue = $RoomValueArray[$key];
				$Results = RoomAssign::where([ ['hotel_id', $Hotel_ID], ['room_number', $RoomNumberValue], ['room_assign_id', '!=', $RoomId] ])->first();
				if(count($Results))
				{
					$errorVariable[] = 1;
					$errorMessage[]  = "Room Number already exists";
					$errorRoomId[]   = $RoomId;
					$errorCount 	 = $errorCount+1;
				}
				else
				{
					$errorVariable[] = 0;
					$errorMessage[]  = "";
					$errorRoomId[]   = $RoomId;
					$errorCount		 = $errorCount+0;
				}
			}
			$stringError   = implode(",", $errorVariable);
			$stringMessage = implode(",", $errorMessage);
			$stringRoomId  = implode(",", $errorRoomId);
			
			return response()->json(['response' => "Successfully", 'status' => 'error', 'stringError' => $stringError, 'stringMessage' => $stringMessage, 'stringRoomId' => $stringRoomId, 'errorCount' => $errorCount ]);
			
		}
		else
		{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
	
	public function unAssignRoomNumber(Request $request)
    {
       	$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		
		if($request->ajax())
		{
			$rsrvId  = $request->rsrvId;
			$Results = ReservationRoomAllot::where([ ['room_reservation_id', $rsrvId], ['hotel_id', $Hotel_ID] ])->first();
			if(count($Results))
			{
				$pastAllot = new ReservationRoomAllotPast;
				$pastAllot->hotel_id			= $Hotel_ID;
				$pastAllot->room_reservation_id = $rsrvId;
				$pastAllot->p_room_assign_id 	= $Results->room_assign_id;
				$pastAllot->room_assign_id 		= 0;
				$pastAllot->reg_ip 				= $_SERVER['REMOTE_ADDR'];
				$pastAllot->added_by 			= $admin_ID;
				$pastAllot->save();
				
				$Results->room_assign_id = 0;
				$Results->reg_ip 		 = $_SERVER['REMOTE_ADDR'];
				$Results->added_by 		 = $admin_ID;
				$Results->save();
				
				return response()->json(['response' => 'Room Number deleted successfully', 'status' => 'success']);
			}
			else
			{
				return response()->json(['response' => 'Room Number not Found', 'status' => 'error']);
			}
		}
		else
		{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
	
	public function lockAssignRoomNumber(Request $request)
    {
       	$Hotel_ID  = Helper::getHotelId();
		$admin_ID  = Helper::getAdminUserId();
		
		if($request->ajax())
		{
			$rsrvId   = $request->rsrvId;
			$rsrvLock = $request->rsrvLock;
			
			$Results = RoomReservation::where([ ['room_reservation_id', $rsrvId], ['hotel_id', $Hotel_ID] ])->first();
			if(count($Results))
			{
				$Results->reservation_locked = $rsrvLock;
				$Results->reg_ip   = $_SERVER['REMOTE_ADDR'];
				$Results->added_by = $admin_ID;
				$Results->save();

				$allotRst = ReservationRoomAllot::where([ ['room_reservation_id', $rsrvId], ['hotel_id', $Hotel_ID] ])->first();
				$RoomRst  = RoomAssign::where([ ['room_assign_id', $allotRst->room_assign_id], ['hotel_id', $Hotel_ID] ])->first();
				$TypeRst  = Roomtype::where([ ['room_type_id', $RoomRst->room_type_id], ['hotel_id', $Hotel_ID] ])->first();
				
				$RoomType = (($TypeRst->room_smoking_type != 1)?'':'N').$TypeRst->room_bed_size;
				
				if($rsrvLock==1)
				{
					$Vars = '<div class="fltL" style="position:relative">'.$RoomRst->room_number.'</div>
							 <div class="fltL marginLeft25"> ('.$RoomType.')</div>
							 <i class="fa fa-lock fltL lockFeaturButton cursorPointer" id="unLockButton" data-id="'.$rsrvId.'" aria-hidden="true"></i>';
				}
				else
				{
					$Vars = '<div class="fltL" style="position:relative">
								<a href="'.url('change-room', $rsrvId).'" class="btn-list-link">'.$RoomRst->room_number.'</a>
								'.(($Results->status == "")?'<i class="closeButton fa fa-times" data-id="'.$rsrvId.'"></i>':'').'
							</div>
							<div class="fltL marginLeft25"> ('.$RoomType.')</div>
							<i class="fa fa-unlock fltL lockFeaturButton cursorPointer" id="LockButton" data-id="'.$rsrvId.'" aria-hidden="true"></i>';
				}
				return response()->json(['content' => $Vars, 'response' => 'Room Number '.(($rsrvLock==1)?'Locked':'Unlocked').' Successfully', 'status' => 'success']);
			}
			else
			{
				return response()->json(['response' => 'Reservation not Found', 'status' => 'error']);
			}
		}
		else
		{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
}
