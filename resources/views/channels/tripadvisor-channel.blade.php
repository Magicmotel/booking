@extends('layouts.layout')

@section('title')
	TripAdvisor Channel Partner
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>TripAdvisor Channel Partner</h3></div>
            @if($loginNum)
        	<div class="fltR AlgnRight zeroRightPadd marginTop5 marginBottom5">
            	@if($loginRst->channel_status == 1)
                    <form action="{!!url('tripadvisor-channel', $loginRst->cm_id)!!}" method="post" onsubmit="return ConfirmAction('Are you sure you want to pause this Channel?')">
                    {!!method_field('PATCH')!!}
                    {!!csrf_field()!!}
                    {!!Form::button('<i class="fa fa-pause-circle-o"></i> Deactivate', ['class'=>'left btn btn-warning zeroMargin', 'role' => 'button', 'type' => 'submit'])!!}
                    </form>
               	@else
                	<form action="{!!url('tripadvisor-channel', $loginRst->cm_id)!!}" method="post" onsubmit="return ConfirmAction('Are you sure you want to active this Channel?')">
                    {!!method_field('PATCH')!!}
                    {!!csrf_field()!!}
                    {!!Form::button('<i class="fa fa-play-circle-o"></i> Active', ['class'=>'left btn btn-success zeroMargin', 'role' => 'button', 'type' => 'submit'])!!}
                    </form>
                @endif
            </div>
            @endif
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    	@if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @elseif ($message = Session::get('danger'))
                            <div class="alert alert-danger">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                    	<div class="form-horizontal form-label-left">
                            <span class="section">CHANNEL CREDENTIALs:</span>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-4">TripAdvisor Hotel Id:</label>
                                <div class="control-value col-md-6 col-sm-8">{!!$loginRst->channel_hotel_id!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-4">Partner ID:</label>
                                <div class="control-value col-md-6 col-sm-8">{!!$loginRst->channel_username!!}</div>
                            </div>
                            <div class="form-group form-group-last marginTop24">
                                <div class="col-md-2 col-sm-8 col-xs-12 col-md-offset-3 zeroPadd">
                                    <a href="{!!url('tripadvisor-channel/credential')!!}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i> Edit Credential</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop      
