@extends('layouts.layout')

@section('title')
	Zeamster Payment Gateway
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Zeamster Payment Gateway</h3></div>
            <div class="fltR AlgnRight zeroRightPadd marginTop5 marginBottom5">
            	@if($credentialRst->gateway_status == 1)
                    <form action="{!!url('zeamster', $credentialRst->gc_id)!!}" method="post" onsubmit="return ConfirmAction('Are you sure you want to pause this Payment Gateway?')">
                    {!!method_field('PATCH')!!}
                    {!!csrf_field()!!}
                    {!!Form::button('<i class="fa fa-pause-circle-o"></i> Deactivate', ['class'=>'left btn btn-warning zeroMargin', 'role' => 'button', 'type' => 'submit'])!!}
                    </form>
               	@else
                	<form action="{!!url('zeamster', $credentialRst->gc_id)!!}" method="post" onsubmit="return ConfirmAction('Are you sure you want to active this Payment Gateway?')">
                    {!!method_field('PATCH')!!}
                    {!!csrf_field()!!}
                    {!!Form::button('<i class="fa fa-play-circle-o"></i> Active', ['class'=>'left btn btn-success zeroMargin', 'role' => 'button', 'type' => 'submit'])!!}
                    </form>
                @endif
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    	<div class="form-horizontal form-label-left">
                            <span class="section">ZEAMSTER CREDENTIALs:</span>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">User Id:</label>
                                <div class="control-value col-md-3 col-sm-3">{!!$credentialRst->user_id!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">User Api Key:</label>
                                <div class="control-value col-md-3 col-sm-3">{!!$credentialRst->user_api_key!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Developer Id:</label>
                                <div class="control-value col-md-3 col-sm-3">{!!$credentialRst->developer_id!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Location Id:</label>
                                <div class="control-value col-md-3 col-sm-3">{!!$credentialRst->location_id!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3">Ticket Hash Key:</label>
                                <div class="control-value col-md-3 col-sm-3">{!!$credentialRst->ticket_hash_key!!}</div>
                            </div>
                            <div class="form-group form-group-last marginTop24">
                                <div class="col-md-2 col-sm-8 col-xs-12 col-md-offset-3 zeroPadd">
                                    <a href="{!!url('zeamster/credentials')!!}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i> Edit Credential</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop      
