<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roomtype extends Model
{
	protected $fillable= [
		'room_type_id_unq', 'hotel_id', 'room_display', 'room_type', 'room_count', 'guest_adult', 'guest_child',
		'room_smoking_type', 'room_bed_size', 'room_rate', 'room_pic_1', 'room_pic_2', 'room_pic_3', 'room_pic_4',
		'description', 'minibar', 'bathtub', 'safe', 'tv', 'balcony', 'radio', 'phone', 'ac',
		'bathacc', 'wc', 'dryer', 'desk', 'towels', 'view', 'heating', 'wifi', 'room_order',
		'trash', 'reg_ip', 'added_by'
	];
	protected $primaryKey = 'room_type_id';
	protected $table = 'roomtypes';
}
