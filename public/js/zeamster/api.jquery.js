function Api() {
    /* This is the location of the API server */
    this.url = "https://apiv2.sandbox.zeamster.com/v2/cardtickets";

    this.requestTicket = function (options) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", this.url, true);
        xmlhttp.setRequestHeader("Content-type", "application/json");
        xmlhttp.setRequestHeader("Accept", "application/json");
        xmlhttp.send(JSON.stringify(options.data));
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4) {
                options.responseHandler(xmlhttp.status, JSON.parse(xmlhttp.responseText));
            }
        }
    }
}

(function ($) {
    $.fn.ticket = function (options) {
        var opts = $.extend({}, $.fn.ticket.defaults, options);
		
        return this.each(function () {
            var $this = $(this);
            $this.submit(function (e) {
				$("#main_container_Loading").show();
				$("#main_container_overlay").show();
                e.preventDefault();
                var $ccdata = $('.ticketed');
                var $exdata = $('.getExtraVal');
				var payAmount = "";
				var payMode   = "";
				var cardHolder = "";
				var cardNumber = "";
				var cardExpiry = "";
				var cardCVV    = "";
				var cardZIP    = "";
                var postdata   = {};
                postdata['location_id'] = opts.location_id;
                postdata['authorization'] = 'api '+ opts.signature;
                postdata['timestamp'] = opts.timestamp;
                postdata['order_id'] = opts.order_id;
				
				var rsrvId = opts.order_id;

                var api = new Api();
                api.url = opts.url;
                $.each($ccdata, function (index, value) {
                    postdata[$(value).attr('data-ticket')] = $(value).val();
					if($(value).attr('name') == 'card_number'){
						cardNumber = $(value).val();
					}
					if($(value).attr('name') == 'exp_date'){
						cardExpiry = $(value).val();
					}
					if($(value).attr('name') == 'cvv'){
						cardCVV = $(value).val();
					}
					if($(value).attr('name') == 'zip'){
						cardZIP = $(value).val();
					}
                });
				$.each($exdata, function (index, value) {
					if($(value).attr('name') == 'pay_amount'){
						payAmount = $(value).val();
					}
					if($(value).attr('name') == 'pay_mode'){
						payMode = $(value).val();
					}
					if($(value).attr('name') == 'account_holder_name'){
						cardHolder = $(value).val();
					}
                });
				
				api.requestTicket({
                	data : postdata,
					async: false,
                	responseHandler : function (status, data) {
                        //console.log(data);
                		if (status === 200 || status === 201) {
                			$.fn.ticket.addTicket($this, data.ticket);
                            //$this.unbind('submit');
                            //$ccdata.remove();
							window.location.href = zeamsterTrans+"?rsrvid="+rsrvId+"&payamount="+payAmount+"&cardholder="+cardHolder+"&card_number="+cardNumber+"&exp_date="+cardExpiry+"&cvv="+cardCVV+"&zip="+cardZIP+"&action="+payMode+"&token="+data.ticket;
							//$this.submit();
                        } else if (status === 422) {
                            $.fn.ticket.addValidationErrors($this, data);
                        } else {
                            $.fn.ticket.handleError($this, data);
                        }
                	}
                });
            });
        });
    };

    $.fn.ticket.handleError = function ($form, data) {
        switch (data.status) {
        case 409: // a user input validation error
            $.fn.ticket.addValidationErrors($form, data);
            break;
        default: // an error either for the remote server or the configuration
            alert("An error has occured, please contact the system administrator");
        }
    }

    $.fn.ticket.addValidationErrors = function ($form, errors) {
        $errorSummary = $form.find('.errorSummary');
        $errorSummary.html('');
        $form.children().removeClass('error');
        $.each(errors, function (index, value) {
            $.each(value, function (i, v) {
                $('*[data-ticket=' + index + ']').addClass('error');
                $error = $('<p>');
                $error.html(v);
                $error.appendTo($errorSummary);
            });
        });
    }

    //adds a ticket to the form
    $.fn.ticket.addTicket = function (form, ticket) {
        var $ticketField = $('<input>');
        $ticketField.attr({
            type: 'hidden',
            value: ticket,
            name: "ticket"
        });
        $ticketField.appendTo(form);
    }

    $.fn.ticket.defaults = {
        url: "https://apiv2.sandbox.zeamster.com/v2/cardtickets"
    };

}(jQuery));