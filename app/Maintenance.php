<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maintenance extends Model
{
    protected $table = 'maintenance';
	protected $primaryKey = 'maintenance_id';
	protected $fillable = [
        'hotel_id', 'room_type_id', 'room_assign_id', 'maintenance_for', 'maintenance_reply',
		'block_by', 'block_date', 'block_ip', 'active_by', 'active_date', 'active_ip', 'status',
		'reg_ip', 'added_by'
    ];
}
