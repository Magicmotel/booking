<?php

namespace App\Helpers;

use Auth;
use App\Hotel;
use App\RoomAssign;
use App\HotelSupport;
use DB;

class Helper
{
    public static function getSiteName()
    {
        return "MagicMotelKey";
    }
	
	public static function getAdminUserId()
    {
		return Auth::guard('admin')->user()->id;
	}
	
	public static function getAdminUserName()
    {
		$fname = Auth::guard('admin')->user()->fname;
		$mname = Auth::guard('admin')->user()->mname;
		$lname = Auth::guard('admin')->user()->lname;
		return $fname.(($mname)?" $mname":"")." ".$lname;
	}
	
	public static function getRoleisGranted()
	{	
		return array_search(Auth::guard('admin')->user()->have_role->name, array(1=>'Administrator', 2=>'Manager'));
	}
	
	public static function getHotelId()
    {
        return Auth::guard('admin')->user()->hotel_id;
    }
	
	public static function getHotelName()
    {
        $Hotel_ID = Helper::getHotelId();
		$Rst = Hotel::where('hotel_id', $Hotel_ID)->first();
		return $Rst->hotel_name;
    }
	
	public static function getUniqueId()
	{
		return $ImageName = str_replace(".", "", microtime(date('Y-m-d H:i:s')));		
	}
	
	public static function getBlankRecord_RoomType($id)
    {
		$Hotel_ID = Helper::getHotelId();
        return $RoomAssRst = RoomAssign::where([ ['room_type_id', '=', $id], ['hotel_id', '=', $Hotel_ID], ['room_number', '=', ''] ])->count();
    }
	
	public static function getMaintenance_RoomType($id, $roomNum)
    {
		$Hotel_ID = Helper::getHotelId();
		$RoomAssRst = RoomAssign::where([ ['room_type_id', $id], ['hotel_id', $Hotel_ID], ['status', 0], ['trash', 0] ])->count();
		return $RoomAssRst;
    }
	
	public static function getMismatchRecord_RoomType($id, $roomNum)
    {
		$Hotel_ID = Helper::getHotelId();
		$RoomAssRst = RoomAssign::where([ ['room_type_id', $id], ['hotel_id', $Hotel_ID], ['trash', 0] ])->count();
		return $roomNum - $RoomAssRst;
    }
	
	public static function getTaxCalculation($roomTax, $roomRate)
    {
		if(strstr($roomTax, '%')){
			return round((($roomRate*str_replace("%","",$roomTax))/100), 2);
		}else{
			return round($roomTax, 2);
		}
    }
	
	public static function getImageFolderPath()
    {
        return url("/");
    }
	
	public static function getImageRootPath()
    {
        return "/home/magicmotel/public_html/booking";
    }
	
	public static function setImgName($string){
		$ImageName = preg_replace("/[^A-Za-z0-9 \-]/", '', $string);
		$ExistString   = array("' and '", "' - '", "'[\s]+'", "'----'", "'---'", "'--'", "'/'");
		$ReplaceString = array("-", "-", "-", "-", "-", "-", "-");
		$ImageName = preg_replace($ExistString, $ReplaceString, $ImageName);
		return strtolower($ImageName);
	}
	
	public static function removeImg($string){
		if(file_exists($string)){
			@unlink($string);
		}
	}
	
	public static function getTicketRecord()
    {
		$Hotel_ID = Helper::getHotelId();
		$CCTicket = HotelSupport::where([ ['status', 1], ['support_from', 2], ['hotel_id', $Hotel_ID] ])->groupBy('hotel_id')->get();
		return count($CCTicket);
    }

	public static function getExpediaMapErrorRecord($Channel_ID)
    {
		$Hotel_ID = Helper::getHotelId();
		$mapRst   = DB::select("SELECT RT.room_type as RoomType, CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomCode,
								CMM.channel_room_type_id as cRoomTypeId, CMM.channel_rate_plan_id as cRatePlanId
								FROM roomtypes as RT LEFT JOIN channel_motel_mapping as CMM ON CMM.room_type_id = RT.room_type_id AND CMM.channel_id IN (".$Channel_ID.")
								WHERE RT.hotel_id = ".$Hotel_ID." AND RT.trash = 0 HAVING cRatePlanId IS NULL
								ORDER BY RT.room_type_id ASC
							   ");
		return count($mapRst);
    }
	
	// Room Type Inventory
	public static function roomTypeInventoryAdjustment($Hotel_ID, $cRoomTypeId, $fromDate, $toDate)
	{
		$room_typeId  = $cRoomTypeId;
		$roomMaxCount = 0; $roomMaxVar = "";
		$Channel_From = $Channel_To = $fromDate;
		
		$date1 = date_create($fromDate);
		$date2 = date_create($toDate);
		$diff = date_diff($date1, $date2);
		$dayNum = $diff->format("%a");
		for($i = 1; $i <= $dayNum; $i++)
		{
			$RoomListQry = DB::select("SELECT
										(	( IF(	(SELECT SUM(RSV.qty_reserve) FROM room_reservation as RSV
													 WHERE RSV.room_type_id = RT.room_type_id AND RSV.status != 'Cancelled' AND
													 (RSV.arrival <= '".$Channel_From."' AND RSV.departure > '".$Channel_From."') GROUP BY RSV.room_type_id
													) != 0, 
													(SELECT SUM(RSV.qty_reserve) FROM room_reservation as RSV
													 WHERE RSV.room_type_id = RT.room_type_id AND RSV.status != 'Cancelled' AND
													 (RSV.arrival <= '".$Channel_From."' AND RSV.departure > '".$Channel_From."') GROUP BY RSV.room_type_id
													), 0              
												 )
											)
										) as availRooms FROM roomtypes as RT
										WHERE RT.hotel_id = ".$Hotel_ID." AND RT.room_type_id = ".$room_typeId."
									");					
			foreach($RoomListQry as $RoomListRst){
				$RoomAvailInvt = $RoomListRst->availRooms;
			}
			if($RoomAvailInvt > $roomMaxCount){
				$roomMaxCount = $RoomAvailInvt;
				$roomMaxVar   = $RoomAvailInvt.",".$Channel_From;
			}
			$Channel_From = $Channel_To = date('Y-m-d', strtotime($Channel_From.'+1 days'));
		}
		return $roomMaxVar;
	}
	
	// CHANNEL PARTNER REQUEST	
	public static function ChannelPartnerRequest($Hotel_ID, $cRoomTypeId, $fromDate, $toDate)
	{
		$credentialQry = DB::select("SELECT * FROM channel_motels WHERE hotel_id = ".$Hotel_ID." AND channel_status = 1 AND channel_id = 1");
		foreach($credentialQry as $credentialRst)
		{
			$Channel_Id		  = $credentialRst->channel_id;
			$Channel_HotelId  = $credentialRst->channel_hotel_id;
			$Channel_Username = $credentialRst->channel_username;
			$Channel_Password = $credentialRst->channel_password;
			
			
			$channelSql = DB::select("SELECT room_type_id, channel_room_type_id, channel_rate_plan_id FROM channel_motel_mapping
									  WHERE room_type_id IN (".$cRoomTypeId.") AND hotel_id = ".$Hotel_ID." AND channel_id = ".$Channel_Id."
									 ");
			if(count($channelSql))
			{
				$requestXML = '<?xml version="1.0" encoding="UTF-8"?>
								<AvailRateUpdateRQ xmlns="http://www.expediaconnect.com/EQC/AR/2011/06">
									<Authentication username="'.$Channel_Username.'" password="'.$Channel_Password.'"/>
									<Hotel id="'.$Channel_HotelId.'"/>';
				foreach($channelSql as $channelRst)
				{
					$room_typeId		= $channelRst->room_type_id;
					$Channel_RoomTypeId = $channelRst->channel_room_type_id;
					$Channel_RatePlanId = $channelRst->channel_rate_plan_id;
					
					$Channel_From = $Channel_To = $fromDate;
					
					$date1 = date_create($fromDate);
					$date2 = date_create($toDate);
					$diff = date_diff($date1, $date2);
					$dayNum = $diff->format("%a");
					for($i = 1; $i <= $dayNum; $i++)
					{
						$RoomAvailInvt = Helper::totalInventoryAvailable($Hotel_ID, $room_typeId, $Channel_From);
						
						$requestXML .=	'
								<AvailRateUpdate>
									<DateRange from="'.$Channel_From.'" to="'.$Channel_To.'"/>
									<RoomType id="'.$Channel_RoomTypeId.'">
										<Inventory totalInventoryAvailable="'.$RoomAvailInvt.'"/>
									</RoomType>
								</AvailRateUpdate>';
					
						$Channel_From = $Channel_To = date('Y-m-d', strtotime($Channel_From.'+1 days'));
					}
				}
				$requestXML .=	'</AvailRateUpdateRQ>';
				$URL = "https://services.expediapartnercentral.com/eqc/ar";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $URL);
				curl_setopt($ch, CURLOPT_VERBOSE, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
				curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXML);
				$responseXML    = curl_exec($ch);
				$headerResponse = curl_getinfo($ch);
				curl_close($ch);
				
				if(isset($headerResponse['http_code']) && $headerResponse['http_code']==200)
				{
					$responseString = simplexml_load_string($responseXML);
					$responseJSON   = json_decode(json_encode($responseString), true);
					if(isset($responseJSON['Success'])){
						if(count($responseJSON['Success']) > 0){
							$ResponseStat = 'Success with Warning';
						}else{
							$ResponseStat = 'Success';
						}
					}
					elseif(isset($responseJSON['Error'])){
						$ResponseStat = 'Error';
					}
				}else{
					$ResponseStat = 'Error';
				}
				DB::select("CALL ChannelCommunication_SP ('".$Hotel_ID."', 'MMK', '".$URL."', '".$requestXML."', 'Expedia', '".$responseXML."', '".$ResponseStat."', '".$_SERVER['REMOTE_ADDR']."')");
			}
		}
	}
	
	public static function totalInventoryAvailable($Hotel_ID, $RoomTypeId, $Channel_From)
	{
		$RoomListQry = DB::select("SELECT
									(	RT.room_count -
										( IF(	(SELECT SUM(RSV.qty_reserve) FROM room_reservation as RSV
												 WHERE RSV.room_type_id = RT.room_type_id AND RSV.status != 'Cancelled' AND
												 (RSV.arrival <= '".$Channel_From."' AND RSV.departure > '".$Channel_From."') GROUP BY RSV.room_type_id
												) != 0, 
												(SELECT SUM(RSV.qty_reserve) FROM room_reservation as RSV
												 WHERE RSV.room_type_id = RT.room_type_id AND RSV.status != 'Cancelled' AND
												 (RSV.arrival <= '".$Channel_From."' AND RSV.departure > '".$Channel_From."') GROUP BY RSV.room_type_id
												), 0              
											 )
										) - 
										( IF(	(SELECT count(R.room_type_id) FROM room_assign as R
												 WHERE R.room_type_id = RT.room_type_id AND (R.status = 0 OR R.trash = 1 OR R.room_number = '') GROUP BY R.room_type_id
												) != 0, 
												(SELECT count(R.room_type_id) FROM room_assign as R
												 WHERE R.room_type_id = RT.room_type_id AND (R.status = 0 OR R.trash = 1 OR R.room_number = '') GROUP BY R.room_type_id
												), 0              
											 )
										) - 
										( IF(
												(SELECT count(DISTINCT(BR.room_assign_id)) FROM block_rooms as BR WHERE BR.room_type_id = RT.room_type_id AND
												 (BR.block_from <= '".$Channel_From."' AND BR.block_to > '".$Channel_From."') GROUP BY BR.room_type_id
												) != 0, 
												(SELECT count(DISTINCT(BR.room_assign_id)) FROM block_rooms as BR WHERE BR.room_type_id = RT.room_type_id AND
												 (BR.block_from <= '".$Channel_From."' AND BR.block_to > '".$Channel_From."') GROUP BY BR.room_type_id
												), 0              
											 )
										)
									) as availRooms FROM roomtypes as RT
									WHERE RT.hotel_id = ".$Hotel_ID." AND RT.room_type_id = ".$RoomTypeId."
								");					
		foreach($RoomListQry as $RoomListRst){
			$RoomAvailInvt = $RoomListRst->availRooms;
			if($RoomAvailInvt<0){
				$RoomAvailInvt = 0;
			}
		}
		return $RoomAvailInvt;
	}
}