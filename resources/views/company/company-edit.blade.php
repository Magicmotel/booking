@extends('layouts.layout')

@section('title')
	Edit Company Profile
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Edit Company Profile</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('company-profile')}}">Manage Company Profiles</a></li>
                <li class="active"><a href="javascript:void(0);">Edit Company Profiles</a></li>
                <li><a href="{{url('company-profile/create')}}">Create Company Profile</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	@if (count($errors))
                <ul class="errorFormMessage">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                    <form id="demo-form2" action="{!!url('company-profile', $Results->profile_id)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                        {!!method_field('PATCH')!!}
                        {!!csrf_field()!!}
                        <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_type">Company Profile Name <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<input type="text" name="company_name" id="company_name" value="{{$Results->company_name}}" placeholder="Enter Company Profile Name" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Address <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-4">
                            <textarea name="company_address" id="company_address" placeholder="Address" class="form-control col-md-7 col-xs-12" required="required">{{$Results->company_address}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Zip Code <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3 col-sm-12">
                            <input type="text" name="company_zip" id="company_zip" value="{{$Results->company_zip}}" placeholder="Enter Zip Code" maxlength="5" size="5" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">City</label>
                            <div class="col-md-3 col-sm-3 col-sm-12">
                            <input type="text" name="company_city" id="company_city" value="{{$Results->company_city}}" placeholder="City" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">State</label>
                            <div class="col-md-3 col-sm-3 col-sm-12">
                            <select name="company_state" id="company_state" class="form-control col-md-2 col-xs-12 zeroPadd">
                                <option value="">Select State</option>
                                @foreach(App\ZipCityState::groupBy('zip_state')->get() as $stateVal)
                                <option value="{{$stateVal->zip_state}}"{{($Results->company_state==$stateVal->zip_state)?" Selected":""}}>{{$stateVal->zip_state}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Language Preference</label>
                            <div class="col-md-2 col-sm-3 col-sm-12">
                            <select name="company_lang" id="company_lang" class="form-control col-md-2 col-xs-12 zeroPadd">
                                <option value="1"{{($Results->company_lang==1)?" Selected":""}}>English</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Email</label>
                            <div class="col-md-4 col-sm-6">
                            <input type="email" name="company_email" id="company_email" value="{{$Results->company_email}}" placeholder="Enter Email Address" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Phone Number <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="company_phone" id="company_phone" value="{{$Results->company_phone}}" placeholder="Enter Phone Number" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Fax</label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="company_fax" id="company_fax" value="{{$Results->company_fax}}" placeholder="Enter Fax Number" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Contact Person</label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            	<input type="text" name="contact_name" id="contact_name" value="{{$Results->contact_name}}" placeholder="Enter Contact Person Name" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Contact Title</label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            	<input type="text" name="contact_title" id="contact_title" value="{{$Results->contact_title}}" placeholder="Enter Contact Person Title" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Notes</label>
                            <div class="col-md-4 col-sm-4">
                            <textarea name="company_notes" id="company_notes" placeholder="Enter Notes..." class="form-control col-md-7 col-xs-12">{{$Results->company_notes}}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Tax Exempt</label> 
                            <div class="col-md-6 col-sm-6">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default btn-toggleYes{!!($Results->company_tax == 1)?' active':''!!}">
                                      <input type="radio" name="company_tax" id="company_tax1" placeholder="" value="1"{!!($Results->company_tax==1)?' checked="checked"':''!!}> Yes
                                    </label>
                                    <label class="btn btn-default btn-toggleNo{!!($Results->company_tax == 0)?' active':''!!}">
                                      <input type="radio" name="company_tax" id="company_tax2" placeholder="" value="0"{!!($Results->company_tax==0)?' checked="checked"':''!!}> No
                                    </label>
                                </div>
                            </div>
                        </div>
                        <span class="section">Room Rate:</span>
                        @foreach(App\Roomtype::where('hotel_id', $Hotel_ID)->orderBy('room_type', 'ASC')->get() as $RoomTypeVal)
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">({!!(($RoomTypeVal->room_smoking_type == 1)?'N':'').$RoomTypeVal->room_bed_size!!}) &nbsp; {{$RoomTypeVal->room_type}} <span class="required">*</span></label>
                            <input type="hidden" name="room_type_id[]" id="room_type_{{$RoomTypeVal->room_type_id}}" value="{{$RoomTypeVal->room_type_id}}" class="form-control col-md-3 col-xs-6" placeholder="Enter Rate" required="required">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                            	<span class="input-group-btn fltL widthAuto"><div class="btn btn-primary cursorInitial">$</div></span>
                                <input type="text" name="company_rate[]" id="company_rate{{$RoomTypeVal->room_type_id}}" value="{{(isset($rateOutPut[$RoomTypeVal->room_type_id]) && $rateOutPut[$RoomTypeVal->room_type_id])?$rateOutPut[$RoomTypeVal->room_type_id]:$RoomTypeVal->room_rate}}" class="form-control width40" placeholder="Enter Rate" required="required" min="0.1" max="999" maxlength="6">
                            </div>
                        </div>
                        @endforeach
                    	<div class="form-group form-group-last marginTop24">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update Company</button>
                                <button type="reset" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop      
