<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::auth();

// Login Panel
Route::get('/', 'Adminauth\AuthController@loginRedirect');
Route::get('/login', 'Adminauth\AuthController@showLoginForm');
Route::post('login', 'Adminauth\AuthController@login');

Route::get('password/reset', 'Adminauth\AuthController@forgotForm');
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');
		
Route::get('dailyrateUpadate', 'Adminauth\CronController@dailyrateUpadate');

Route::group(['middleware' => ['admin', 'roles']], function(){
	//Route::get('/dashboard', 'Admin\AdminController@dashboard');
	Route::get('/dashboard', [
		'uses' => 'Admin\AdminController@dashboard',
		'roles' => ['administrator', 'manager'] // Only an administrator, or a manager can access this route
	]);
	
	Route::post('backend_Rooms', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\FrontDeskController@backend_rooms']);
	Route::post('backend_Events', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\FrontDeskController@backend_events']);
		
	//===========   RESERVATION   ===========//
	
	Route::get('reservation', ['roles' => ['administrator', 'manager', 'GM'], 'uses' => 'Admin\RoomReservationController@index']);
	Route::get('reservation-payment', ['roles' => ['administrator', 'manager', 'GM'], 'uses' => 'Admin\RoomReservationController@payment_gateway']);
    Route::get('reservation/create', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomReservationController@create']);
	Route::get('reservation/walkin', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomReservationController@walkin']);
    Route::post('reservation/guest', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomReservationController@guest']);
    Route::post('reservation/walkin-guest', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomReservationController@guest']);
    Route::post('reservation', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomReservationController@store']);
    Route::get('incoming-reservation', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomReservationController@incoming_reservation']);
    Route::get('checked-in-reservation/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomReservationController@checkin_reservation']);
    Route::post('special-request/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomReservationController@special_request']);
	
	Route::get('reservation/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomReservationController@edit']);
    Route::patch('reservation/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomReservationController@update']);
    Route::delete('reservation/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomReservationController@destroy']);
	
	Route::post('rate_behalf_company', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomReservationController@rateBehalfCompany']);
	
	//===========   ROOM   ===========//
	
	Route::get('room/room-type', ['roles' => ['administrator', 'manager', 'GM'], 'uses' => 'Admin\RoomtypeController@index']);
    Route::get('room/room-type/create', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomtypeController@create']);
    Route::post('room/room-type', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomtypeController@store']);
    Route::get('room/room-type/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomtypeController@edit']);
    Route::get('room/room-type/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomtypeController@trash']);
    Route::patch('room/room-type/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomtypeController@update']);
    Route::delete('room/room-type/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomtypeController@destroy']);
	Route::post('room_removeImage', ['roles' => ['administrator', 'manager', 'GM'], 'uses' => 'Admin\RoomtypeController@removeImage']);
	Route::post('roomTypeAvailable', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomtypeController@roomTypeAvailable']);
	
	Route::get('room/assign-room', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomAssignController@index']);
	Route::get('room/assign-room/create/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomAssignController@create']);
    //Route::get('room/assign-room/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomAssignController@trash']);
    Route::get('room/assign-room/status/{id}', ['roles' => ['administrator', 'manager', 'General Managaer'], 'uses' => 'Admin\RoomAssignController@index']);
    Route::post('room/assign-room', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomAssignController@store']);
    Route::get('room/assign-room/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomAssignController@edit']);
	Route::get('room/assign-room/{id}/status', ['roles' => ['administrator', 'manager', 'General Managaer'], 'uses' => 'Admin\RoomAssignController@statusUpdate']);
    Route::patch('room/assign-room/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomAssignController@update']);
    Route::delete('room/assign-room/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomAssignController@destroy']);
	Route::post('existsRoomNumber', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomAssignController@existsRoomNumber']);
	
	Route::post('unAssignRoomNumber', ['roles' => ['administrator', 'manager', 'GM'], 'uses' => 'Admin\RoomAssignController@unAssignRoomNumber']);
	Route::post('lockAssignRoomNumber', ['roles' => ['administrator', 'manager', 'GM'], 'uses' => 'Admin\RoomAssignController@lockAssignRoomNumber']);
	
	Route::get('block-room', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\BlockRoomController@index']);
	Route::get('block-room/create', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\BlockRoomController@create']);
	Route::post('block-room', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\BlockRoomController@store']);
	Route::delete('block-room/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\BlockRoomController@destroy']);
	Route::post('getRoomListBlock', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\BlockRoomController@getRoomListBlock']);
	
	Route::get('room/room-rate', ['roles' => ['administrator', 'manager', 'General Managaer'], 'uses' => 'Admin\RoomRateController@index']);
    Route::post('room/room-rate', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoomRateController@store']);
	Route::get('room/setavailRate', ['roles' => ['administrator', 'manager', 'General Managaer'], 'uses' => 'Admin\RoomRateController@setavailRate']);
	
	Route::get('room/taxes', ['roles' => ['administrator', 'manager', 'General Managaer'], 'uses' => 'Admin\TaxesController@index']);
    Route::post('room/taxes', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\TaxesController@store']);
	
	Route::get('room/discounts', ['roles' => ['administrator', 'manager', 'General Managaer'], 'uses' => 'Admin\DiscountsController@index']);
    Route::post('room/discounts', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\DiscountsController@store']);
	Route::get('room/discounts/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\DiscountsController@edit']);
    Route::patch('room/discounts/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\DiscountsController@update']);
	Route::delete('room/discounts/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\DiscountsController@destroy']);
	
	//===========   GUEST   ===========//
	
	Route::get('guests', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GuestController@index']);
	Route::get('guests/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GuestController@edit']);
	Route::get('guests/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GuestController@show']);
    Route::patch('guests/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GuestController@update']);
	
	Route::post('findGuest', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GuestController@findGuest']);
	Route::post('fillGuest', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GuestController@fillGuest']);
	Route::post('getCityData', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GuestController@getCityData']);
	Route::get('bad-mark/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GuestController@bad_mark']);
	Route::get('bad-guest', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GuestController@bad_guest']);
	Route::patch('bad-guest/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GuestController@bad_guest_create']);
	Route::get('remove-bad-guest/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GuestController@remove_bad_guest']);
	
	Route::get('front-desk', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\FrontDeskController@front_desk']);
	Route::get('reservation/support', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\FrontDeskController@new_reservation']);
	Route::get('showReservation/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\FrontDeskController@show_reservation']);
	Route::get('editReservation/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\FrontDeskController@edit_reservation']);
	Route::post('updateReservation', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\FrontDeskController@update_reservation']);
	Route::get('cancelReservation', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\FrontDeskController@cancel_reservation']);
	Route::get('unBlockRoom', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\FrontDeskController@unblock_room']);
	
	Route::get('in-house', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@index']);
	Route::get('departure', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@departure']);
    Route::get('checked-out-reservation/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@checkout_reservation']);
	Route::get('checked-out-guests', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@checkout_guest']);
	
	Route::get('guest-info/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@guestinfo']);
	Route::get('payment-info/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@paymentinfo']);
	Route::post('payment-reversal', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@payment_reversal']);
	Route::post('payment-reversal/transaction', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ZeamsterController@payment_reversal_post']);	
	Route::get('change-room/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@changeroom']);
	Route::patch('allot-room/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@allotroom']);
	Route::get('guest-folio/post-charge/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@postcharges']);
	Route::post('guest-folio/post-charge', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@postcharges_update']);
	Route::get('guest-folio/post-adjustment/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@postadjustment']);
	Route::post('guest-folio/post-adjustment', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@postadjustment_update']);
	Route::get('guest-folio/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@guestfolio']);
	Route::post('guest-folio', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@guestfolio_store']);
	Route::get('change-stay/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@changestay']);
	Route::patch('change-stay/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@modify_stay']);
	Route::get('cancel-reservation/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@cancelreserv']);
	Route::get('no-show-reservation/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@noshow_reserv']);
	Route::get('undo-check-in/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@undo_check_in']);
	Route::post('cancel-reservation', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@cancelreserv_reason']);
	Route::get('view-changes/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@view_change']);
	
	Route::get('payment-pay/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@payment_pay']);
	Route::patch('payment-pay/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@payment_pay_store']);
	
	Route::post('adjustAmount', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@adjustAmount']);
	
	Route::get('guest-info/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@guestinfo_edit']);
	Route::patch('guest-info/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@guestinfo_update']);
	
	Route::get('confirmation-letter/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@confirmation_letter']);
	Route::post('confirmation-mail', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@confirmation_mail']);
	Route::get('registration-card/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@registration_card']);
	Route::get('folio-print/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@folio_print']);
	
	Route::post('dayWiseRatePlan', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@dayWiseRatePlan']);
	Route::post('transactionDetailPayment', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\InHouseController@transactionDetailPayment']);
	
	//===========   MOTEL   ===========//
	
	Route::get('motels', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HotelController@index']);
	Route::get('motels/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HotelController@edit']);
    Route::patch('motels/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HotelController@update']);
	Route::post('motel_removeImage', ['roles' => ['administrator', 'manager', 'GM'], 'uses' => 'Admin\HotelController@removeImage']);
	
	Route::get('payment-motels', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HotelController@payment_index']);
	Route::get('payment-motels/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HotelController@payment_edit']);
    Route::patch('payment-motels/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HotelController@payment_update']);
	
	// Wi-Fi Password
	Route::post('hotel-wifi-password', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HotelController@wifi_password_update']);
	
	
	//===========   EMPLOYEE   ===========//
	
	Route::get('record-shifts', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ShiftAssignController@record']);
	Route::post('setassignShift', ['roles' => ['administrator', 'manager', 'General Managaer'], 'uses' => 'Admin\ShiftAssignController@setassignShift']);
	Route::patch('setassignShift', ['roles' => ['administrator', 'manager', 'General Managaer'], 'uses' => 'Admin\ShiftAssignController@setassignShift']);
	Route::get('assign-shifts', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ShiftAssignController@index']);
	Route::post('assign-shifts', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ShiftAssignController@store']);
	
	Route::get('employees', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\EmployeeController@index']);
    Route::get('employees/create', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\EmployeeController@create']);
    Route::post('employees', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\EmployeeController@store']);
    Route::get('employees/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\EmployeeController@edit']);
    Route::patch('employees/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\EmployeeController@update']);
    Route::delete('employees/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\EmployeeController@destroy']);
	
	Route::get('job-titles', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoleController@index']);
    Route::get('job-titles/create', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoleController@create']);
    Route::post('job-titles', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoleController@store']);
    Route::get('job-titles/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoleController@edit']);
    Route::patch('job-titles/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoleController@update']);
    Route::delete('job-titles/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\RoleController@destroy']);
	
	Route::get('shifts', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ShiftController@index']);
    Route::get('shifts/create', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ShiftController@create']);
    Route::post('shifts', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ShiftController@store']);
    Route::get('shifts/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ShiftController@edit']);
    Route::patch('shifts/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ShiftController@update']);
    Route::delete('shifts/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ShiftController@destroy']);
	
	//===========   COMPANY PROFILE   ===========//
	
	Route::get('company-profile', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\CompanyProfileController@index']);
    Route::get('company-profile/create', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\CompanyProfileController@create']);
    Route::post('company-profile', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\CompanyProfileController@store']);
    Route::get('company-profile/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\CompanyProfileController@edit']);
    Route::get('company-profile/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\CompanyProfileController@trash']);
    Route::patch('company-profile/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\CompanyProfileController@update']);
    Route::delete('company-profile/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\CompanyProfileController@destroy']);
	
	Route::post('getCityDataforCompany', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\CompanyProfileController@getCityData']);
	
	//===========   USER   ===========//
	
	Route::get('users', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\UserController@index']);
    Route::get('users/create', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\UserController@create']);
    Route::post('users', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\UserController@store']);
    Route::get('users/{id}/edit', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\UserController@edit']);
    Route::patch('users/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\UserController@update']);
    Route::delete('users/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\UserController@destroy']);
	
	//===========   HOUSE KEEPING   ===========//
	Route::get('housekeeping-headquarters', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HouseKeepingController@index']);
	Route::post('housekeeping-headquarters', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HouseKeepingController@store']);
	Route::post('housekeeping-reverse', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HouseKeepingController@cleanroom']);
	Route::get('housekeeping-print', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HouseKeepingController@assignment_print']);
	
	//===========   MAINTENANCE   ===========//
	Route::get('maintenance', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\MaintenanceController@index']);
    Route::get('maintenance/create', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\MaintenanceController@create']);
    Route::post('maintenance', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\MaintenanceController@store']);
    Route::get('maintenance/{id}/reply', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\MaintenanceController@edit']);
    Route::patch('maintenance/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\MaintenanceController@update']);
    Route::delete('maintenance/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\MaintenanceController@destroy']);
	
	
	//===========   REPORTING   ===========//
	Route::get('/report-daily-financial', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ReportController@financial_daily']);
	Route::get('/report-monthly-financial', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ReportController@financial_montly']);
	Route::get('/report-annual-financial', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ReportController@financial_annual']);
	Route::get('/report-tax-exemption', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ReportController@tax_exemption']);
	
	Route::get('/report-inhouse-guest', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ReportController@inhouse_guest']);
	Route::get('/report-incoming-reservation', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ReportController@incoming_reservation']);
	
	Route::get('/report-employee-log-history', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ReportController@employee_log_history']);
	Route::get('/report-employee-list', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ReportController@employee_list']);
	
	Route::get('/report-housekeeping', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ReportController@housekeeping']);
	
	
	
	//===========   CHANNEL PARTNERS   ===========//
	Route::get('expedia-channel', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ChannelController@expedia']);
	Route::patch('expedia-channel/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ChannelController@expedia_status']);
	Route::get('expedia-channel/credential', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ChannelController@expedia_credential']);
    Route::post('expedia-channel-credential', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ChannelController@expedia_credential_store']);
	Route::get('expedia-channel/mapping', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ChannelController@expedia_mapping']);
    Route::post('expedia-channel-mapping', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ChannelController@expedia_mapping_store']);
	
	Route::get('tripadvisor-channel', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ChannelController@tripadvisor']);
	Route::patch('tripadvisor-channel/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ChannelController@tripadvisor_status']);
	Route::get('tripadvisor-channel/credential', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ChannelController@tripadvisor_credential']);
    Route::post('tripadvisor-channel-credential', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ChannelController@tripadvisor_credential_store']);
	
	Route::get('booking-channel', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ChannelController@booking']);
	
	
	//===========   SUPPORT   ===========//
	
	Route::get('/support', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HotelController@support']);
	Route::post('/support/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\HotelController@support_update']);
	
	
	//===========   PAYMENT GATEWAYS   ===========//
	Route::get('zeamster', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GatewayPaymentController@zeamster']);
	Route::patch('zeamster/{id}', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GatewayPaymentController@zeamster_status']);
	Route::get('zeamster/credentials', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GatewayPaymentController@zeamster_credential']);
    Route::post('zeamster-credentials', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\GatewayPaymentController@zeamster_credential_store']);
	
	//===========   PAYMENT THROUGH ZEAMSTER   ===========//
	Route::post('reservation/payment/card', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ZeamsterController@payment_card']);
	Route::post('reservation/payment/transaction', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ZeamsterController@payment_post']);
	
	Route::get('reservation/zeamster/transaction', ['roles' => ['administrator', 'manager'], 'uses' => 'Admin\ZeamsterSDKController@zeamster_transaction']);
		
	Route::get('/logout', 'Adminauth\AuthController@logout');
	Route::post('/logout', 'Adminauth\AuthController@logout');
});

/*
Route::get('/create', function(){
	App\User::create([
		'name' => 'Pradeep Kumar Jangid',
		'account' => 'Cogent404',
		'email' => 'jangir.40@gmail.com',
		'password' => bcrypt('jangir123'),
	]);
});
*/