<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';
	protected $primaryKey = 'state_id';
	protected $fillable = [
        'state_name', 'state_alias'
    ];
}
