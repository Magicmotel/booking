@extends('layouts.layout')

@section('title')
	TripAdvisor Channel Credentials
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>TripAdvisor Channel Credentials</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @elseif ($message = Session::get('danger'))
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <form id="demo-form2" action="{!!url('tripadvisor-channel-credential')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
               			{!!csrf_field()!!}
                        <input type="hidden" name="cm_id" id="cm_id" value="{!!$Results->cm_id or ''!!}">
                        <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Results->hotel_id or $Hotel_ID!!}">
                    	<div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">TripAdvisor Hotel ID <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6">
                            <input type="text" name="channel_hotel_id" id="channel_hotel_id" value="{!!$Results->channel_hotel_id or old('channel_hotel_id')!!}" placeholder="Tripadvisor Hotel ID" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Partner ID <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6">
                            <input type="text" name="channel_username" id="channel_username" value="{!!$Results->channel_username or old('channel_username')!!}" placeholder="Partner ID" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop24">
                            <div class="col-md-7 col-sm-7 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update Credential</button>
                                <button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script>
	function ConfirmAction(msg)
	{
		var x = confirm(msg);
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop      
