<?php

/**
 * This file contains functions for get single Transaction Details.
 * 
 * getTransactionDetails class  
 * 
 */

namespace App\Zeamsters\lib;

//include_once BASE_PATH . DS . 'includes/Logging.php';

use App\Zeamsters\lib\ZeamsterPaymentTransaction as zeamsterApi;
use App\Zeamsters\includes\Logging;

class getTransactionDetails {

    /**
     * Get / view single transaction details.
     * 
     * @param string $originalTransactionId
     * 
     * @return json
     */
    public static function getTransaction($originalTransactionId = null,$filter = null, $perPage = null,$sort = null,$pageNo =null) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            if (empty($originalTransactionId)) {
                if (!empty($perPage) || !empty($filter) || !empty($sort) || !empty($pageNo)) {
                    $request_url = $api_url . "/transactions?{$filter}&page_size={$perPage}&sort={$sort}&page={$pageNo}";
                } else {
                    $request_url = $api_url . "/transactions";
                }
            } else {

                $request_url = $api_url . "/transactions/{$originalTransactionId}?expand=is_refundable";
            }

            $response = $objzeamsterApi::send_transaction($request_url, null, null, null);

            return $response;
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

}
