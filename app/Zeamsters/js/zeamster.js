/* 
 * This file contains method to generate token / ticket using Zeamster API.
 * 
*/

function getToken(url,jsonData){
    
    var token;
    var jsonData = {
                    "location_id": jsonData.location_id,
                    "authorization": jsonData.authorization,
                    "timestamp": jsonData.timestamp,
                    "order_id": jsonData.order_id,
                    "billing_zip": jsonData.billing_zip,
                    "billing_street": jsonData.billing_street,
                    "card_number": jsonData.card_number,
                    "exp_date": jsonData.exp_date,
                    "cv": jsonData.cv
               }
               
     $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: "json",
                    data: jsonData,
                    async:false,
                    /**
                     * Success callback for token request.
                     * @param {Json} response
                     */
                    success: function (data, status, xhr) {
                        if (xhr.status === 200 || xhr.status === 201) {
                           token =  data.ticket;
                        } else if (xhr.status === 422) {
                            alert('Card Verification Failed');
                            return false;
                        } else {
                            alert('Card Verification Failed');
                            return false;
                        }
                    },
                    /**
                     * Error callback for token request.
                     * @param {*} response
                     */
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Zeamster Token Error " + errorThrown);
                       
                    }
                });
          return token;
}
