<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Guest;
use App\Roomtype;
use App\RoomAssign;
use App\RoomReservation;
use App\RoomReservationFolio;
use App\ReservationRoomAllot;
use App\ReservationRoomAllotPast;
use App\RoomReservationDiscount;
use App\CompanyProfile;
use App\CompanyProfileRate;
use App\BlockRoom;
use Helper;
use DB;

class Room {}
class Event {}
class Result {}

class FrontDeskController extends Controller
{
	public function front_desk()
    {
   		return view('frontdesk.frontdesk');
    }
	
	public function new_reservation(Request $request)
    {
		$from = date('m/d/Y', strtotime($request->from));
		$to   = date('m/d/Y', strtotime($request->to));
		$date1 = date_create($request->from);
		$date2 = date_create($request->to);
		$diff = date_diff($date1, $date2);
		$diff = date_diff($date1, $date2);
		$dayNum = $diff->format("%a");		
		return redirect("reservation/create?from=".$from."&to=".$to."&reserv_night=".$dayNum."&guest_company=0");
    }
	
	public function show_reservation(Request $request, $id)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		
		$RsrvRst = RoomReservation::where('room_reservation_id', $id)->first();
		$GstRst  = Guest::where('guest_id', $RsrvRst->guest_id)->first();
		return view('frontdesk.show')->with('RsrvRst', $RsrvRst)->with('GstRst', $GstRst);
    }
	
	public function edit_reservation(Request $request, $id)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		
		$RsrvRst = RoomReservation::where('room_reservation_id', $id)->first();
		$GstRst  = Guest::where('guest_id', $RsrvRst->guest_id)->first();
		return view('frontdesk.edit')->with('RsrvRst', $RsrvRst)->with('GstRst', $GstRst);
    }
	
	public function update_reservation(Request $request)
    {
		$Hotel_ID = Helper::getHotelId();
		$admin_ID = Helper::getAdminUserId();
		
		$rsrv_id       = $request->reservation_id;
		$ArrivalDate   = date("Y-m-d", strtotime($request->arrival));
		$DepartureDate = date("Y-m-d", strtotime($request->departure));
		$NightStay     = $request->reserv_night;
		$PriceStat     = $request->price_stat;
		$RoomAssignId  = $request->room_id;
		
		$RoomReservationRst = RoomReservation::where([ ['room_reservation_id', $rsrv_id], ['hotel_id', $Hotel_ID] ])->first();
		if(count($RoomReservationRst))
		{
			$rsrvTaxBool = $RoomReservationRst->reservation_tax_exmpt;
			$Crnt_ArrivalDate   = date("Y-m-d", strtotime($RoomReservationRst->arrival));
			$Crnt_DepartureDate = date("Y-m-d", strtotime($RoomReservationRst->departure));
			
			$fromDate = date('Y-m-d', strtotime($ArrivalDate));
			$toDate   = date('Y-m-d', strtotime($DepartureDate.'-1 days'));
			
			$toDay = date("Y-m-d");
			$fromDate = ($toDay >= $ArrivalDate)?$toDay:$ArrivalDate;
			
			$Reservation_ID = $RoomReservationRst->room_reservation_id;
			$profile_ID     = $RoomReservationRst->guest_profile_id;
			if($RoomAssignId){
				$RoomTypeRst   = RoomAssign::where([ ['room_assign_id', $RoomAssignId], ['hotel_id', $Hotel_ID] ])->first();
				$room_typeId   = $RoomTypeRst->room_type_id;
				$room_typeId_C = $RoomReservationRst->room_type_id;
				
				$RoomAssignRst  = ReservationRoomAllot::where([ ['room_reservation_id', $Reservation_ID], ['hotel_id', $Hotel_ID] ])->first();
				$RoomAssignId_C = $RoomAssignRst->room_assign_id;
			}else{
				$room_typeId   = $RoomReservationRst->room_type_id;
				$room_typeId_C = $RoomReservationRst->room_type_id;
				
				$RoomAssignRst = ReservationRoomAllot::where([ ['room_reservation_id', $Reservation_ID], ['hotel_id', $Hotel_ID] ])->first();
				$RoomAssignId   = $RoomAssignRst->room_assign_id;
				$RoomAssignId_C = $RoomAssignRst->room_assign_id;
			}
		
			if($ArrivalDate == $Crnt_ArrivalDate && $DepartureDate == $Crnt_DepartureDate && $RoomAssignId_C == $RoomAssignId){
				return response()->json(['message' => 'No changes found in Request.', 'result' => 'OK', 'status' => 'error']);
			}
			else
			{
				$varQry  = " AND NOT ((RSV.departure <= '".$ArrivalDate."') OR (RSV.arrival >= '".$DepartureDate."'))";
				$varQry1 = " AND NOT ((BR.block_to <= '".$ArrivalDate."') OR (BR.block_from >= '".$DepartureDate."'))";
								 
				$vcntRoom_Qry = DB::select("SELECT R1.room_assign_id as RoomId, R1.room_number as RoomNumber, R1.room_stat as RoomStat,
											R1.room_type_id as RoomTypeId, CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomType
											FROM room_assign as R1 INNER JOIN roomtypes RT ON RT.room_type_id = R1.room_type_id
											WHERE R1.hotel_id = ".$Hotel_ID." AND R1.status = 1 AND R1.trash = 0 AND
											R1.room_type_id = ".$room_typeId." AND R1.room_assign_id = ".$RoomAssignId." AND 
											R1.room_assign_id NOT IN(
												SELECT R.room_assign_id
												FROM room_reservation as RSV
												INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = RSV.room_reservation_id
												INNER JOIN room_assign as R ON R.room_assign_id = RRA.room_assign_id
												WHERE R.room_type_id = ".$room_typeId." AND RSV.hotel_id = ".$Hotel_ID.$varQry." AND
												RSV.room_reservation_id != ".$Reservation_ID." AND RSV.status != 'Checked out' AND RSV.status != 'Cancelled'
											) AND
											R1.room_assign_id NOT IN(
												SELECT BR.room_assign_id
												FROM block_rooms as BR
												WHERE BR.room_type_id = ".$room_typeId." AND BR.hotel_id = ".$Hotel_ID.$varQry1."
											)
											ORDER BY R1.room_number ASC");
				if(count($vcntRoom_Qry))
				{
					// RESERVATION MODIFICATION
					if($ArrivalDate > $Crnt_ArrivalDate)
					{
						DB::select("DELETE FROM room_reservation_folio
									WHERE folio_date < '".$ArrivalDate."' AND hotel_id = ".$Hotel_ID." AND room_reservation_id = ".$Reservation_ID." AND folio_order > 0");
					}
					if($DepartureDate < $Crnt_DepartureDate)
					{
						DB::select("DELETE FROM room_reservation_folio
									WHERE folio_date >= '".$DepartureDate."' AND hotel_id = ".$Hotel_ID." AND room_reservation_id = ".$Reservation_ID." AND folio_order > 0");
					}
	
					$room_Rate = 0;
					if($profile_ID)
					{
						$CompanyPrice = CompanyProfileRate::where([ ['hotel_id', $Hotel_ID], ['room_type_id', $room_typeId], ['profile_id', $profile_ID] ])->first();
						$room_Rate	  = $CompanyPrice->company_rate;
					}
					
					$discountValues = "";
					$discountIdArray = RoomReservationDiscount::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $Reservation_ID] ])->get();
					if(count($discountIdArray))
					{
						$discountValuesArray = array(); 
						foreach($discountIdArray as $discountId){
							$discountValuesArray[] = $discountId->discount_id;
						}
						$discountValues = implode(",", $discountValuesArray);
					}
					
					$folioRateQry = DB::select("SELECT room_rate_price as fRoomRate, room_rate_date as fRoomDate FROM room_rate_option
												WHERE room_type_id = ".$room_typeId." AND hotel_id = ".$Hotel_ID." AND
												(room_rate_date BETWEEN '".$fromDate."' AND '".$toDate."') ORDER BY room_rate_date ASC
											   ");
					foreach($folioRateQry as $folioRateRst)
					{
						$folio_Data  = $DiscountCodeString = ""; $discountWiseRate = 0;
						$dayWiseRate = ($room_Rate > 0)?$room_Rate:$folioRateRst->fRoomRate;
						$folio_Data  = $dayWiseRate;
						$dayWiseDate = $folioRateRst->fRoomDate;
						
						$RsrvFolioRst = RoomReservationFolio::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $Reservation_ID], ['folio_date', $dayWiseDate] ])->first();
						if(count($RsrvFolioRst))
						{
							if($discountValues)
							{
								$discountQry = DB::select("SELECT discount_id as DiscountId, discount_code as DiscountCode, discount_value as DiscountValue,
														   discount_type as DiscountType
														   FROM discounts
														   WHERE hotel_id = ".$Hotel_ID." AND discount_id IN (".$discountValues.")
														   ORDER BY discount_code ASC
														  ");
								$discountWiseRate = 0; $DiscountCodeArray = array();
								foreach($discountQry as $discountRst)
								{
									if($discountRst->DiscountType == 2){
										$discountWiseRate = $discountWiseRate+$discountRst->DiscountValue;
									}else{
										$discountWiseRate = $discountWiseRate+(($dayWiseRate*$discountRst->DiscountValue)/100);
									}
									$DiscountCodeArray[] = $discountRst->DiscountCode;
								}
								$DiscountCodeString = implode(", ",$DiscountCodeArray);
								$discountWiseRate = round($discountWiseRate, 2);
							}
							$FinalRoomRate_wo_Tax = $dayWiseRate - $discountWiseRate;
							if($FinalRoomRate_wo_Tax<=0){
								$FinalRoomRate_wo_Tax = 1;
							}
							
							$FolioRst = RoomReservationFolio::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $Reservation_ID],
																  	  ['folio_date', $dayWiseDate], ['folio_pay_type', 'Room Charge'] ])->first();
							$FolioRst->room_type_id	 = $room_typeId;
							if($PriceStat == 1){
							$FolioRst->folio_amount	 = $FinalRoomRate_wo_Tax;
							$FolioRst->folio_data	 = $folio_Data;
							$FolioRst->folio_comment = $DiscountCodeString;
							}
							$FolioRst->reg_ip 		 = $_SERVER['REMOTE_ADDR'];
							$FolioRst->added_by 	 = $admin_ID;
							$FolioRst->save();
							
							if($rsrvTaxBool == 0)
							{
								$folioTaxQry = DB::select("SELECT tax_type as fTaxType, tax_value as fTaxValue, tax_value_type as fValueType
														   FROM taxes WHERE hotel_id = ".$Hotel_ID." AND status = 0 AND
														   NOT( (tax_end != '0000-00-00' AND tax_end < '".$dayWiseDate."') OR
																(tax_start != '0000-00-00' AND tax_start >= '".$dayWiseDate."') OR
																(tax_end = '0000-00-00' AND tax_start > '".$dayWiseDate."'AND tax_start < '".$dayWiseDate."') OR
																(tax_start = '0000-00-00' AND tax_end < '".$dayWiseDate."' AND tax_end > '".$dayWiseDate."')
															  )
														  ");
								foreach($folioTaxQry as $folioTaxRst){
									
									$dayTaxAmount = "";
									if($folioTaxRst->fValueType == 1){
										$dayTaxAmount = round((($FinalRoomRate_wo_Tax*$folioTaxRst->fTaxValue)/100), 2);
										$folio_Data = $folioTaxRst->fTaxValue."%";
									}elseif($folioTaxRst->fValueType == 2){
										$dayTaxAmount = $dayTaxAmount+( round($folioTaxRst->fTaxValue, 2) );
										$folio_Data = $folioTaxRst->fTaxValue;
									}
									
									$FoliosRst = RoomReservationFolio::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $Reservation_ID],
																			  ['folio_date', $dayWiseDate], ['folio_pay_type', $folioTaxRst->fTaxType] ])->first();
									$FoliosRst->room_type_id = $room_typeId;
									if($PriceStat == 1){									  
									$FoliosRst->folio_amount = $dayTaxAmount;
									$FoliosRst->folio_data	 = $folio_Data;
									}
									$FoliosRst->reg_ip 		 = $_SERVER['REMOTE_ADDR'];
									$FoliosRst->added_by 	 = $admin_ID;
									$FoliosRst->save();
								}
							}
						}
						else
						{
							if($discountValues)
							{
								$discountQry = DB::select("SELECT discount_id as DiscountId, discount_code as DiscountCode, discount_value as DiscountValue,
														   discount_type as DiscountType
														   FROM discounts
														   WHERE hotel_id = ".$Hotel_ID." AND discount_id IN (".$discountValues.")
														   ORDER BY discount_code ASC
														  ");
								$discountWiseRate = 0; $DiscountCodeArray = array();
								foreach($discountQry as $discountRst){
									if($discountRst->DiscountType == 2){
										$discountWiseRate = $discountWiseRate+$discountRst->DiscountValue;
									}else{
										$discountWiseRate = $discountWiseRate+(($dayWiseRate*$discountRst->DiscountValue)/100);
									}
									$DiscountCodeArray[] = $discountRst->DiscountCode;
								}
								$DiscountCodeString = implode(", ",$DiscountCodeArray);
								$discountWiseRate = round($discountWiseRate, 2);
							}
							
							$FinalRoomRate_wo_Tax = $dayWiseRate - $discountWiseRate;
							if($FinalRoomRate_wo_Tax<=0){
								$FinalRoomRate_wo_Tax = 1;
							}
							
							$ReservationFolioArray   = array();
							$ReservationFolioArray[] = $Hotel_ID;
							$ReservationFolioArray[] = $room_typeId;
							$ReservationFolioArray[] = $Reservation_ID;
							$ReservationFolioArray[] = $dayWiseDate;
							$ReservationFolioArray[] = "Room Charge";
							$ReservationFolioArray[] = $FinalRoomRate_wo_Tax;
							$ReservationFolioArray[] = $folio_Data;
							$ReservationFolioArray[] = $DiscountCodeString;
							$ReservationFolioArray[] = 1;
							$ReservationFolioArray[] = $_SERVER['REMOTE_ADDR'];
							$ReservationFolioArray[] = $admin_ID;
							
							DB::select('CALL Reservation_Folio_Insert_SP(?,?,?,?,?,?,?,?,?,?,?)', $ReservationFolioArray);
						
							if($rsrvTaxBool == 0)
							{
								$folioTaxQry = DB::select("SELECT tax_type as fTaxType, tax_value as fTaxValue, tax_value_type as fValueType
														   FROM taxes WHERE hotel_id = ".$Hotel_ID." AND status = 0 AND
														   NOT( (tax_end != '0000-00-00' AND tax_end < '".$dayWiseDate."') OR
																(tax_start != '0000-00-00' AND tax_start >= '".$dayWiseDate."') OR
																(tax_end = '0000-00-00' AND tax_start > '".$dayWiseDate."'AND tax_start < '".$dayWiseDate."') OR
																(tax_start = '0000-00-00' AND tax_end < '".$dayWiseDate."' AND tax_end > '".$dayWiseDate."')
															  )
														  ");
								$SJ = 2;
								foreach($folioTaxQry as $folioTaxRst){
									$folio_Data = "";
									$dayTaxAmount = "";
									if($folioTaxRst->fValueType == 1){
										$dayTaxAmount = round((($FinalRoomRate_wo_Tax*$folioTaxRst->fTaxValue)/100), 2);
										$folio_Data = $folioTaxRst->fTaxValue."%";
									}elseif($folioTaxRst->fValueType == 2){
										$dayTaxAmount = $dayTaxAmount+( round($folioTaxRst->fTaxValue, 2) );
	
										$folio_Data = $folioTaxRst->fTaxValue;
									}
									
									$ReservationFolioTArray   = array();
									$ReservationFolioTArray[] = $Hotel_ID;
									$ReservationFolioTArray[] = $room_typeId;
									$ReservationFolioTArray[] = $Reservation_ID;
									$ReservationFolioTArray[] = $dayWiseDate;
									$ReservationFolioTArray[] = $folioTaxRst->fTaxType;
									$ReservationFolioTArray[] = $dayTaxAmount;
									$ReservationFolioTArray[] = $folio_Data;
									$ReservationFolioTArray[] = "";
									$ReservationFolioTArray[] = $SJ;
									$ReservationFolioTArray[] = $_SERVER['REMOTE_ADDR'];
									$ReservationFolioTArray[] = $admin_ID;
									
									DB::select('CALL Reservation_Folio_Insert_SP(?,?,?,?,?,?,?,?,?,?,?)', $ReservationFolioTArray);
									
									$SJ++;
								}
							}
						}
					}
					
					$RoomReservationRst->room_type_id = $room_typeId;
					$RoomReservationRst->arrival = $ArrivalDate;
					$RoomReservationRst->departure = $DepartureDate;
					$RoomReservationRst->reserv_night = $NightStay;
					$RoomReservationRst->reg_ip = $_SERVER['REMOTE_ADDR'];
					$RoomReservationRst->added_by = $admin_ID;
					$RoomReservationRst->save();
					
					DB::select("UPDATE room_reservation_folio SET
								room_type_id = '".$room_typeId."', reg_ip = '".$_SERVER['REMOTE_ADDR']."', added_by = '".$admin_ID."'
								WHERE room_reservation_id = ".$Reservation_ID." AND hotel_id = ".$Hotel_ID."");
					
					if($RoomAssignId_C != $RoomAssignId){
						$pastAllot = new ReservationRoomAllotPast;
						$pastAllot->hotel_id = $Hotel_ID;
						$pastAllot->room_reservation_id = $Reservation_ID;
						$pastAllot->p_room_assign_id = $RoomAssignRst->room_assign_id;
						$pastAllot->room_assign_id   = $RoomAssignId;
						$pastAllot->reg_ip = $_SERVER['REMOTE_ADDR'];
						$pastAllot->added_by = $admin_ID;
						$pastAllot->save();
							
						$RoomAssignRst->room_assign_id = $RoomAssignId;
						$RoomAssignRst->reg_ip   = $_SERVER['REMOTE_ADDR'];
						$RoomAssignRst->added_by = $admin_ID;
						$RoomAssignRst->save();
					}
					
					return response()->json(['message' => 'Reservation# '.$Reservation_ID.' updated Successfully', 'result' => 'OK', 'status' => 'success']);
				}else{
					return response()->json(['message' => 'Room not available for Booking.', 'result' => 'OK', 'status' => 'error']);
				}
			}
		}else{
			return response()->json(['message' => 'Reservation not found in this Motel.', 'result' => 'OK', 'status' => 'error']);
		}
	}
	
	public function cancel_reservation(Request $request)
    {
       	$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		$RsrvId = $request->room_reservation_id;
		
		$Results = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $RsrvId] ])->first();
		if(count($Results))
		{
			$ArrivalDate   = $Results->arrival;
			$DepartureDate = $Results->departure;
			$cRoomTypeId   = $Results->room_type_id;
		
			$Results->status = 'Cancelled';
			$Results->reserv_reason = $request->rsrv_comment;
			$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
			$Results->added_by = $admin_ID;
			$Results->save(); 
			
			$Helper->ChannelPartnerRequest($Hotel_ID, $cRoomTypeId, $ArrivalDate, $DepartureDate);
			
			return response()->json(['message' => 'Reservation# '.$RsrvId.' cancelled successfully.', 'result' => 'OK', 'status' => 'success']);
		}else{
			return response()->json(['message' => 'Reservation# not found.', 'result' => 'false', 'status' => 'error']);
		}
	}
	
	public function unblock_room(Request $request)
    {
       	$Helper   = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		$Block_ID = $request->block_room_id;
		
		$Results = BlockRoom::where([ ['hotel_id', $Hotel_ID], ['block_room_id', $Block_ID] ])->first();
		if(count($Results))
		{
			$ArrivalDate   = $Results->block_from;
			$DepartureDate = $Results->block_to;
			$RoomTypeId    = $Results->room_type_id;
		
			$Result = BlockRoom::destroy($Block_ID);
			
			$Helper->ChannelPartnerRequest($Hotel_ID, $RoomTypeId, $ArrivalDate, $DepartureDate);

			return response()->json(['message' => 'Room unblocked successfully.', 'result' => 'OK', 'status' => 'success']);
		}else{
			return response()->json(['message' => 'Record not found.', 'result' => 'false', 'status' => 'error']);
		}
	}
	
    public function backend_rooms(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$Results  = DB::select("SELECT RT.room_type_id, RT.room_type, RA.room_assign_id, RA.room_number, RA.room_stat, RA.status
								FROM roomtypes as RT INNER JOIN room_assign as RA ON RA.room_type_id = RT.room_type_id
								WHERE RT.hotel_id = '".$Hotel_ID."' AND RA.room_number != ''
								ORDER BY RT.room_type ASC, RA.room_number ASC");
		
		$result = array();
		foreach($Results as $room)
		{
		 	$r = new Room();
		  	$r->id = $room->room_assign_id;
		  	$r->name = $room->room_number;
		  	$r->room_type = $room->room_type;
			$r->room_type_id = $room->room_type_id;
		  	$r->status = ($room->status == 0)?'X':(($room->room_stat == 1)?'C':'D');
		  	$result[] = $r;
		}
		
		header('Content-Type: application/json');
		echo json_encode($result);
	}
	
	public function backend_events(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		
		$from = date('Y-m-d', strtotime($request->start));
		$to   = date('Y-m-d', strtotime($request->end));
		
		$events = array();
		
		$Results  = DB::select("SELECT R.room_reservation_id, R.arrival, R.departure, R.status, R.reservation_locked,
								RA.room_assign_id, RA.room_type_id, G.guest_fname, G.guest_lname, RA.status as RoomStat
								FROM room_reservation as R INNER JOIN reservation_room_allot as RRA ON RRA.room_reservation_id = R.room_reservation_id
								INNER JOIN room_assign as RA ON RA.room_assign_id = RRA.room_assign_id
								INNER JOIN guest as G ON G.guest_id = R.guest_id
								WHERE R.hotel_id = '".$Hotel_ID."' AND NOT ((departure <= '".$from."') OR (arrival >= '".$to."')) AND R.status != 'Cancelled'
								AND RA.room_number != ''");
		foreach($Results as $row)
		{
			$e = new Event();
			$e->id = $row->room_reservation_id;
			$e->text = $row->guest_fname." ".$row->guest_lname;
			$e->start = $row->arrival." 12:00:00";
			$e->end = $row->departure." 12:00:00";
			$e->resource = $row->room_assign_id;
			$e->room_type_id = $row->room_type_id;
			$e->rsrvlock = $row->reservation_locked;
			$e->bubbleHtml = "Reservation# ".$e->id."\n".$e->text;;
			
			// additional properties
			$e->status = $row->status;
			$e->paid = 0;
			$events[] = $e;
		}
		
		$Results  = DB::select("SELECT RB.block_room_id, RB.block_from, RB.block_to, RB.room_assign_id, RB.room_type_id
								FROM block_rooms as RB INNER JOIN room_assign as RA ON RA.room_assign_id = RB.room_assign_id
								WHERE RB.hotel_id = '".$Hotel_ID."' AND NOT ((RB.block_to <= '".$from."') OR (RB.block_from >= '".$to."'))");
		foreach($Results as $row)
		{
			$e = new Event();
			$e->id = $row->block_room_id;
			$e->text = 'Blocked';
			$e->start = $row->block_from." 12:00:00";
			$e->end = $row->block_to." 12:00:00";
			$e->resource = $row->room_assign_id;
			$e->room_type_id = $row->room_type_id;
			$e->rsrvlock = 1;
			$e->bubbleHtml = "Room ".$e->text;
			$e->status = 'Blocked';
			$e->paid = 0;
			$events[] = $e;
		}
		
		header('Content-Type: application/json');
		echo json_encode($events);
	}
	
	public function create()
	{
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		
        return view('company.company-create')->with("Hotel_ID", $Hotel_ID);
	}

    public function store(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		if($Hotel_ID == $request->hotel_id)
		{
			$RequestArray = array();
			$RequestArray[] = $Hotel_ID;
			$RequestArray[] = $request->company_name;
			$RequestArray[] = $request->company_address;
			$RequestArray[] = $request->company_zip;
			$RequestArray[] = $request->company_city;
			$RequestArray[] = $request->company_state;
			$RequestArray[] = $request->company_lang;
			$RequestArray[] = $request->company_email;
			$RequestArray[] = $request->company_phone;
			$RequestArray[] = $request->company_fax;
			$RequestArray[] = $request->contact_name;
			$RequestArray[] = $request->contact_title;
			$RequestArray[] = $request->company_notes;
			$RequestArray[] = ($request->company_tax)?$request->company_tax:0;
			$RequestArray[] = $_SERVER['REMOTE_ADDR'];
			$RequestArray[] = $admin_ID;
			
			$Results = DB::select('CALL CompanyProfile_Insert_SP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $RequestArray);
			$CPIdArray = DB::select('SELECT LAST_INSERT_ID()');
			foreach(json_decode(json_encode($CPIdArray), true)[0] as $val){
				$CompanyProfileId = $val;
			}
			
			foreach($request->room_type_id as $key=>$vals)
			{
				$RequestArray = array();	
				$RequestArray[] = $Hotel_ID;
				$RequestArray[] = $CompanyProfileId;
				$RequestArray[] = $vals;
				$RequestArray[] = $request->company_rate[$key];
				$RequestArray[] = $_SERVER['REMOTE_ADDR'];
				$RequestArray[] = $admin_ID;
				DB::select('CALL CompanyProfileRate_Insert_SP (?,?,?,?,?,?)', $RequestArray);
			}
			return redirect('company-profile')->with('success','Company Profile added successfully.');
		}else{
			return redirect('company-profile')->with("danger", "Please select your Motel");
		}
    }
	
	public function edit($id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		
		$Results  = CompanyProfile::where([ ['hotel_id', $Hotel_ID], ['profile_id', $id] ])->first();
		$rateResults  = CompanyProfileRate::where([ ['hotel_id', $Hotel_ID], ['profile_id', $id] ])->get();
		$rateOutPut = array();
		foreach($rateResults as $rateVal){
			$rateOutPut[$rateVal->room_type_id] = $rateVal->company_rate;
		}
		if($Results->hotel_id == $Hotel_ID){
			return view('company.company-edit')->with("Hotel_ID", $Hotel_ID)->with("Results", $Results)->with("rateOutPut", $rateOutPut);
		}else{
			return redirect('company-profile')->with("danger", "Please select your Motel");
		}
    }
	
	 public function update(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		if($Hotel_ID == $request->hotel_id)
		{
			$profileOBJ = CompanyProfile::where([ ['hotel_id', $Hotel_ID], ['profile_id', $id] ])->first();
			$profileOBJ->hotel_id = $Hotel_ID;
			$profileOBJ->company_name = $request->company_name;
			$profileOBJ->company_address = $request->company_address;
			$profileOBJ->company_zip = $request->company_zip;
			$profileOBJ->company_city = $request->company_city;
			$profileOBJ->company_state = $request->company_state;
			$profileOBJ->company_lang = $request->company_lang;
			$profileOBJ->company_email = $request->company_email;
			$profileOBJ->company_phone = $request->company_phone;
			$profileOBJ->company_fax = $request->company_fax;
			$profileOBJ->contact_name = $request->contact_name;
			$profileOBJ->contact_title = $request->contact_title;
			$profileOBJ->company_notes = $request->company_notes;
			$profileOBJ->company_tax = ($request->company_tax)?$request->company_tax:0;
			$profileOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
			$profileOBJ->added_by = $admin_ID;
			$profileOBJ->save();
			
			foreach($request->room_type_id as $key=>$vals)
			{
				$rateOBJ = CompanyProfileRate::where([ ['hotel_id', $Hotel_ID], ['profile_id', '=', $id], ['room_type_id', '=', $vals] ])->first();
				if(count($rateOBJ)){
					$rateOBJ->hotel_id = $Hotel_ID;
					$rateOBJ->profile_id = $id;
					$rateOBJ->room_type_id = $vals;
					$rateOBJ->company_rate = $request->company_rate[$key];
					$rateOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
					$rateOBJ->added_by = $admin_ID;
					$rateOBJ->save();
				}else{
					$RequestArray = array();	
					$RequestArray[] = $Hotel_ID;
					$RequestArray[] = $id;
					$RequestArray[] = $vals;
					$RequestArray[] = $request->company_rate[$key];
					$RequestArray[] = $_SERVER['REMOTE_ADDR'];
					$RequestArray[] = $admin_ID;
					DB::select('CALL CompanyProfileRate_Insert_SP (?,?,?,?,?,?)', $RequestArray);
				}
			}		
			return redirect('company-profile')->with('success','Company Profile updated successfully.');
		}else{
			return redirect('company-profile')->with("danger", "Please select your Motel");
		}
    }
	
	public function getCityData(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$zipCode = $request->zipCode;
		
		if($request->ajax()) {
			$Results = ZipCityState::where('zip_code', $zipCode)->first();
			if(count($Results)){
				return response()->json(['response' => "City, State get successfully", 'status' => 'success',
										 'zip_city' => $Results->zip_city, 'zip_state' => $Results->zip_state
										]);
			}else{
				return response()->json(['response' => "Zip Code not found in Table", 'status' => 'error',
										 'zip_city' => '', 'zip_state' => ''
										]);
			}
		}else{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
}