<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZeamsterLog extends Model
{
    protected $table = 'zeamster_log';
	protected $primaryKey = 'log_id';
	protected $fillable = [
        'hotel_id', 'transaction_id', 'payment_method', 'account_vault_id', 'recurring_id', 'first_six', 'last_four', 'account_holder_name', 'transaction_amount',
		'description', 'transaction_code', 'avs', 'batch', 'order_num', 'verbiage', 'transaction_settlement_status', 'effective_date', 'routing', 'return_date',
		'created_ts', 'modified_ts', 'transaction_api_id', 'terms_agree', 'notification_email_address', 'notification_email_sent', 'response_message', 'auth_amount',
		'auth_code', 'status_id', 'type_id', 'location_id', 'reason_code_id', 'contact_id', 'billing_zip', 'billing_street', 'product_transaction_id', 'tax', 'customer_ip',
		'customer_id', 'po_number', 'avs_enhanced', 'cvv_response', 'billing_phone', 'billing_city', 'billing_state', 'clerk_number', 'batch_close_ts', 'settle_date',
		'charge_back_date', 'void_date', 'account_type', 'is_recurring', 'is_accountvault', 'transaction_c1', 'transaction_c2', 'transaction_c3', 'terminal_serial_number',
		'entry_mode_id', 'terminal_id', 'checkin_date', 'checkout_date', 'room_num', 'room_rate', 'advance_deposit', 'no_show', 'emv_receipt_data', 'links',
		'reg_ip', 'added_by'
    ];
}