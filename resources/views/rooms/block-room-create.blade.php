@extends('layouts.layout')

@section('title')
	Create Room Block
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Create Room Block</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('block-room')}}">Manage Blocked Rooms</a></li>
                <li class="active"><a href="javascript:void(0);">Create Room Block</a></li>
            </ul>
        </div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                    <form id="demo-form2" action="{!!url('block-room')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
               			{!!csrf_field()!!}
                    	<input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Room Type <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3 col-xs-12 LR_Padd5">
                            	{{--*/
                                $roomRst = DB::select("SELECT RT.room_type_id as RoomTypeId, RT.room_type as RoomType,
                                					   CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomCode
                                                       FROM roomtypes as RT WHERE RT.hotel_id = ".$Hotel_ID." AND RT.trash = 0 ORDER BY RT.room_type ASC");
                                /*--}}
                                <select name="room_type_id" id="room_type_id" class="form-control col-md-3 col-xs-6 LR_Padd5 uPPerLetter" required="required">
                                    <option value="">Select Room Type</option>
                                    @foreach( $roomRst as $val )
                                    <option value="{!!$val->RoomTypeId!!}" class="uPPerLetter"{!!($request->RoomTypeSelectedId == $val->RoomTypeId)?" Selected":""!!}>{!!$val->RoomType!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Block Date <span class="required">*</span></label>
                            <div class="col-md-2 LR_Padd5">
                                <input type="text" name="block_from" id="block_from" class="form-control has-feedback-left" placeholder="From" value="" required="required" readonly="readonly">
                                <span class="fa fa-calendar-o form-control-feedback left" style="left:5px;" aria-hidden="true"></span>
                            </div>
                            <div class="fltL control-value LR_Padd5">to</div>
                            <div class="col-md-2 LR_Padd5">
                                <input type="text" name="block_to" id="block_to" class="form-control has-feedback-left" placeholder="To" value="" required="required" readonly="readonly">
                                <span class="fa fa-calendar-o form-control-feedback left" style="left:5px;" aria-hidden="true"></span>
                            </div>
                        </div>
                        <div class="form-group displayNone" id="datevalueofRoom">
                            
                        </div>
                    	<div class="form-group form-group-last marginTop20">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 LR_Padd5">
                                <button type="submit" class="btn btn-danger"><i class="fa fa-ban"></i> Block Room</button>
                                <button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                    	</div>                    
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function FilterSource(str){
		if(str){
			RedirectStr = '?';
			if(str){
				RedirectStr += "RoomTypeSelectedId="+str+"&";
			}
			RedirectStr = RedirectStr.replace(/&+$/,'');
			window.location.href="{!!url('block-room/create')!!}"+RedirectStr;
		}else if(!str){
			window.location.href="{!!url('block-room/create')!!}";
		}
	}
</script>
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var checkin = $('#block_from').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		var newDate = new Date(ev.date);
		newDate.setDate(newDate.getDate() );
		checkout.setValue(newDate);
		checkin.hide();
		getListofRoom();
		$('#block_to')[0].focus();
	}).data('datepicker');
	
	var checkout = $('#block_to').datepicker({
		onRender: function(date) {
			return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		checkout.hide();
		getListofRoom();
	}).data('datepicker');
	
	$("#room_type_id").change(function(){		
		getListofRoom();
	});
	
	function getListofRoom(){
		var RoomType  = $("#room_type_id").val();		
		var BlockFrom = $("#block_from").val();
		var BlockTo   = $("#block_to").val();
		if(RoomType && BlockFrom && BlockTo){
			$("#main_container_Loading").show();
			$("#main_container_overlay").show();
			$.ajax({
				url: "{{ url('/getRoomListBlock') }}",
				type: 'POST',
				data:{"RoomType":RoomType,"BlockFrom":BlockFrom,"BlockTo":BlockTo, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				async: 'true',
				success: function( msg ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( msg.status === 'success' ) {
						$('#datevalueofRoom').show();
						$('#datevalueofRoom').html(msg.response);
					}
					if ( msg.status === 'error' ) {
					}
				},
				error: function( data ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( data.status === 422 ) {
						toastr.error('Cannot delete the Image');
					}
				}
			});
			return false;
		}
	}
});
</script>
@stop