<?php

/**
 * This file contains functions related to create,update, view  and delete 
 * for Locations endpoints.
 * 
 * Locations class 
 */

namespace App\Zeamsters\lib;

//include_once BASE_PATH . DS . 'includes/Logging.php';

use App\Zeamsters\lib\ZeamsterPaymentTransaction as zeamsterApi;
use Exception;
use App\Zeamsters\includes\Logging;

class Locations extends Exception {

    public $id = "";
    public $aba = "";
    public $account_number = "";
    public $address1 = "";
    public $address2 = "";

    /**
     * @var string $branding_domain_url(Required) Zeamster API to use for requests.
     */
    public $branding_domain_url = "";
    public $city = "";
    public $created_ts = "";
    public $dda = "";
    public $fax = "";
    public $location_api_id = "";
    public $location_api_key = "";
    public $modified_ts = "";

    /**
     * @var string $name(Required) Zeamster API to use for requests.
     */
    public $name = "";
    public $office_phone = "";
    public $office_ext_phone = "";

    /**
     * @var string $parent_id(Required) Zeamster API to use for requests.
     */
    public $parent_id = "";
    public $receipt_logo = "";
    public $state = "";
    public $ticket_hash_key = "";
    public $tz = "";
    public $zip_code = "";

    /**
     * Create new location record.
     * 
     * @params object $objData   
     * 
     * @return json 
     */
    public static function createLocation($objData) {
        $request_data = zeamsterApi::convertObjectToArray($objData);
        try {
            if (!empty($request_data)) {
                $objzeamsterApi = zeamsterApi::Instance();
                $api_url = $objzeamsterApi->get_gateway_url();
                $request_url = $api_url . '/locations';
                $method = 'POST';

                if (!empty($request_data)) {
                    $request = array('location' => array_filter($request_data));
                }
                $request = json_encode($request);

                $response = $objzeamsterApi::send_transaction($request_url, $method, null, $request);

                return $response;
            }
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Get / view location record.
     * 
     * @params string $location_id   
     * 
     * @return json 
     */
    public static function getLocation($location_id = null, $filter = null, $perPage = null, $sort = null, $pageNo = null) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            if (!empty($location_id)) {
                $request_url = $api_url . "/locations/{$location_id}";
            } else {
                if (!empty($perPage) || !empty($filter) || !empty($sort) || !empty($pageNo)) {
                    $request_url = $api_url . "/locations?{$filter}&page_size={$perPage}&sort={$sort}&page={$pageNo}";
                } else {
                    $request_url = $api_url . "/locations";
                }
            }
            $method = 'GET';

            $response = $objzeamsterApi::send_transaction($request_url, $method, null, null);

            return $response;
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Update location record
     * 
     * @params object $objData
     * @params string $location_id
     *  
     * @return  json
     */
    public static function updateLocation($objData, $location_id) {
        try {
            $request_data = zeamsterApi::convertObjectToArray($objData);
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/locations/{$location_id}";
            $method = 'PUT';
            if (!empty($request_data)) {
                $request = array('location' => array_filter($request_data));
            }
            $request = json_encode($request);

            $response = $objzeamsterApi::send_transaction($request_url, $method, null, $request);

            return $response;
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Delete location record
     * 
     * @params string $location_id   
     * 
     * @return json
     */
    public static function deleteLocation($location_id) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/locations/{$location_id}";

            $method = 'DELETE';

            $response = $objzeamsterApi::send_transaction($request_url, $method);

            return $response;
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

}
