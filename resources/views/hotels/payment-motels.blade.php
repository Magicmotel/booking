@extends('layouts.layout')

@section('title')
	Motel Payment Information
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Motel Payment Information</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	@if (count($errors))
                <ul class="errorFormMessage">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @elseif($message = Session::get('danger'))
                <div class="alert alert-danger">
                    <p>{{ $message }}</p>
                </div>
            @endif
            
        	<div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <span class="section">CREDIT CARD:</span>
                            <div class="form-group">
                                <label class="control-label col-md-5 col-sm-5">Cardholder - First Name:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->cc_fname!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5 col-sm-5">Cardholder - Middle Name:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->cc_mname!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-col-md-5 col-sm-5">Cardholder - Last Name:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->cc_lname!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-col-md-5 col-sm-5">Card Number:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->cc_card_no!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-col-md-5 col-sm-5">Expiration Date (MM/YY):</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->cc_expiry!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-col-md-5 col-sm-5">CVV Code (3 digit code):</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->cc_cvv!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-col-md-5 col-sm-5">Billing Address:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!nl2br($Results->cc_address)!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-col-md-5 col-sm-5">Billing City, State:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->cc_city!!}</div>
                            </div>
                            <div class="form-group form-group-last">
                                <label class="control-label col-col-md-5 col-sm-5">Billing Zip Code:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->cc_zip!!}</div>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
            
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <span class="section">BANK ACCOUNT:</span>
                            <div class="form-group">
                                <label class="control-label col-md-5 col-sm-5">BA Holder - First Name:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->ba_fname!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5 col-sm-5">BA Holder - Middle Name:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->ba_mname!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-col-md-5 col-sm-5">BA Holder - Last Name:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->ba_lname!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-col-md-5 col-sm-5">Bank Name:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->ba_bankname!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-col-md-5 col-sm-5">Bank Account Number:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->ba_account!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-col-md-5 col-sm-5">Routing Number:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->ba_routing!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-col-md-5 col-sm-5">Billing Address:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!nl2br($Results->ba_address)!!}</div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-col-md-5 col-sm-5">Billing City, State:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->ba_city!!}</div>
                            </div>
                            <div class="form-group form-group-last">
                                <label class="control-label col-col-md-5 col-sm-5">Billing Zip Code:</label>
                                <div class="control-value col-md-7 col-sm-7">{!!$Results->ba_zip!!}</div>
                            </div>
                        </div>
                    </div>
                </div>
           	</div>
            
            <div class="col-md-12 col-sm-12 col-xs-12">     
                <div class="x_panel">
                    <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <span class="section">PAYPAL:</span>
                            <div class="form-group form-group-last">
                                <label class="control-label col-md-4 col-sm-4 AlgnLeft">Link:</label>
                                <div class="control-value col-md-8 col-sm-8">{!!$Results->paypal_link!!}</div>
                            </div>
                        </div>
                    </div>
                </div>
        	</div>
            
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <div class="form-horizontal form-label-left">
                    	<div class="form-group form-group-last">
                            <div class="col-md-2 col-sm-8 col-xs-12 col-md-offset-5">
                            	<a href="{!!url('payment-motels', $Results->payment_id)!!}/edit" class="btn btn-success buTTonResize"><i class="fa fa-pencil-square-o"></i> Edit Payment Settings</a>
                            </div>
                    	</div>
                   	</div>
                    </div>
             	</div>
          	</div>
        </div>
    </div>
</div>
@stop