@extends('layouts.layout')

@section('title')
	Manage Roles
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Roles</h3></div>
        </div>
        <div class="col-md-2 col-sm-2 fltR marginTop5" style="text-align:right;">
        	<a href="{!!url('roles/create')!!}" class="AddLink"><i class="fa fa-plus-circle"></i> <b>Add Role</b></a>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                    	<th class="column-title">No</th>
                                    	<th class="column-title">Role Name</th>
                                        <th class="column-title no-link last"><span class="nobr">Action</span></th>
                                    </tr>
                                </thead>
                            
                                <tbody>
                                	@foreach($Results as $vals)
                                    	<tr class="even pointer">
                                        	<td class=" ">{{ ++$i }}</td>
                                            <td class=" ">{!!$vals->RoleName!!}</td>
                                            <td class=" last">
                                                <a href="{!!url('users', $vals->UserId)!!}/edit" class="left btn btn-info buTTonResize"><i class="fa fa-pencil"></i> Edit</a>
                                                <a href="{!!url('users', $vals->UserId)!!}" class="left btn btn-danger buTTonResize marginLeft5" onclick="return ConfirmDelete();"><i class="fa fa-trash-o"></i> Delete</a>
                                            </td>
                                        </tr>
                                   	@endforeach
                                </tbody>
                            </table>
                            {!! $Results->render() !!}
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function ConfirmDelete()
	{
		var x = confirm("Are you sure you want to delete this User?");
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop      
