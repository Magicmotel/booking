@extends('layouts.layout')

@section('title')
	Edit Motel Information
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
	{!!Html::style('vendors/timepicker/css/timepicki.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Edit Motel Information</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    @if (count($errors))
                    <ul class="errorFormValidator">
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                    @endif
                    <form id="demo-form2" action="{!!url('motels', $Results->hotel_id)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                    {!!method_field('PATCH')!!}
               		{!!csrf_field()!!}
                    	<span class="section">MOTEL INFO:</span>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Motel Name <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="hotel_name" id="hotel_name" value="{!!$Results->hotel_name or old('hotel_name')!!}" placeholder="Motel Name" class="form-control readonlyDisabled" required="required" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">No of Rooms <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="hotel_rooms" id="hotel_rooms" value="{!!$Results->hotel_rooms or old('hotel_rooms')!!}" placeholder="Middle Name" class="form-control readonlyDisabled" required="required" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Website</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="hotel_website" id="hotel_website" value="{!!$Results->hotel_website or old('hotel_website')!!}" placeholder="Last Name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Email Address</label>
                            <div class="col-md-6 col-sm-6">
                            <input type="text" name="hotel_email" id="hotel_email" value="{!!$Results->hotel_email or old('hotel_email')!!}" placeholder="Middle Name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Address <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                            <textarea name="hotel_address" id="hotel_address" placeholder="Address" class="form-control readonlyDisabled" required="required" readonly="readonly">{!!$Results->hotel_address or old('hotel_address')!!}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">ZIP Code <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_zip" id="hotel_zip" value="{!!$Results->hotel_zip or old('hotel_zip')!!}" placeholder="Middle Name" class="form-control readonlyDisabled" required="required" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">City <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_city" id="hotel_city" value="{!!$Results->hotel_city or old('hotel_city')!!}" placeholder="Middle Name" class="form-control readonlyDisabled" required="required" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">County <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_county" id="hotel_county" value="{!!$Results->hotel_county or old('hotel_county')!!}" placeholder="Middle Name" class="form-control readonlyDisabled" required="required" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">State <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6">
                            <select name="hotel_state" id="hotel_state" class="form-control LR_Padd5" required="required" disabled="disabled">
                                <option value="">Select State</option>
                                @foreach(App\ZipCityState::groupBy('zip_state')->get() as $stateVal)
                                <option value="{{$stateVal->zip_state}}"{!!($Results->hotel_state==$stateVal->zip_state)?" Selected":""!!}>{{$stateVal->zip_state}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Country <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_country" id="hotel_country" value="{!!$Results->hotel_country or old('hotel_country')!!}" placeholder="Middle Name" class="form-control readonlyDisabled" required="required" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Phone No <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_phone_1" id="hotel_phone_1" value="{!!$Results->hotel_phone_1 or old('hotel_phone_1')!!}" placeholder="Phone No#1" class="form-control" required="required">
                            </div>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_phone_2" id="hotel_phone_2" value="{!!$Results->hotel_phone_2 or old('hotel_phone_2')!!}" placeholder="Phone No#2" class="form-control">
                            </div>
                       	</div>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-6 col-md-offset-3">
                            <input type="text" name="hotel_phone_3" id="hotel_phone_3" value="{!!$Results->hotel_phone_3 or old('hotel_phone_3')!!}" placeholder="Phone No#3" class="form-control">
                            </div>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_phone_4" id="hotel_phone_4" value="{!!$Results->hotel_phone_4 or old('hotel_phone_4')!!}" placeholder="Phone No#4" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Fax Number</label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_fax_1" id="hotel_fax_1" value="{!!$Results->hotel_fax_1 or old('hotel_fax_1')!!}" placeholder="Fax No#1" class="form-control">
                            </div>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_fax_2" id="hotel_fax_2" value="{!!$Results->hotel_fax_2 or old('hotel_fax_2')!!}" placeholder="Fax No#2" class="form-control">
                            </div>
                       	</div>
                        <span class="section">OWNER INFO:</span>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span></label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            	<input type="text" name="hotel_o_fname" id="hotel_o_fname" value="{!!$Results->hotel_o_fname or old('hotel_o_fname')!!}" class="form-control" placeholder="First Name" required="required">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            	<input type="text" name="hotel_o_mname" id="hotel_o_mname" value="{!!$Results->hotel_o_mname or old('hotel_o_mname')!!}" class="form-control" placeholder="Middle Name">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            	<input type="text" name="hotel_o_lname" id="hotel_o_lname" value="{!!$Results->hotel_o_lname or old('hotel_o_lname')!!}" class="form-control" placeholder="Last Name" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Phone No <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_o_phone_1" id="hotel_o_phone_1" value="{!!$Results->hotel_o_phone_1 or old('hotel_o_phone_1')!!}" placeholder="Phone No#1" class="form-control" required="required">
                            </div>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_o_phone_2" id="hotel_o_phone_2" value="{!!$Results->hotel_o_phone_2 or old('hotel_o_phone_2')!!}" placeholder="Phone No#2" class="form-control">
                            </div>
                       	</div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<input type="email" name="hotel_o_email" value="{!!$Results->hotel_o_email or old('hotel_o_email')!!}" id="email" class="form-control" placeholder="Email Address" required="required">
                            </div>
                        </div>
                        <span class="section">MANAGER INFO:</span>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span></label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            	<input type="text" name="hotel_m_fname" id="hotel_m_fname" value="{!!$Results->hotel_m_fname or old('hotel_m_fname')!!}" class="form-control" placeholder="First Name" required="required">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            	<input type="text" name="hotel_m_mname" id="hotel_m_mname" value="{!!$Results->hotel_m_mname or old('hotel_m_mname')!!}" class="form-control" placeholder="Middle Name">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            	<input type="text" name="hotel_m_lname" id="hotel_m_lname" value="{!!$Results->hotel_m_lname or old('hotel_m_lname')!!}" class="form-control" placeholder="Last Name" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Phone No <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_m_phone_1" id="hotel_m_phone_1" value="{!!$Results->hotel_m_phone_1 or old('hotel_m_phone_1')!!}" placeholder="Phone No#1" class="form-control" required="required">
                            </div>
                            <div class="col-md-3 col-sm-6">
                            <input type="text" name="hotel_m_phone_2" id="hotel_m_phone_2" value="{!!$Results->hotel_m_phone_2 or old('hotel_m_phone_2')!!}" placeholder="Phone No#2" class="form-control">
                            </div>
                       	</div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<input type="email" name="hotel_m_email" value="{!!$Results->hotel_m_email or old('hotel_m_email')!!}" id="hotel_m_email" class="form-control" placeholder="Email Address">
                            </div>
                        </div>
                        <span class="section">MOTEL GALLERY:</span>
                        {{--*/ $fileFolderPath = $folderImage_Path."/motel/"; /*--}}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Motel Logo </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	@if($Results->hotel_logo)
                                <div class="motelImgBlock" id="img_hotel_logo">
                                {!!Html::image($fileFolderPath.$Results->hotel_logo, 'alt', array( 'class' => 'marginBottom5', 'style'=>'max-width:200px; max-height:150px;' ))!!}
                                </div>
                                <div class="removeButton" id="close_hotel_logo" data-title="hotel_logo">X</div>
                                @endif
                                <div id="input_hotel_logo" class="col-md-12 zeroPadd{!!($Results->hotel_logo)?' displayNone':''!!}">
                                    <input type="file" name="hotel_logo" id="hotel_logo" class="form-control col-md-3 col-xs-6">
                                   	<span class="dg_btm_text">SIZE : 120 PX x 120 PX</span>
                                    <span class="dg_btm_text">JPG/JPEG, PNG, GIF & Max 512 KB</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Image of Motel</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            	@if($Results->hotel_pic_1)
                                <div class="motelImgBlock" id="img_hotel_pic_1">
                                {!!Html::image($fileFolderPath.$Results->hotel_pic_1, 'alt', array( 'class' => 'marginBottom5', 'style'=>'max-width:200px; max-height:150px;' ))!!}
                                </div>
                                <div class="removeButton" id="close_hotel_pic_1" data-title="hotel_pic_1">X</div>
                                @endif
                                <div id="input_hotel_pic_1" class="col-md-12 zeroPadd{!!($Results->hotel_pic_1)?' displayNone':''!!}">
                                    <input type="file" name="hotel_pic_1" id="hotel_pic_1" class="form-control col-md-3 col-xs-6">
                                   	<span class="dg_btm_text">SIZE : 800 PX x 350 PX</span>
                                    <span class="dg_btm_text">JPG/JPEG, PNG, GIF & Max 2 MB</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            	@if($Results->hotel_pic_2)
                                <div class="motelImgBlock" id="img_hotel_pic_2">
                                {!!Html::image($fileFolderPath.$Results->hotel_pic_2, 'alt', array( 'class' => 'marginBottom5', 'style'=>'max-width:200px; max-height:150px;' ))!!}
                                </div>
                                <div class="removeButton" id="close_hotel_pic_2" data-title="hotel_pic_2">X</div>
                                @endif
                                <div id="input_hotel_pic_2" class="col-md-12 zeroPadd{!!($Results->hotel_pic_2)?' displayNone':''!!}">
                                    <input type="file" name="hotel_pic_2" id="hotel_pic_2" class="form-control col-md-3 col-xs-6">
                                   	<span class="dg_btm_text">SIZE : 800 PX x 350 PX</span>
                                    <span class="dg_btm_text">JPG/JPEG, PNG, GIF & Max 2 MB</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3">
                            	@if($Results->hotel_pic_3)
                                <div class="motelImgBlock" id="img_hotel_pic_3">
                                {!!Html::image($fileFolderPath.$Results->hotel_pic_3, 'alt', array( 'class' => 'marginBottom5', 'style'=>'max-width:200px; max-height:150px;' ))!!}
                                </div>
                                <div class="removeButton" id="close_hotel_pic_3" data-title="hotel_pic_3">X</div>
                                @endif
                                <div id="input_hotel_pic_3" class="col-md-12 zeroPadd{!!($Results->hotel_pic_3)?' displayNone':''!!}">
                                    <input type="file" name="hotel_pic_3" id="hotel_pic_3" class="form-control col-md-3 col-xs-6">
                                   	<span class="dg_btm_text">SIZE : 800 PX x 350 PX</span>
                                    <span class="dg_btm_text">JPG/JPEG, PNG, GIF & Max 2 MB</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            	@if($Results->hotel_pic_4)
                                <div class="motelImgBlock" id="img_hotel_pic_4">
                                {!!Html::image($fileFolderPath.$Results->hotel_pic_4, 'alt', array( 'class' => 'marginBottom5', 'style'=>'max-width:200px; max-height:150px;' ))!!}
                                </div>
                                <div class="removeButton" id="close_hotel_pic_4" data-title="hotel_pic_4">X</div>
                                @endif
                                <div id="input_hotel_pic_4" class="col-md-12 zeroPadd{!!($Results->hotel_pic_4)?' displayNone':''!!}">
                                    <input type="file" name="hotel_pic_4" id="hotel_pic_4" class="form-control col-md-3 col-xs-6">
                                   	<span class="dg_btm_text">SIZE : 800 PX x 350 PX</span>
                                    <span class="dg_btm_text">JPG/JPEG, PNG, GIF & Max 2 MB</span>
                                </div>
                            </div>
                        </div>
                        <span class="section">ADDITIONAL SETTING:</span>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Check In Time <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <span class="dg_small_input">
                                    <input type="text" name="hotel_checkin_time" id="hotel_checkin_time" value="{{$Results->hotel_checkin_time}}" class="form-control" placeholder="00 : 00 : AM" required>
                                </span>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Check Out Time <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            	<span class="dg_small_input">
                                    <input type="text" name="hotel_checkout_time" id="hotel_checkout_time" value="{{$Results->hotel_checkout_time}}" class="form-control" placeholder="00 : 00 : AM" required>
                                </span>
                            </div>
                       	</div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Cancellation Period <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                            	<input type="number" name="hotel_cancel_period" id="hotel_cancel_period" value="{!!$Results->hotel_cancel_period!!}" class="form-control fltL width30" placeholder="0" min="1" max="100" required="required">
                                <div class="col-md-4 col-sm-6 col-xs-12 paddingTop10" style="position:absolute;left:30%;">Hour(s)</div>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Disclaimer <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                            <textarea name="hotel_disclaimer" id="hotel_disclaimer" placeholder="Disclaimer" class="form-control" required="required" style="height:150px;">{!!$Results->hotel_disclaimer!!}</textarea>
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop20">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                                <button type="reset" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
	{!!Html::script('vendors/timepicker/js/timepicki.js')!!}
@stop  

@section('jQuery')
<script>
$(document).ready(function() {
	$('#hotel_checkin_time').timepicki({increase_direction:'up'}); 
	$('#hotel_checkout_time').timepicki({increase_direction:'up'});
	
	$('.removeButton').on('click', function(e) {
		var dataRef = $(this).attr('data-title');
		$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		$.ajax({
			url: "{{ url('/motel_removeImage') }}",
			type: 'POST',
			data:{"hotel_pic_no":dataRef, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				if ( msg.status === 'success' ) {
					$('#img_'+dataRef).hide();
					$('#close_'+dataRef).hide();
					$('#input_'+dataRef).show();
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.success( msg.response );
				}
				if ( msg.status === 'error' ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.error( msg.response );
				}
			},
			error: function( data ) {
				if ( data.status === 422 ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.error('Try Again!!');
				}
			}
		});
	
		return false;
	});
	$("#hotel_zip").on("keyup", function() {
		var zipCode = $(this).val().substring(0, 5);
		$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		if (zipCode.length == 5 && /^[0-9]+$/.test(zipCode))
		{
			$.ajax({
				url: "{{ url('/getCityData') }}",
				type: 'POST',
				data:{"zipCode":zipCode, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					if ( msg.status === 'success' ) {
						$("#hotel_city").val(msg.zip_city);
						$("#hotel_state").val(msg.zip_state);
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						$("#hotel_city").val(msg.zip_city);
						$("#hotel_state").val(msg.zip_state);
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					if ( data.status === 422 ) {
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.error('Try Again!!');
					}
				}
			});
			return false;
		}
	});
});
</script>
@stop