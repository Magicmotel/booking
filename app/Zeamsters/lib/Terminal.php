<?php

/**
 * This file contains functions related to view record 
 * for terminal endpoints.
 * 
 * Tags class
 * 
 */

namespace App\Zeamsters\lib;

//include_once BASE_PATH . DS . 'includes/Logging.php';

use App\Zeamsters\lib\ZeamsterPaymentTransaction as zeamsterApi;
use Exception;
use App\Zeamsters\includes\Logging;

class Terminal extends Exception {

    public $id = "";
    public $active = "";
    public $created_ts = "";
    public $last_registration_ts = "";
    public $local_ip_address = "";
    public $location_id = "";
    public $mac_address = "";
    public $modified_ts = "";
    public $port = "";
    public $serial_number = "";
    public $terminal_number = "";
    public $title = "";

    /**
     * Get Terminals Details.
     * 
     * @params string $terminal_id  
     * 
     * @return json
     */
    public static function getTerminalsRecords($terminal_id = null, $filter = null, $perPage = null, $sort = null, $pageNo = null) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            if (!empty($terminal_id)) {
                $request_url = $api_url . "/terminals/{$terminal_id}";
            } else {
                if (!empty($perPage) || !empty($filter) || !empty($sort) || !empty($pageNo)) {
                    $request_url = $api_url . "/terminals?{$filter}&page_size={$perPage}&sort={$sort}&page={$pageNo}";
                } else {
                    $request_url = $api_url . "/terminals";
                }
            }

            $method = 'GET';
            $response = $objzeamsterApi::send_transaction($request_url, $method, null, null);

            return $response;
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

}
