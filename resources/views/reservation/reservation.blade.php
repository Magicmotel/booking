@extends('layouts.layout')

@section('title')
	Search Reservation
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
    {!!Html::style('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-dragtable/css/dragtable.css')!!}
    
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Search Reservation</h3></div>
        </div>
        <div class="col-md-2 col-sm-2 fltR marginTop5" style="text-align:right;">
        	<a href="javascript:void(0);" class="AddLink" id="advanceSearchClick"><i class="fa fa-plus-circle"></i> <b>Advance Search</b></a>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
          	    <div class="x_panel">
                  	<div class="x_content">
                        <form id="demo-form2" action="{!!url('reservation')!!}" method="get" data-parsley-validate class="form-horizontal form-label-left">
                            <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                                <div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-3 col-xs-12">First Name</label>
                                        <div class="col-md-7 col-sm-3 col-xs-12">
                                            <input type="text" name="guest_fname" id="guest_fname" class="form-control" placeholder="Guest First Name" value="{!!$request->guest_fname!!}" >
                                        </div>
                                    </div>
                               	</div>
                                <div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Last Name</label>
                                        <div class="col-md-7 col-sm-3 col-xs-12">
                                            <input type="text" name="guest_lname" id="guest_lname" class="form-control" placeholder="Guest Last Name" value="{!!$request->guest_lname!!}" >
                                        </div>
                                    </div>
                              	</div>
                                <div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Phone</label>
                                        <div class="col-md-7 col-sm-3 col-xs-12">
                                            <input type="text" name="guest_phone" id="guest_phone" class="form-control" placeholder="Guest Phone Number" value="{!!$request->guest_phone!!}" >
                                        </div>
                                    </div>
                                </div>
                           	</div>
                            <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd{!!($cntSearch)?'':' displayNone'!!}" id="advanceSearch">
                            <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                            	<div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Postal Code</label>
                                        <div class="col-md-7 col-sm-3 col-xs-12">
                                            <input type="text" name="guest_postal" id="guest_postal" class="form-control" placeholder="Postal Code" value="{!!$request->guest_postal!!}" maxlength="5" >
                                        </div>
                                    </div>
                              	</div>
                                <div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Company</label>
                                        <div class="col-md-7 col-sm-3 col-xs-12">
                                        	<select name="company_profile" id="company_profile" class="form-control col-md-2 col-xs-12 zeroPadd">
                                                <option value="0">Select Company</option>
                                                @foreach(App\CompanyProfile::where('hotel_id', $Hotel_ID)->get() as $companyVal)
                                                <option value="{{$companyVal->profile_id}}"{!!($request->company_profile == $companyVal->profile_id)?' Selected':''!!}>{{$companyVal->company_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                               	</div>
                                <div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Group</label>
                                        <div class="col-md-7 col-sm-3 col-xs-12">
                                            <input type="text" name="guest_group" id="guest_group" class="form-control" placeholder="Group Name" value="{!!$request->guest_group!!}" >
                                        </div>
                                    </div>
                               	</div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                            	<div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Start Date</label>
                                        <div class="col-md-7 col-sm-3 col-xs-12">
                                            <input type="text" name="start_date" id="start_date" class="form-control has-feedback-left" placeholder="Start Date" value="{!!$request->start_date!!}" readonly="readonly">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                    </div>
                               	</div>
                                <div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-3 col-xs-12">End Date</label>
                                        <div class="col-md-7 col-sm-3 col-xs-12">
                                            <input type="text" name="end_date" id="end_date" class="form-control has-feedback-left" placeholder="End Date" value="{!!$request->end_date!!}" readonly="readonly">
                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                    </div>
                               	</div>
                                <div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                                    <div class="form-group">
                                        <label class="control-label col-md-4 col-sm-3 col-xs-12">Room No</label>
                                        <div class="col-md-7 col-sm-3 col-xs-12">
                                            <input type="text" name="room_number" id="room_number" class="form-control" placeholder="Room Number" value="{!!$request->room_number!!}" >
                                        </div>
                                    </div>
                               	</div>
                            </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group form-group-last marginTop15">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 AlgnCenter">
                                        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Search</button>
                                        <div class="btn btn-primary marginLeft10" id="clearSearch"><i class="fa fa-undo"></i> Clear</div>
                                    </div>
                               	</div>
                            </div>
                        </form>
                  	</div>
                </div>
          	</div>
            @if($cntValue && $rstNum)
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  	<div class="x_content">
                        <div class="table-responsive">
                        	<table id="datatable-responsive" class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                	<th class="column-title">Name</th>
                                	<th class="column-title">Phone</th>
                                    <th class="column-title">Status</th>
                                    <th class="column-title">Arrival</th>
                                    <th class="column-title">Departure</th>
                                    <th class="column-title">Room</th>
                                    <th class="column-title">Type</th>
                                    <th class="column-title">Reservation#</th>
                                    <th class="column-title no-link last">Source</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Results as $vals)
                                <tr>
                                	<td class="uPPerLetter">
                                    	<a href="{{url('guest-info', $vals->RsrvId)}}" class="btn-list-link">{{$vals->GuestLname}}, {{$vals->GuestFname}}</a>
                                        @if($vals->GuestBadStatus == 1)
                                    	<i class="fa fa-user activeGuest"></i>
                                        @else
                                        <i class="fa fa-user-times badGuest"></i>
                                        @endif
                                  	</td>
                                    <td>{!!$vals->GuestPhone!!}</td>
                                    <td>{!!($vals->RsrvNoShow=='No Show')?'No Show':$vals->RsrvStat!!}</td>
                                    <td>{!!$vals->ArvlDate!!}</td>
                                    <td>{!!$vals->DprtDate!!}</td>
                                    <td id="RoomAssignId_{!!$vals->RsrvId!!}">
                                    	@if($vals->RsrvStat == 'Checked Out' || $vals->RsrvStat == 'Cancelled')
                                        {{$vals->RoomNumber}}
                                        @else
                                        	@if($vals->RoomNumber)
                                                @if($vals->RsrvLock)
                                                    <div class="fltL" style="position:relative; padding-top:2px;">{{$vals->RoomNumber}}</div>
                                                    <i class="fa fa-lock lockFeaturButton" aria-hidden="true"></i>
                                                @else
                                                	<div class="fltL" style="position:relative">
                                                    <a href="{{url('change-room', $vals->RsrvId)}}" class="btn-list-link">{{$vals->RoomNumber}}</a>
                                                    @if($vals->RsrvStat == "")
                                                    <i class="closeButton fa fa-times" data-id="{!!$vals->RsrvId!!}"></i>
                                                    @endif
                                                    </div>
                                                @endif
                                            @else
                                                <a href="{{url('change-room', $vals->RsrvId)}}" class="btn-list-link">Select Room</a>
                                            @endif
                                            	
                                        @endif
                                    </td>
                                    <td>{!!$vals->RoomTypes!!}</td>
                                    <td>{!!$vals->RsrvId!!}</td>
                                    <td class="last">{{($vals->RsrvSource)?$vals->RsrvSource:"Local"}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                           	</table>
                        </div>
                  	</div>
                </div>
         	</div>
            @elseif($cntValue)
            <div class="col-md-12 col-sm-12 col-xs-12">
          	    <div class="x_panel">
                  	<div class="x_content">
                        <div class="alert alert-warning marginZero AlgnCenter">
                            <p>No Reservation found related to search keyword</p>
                        </div>
                  	</div>
              	</div>
           	</div>
            @endif
      	</div>
    </div>
</div>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
    {!!Html::script('vendors/datatables.net-dragtable/js/jquery.dragtable.js')!!}
    {!!Html::script('vendors/datatables.net/js/jquery.dataTables.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')!!}
    {!!Html::script('vendors/jszip/dist/jszip.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/pdfmake.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/vfs_fonts.js')!!} 
@stop  

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var checkin = $('#start_date').datepicker({
		startDate: moment()
	}).on('changeDate', function(ev){
		var newDate = new Date(ev.date);
		newDate.setDate(newDate.getDate() + 1);
		checkout.setValue(newDate);
		checkin.hide();
		$('#end_date')[0].focus();
	}).data('datepicker');
	
	var checkout = $('#end_date').datepicker({
		onRender: function(date) {
			return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		checkout.hide();
	}).data('datepicker');
	
	$('#datatable-responsive').DataTable({
		"aaSorting": [[3, 'desc']],
		"bLengthChange": false,
		"bFilter": false,
		"bInfo": true,
		"bPaginate": true
	});
	
	$('#advanceSearchClick').click(function(){
		$("#advanceSearch").toggle();
	});
	$('#clearSearch').click(function(){
		var LOCATE_URL = window.location.href.split("#")[0].split("?")[0];
		window.location.href = LOCATE_URL;
	});
	$(document).on("click", ".closeButton",function(e) {
		var x = confirm("Are you sure you want to un assigned this Room Number?");
		if (x){
			var rsrvId = $(this).attr('data-id');
			$.ajax({
				url: "{{ url('/unAssignRoomNumber') }}",
				type: 'POST',
				data:{"rsrvId":rsrvId, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					if ( msg.status === 'success' ) {
						$('#RoomAssignId_'+rsrvId).html("<a href=\"{{url('change-room')}}/"+rsrvId+"\" class=\"btn-list-link\">Select Room</a>");
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					if ( data.status === 422 ) {
						toastr.error('Cannot delete the Assigned Room');
					}
				}
			});
			return true;
		}else{
			return false;
		}
	});
});
</script>
@stop