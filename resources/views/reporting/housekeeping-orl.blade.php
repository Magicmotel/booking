@extends('layouts.layout')

{{--*/ $TitleSubject = "Hotel# MMKiD".$Hotel_ID.", ".$MotelInfo->hotel_name." :: Occupied Room List";/*--}} 
@section('title')
	{{$TitleSubject}}
@stop

@section('CascadingSheet')
    {!!Html::script('vendors/pdfmake/build/sprintf.js')!!}
    {!!Html::script('vendors/pdfmake/build/jspdf.js')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Room Status List</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('report-housekeeping')}}">Vacant Room List</a></li>
                <li class="active"><a href="javascript:void(0);">Occupied Room List</a></li>
                <li><a href="{{url('report-housekeeping?type=rsl')}}">Room Status List</a></li>
            </ul>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content" id="printableArea">
                        <table class="table table-striped" id="pdfableArea">
                            <tr class="backgroundNone">
                                <td width="20%" class="borderNone height140">
                                	{!!Html::image('motel/'.$MotelInfo->hotel_logo, 'alt', array( 'title' => 'Motel Logo', 'style' => 'max-width:120px; max-height:120px;'))!!}
                              	</td>
                                <td width="60%" align="center" valign="middle" class="zeroPadd borderNone labelText font16">
                                	Occupied Room List
                                </td>
                                <td width="20%" class="zeroPadd borderNone">
                                	<div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnLeft LneHeight24">Date: {{date('m/d/Y')}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnLeft LneHeight24">Time: {{date('h:i A')}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 labelText marginBottom5 AlgnLeft LneHeight24">User: {{Helper::getAdminUserName()}}</div>
                               	</td>
                            </tr>
                            <tr class="backgroundNone">
                            	<td colspan="3" class="zeroPadd borderNone">
                                	<table id="exelableArea" class="table table-bordered table-striped jambo_table">
                                        <thead>
                                            <tr>
                                                <th>Room#</th>
                                                <th>Type</th>
                                                <th>No of Adult</th>
                                                <th>No of Child(ren)</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($roomSql))
                                            @foreach($roomSql as $roomVals)
                                            <tr>
                                                <td>{{$roomVals->RoomNumber}}</td>
                                                <td>{{$roomVals->RoomTypes}} &nbsp; ({{$roomVals->RoomType}})</td>
                                                <td>{{$roomVals->RsvAdult}}</td>
                                                <td>{{$roomVals->RsvChild}}</td>
                                                <td>{{($roomVals->RoomStat==1)?"Clean":"Dirty"}}</td>
                                            </tr>
                                            @endforeach
                                        @else
                                        	<tr>
                                                <td colspan="5" class="errorMessageTR">No Record Found</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                              	</td>
                            </tr>
                        </table>
                    </div>
                    <button class="btn btn-primary marginTop20 margin5" id="btnPrint"><i class="fa fa-print"></i> Print</button>
                    <button class="btn btn-success marginTop20 margin5" id="btnExcel"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Excel</button>
                    <button class="btn btn-danger marginTop20 margin5" id="btnPDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</button>
                </div>
         	</div>
      	</div>
    </div>
</div>
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	$("#btnPrint").click(function(e){
		var printContents = document.getElementById('printableArea').innerHTML;
		var originalContents = document.body.innerHTML;
		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
		window.location.reload();
	});
	$("#btnExcel").click(function(e){
		e.preventDefault();
		
		var data_type = 'data:application/vnd.ms-excel';
		var table_div = document.getElementById('exelableArea');
		var table_html = table_div.outerHTML.replace(/ /g, '%20');
		
		var a = document.createElement('a');
		a.href = data_type + ', ' + table_html;
		a.download = '{{$TitleSubject}}.xls';
		a.click();
	});
	$("#btnPDF").click(function(e){
		var pdf = new jsPDF('p', 'pt', 'letter');
		source = $('#pdfableArea')[0];
		specialElementHandlers = {
			// element with id of "bypass" - jQuery style selector
			'#bypassme': function(element, renderer) {
				// true = "handled elsewhere, bypass text extraction"
				return true
			}
		};
		margins = {
			top: 40,
			bottom: 40,
			left: 40,
			width:572
		};
		
		pdf.fromHTML(
				source, // HTML string or DOM elem ref.
				margins.left, // x coord
				margins.top,
				{// y coord
					'width': margins.width, // max width of content on PDF
					'elementHandlers': specialElementHandlers
				},
				function(dispose) {
					pdf.save('{{$TitleSubject}}.pdf');
				},
				margins
		);
	});
});
</script>
@stop