<?php

/**
 * This file contains functions related to create,update,view and delete
 * for Contact Endpoints.
 * 
 * Contact class
 */

namespace App\Zeamsters\lib;

//include_once BASE_PATH . DS . 'includes/Logging.php';

use App\Zeamsters\lib\ZeamsterPaymentTransaction as zeamsterApi;
use Exception;
use App\Zeamsters\includes\Logging;

class Contact extends Exception {

    public $id = "";
    public $account_number = "";
    public $address = "";
    public $cell_phone = "";
    public $city = "";
    public $company_name = "";
    public $contact_api_id = "";
    public $created_ts = "";
    public $date_of_birth = "";
    public $email = "";
    public $email_trx_receipt = "";
    public $first_name = "";
    public $header_message = "";
    public $header_message_type = "";
    public $home_phone = "";

    /**
     * @var string $last_name(Required) Zeamster API to use for requests.
     */
    public $last_name = "";

    /**
     * @var string $location_id(Required) Zeamster API to use for requests.
     */
    public $location_id = "";
    public $modified_ts = "";
    public $office_ext_phone = "";
    public $office_phone = "";
    public $state = "";
    public $update_if_exists = "";
    public $zip = "";

    /**
     * Create new customer contact record.
     * 
     * @params object $objData  
     * 
     * @return json 
     */
    public static function createCustomerContact($objData) {

        $request_data = zeamsterApi::convertObjectToArray($objData);

        try {
            if (!empty($request_data)) {
                $objzeamsterApi = zeamsterApi::Instance();
                $api_url = $objzeamsterApi->get_gateway_url();
                $request_url = $api_url . '/contacts';
                $method = 'POST';

                $request = array('contact' => array_filter($request_data));

                $request = json_encode($request);

                $response = $objzeamsterApi::send_transaction($request_url, $method, null, $request);

                return $response;
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Get / view customer contact details.
     * 
     * @param string $contact_id
     * 
     * @return json 
     */
    public static function getCustomerContact($contact_id = null, $filter = null, $perPage = null, $sort = null, $pageNo = null) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            if (!empty($contact_id)) {
                $request_url = $api_url . "/contacts/{$contact_id}";
            } else {
                if (!empty($perPage) || !empty($filter) || !empty($sort) || !empty($pageNo)) {
                    $request_url = $api_url . "/contacts?{$filter}&page_size={$perPage}&sort={$sort}&page={$pageNo}";
                } else {
                    $request_url = $api_url . "/contacts";
                }
            }

            $method = 'GET';
            $response = $objzeamsterApi::send_transaction($request_url, $method, null, null);

            return $response;
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Update customer contact .
     * @params object $objData
     * @params string $contact_id 
     * 
     * @return json 
     */
    public static function updateCustomerContact($objData, $contact_id) {
        $request_data = zeamsterApi::convertObjectToArray($objData);
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/contacts/{$contact_id}";
            $method = 'PUT';
            if (!empty($request_data)) {
                $request = array('contact' => array_filter($request_data));
            }
            $request = json_encode($request);

            $response = $objzeamsterApi::send_transaction($request_url, $method, null, $request);

            return $response;
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Delete customer contact record.
     * @params string $contact_id  
     *  
     * @return json  
     */
    public static function deleteCustomerContact($contact_id) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/contacts/{$contact_id}";
            $method = 'DELETE';
            $response = $objzeamsterApi::send_transaction($request_url, $method);

            return $response;
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

}
