@extends('layouts.layout')

@section('title')
	Maintenance Query Reply
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Maintenance Query Reply</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('maintenance')}}">Manage Maintenance Queries</a></li>
                <li class="active"><a href="javascript:void(0);">Maintenance Query Reply</a></li>
            </ul>
        </div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                    <form id="demo-form2" action="{!!url('maintenance', $Results->maintenance_id)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                        {!!method_field('PATCH')!!}
                        {!!csrf_field()!!}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_type">Room Type <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            	<select name="room_type_id" id="room_type_id" class="form-control col-md-3 col-xs-6" required="required" disabled="disabled">
                                    <option value="">Select Room Type</option>
                                    @foreach( App\Roomtype::where([ ['hotel_id', $Hotel_ID], ['trash', 0] ])->orderBy('room_type', 'ASC')
                                    						->get(['room_type as RoomType', 'room_type_id as RoomTypeId']) as $val )
                                    <option value="{!!$val->RoomTypeId!!}"{!!($Results->room_type_id == $val->RoomTypeId)?" Selected":""!!}>{!!$val->RoomType!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_count">Room Number <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            	<select name="room_assign_id" id="room_assign_id" class="form-control col-md-3 col-xs-6" required="required" disabled="disabled">
                                    <option value="">Select Room Number</option>
                                    @if($Results->room_type_id)
                                    @foreach( App\RoomAssign::where([ ['hotel_id', $Hotel_ID], ['trash', 0], ['room_type_id', $Results->room_type_id] ])->orderBy('room_number', 'ASC')->get() as $rooms )
                                    <option value="{!!$rooms->room_assign_id!!}"{!!($Results->room_assign_id == $rooms->room_assign_id)?" Selected":""!!}>
                                        {!!$rooms->room_number!!}
                                    </option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="guest_adult" class="control-label col-md-3 col-sm-3 col-xs-12">Maintenance For <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<textarea name="maintenance_for" id="maintenance_for" class="form-control col-md-7 col-xs-12" placeholder="Maintenance For" required="required" readonly="readonly">{!!$Results->maintenance_for!!}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="guest_adult" class="control-label col-md-3 col-sm-3 col-xs-12">Maintenance Reply</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<textarea name="maintenance_reply" id="maintenance_reply" class="form-control col-md-7 col-xs-12" placeholder="Maintenance Reply"></textarea>
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop24">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update Status</button>
                                <button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop