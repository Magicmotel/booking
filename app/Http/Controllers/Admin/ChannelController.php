<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Roomtype;
use App\ChannelPartner;
use App\ChannelMotel;
use App\ChannelMapping;
use Helper;
use DB;

class ChannelController extends Controller
{
	public function expedia(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID   = $Helper->getHotelId();
		$Channel_ID = 1;
		$loginNum = $mapNum = 1;
		$loginRst = ChannelMotel::where([ ['hotel_id', $Hotel_ID], ['channel_id', $Channel_ID] ])->first();
		$mapRst   = DB::select("SELECT RT.room_type as RoomType, CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomCode,
								CMM.channel_room_type_id as cRoomTypeId, CMM.channel_rate_plan_id as cRatePlanId
								FROM roomtypes as RT LEFT JOIN channel_motel_mapping as CMM ON CMM.room_type_id = RT.room_type_id AND CMM.channel_id = ".$Channel_ID."
								WHERE RT.hotel_id = ".$Hotel_ID." AND RT.trash = 0
								ORDER BY RT.room_type ASC
							   ");
		if(!count($loginRst)){
			$loginNum = 0;
			return redirect('expedia-channel/credential');
		}
		if(!count($mapRst)){
			$mapNum = 0;
			return redirect('expedia-channel/mapping');
		}
		return view('channels.expedia-channel')->with("loginNum", $loginNum)->with("loginRst", $loginRst)
											   ->with("mapNum", $mapNum)->with("mapRst", $mapRst)
											   ->with("Hotel_ID", $Hotel_ID);
    }
	
	public function expedia_status($id)
    {	
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$Results = ChannelMotel::where([ ['cm_id', '=', $id], ['hotel_id', '=', $Hotel_ID] ])->first();
		if(count($Results))
		{
			if($Results->channel_status == 1){
				$Results->channel_status = 0;
				$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Results->added_by = $admin_ID;
				$Results->save();
				return redirect('expedia-channel')->with('success','Expedia Hotel API paused successfully');
			}else{
				$Results->channel_status = 1;
				$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Results->added_by = $admin_ID;
				$Results->save();
				return redirect('expedia-channel')->with('success','Expedia Hotel API activated successfully');
			}
		}else{
			return redirect('expedia-channel')->with("danger", "Please select your Motel");
		}
    }
	
	public function expedia_credential(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID   = $Helper->getHotelId();
		$Channel_ID = 1;
		$Num = 1;
		$Results = ChannelMotel::where([ ['hotel_id', $Hotel_ID], ['channel_id', $Channel_ID] ])->first();
		return view('channels.expedia-channel-credential')->with("Results", $Results)->with("Hotel_ID", $Hotel_ID);
    }
	
	public function expedia_credential_store(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		if($Hotel_ID == $request->hotel_id)
		{
			if($request->cm_id){
				$cm_id = $request->cm_id;
				$Rst = ChannelMotel::where('cm_id', $cm_id)->first();
				$Rst->hotel_id = $Hotel_ID;
				$Rst->channel_id  = 1;
				$Rst->channel_hotel_id  = $request->channel_hotel_id;
				$Rst->channel_username = $request->channel_username;
				$Rst->channel_password = $request->channel_password;
				$Rst->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Rst->added_by = $admin_ID;
				$Rst->save();
			}else{
				$RequestArray = array();
				$RequestArray[] = $Hotel_ID;
				$RequestArray[] = 1;
				$RequestArray[] = $request->channel_hotel_id;
				$RequestArray[] = $request->channel_username;
				$RequestArray[] = $request->channel_password;
				$RequestArray[] = $_SERVER['REMOTE_ADDR'];
				$RequestArray[] = $admin_ID;
				
				DB::select('CALL ChannelMotel_Insert_SP(?,?,?,?,?,?,?)', $RequestArray);
			}
			return redirect('expedia-channel')->with('success','Expedia Hotel Credential updated successfully');
		}
		else
		{
			return redirect('expedia-channel')->with("danger", "Please select your Motel");
		}
    }
	
	public function expedia_mapping(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID   = $Helper->getHotelId();
		$Channel_ID = 1;
		$loginNum = 1;
		$loginRst = ChannelMotel::where([ ['hotel_id', $Hotel_ID], ['channel_id', $Channel_ID] ])->first();
		if(!count($loginRst)){
			return redirect('expedia-channel/credential');
		}
		$roomRst = DB::select("SELECT RT.room_type_id as RoomTypeId, RT.room_type as RoomType,
							   CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomCode
							   FROM roomtypes as RT
							   WHERE RT.hotel_id = ".$Hotel_ID." AND RT.trash = 0
							   ORDER BY RT.room_type ASC
							  ");							   
		return view('channels.expedia-channel-mapping')->with("loginRst", $loginRst)->with("roomRst", $roomRst)->with("Hotel_ID", $Hotel_ID)->with("Channel_ID", $Channel_ID);
    }
	
	public function expedia_mapping_store(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		if($Hotel_ID == $request->hotel_id)
		{
			foreach($request->map_id as $Key => $Vals){
				$MapId = $Vals;
				$RoomTypeId  = $request->room_type_id[$Key];
				$cRoomTypeId = $request->channel_room_type_id[$Key];
				$cRatePlanId = $request->channel_rate_plan_id[$Key];
				if($MapId){
					$Rst = ChannelMapping::where('map_id', $MapId)->first();
					$Rst->hotel_id     = $Hotel_ID;
					$Rst->room_type_id = $RoomTypeId;
					$Rst->channel_id   = 1;
					$Rst->channel_room_type_id = $cRoomTypeId;
					$Rst->channel_rate_plan_id = $cRatePlanId;
					$Rst->reg_ip = $_SERVER['REMOTE_ADDR'];
					$Rst->added_by = $admin_ID;
					$Rst->save();
				}else{
					$RequestArray = array();
					$RequestArray[] = $Hotel_ID;
					$RequestArray[] = $RoomTypeId;
					$RequestArray[] = 1;
					$RequestArray[] = $cRoomTypeId;
					$RequestArray[] = $cRatePlanId;
					$RequestArray[] = $_SERVER['REMOTE_ADDR'];
					$RequestArray[] = $admin_ID;
					
					DB::select('CALL ChannelMapping_Insert_SP(?,?,?,?,?,?,?)', $RequestArray);
				}
			}
			return redirect('expedia-channel')->with('success','Expedia Hotel Mapping updated successfully');
		}
		else
		{
			return redirect('expedia-channel')->with("danger", "Please select your Motel");
		}
    }
	
	// TRIPADVISOR
	public function tripadvisor(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID   = $Helper->getHotelId();
		$Channel_ID = 2;
		$loginNum = 1;
		$loginRst = ChannelMotel::where([ ['hotel_id', $Hotel_ID], ['channel_id', $Channel_ID] ])->first();
		if(!count($loginRst)){
			$loginNum = 0;
			return redirect('tripadvisor-channel/credential');
		}
		return view('channels.tripadvisor-channel')->with("loginNum", $loginNum)->with("loginRst", $loginRst)->with("Hotel_ID", $Hotel_ID);
    }
	
	public function tripadvisor_status($id)
    {	
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$Results = ChannelMotel::where([ ['cm_id', '=', $id], ['hotel_id', '=', $Hotel_ID] ])->first();
		if(count($Results))
		{
			if($Results->channel_status == 1){
				$Results->channel_status = 0;
				$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Results->added_by = $admin_ID;
				$Results->save();
				return redirect('tripadvisor-channel')->with('success','TripAdvisor Hotel API paused successfully');
			}else{
				$Results->channel_status = 1;
				$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Results->added_by = $admin_ID;
				$Results->save();
				return redirect('tripadvisor-channel')->with('success','TripAdvisor Hotel API activated successfully');
			}
		}else{
			return redirect('tripadvisor-channel')->with("danger", "Please select your Motel");
		}
    }
	
	public function tripadvisor_credential(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID   = $Helper->getHotelId();
		$Channel_ID = 2;
		$Num = 1;
		$Results = ChannelMotel::where([ ['hotel_id', $Hotel_ID], ['channel_id', $Channel_ID] ])->first();
		return view('channels.tripadvisor-channel-credential')->with("Results", $Results)->with("Hotel_ID", $Hotel_ID);
    }
	
	public function tripadvisor_credential_store(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		if($Hotel_ID == $request->hotel_id)
		{
			if($request->cm_id){
				$cm_id = $request->cm_id;
				$Rst = ChannelMotel::where('cm_id', $cm_id)->first();
				$Rst->hotel_id = $Hotel_ID;
				$Rst->channel_id  = 2;
				$Rst->channel_hotel_id  = $request->channel_hotel_id;
				$Rst->channel_username = $request->channel_username;
				$Rst->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Rst->added_by = $admin_ID;
				$Rst->save();
			}else{
				$RequestArray = array();
				$RequestArray[] = $Hotel_ID;
				$RequestArray[] = 2;
				$RequestArray[] = $request->channel_hotel_id;
				$RequestArray[] = $request->channel_username;
				$RequestArray[] = "";
				$RequestArray[] = $_SERVER['REMOTE_ADDR'];
				$RequestArray[] = $admin_ID;
				
				DB::select('CALL ChannelMotel_Insert_SP(?,?,?,?,?,?,?)', $RequestArray);
			}
			return redirect('tripadvisor-channel')->with('success','TripAdvisor Hotel Credential updated successfully');
		}
		else
		{
			return redirect('tripadvisor-channel')->with("danger", "Please select your Motel");
		}
    }
}
