@extends('layouts.layout')
<? 	foreach($Results as $val){
		$outPut = $val;
	}
?>
@section('title')
	Guest : {{$outPut->GuestLname}}, {{$outPut->GuestFname}}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Guest : {{$outPut->GuestLname}}, {{$outPut->GuestFname}}</h3></div>
            @if($outPut->RsrvSource)
                <div class="col-md-3 col-sm-3 col-xs-6 fltR zeroRightPadd">
                    <h3 class="AlgnCenter">{{$outPut->RsrvSource}}</h3>
                </div>
            @endif
        </div>
        {{--*/ $PayBalance = ($outPut->PayAmount-$outPut->PayDeposit); /*--}}
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('guest-info', $outPut->RsrvId)}}">Guest Info</a></li>
                <li><a href="{{url('payment-info', $outPut->RsrvId)}}">Payment Info</a></li>
                <li><a href="{{url('change-room', $outPut->RsrvId)}}">Room Change</a></li>
                <li><a href="{{url('guest-folio', $outPut->RsrvId)}}">Guest Folio</a></li>
                <li class="active"><a href="javascript:void(0);">Change Stay</a></li>
                <li><a href="{{url('view-changes', $outPut->RsrvId)}}">View Changes</a></li>
                @if($outPut->RsrvStat == '' && $PayBalance == 0)
                <li><a href="{{url('cancel-reservation', $outPut->RsrvId)}}">Cancel Reservation</a></li>
                @endif
            </ul>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                    	@if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                        @elseif ($message = Session::get('danger'))
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                        @endif
                    	<form id="demo-form2" action="{!!url('change-stay', $outPut->RsrvId)!!}"  method="post" class="form-horizontal form-label-left" data-parsley-validate>
                        	{!!method_field('PATCH')!!}
                            {!!csrf_field()!!}
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Guest Name:
                                </div>
                                <div class="col-md-8 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->GuestLname}}, {{$outPut->GuestFname}}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Company Name:
                                </div>
                                <div class="col-md-8 col-sm-12 col-xs-12 UpperLetter">
                                    {{$outPut->GuestCompany}}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Reservation#:
                                </div>
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    {{$outPut->RsrvId}}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Room#
                                </div>
                                <div class="col-md-8 col-sm-12 col-xs-12" id="RoomAssignId_{!!$outPut->RsrvId!!}">
                                    @if($outPut->RsrvStat == 'Checked Out' || $outPut->RsrvStat == 'Cancelled')
                                      	<div class="fltL">{{$outPut->RoomNumber}}</div>
                                      	<div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                    @else
                                        @if($outPut->RoomNumber)
                                        	@if($outPut->RsrvLock)
                                                <div class="fltL" style="position:relative">{{$outPut->RoomNumber}}</div>
                                        		<div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                                <i class="fa fa-lock lockFeaturButton cursorPointer" id="unLockButton" data-id="{!!$outPut->RsrvId!!}" aria-hidden="true"></i>
                                            @else
                                                <div class="fltL" style="position:relative">
                                                <a href="{{url('change-room', $outPut->RsrvId)}}" class="btn-list-link">{{$outPut->RoomNumber}}</a>
                                                @if($outPut->RsrvStat == "")
                                                <i class="closeButton fa fa-times" data-id="{!!$outPut->RsrvId!!}"></i>
                                                @endif
                                                </div>
                                    			<div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                                <i class="fa fa-unlock lockFeaturButton cursorPointer" id="LockButton" data-id="{!!$outPut->RsrvId!!}" aria-hidden="true"></i>
                                            @endif
                                        @else
                                            <a href="{{url('change-room', $outPut->RsrvId)}}" class="fltL btn-list-link">Select Room</a>
                                    		<div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop24">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Current Arrival Date:
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 UpperLetter">
                                	<input type="hidden" name="current_from" id="current_from" value="{!!$outPut->ArvlDate!!}" >
                                    {{$outPut->ArvlDate}}
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    Current Departure Date:
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 UpperLetter">
                                	<input type="hidden" name="current_to" id="current_to" value="{!!$outPut->DprtDate!!}" >
                                    {{$outPut->DprtDate}}
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop20">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText marginTop8">
                                    New Arrival Date
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    @if($outPut->RsrvStat != '')
                                    	<input type="hidden" name="from" id="from" value="{!!$outPut->ArvlDate!!}" >
                                        <input type="text" name="from_show" id="from_show" class="form-control has-feedback-left" placeholder="Arrival Date" value="{!!$outPut->ArvlDate!!}" disabled >
                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    @else
                                        <input type="text" name="from" id="from" class="form-control has-feedback-left" placeholder="Arrival Date" value="{!!$outPut->ArvlDate!!}" required="required" readonly="readonly">
                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    @endif
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText marginTop8">
                                    New Departure Date:
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12 UpperLetter">
                                    @if($outPut->RsrvStat == 'Checked Out' || $outPut->RsrvStat == 'Cancelled')
                                        <input type="hidden" name="to" id="to" value="{!!$outPut->DprtDate!!}" >
                                        <input type="text" name="to_show" id="to_show" class="form-control has-feedback-left" placeholder="Departure Date" value="{!!$outPut->DprtDate!!}" disabled >
                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    @else
                                        <input type="text" name="to" id="to" class="form-control has-feedback-left" placeholder="Departure Date" value="{!!$outPut->DprtDate!!}" required="required" readonly="readonly">
                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop24">
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText marginTop8">
                                    Number of Nights:
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12 UpperLetter">
                                    
                                    <input type="number" name="reserv_night" id="reserv_night" value="{{$outPut->StayNights}}" class="form-control col-md-3 col-xs-6" min="1" max="30" readonly="readonly">
                                </div>
                            </div>
                            @if($outPut->RsrvStat != 'Checked Out' && $outPut->RsrvStat != 'Cancelled')
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop24">
                                <div class="form-group form-group-last">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-2">
                                        <a href="{{ URL::previous() }}" class="btn btn-default"><i class="fa fa-chevron-left"></i> Back</a>
                                        <button type="submit" class="btn btn-success marginLeft10"><i class="fa fa-check-square-o"></i> Update Reservation</button>
                                    </div>
                                </div>
                            </div>
                            @endif
                       	</form>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
@stop  

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var checkin = $('#from').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		var startVal = $('#from').val();
		var endVal = $('#to').val();
		if(startVal >= endVal){
			var newDate = new Date(ev.date);
			newDate.setDate(newDate.getDate() + 1);
		}else{
			var newDate = new Date(endVal);
			newDate.setDate(newDate.getDate());
		}
		checkin.hide();
		checkout.setValue(newDate);
		var startVal = $('#from').val();
		var endVal = $('#to').val();
		if(startVal && endVal){
			$('#reserv_night').val(daydiff(parseDate(startVal), parseDate(endVal)));
		}
		$('#to')[0].focus();
	}).data('datepicker');
	
	var checkout = $('#to').datepicker({
		onRender: function(date) {
			if(now.valueOf()>checkin.date.valueOf()){
			return date.valueOf() <= now.valueOf() ? 'disabled' : '';
			}else{
			return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';	
			}
		}
	}).on('changeDate', function(ev){
		checkout.hide();
		var startVal = $('#from').val();
		var endVal = $('#to').val();
		if(startVal && endVal){
			$('#reserv_night').val(daydiff(parseDate(startVal), parseDate(endVal)));
		}
		var currentArrivalDate = $("#current_from").val();
		var currentDepartDate = $("#current_to").val();
		if(startVal != currentArrivalDate || endVal != currentDepartDate){
			checkAvailability();
		}
	}).data('datepicker');
	
	function parseDate(str) {
		var mdy = str.split('/');
		return new Date(mdy[2], mdy[0]-1, mdy[1]);
	}
	
	function daydiff(first, second) {
		return Math.round((second-first)/(1000*60*60*24));
	}
	
	$('#from').on("keydown", function(t) {
		if(t.which == 13){
			var startVal = $('#from').val();
			var endVal = $('#to').val();
			var currentArrivalDate = $("#current_from").val();
			var currentDepartDate = $("#current_to").val();
			if(startVal != currentArrivalDate || endVal != currentDepartDate){
				checkAvailability();
			}
		}
    })
	
	$('#to').on("keydown", function(t) {
		if(t.which == 13){
			var startVal = $('#from').val();
			var endVal = $('#to').val();
			var currentArrivalDate = $("#current_from").val();
			var currentDepartDate = $("#current_to").val();
			if(startVal != currentArrivalDate || endVal != currentDepartDate){
				checkAvailability();
			}
		}
    })
	
	function checkAvailability(){
		var RsrvId = '{{$outPut->RsrvId}}';
		var ArvlDate = $("#from").val();
		var DprtDate = $("#to").val();
		return false;
		/*$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		$.ajax({
			url: "{{ url('/check_availability_Room') }}",
			type: 'POST',
			data:{"RsrvId":RsrvId, "ArvlDate":ArvlDate, "DprtDate":DprtDate, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				if ( msg.status === 'success' ) {
					$("#behalfCompany").html(msg.response);
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.error( msg.response );
				}
				if ( msg.status === 'error' ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.error( msg.response );
				}
			},
			error: function( data ) {
				if ( data.status === 422 ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.error('Cannot find the result');
				}
			}
		});
		return false;*/
	}
	$('.closeButton').on('click', function(e) {
		var x = confirm("Are you sure you want to un assigned this Room Number?");
		if (x){
			var rsrvId = $(this).attr('data-id');
			$.ajax({
				url: "{{ url('/unAssignRoomNumber') }}",
				type: 'POST',
				data:{"rsrvId":rsrvId, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					if ( msg.status === 'success' ) {
						$('#RoomAssignId_'+rsrvId).html("<a href=\"{{url('change-room')}}/"+rsrvId+"?type=income-wo-check-in\" class=\"btn-list-link\">Select Room</a>");
						$('#CheckInAssignId_'+rsrvId).html("<a href=\"{{url('change-room')}}/"+rsrvId+"?type=check-in\" class=\"left btn btn-info buTTonResize\">Check-In</a>");
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					if ( data.status === 422 ) {
						toastr.error('Cannot delete the Assigned Room');
					}
				}
			});
			return true;
		}else{
			return false;
		}
	});
	
	$(document).on('click', '.lockFeaturButton', function(e){
		var selfId = $(this).attr('id');
		if(selfId == 'LockButton'){
			var C_selfId = 'unLockButton';
			var rsrvLock = 1;
			var Msg = "Are you sure you want to Lock this Room Number?";
		}else{
			var C_selfId = 'LockButton';
			var rsrvLock = 0;
			var Msg = "Are you sure you want to Unlock this Room Number?";
		}
		var x = confirm(Msg);
		if (x){
			var rsrvId = $(this).attr('data-id');
			$("#main_container_Loading").show();
			$("#main_container_overlay").show();
			$.ajax({
				url: "{{ url('/lockAssignRoomNumber') }}",
				type: 'POST',
				data:{"rsrvId":rsrvId, "rsrvLock":rsrvLock, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( msg.status === 'success' ) {
						$('#RoomAssignId_'+rsrvId).html(msg.content);
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( data.status === 422 ) {
						toastr.error('Cannot handle Request.');
					}
				}
			});
			return true;
		}else{
			return false;
		}
	});
});
</script>
@stop