<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Zeamsters\lib\getTransactionDetails as getTransaction;
use App\Zeamsters\lib\transactionProcess;
use App\Zeamsters\lib\ZeamsterPaymentTransaction as zeamsterApi;
use App\Zeamsters\lib\Contact;
use App\Zeamsters\lib\Locations;
use App\Zeamsters\lib\Tags;
use App\Zeamsters\lib\Notes;
use App\Zeamsters\lib\Accountvaults;
use App\Zeamsters\lib\User;
use App\Zeamsters\lib\Recurring;
use App\Zeamsters\lib\Terminal;

use App\RoomReservation;
use App\RoomReservationFolio;
use App\RoomReservationFolioMeta;
use App\Roomtype;
use App\GatewayPayment;
use App\GatewayCredential;
use App\ZeamsterLog;
use App\GatewayToken;
use App\CardGuarantee;
use App\PaymentOption;
use Helper;
use DB;

class ZeamsterSDKController extends Controller
{
	public function __construct()
    {
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();

		$GatewayId = 1;
		$cRst = GatewayCredential::where([['hotel_id', $Hotel_ID], ['gateway_id', $GatewayId]])->first();
		
		$LocationId	  = $cRst->location_id;
		$TicketHash	  = $cRst->ticket_hash_key;
		$UserId		  = $cRst->user_id;
		$UserApiKey	  = $cRst->user_api_key;
		$DeveloperId  = $cRst->developer_id;

	
        define('BASE_PATH',dirname(__FILE__));
		define('BASE_URL',$_SERVER['HTTP_HOST']);
		define('DS', DIRECTORY_SEPARATOR);
		
		/* Set variable for Log file */
		
		define("ZEAMSTER_LOG", "zeamster.log");
		
		/* Set variable for Zeamster API Config */
		/* All CONSTANT have system generated value using Zeamster Sandbox or Live account */
		
		define("LOCATION_ID", $LocationId);		// MMK Location Id
		define("TICKET_HASH", $TicketHash);		// MMK Ticket Hash
		define("USER_ID",     $UserId);			// MMK User Id
		define("USER_API_KEY",$UserApiKey);		// MMK User Api Key
		define("DEVELOPER_ID",$DeveloperId);	// MMK Developer Id   
    }
	
	public function zeamster_transaction(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		
		$GatewayId = 1;
		
		$rsrvId 	 = $request->rsrvid;
		$cardHolder  = $request->cardholder;
		$cardNumber  = $request->card_number;
		$cardExpiry  = $request->exp_date;
		$cardCVV     = $request->cvv;
		$cardZIP     = $request->zip;
		$transAction = $request->action;
		$transTicket = $request->token;
		$transAmount = $request->payamount;
		$transType   = (isset($request->type) && !empty($request->type))?$request->type:0;
		
		$Rst = DB::select("SELECT RSRV.hotel_id, RSRV.room_type_id, RSRV.arrival, RSRV.departure,
						   GST.guest_id, GST.guest_fname, GST.guest_lname, GST.guest_address, GST.guest_phone
						   FROM room_reservation as RSRV INNER JOIN guest as GST ON GST.guest_id = RSRV.guest_id
						   WHERE RSRV.room_reservation_id = ".$rsrvId." AND RSRV.hotel_id = ".$Hotel_ID);
		foreach($Rst as $Vals){
			$GuestId    = $Vals->guest_id;
			$RoomTypeId = $Vals->room_type_id;
			$checkIn    = $Vals->arrival;
			$checkOut   = $Vals->departure;
			if(!$cardHolder){
				$cardHolder = $Vals->guest_fname." ".$Vals->guest_lname;
			}
		}
		$billingStreet = "";
		$billingPhone  = "";
	
		$objTransaction = new transactionProcess();
		
		if (!empty($transAction))
		{
			$objTransaction->account_holder_name = $cardHolder;
			$objTransaction->billing_street = $billingStreet;
			$objTransaction->billing_phone = $billingPhone;
			$objTransaction->payment_method = 'cc';
			$objTransaction->account_vault_id = '';
			$objTransaction->transaction_amount = $transAmount;
			$objTransaction->checkin_date = $checkIn;
			$objTransaction->checkout_date = $checkOut;
			$objTransaction->room_rate = $transAmount;
			if(strtolower($transAction) == 'sale'){
				$objTransaction->action = zeamsterApi::CHARGE;
			}elseif(strtolower($transAction) == 'auth'){
				$objTransaction->action = zeamsterApi::AUTHONLY;
			}
			$objTransaction->location_id = LOCATION_ID;
			$objTransaction->order_num = $rsrvId;
			$objTransaction->contact_id = '';
			if($transType == 1){
				$objTransaction->previous_transaction_id = $transTicket;
			}else{
				$objTransaction->ticket = $transTicket;
			}
			$resultArray = transactionProcess::charge($objTransaction);
			if(isset($resultArray['transaction']) && !empty($resultArray['transaction']))
			{
				$FirstSix   = $resultArray['transaction']['first_six'];
				$LastFour   = $resultArray['transaction']['last_four'];
				$CardType   = $resultArray['transaction']['account_type'];
				$tokenTicket= $resultArray['transaction']['id'];
				$statTicket = $resultArray['transaction']['status_id'];
				
				if($resultArray['transaction']['status_id'] == '101' || $resultArray['transaction']['status_id'] == '102')
				{
					if(strtolower($transAction) == 'sale'){
						$paymentMode  = 2; 
						$FolioPayType = 'CC Payment';
						$unique_ID = "CRD".$Helper->getUniqueId().$rsrvId;
					}elseif(strtolower($transAction) == 'auth'){
						$paymentMode  = 3;
						$FolioPayType = 'CC Payment Auth Only';
						$unique_ID = "ATH".$Helper->getUniqueId().$rsrvId;
					}
					
					$PaymentOptionOBJ = new PaymentOption;
					$PaymentOptionOBJ->transaction_id = $unique_ID;
					$PaymentOptionOBJ->payment_mode   = $paymentMode;
					$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
					$PaymentOptionOBJ->room_reservation_id    = $rsrvId;
					$PaymentOptionOBJ->gateway_transaction_id = $resultArray['transaction']['id'];
					$PaymentOptionOBJ->reg_ip   = $_SERVER['REMOTE_ADDR'];
					$PaymentOptionOBJ->added_by = $admin_ID;
					$PaymentOptionOBJ->save();
					
					$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
					$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
					$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrvId;
					$RoomReservationFolioMetaOBJ->folio_type = 0;
					$RoomReservationFolioMetaOBJ->folio_date = date("Y-m-d");
					$RoomReservationFolioMetaOBJ->folio_pay_type = $FolioPayType;
					$RoomReservationFolioMetaOBJ->folio_amount = $transAmount;
					$RoomReservationFolioMetaOBJ->folio_comment = $FirstSix."xxxxxxxxxxx".$LastFour;
					$RoomReservationFolioMetaOBJ->folio_auto  = 0;
					$RoomReservationFolioMetaOBJ->folio_order = 0;
					$RoomReservationFolioMetaOBJ->log_id = $PaymentOptionOBJ->log_id;
					$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
					$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
					$RoomReservationFolioMetaOBJ->save();
				}
				
				$ZeamsterLogArray   = array();
				$ZeamsterLogArray[] = $Hotel_ID;
				$ZeamsterLogArray[] = $resultArray['transaction']['id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['payment_method'];
				$ZeamsterLogArray[] = $resultArray['transaction']['account_vault_id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['recurring_id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['first_six'];
				$ZeamsterLogArray[] = $resultArray['transaction']['last_four'];
				$ZeamsterLogArray[] = $resultArray['transaction']['account_holder_name'];
				$ZeamsterLogArray[] = $resultArray['transaction']['transaction_amount'];
				$ZeamsterLogArray[] = $resultArray['transaction']['description'];
				$ZeamsterLogArray[] = $resultArray['transaction']['transaction_code'];
				$ZeamsterLogArray[] = $resultArray['transaction']['avs'];
				$ZeamsterLogArray[] = $resultArray['transaction']['batch'];
				$ZeamsterLogArray[] = $resultArray['transaction']['order_num'];
				$ZeamsterLogArray[] = $resultArray['transaction']['verbiage'];
				$ZeamsterLogArray[] = $resultArray['transaction']['transaction_settlement_status'];
				$ZeamsterLogArray[] = $resultArray['transaction']['effective_date'];
				$ZeamsterLogArray[] = $resultArray['transaction']['routing'];
				$ZeamsterLogArray[] = $resultArray['transaction']['return_date'];
				$ZeamsterLogArray[] = $resultArray['transaction']['created_ts'];
				$ZeamsterLogArray[] = $resultArray['transaction']['modified_ts'];
				$ZeamsterLogArray[] = $resultArray['transaction']['transaction_api_id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['terms_agree'];
				$ZeamsterLogArray[] = $resultArray['transaction']['notification_email_address'];
				$ZeamsterLogArray[] = $resultArray['transaction']['notification_email_sent'];
				$ZeamsterLogArray[] = $resultArray['transaction']['response_message'];
				$ZeamsterLogArray[] = $resultArray['transaction']['auth_amount'];
				$ZeamsterLogArray[] = $resultArray['transaction']['auth_code'];
				$ZeamsterLogArray[] = $resultArray['transaction']['status_id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['type_id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['location_id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['reason_code_id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['contact_id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['billing_zip'];
				$ZeamsterLogArray[] = $resultArray['transaction']['billing_street'];
				$ZeamsterLogArray[] = $resultArray['transaction']['product_transaction_id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['tax'];
				$ZeamsterLogArray[] = $resultArray['transaction']['customer_ip'];
				$ZeamsterLogArray[] = $resultArray['transaction']['customer_id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['po_number'];
				$ZeamsterLogArray[] = $resultArray['transaction']['avs_enhanced'];
				$ZeamsterLogArray[] = $resultArray['transaction']['cvv_response'];
				$ZeamsterLogArray[] = $resultArray['transaction']['billing_phone'];
				$ZeamsterLogArray[] = $resultArray['transaction']['billing_city'];
				$ZeamsterLogArray[] = $resultArray['transaction']['billing_state'];
				$ZeamsterLogArray[] = $resultArray['transaction']['clerk_number'];
				$ZeamsterLogArray[] = (isset($resultArray['transaction']['batch_close_ts']))?$resultArray['transaction']['batch_close_ts']:'';
				$ZeamsterLogArray[] = $resultArray['transaction']['settle_date'];
				$ZeamsterLogArray[] = $resultArray['transaction']['charge_back_date'];
				$ZeamsterLogArray[] = $resultArray['transaction']['void_date'];
				$ZeamsterLogArray[] = $resultArray['transaction']['account_type'];
				$ZeamsterLogArray[] = $resultArray['transaction']['is_recurring'];
				$ZeamsterLogArray[] = $resultArray['transaction']['is_accountvault'];
				$ZeamsterLogArray[] = $resultArray['transaction']['transaction_c1'];
				$ZeamsterLogArray[] = $resultArray['transaction']['transaction_c2'];
				$ZeamsterLogArray[] = $resultArray['transaction']['transaction_c3'];
				$ZeamsterLogArray[] = $resultArray['transaction']['terminal_serial_number'];
				$ZeamsterLogArray[] = $resultArray['transaction']['entry_mode_id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['terminal_id'];
				$ZeamsterLogArray[] = $resultArray['transaction']['checkin_date'];
				$ZeamsterLogArray[] = $resultArray['transaction']['checkout_date'];
				$ZeamsterLogArray[] = $resultArray['transaction']['room_num'];
				$ZeamsterLogArray[] = $resultArray['transaction']['room_rate'];
				$ZeamsterLogArray[] = $resultArray['transaction']['advance_deposit'];
				$ZeamsterLogArray[] = $resultArray['transaction']['no_show'];
				$ZeamsterLogArray[] = $resultArray['transaction']['emv_receipt_data'];
				$ZeamsterLogArray[] = $resultArray['transaction']['_links']['self']['href'];
				$ZeamsterLogArray[] = $_SERVER['REMOTE_ADDR'];
				$ZeamsterLogArray[] = $admin_ID;
				
				DB::select('CALL ZeamsterLog_Insert_SP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $ZeamsterLogArray);
				
				$TokenQry = GatewayToken::where([ ['guest_id', $GuestId], ['first_six', $FirstSix], ['last_four', $LastFour], 
												  ['gateway_id', $GatewayId], ['room_reservation_id', $rsrvId] ])->first();
				if(count($TokenQry)){
					if($TokenQry->ticket != $tokenTicket)
					{
						$TokenQry->ticket     = $tokenTicket;
						$TokenQry->ticket_stat= $statTicket;
						$TokenQry->reg_ip     = $_SERVER['REMOTE_ADDR'];
						$TokenQry->added_by   = $admin_ID;
						$TokenQry->save();
					}else{
						$TokenQry->ticket_stat= $statTicket;
						$TokenQry->reg_ip     = $_SERVER['REMOTE_ADDR'];
						$TokenQry->added_by   = $admin_ID;
						$TokenQry->save();
					}
				}else{
					$GatewayTokenArray   = array();
					$GatewayTokenArray[] = $GuestId;
					$GatewayTokenArray[] = $rsrvId;
					$GatewayTokenArray[] = $GatewayId;
					$GatewayTokenArray[] = $CardType;
					$GatewayTokenArray[] = $FirstSix;
					$GatewayTokenArray[] = $LastFour;
					$GatewayTokenArray[] = $tokenTicket;
					$GatewayTokenArray[] = $statTicket;
					$GatewayTokenArray[] = $_SERVER['REMOTE_ADDR'];
					$GatewayTokenArray[] = $admin_ID;
					
					DB::select('CALL GatewayToken_Insert_SP(?,?,?,?,?,?,?,?,?,?)', $GatewayTokenArray);
				}
				
				if($transType == 0){
					$cardGuarantee = CardGuarantee::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $rsrvId], ['card_number', $cardNumber] ])->first();
					if(!count($cardGuarantee)){
						$CardGuaranteeArray   = array();
						$CardGuaranteeArray[] = $Hotel_ID;
						$CardGuaranteeArray[] = $rsrvId;
						$CardGuaranteeArray[] = '0';
						$CardGuaranteeArray[] = $CardType;
						$CardGuaranteeArray[] = $cardNumber;
						$CardGuaranteeArray[] = $cardExpiry;
						$CardGuaranteeArray[] = $cardCVV;
						$CardGuaranteeArray[] = $cardHolder;
						$CardGuaranteeArray[] = "";
						$CardGuaranteeArray[] = "";
						$CardGuaranteeArray[] = $cardZIP;
						$CardGuaranteeArray[] = "";
						$CardGuaranteeArray[] = "";
						$CardGuaranteeArray[] = $_SERVER['REMOTE_ADDR'];
						$CardGuaranteeArray[] = $admin_ID;
						
						DB::select('CALL Reservation_Guarantee_Insert_SP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $CardGuaranteeArray);
					}else{
						$cardGuarantee->hotel_id    = $Hotel_ID;
						$cardGuarantee->room_reservation_id = $rsrvId;
						$cardGuarantee->card_code	= $CardType;
						$cardGuarantee->card_number	= $cardNumber;
						$cardGuarantee->card_expiry	= $cardExpiry;
						$cardGuarantee->card_cvv	= $cardCVV;
						$cardGuarantee->card_holder	= $cardHolder;
						$cardGuarantee->postal_code	= $cardZIP;
						$cardGuarantee->reg_ip      = $_SERVER['REMOTE_ADDR'];
						$cardGuarantee->added_by 	= $admin_ID;
						$cardGuarantee->save();
					}
				}
				
				if($resultArray['transaction']['status_id'] == '101')
				{
					return redirect('guest-folio/'.$rsrvId)->with("success", "Payment Approved");
				}
				elseif($resultArray['transaction']['status_id'] == '102')
				{
					return redirect('guest-folio/'.$rsrvId)->with("success", "Payment AuthOnly");
				}
				elseif($resultArray['transaction']['status_id'] == '201')
				{
					return redirect('guest-folio/'.$rsrvId)->with("warning", "Payment Voided");
				}
				elseif($resultArray['transaction']['status_id'] == '301')
				{
					return redirect('guest-folio/'.$rsrvId)->with("warning", "Payment Declined");
				}
			}
			elseif(isset($resultArray['errors']['ticket'][0]) && !empty($resultArray['errors']['ticket'][0]))
			{
				$TokenQry = GatewayToken::where([ ['guest_id', $GuestId], ['ticket', $transTicket], ['gateway_id', $GatewayId], ['room_reservation_id', $rsrvId] ])->first();
				if(count($TokenQry)){
					$TokenQry->ticket_stat= $resultArray['errors']['ticket'][0];
					$TokenQry->reg_ip     = $_SERVER['REMOTE_ADDR'];
					$TokenQry->added_by   = $admin_ID;
					$TokenQry->save();
				}
				return redirect('guest-folio/'.$rsrvId)->with("danger", $resultArray['errors']['ticket'][0]);
			}
			elseif(isset($resultArray['errors']['checkin_date'][0]) && !empty($resultArray['errors']['checkin_date'][0]))
			{
				$TokenQry = GatewayToken::where([ ['guest_id', $GuestId], ['ticket', $transTicket], ['gateway_id', $GatewayId], ['room_reservation_id', $rsrvId] ])->first();
				if(count($TokenQry)){
					$TokenQry->ticket_stat= $resultArray['errors']['checkin_date'][0];
					$TokenQry->reg_ip     = $_SERVER['REMOTE_ADDR'];
					$TokenQry->added_by   = $admin_ID;
					$TokenQry->save();
				}
				return redirect('guest-folio/'.$rsrvId)->with("danger", $resultArray['errors']['checkin_date'][0]);
			}
		}
	}
}
