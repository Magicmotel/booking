<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    protected $table = 'shifts';
	protected $primaryKey = 'shift_id';
	protected $fillable = [
        'hotel_id', 'shift_name', 'shift_from_time', 'shift_from_meridiem', 'shift_to_time', 'shift_to_meridiem', 'shift_color',
		'reg_ip', 'added_by'
    ];
}
