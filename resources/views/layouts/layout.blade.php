<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{url('/favicon.png')}}"/>
    {!!Html::style('vendors/bootstrap/dist/css/bootstrap.min.css')!!}
    {!!Html::style('vendors/font-awesome/css/font-awesome.min.css')!!}
    {!!Html::style('vendors/nprogress/nprogress.css')!!}
    {!!Html::style('vendors/iCheck/skins/flat/green.css')!!}
    
    {!!Html::style('vendors/google-code-prettify/bin/prettify.min.css')!!}
    {!!Html::style('vendors/select2/dist/css/select2.min.css')!!}
    {!!Html::style('vendors/switchery/dist/switchery.min.css')!!}
    {!!Html::style('vendors/starrr/dist/starrr.css')!!}
    
    {!!Html::style('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css')!!}
    {!!Html::style('vendors/jqvmap/dist/jqvmap.min.css')!!}
    {!!Html::style('vendors/bootstrap-daterangepicker/datepicker.css')!!}
    
    {!!Html::style('vendors/fullcalendar/dist/fullcalendar.min.css')!!}
    {!!Html::style('vendors/fullcalendar/dist/fullcalendar.print.css', array('media' => 'print'))!!}
    
    @yield('CascadingSheet')
    
    {!!Html::style('build/css/custom.min.css')!!}
</head>

<body class="nav-md">
	<div id="main_container_Loading"><div class="LoadingImg">{!!Html::image('images/loading.gif', 'alt')!!}</div></div> <div id="main_container_overlay"></div>
	<div class="container body">
  		<div class="main_container">
			<!-- Left Menu Navigation -->
			@include('layouts.menu')
            <!-- /Left Menu Navigation -->
            
            <!-- Top Right Side Navigation -->
			@include('layouts.top_navigation')
        	<!-- /Top Right Side Navigation -->
            
            <!-- page content -->
            @yield('body')
        	<!-- /page content -->
            
        	<!-- footer content -->
			@include('layouts.footer')      		
       		<!-- /footer content -->
      	</div>
    </div>
	<!-- jQuery -->
    {!!Html::script('vendors/jquery/dist/jquery.min.js')!!}
    {!!Html::script('vendors/jquery/dist/jquery-ui.min.js')!!}
    <!-- Bootstrap -->
    {!!Html::script('vendors/bootstrap/dist/js/bootstrap.min.js')!!}
    <!-- FastClick -->
    {!!Html::script('vendors/fastclick/lib/fastclick.js')!!}
    <!-- NProgress -->
    {!!Html::script('vendors/nprogress/nprogress.js')!!}
    <!-- Chart.js -->
    {!!Html::script('vendors/Chart.js/dist/Chart.min.js')!!}
    <!-- Sparklines.js -->
    {!!Html::script('vendors/jquery-sparkline/dist/jquery.sparkline.min.js')!!}
    <!-- gauge.js -->
    {!!Html::script('vendors/gauge.js/dist/gauge.min.js')!!}
    <!-- bootstrap-progressbar -->
    {!!Html::script('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')!!}
    <!-- iCheck -->
    {!!Html::script('vendors/iCheck/icheck.min.js')!!}
    <!-- Skycons -->
    {!!Html::script('vendors/skycons/skycons.js')!!}
    <!-- Flot -->
    {!!Html::script('vendors/Flot/jquery.flot.js')!!}
    {!!Html::script('vendors/Flot/jquery.flot.pie.js')!!}
    {!!Html::script('vendors/Flot/jquery.flot.time.js')!!}
    {!!Html::script('vendors/Flot/jquery.flot.stack.js')!!}
    {!!Html::script('vendors/Flot/jquery.flot.resize.js')!!}
    <!-- Flot plugins -->
    {!!Html::script('vendors/flot.orderbars/js/jquery.flot.orderBars.js')!!}
    {!!Html::script('vendors/flot-spline/js/jquery.flot.spline.min.js')!!}
    {!!Html::script('vendors/flot.curvedlines/curvedLines.js')!!}
    <!-- DateJS -->
    {!!Html::script('vendors/DateJS/build/date.js')!!}
    <!-- JQVMap
    {!!Html::script('vendors/jqvmap/dist/jquery.vmap.js')!!}
    {!!Html::script('vendors/jqvmap/dist/maps/jquery.vmap.world.js')!!}
    {!!Html::script('vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')!!}
    <!-- bootstrap-daterangepicker -->
    {!!Html::script('vendors/moment/min/moment.min.js')!!}
    {!!Html::script('vendors/bootstrap-daterangepicker/datepicker.js')!!}
    <!--{!!Html::script('vendors/fullcalendar/dist/fullcalendar.min.js')!!} -->
    <!-- bootstrap-wysiwyg -->
    {{ HTML::script('vendors/assets/js/ckeditor/ckeditor.js') }}
    {!!Html::script('vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')!!}
    {!!Html::script('vendors/jquery.hotkeys/jquery.hotkeys.js')!!}
    {!!Html::script('vendors/google-code-prettify/src/prettify.js')!!}
    <!-- jQuery Tags Input -->
    {!!Html::script('vendors/jquery.tagsinput/src/jquery.tagsinput.js')!!}
    <!-- Switchery -->
    {!!Html::script('vendors/switchery/dist/switchery.min.js')!!}
    <!-- Select2 -->
    {!!Html::script('vendors/select2/dist/js/select2.full.min.js')!!}
    <!-- Parsley -->
    {!!Html::script('vendors/parsleyjs/dist/parsley.min.js')!!}
    <!-- Autosize -->
    {!!Html::script('vendors/autosize/dist/autosize.min.js')!!}
    <!-- jQuery autocomplete -->
    {!!Html::script('vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js')!!}
    <!-- starrr -->
    {!!Html::script('vendors/starrr/dist/starrr.js')!!}
    
    @yield('JavascriptSRC')
    
    <!-- Custom Theme Scripts -->
    {!!Html::script('build/js/custom.min.js')!!}

    <!-- bootstrap-daterangepicker -->
    @yield('jQuery')  
    <script type="text/javascript" language="javascript">
        $(window).load(function() {
			$("#main_container_Loading").hide();
			$("#main_container_overlay").hide();
		});
    </script>
</body>
</html>