<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Hotel;
use App\HotelPayment;
use App\HotelSupport;
use Image;
use Helper;
use DB;

class HotelController extends Controller
{
	public function index()
    {
        $Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$folderImage_Path = $Helper->getImageFolderPath();
		
		$Results = Hotel::where('hotel_id', $Hotel_ID)->first();
		return view('hotels.motels')->with("Results", $Results)->with("folderImage_Path", $folderImage_Path);
    }

    public function edit($id)
    {	
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$folderImage_Path = $Helper->getImageFolderPath();
		
		if($Hotel_ID == $id){
			$Results  = Hotel::find($id);
			return view('hotels.motels-edit')->with("Results", $Results)->with("folderImage_Path", $folderImage_Path);
		}else{
			return redirect('motels')->with('danger','Please select your Motel!');
		}
    }

    public function update(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID    = $Helper->getHotelId();
		$admin_ID    = $Helper->getAdminUserId();
		$rootImage_Path = $Helper->getImageRootPath();
		$imageName   = $Helper->setImgName($request->hotel_name);
		$imagePrefix = $imageName."-".$Hotel_ID;
		
		$this->validate($request, [
			'hotel_logo'  => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:512',
			'hotel_pic_1' => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:2048',
			'hotel_pic_2' => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:2048',
			'hotel_pic_3' => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:2048',
			'hotel_pic_4' => 'mimes:jpeg,JPEG,jpg,JPG,png,PNG,gif,GIF | max:2048'
		]);
		
		$Results = Hotel::where('hotel_id', $id)->first();
		if(count($Results)){
			$Results->hotel_name  = $request->hotel_name;
			$Results->hotel_rooms  = $request->hotel_rooms;
			
			$Results->hotel_address = $request->hotel_address;
			$Results->hotel_city = $request->hotel_city;
			$Results->hotel_county = $request->hotel_county;
			//$Results->hotel_state = $request->hotel_state;
			$Results->hotel_zip = $request->hotel_zip;
			$Results->hotel_country = $request->hotel_country;
			
			$Results->hotel_phone_1 = $request->hotel_phone_1;
			$Results->hotel_phone_2 = $request->hotel_phone_2;
			$Results->hotel_phone_3 = $request->hotel_phone_3;
			$Results->hotel_phone_4 = $request->hotel_phone_4;
			$Results->hotel_fax_1 = $request->hotel_fax_1;
			$Results->hotel_fax_2 = $request->hotel_fax_2;
			$Results->hotel_website = $request->hotel_website;
			$Results->hotel_email = $request->hotel_email;
			
			$Results->hotel_o_fname = $request->hotel_o_fname;
			$Results->hotel_o_mname = $request->hotel_o_mname;
			$Results->hotel_o_lname = $request->hotel_o_lname;
			$Results->hotel_o_phone_1 = $request->hotel_o_phone_1;
			$Results->hotel_o_phone_2 = $request->hotel_o_phone_2;
			$Results->hotel_o_email   = $request->hotel_o_email;
			
			$Results->hotel_m_fname = $request->hotel_m_fname;
			$Results->hotel_m_mname = $request->hotel_m_mname;
			$Results->hotel_m_lname = $request->hotel_m_lname;
			$Results->hotel_m_phone_1 = $request->hotel_m_phone_1;
			$Results->hotel_m_phone_2 = $request->hotel_m_phone_2;
			$Results->hotel_m_email = $request->hotel_m_email;
			
			$fileFolderPath = $rootImage_Path."/motel";
			
			$logo_img = $request->file('hotel_logo');
			$logo_name = "";
			if($logo_img){
				$logo_name = $imagePrefix."-logo.".$logo_img->getClientOriginalExtension();
				$logo_img->move($fileFolderPath, $logo_name);
				Image::make($fileFolderPath."/".$logo_name)->resize(120, 120, function ($constraint) { $constraint->aspectRatio(); })->save();
			}else{
				$logo_name = $Results->hotel_logo;
			}
			
			$pic_1 = $request->file('hotel_pic_1');
			$pic_1_name = "";
			if($pic_1){
				$pic_1_name = $imagePrefix."-1.".$pic_1->getClientOriginalExtension();
				$pic_1->move($fileFolderPath, $pic_1_name);
				Image::make($fileFolderPath."/".$pic_1_name)->resize(800, 350, function ($constraint) { $constraint->aspectRatio(); })->save();
			}else{
				$pic_1_name = $Results->hotel_pic_1;
			}
			
			$pic_2 = $request->file('hotel_pic_2');
			$pic_2_name = "";
			if($pic_2){
				$pic_2_name = $imagePrefix."-2.".$pic_2->getClientOriginalExtension();
				$pic_2->move($fileFolderPath, $pic_2_name);
				Image::make($fileFolderPath."/".$pic_2_name)->resize(800, 350, function ($constraint) { $constraint->aspectRatio(); })->save();
			}else{
				$pic_2_name = $Results->hotel_pic_2;
			}
			
			$pic_3 = $request->file('hotel_pic_3');
			$pic_3_name = "";
			if($pic_3){
				$pic_3_name = $imagePrefix."-3.".$pic_3->getClientOriginalExtension();
				$pic_3->move($fileFolderPath, $pic_3_name);
				Image::make($fileFolderPath."/".$pic_3_name)->resize(800, 350, function ($constraint) { $constraint->aspectRatio(); })->save();
			}else{
				$pic_3_name = $Results->hotel_pic_3;
			}
			
			$pic_4 = $request->file('hotel_pic_4');
			$pic_4_name = "";
			if($pic_4){
				$pic_4_name = $imagePrefix."-4.".$pic_4->getClientOriginalExtension();
				$pic_4->move($fileFolderPath, $pic_4_name);
				Image::make($fileFolderPath."/".$pic_4_name)->resize(800, 350, function ($constraint) { $constraint->aspectRatio(); })->save();
			}else{
				$pic_4_name = $Results->hotel_pic_4;
			}		
			
			$Results->hotel_logo  = $logo_name;
			$Results->hotel_pic_1 = $pic_1_name;
			$Results->hotel_pic_2 = $pic_2_name;
			$Results->hotel_pic_3 = $pic_3_name;
			$Results->hotel_pic_4 = $pic_4_name;
			
			$Results->hotel_checkin_time  = $request->hotel_checkin_time;
			$Results->hotel_checkout_time = $request->hotel_checkout_time;
			$Results->hotel_cancel_period = $request->hotel_cancel_period;
			$Results->hotel_disclaimer    = $request->hotel_disclaimer;
					
			$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
			$Results->added_by = $admin_ID;
			$Results->save();
			
			return redirect('motels')->with('success','Motel updated successfully');
		}
		return redirect('motels')->with('danger','Motel not found');
    }
	
	// Payment Information
	
	public function payment_index()
    {
		$Hotel_ID = Helper::getHotelId();
		$Results = HotelPayment::where('hotel_id', $Hotel_ID)->first();
		return view('hotels.payment-motels')->with("Results", $Results);
    }

    public function payment_edit($id)
    {	
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$Results  = HotelPayment::find($id);
		if($Hotel_ID == $Results->hotel_id){
			return view('hotels.payment-motels-edit')->with("Results", $Results);
		}else{
			return redirect('payment-motels')->with('danger','Please select your Motel Payment!');
		}
    }

    public function payment_update(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		
		$Results = HotelPayment::where([ ['payment_id', $id], ['hotel_id', $Hotel_ID] ])->first();
		if(count($Results))
		{
			$Results->cc_fname  = $request->cc_fname;
			$Results->cc_mname  = $request->cc_mname;
			$Results->cc_lname 	= $request->cc_lname;
			$Results->cc_card_no= $request->cc_card_no;
			$Results->cc_expiry = $request->cc_expiry;
			$Results->cc_cvv   	= $request->cc_cvv;
			$Results->cc_address= $request->cc_address;
			$Results->cc_city 	= $request->cc_city;
			$Results->cc_zip 	= $request->cc_zip;
			
			$Results->ba_fname   = $request->ba_fname;
			$Results->ba_mname   = $request->ba_mname;
			$Results->ba_lname 	 = $request->ba_lname;
			$Results->ba_bankname= $request->ba_bankname;
			$Results->ba_account = $request->ba_account;
			$Results->ba_routing = $request->ba_routing;
			$Results->ba_address = $request->ba_address;
			$Results->ba_city 	 = $request->ba_city;
			$Results->ba_zip 	 = $request->ba_zip;
			
			$Results->paypal_link = $request->paypal_link;			
						
			$Results->reg_ip   = $_SERVER['REMOTE_ADDR'];
			$Results->added_by = $admin_ID;
			
			$Results->save();
			
			return redirect('payment-motels')->with('success','Motel Payment Information updated successfully');
		}
		return redirect('payment-motels')->with('danger','Motel not found');
    }
	
	public function removeImage(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		$rootImage_Path = $Helper->getImageRootPath();
		
		$hotel_pic_no = $request->hotel_pic_no;
		
		$Results = Hotel::where([ ['hotel_id', '=', $Hotel_ID] ])->first();
		
		$fileFolderPath = $rootImage_Path."/motel";
		$fileName = $fileFolderPath.'/'.$Results->{$hotel_pic_no};
		
		Helper::removeImg($fileName);
		
		$Results->{$hotel_pic_no} = "";
		$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
		$Results->added_by = $admin_ID;
		$Results->save();
		
		if($request->ajax()) {
			return response()->json(['response' => 'Motel Image deleted successfully', 'status' => 'success']);
		}else{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
	
	public function wifi_password_update(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		
		if($request->ajax())
		{
			$Results = Hotel::where('hotel_id', $Hotel_ID)->first();
			if(count($Results))
			{
				$Results->hotel_wifi_pass = $request->hotel_wifi_pass;
				$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Results->added_by = $admin_ID;
				$Results->save();
				return response()->json(['response' => 'Wi-Fi Password updated!', 'status' => 'success']);
			}
			return response()->json(['response' => 'Motel Not Found', 'status' => 'error']);
    	}else{
			return response()->json(['response' => 'Try Again', 'status' => 'error']);
		}
	}
	
	public function support()
    {
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		
		$supportRst = DB::select("SELECT U.fname as S_fname, U.lname as S_lname,
								  MU.fname as R_fname, MU.lname as R_lname, HS.*
								  FROM hotel_support as HS
								  LEFT JOIN users as U ON U.id = HS.added_by
								  LEFT JOIN mmk_users as MU ON MU.id = HS.updated_by
								  WHERE HS.hotel_id = ".$Hotel_ID." ORDER BY HS.support_id DESC");
		$supportNum = count($supportRst);
		DB::select("UPDATE hotel_support SET status = 0 WHERE hotel_id = ".$Hotel_ID." AND support_from = 2 AND status = 1");
		return view('hotels.support')->with('supportNum', $supportNum)->with('supportRst', $supportRst)->with('Hotel_ID', $Hotel_ID);
    }
	
	public function support_update(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		
		$supportRst = new HotelSupport;
		$supportRst->hotel_id = $id;
		$supportRst->support_body = $request->support_body;
		$supportRst->reg_ip = $_SERVER['REMOTE_ADDR'];
		$supportRst->added_by = $admin_ID;
		$supportRst->save();
		return redirect('support')->with('success', 'Thank you, someone will be in touch within 2 business days');
    }
}
