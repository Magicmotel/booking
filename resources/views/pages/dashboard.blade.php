@extends('layouts.layout')

@section('title')
	DashBoard Page
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12 marginTop10">
                <div class="tile-stats">
                    <h4>Today's Occupancy</h4>
                    <canvas width="232" height="116" id="foo" class="" style="width:232px; height:116px;"></canvas>
                    <div class="goal-wrapper" style="width:165px; margin:auto">
                        <span id="gauge-text" class="gauge-value pull-left">{{$occupancyRoom}}</span>
                        <span class="gauge-value pull-left">%</span>
                        <span id="goal-text" class="goal-value pull-right">100%</span>
                    </div>
                </div>
            </div>
            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12 marginTop10">
                <div class="tile-stats AlgnLeft">
                    <h4 class="">Total Arrival Today ( {{$todayCheckIN}} )</h4>
                    <table class="" style="width:100%">
                        <tr>
                            <td>
                                <canvas id="canvas1" height="120" width="120" style="margin:8px 0px 7px 5px"></canvas>
                            </td>
                            <td>
                                <table class="tile_info">
                                    <tr>
                                        <td><p><i class="fa fa-square green"></i>Check-Ins Completed </p></td>
                                    	<td>{{$cmpltCheckIN}}</td>
                                    </tr>
                                    <tr>
                                        <td><p><i class="fa fa-square blue"></i>Check-Ins Remaining </p></td>
                                        <td>{{$rmainCheckIN}}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="animated flipInY col-lg-5 col-md-5 col-sm-6 col-xs-12 marginTop10">
                <div class="tile-stats AlgnLeft">
                    <table class="" style="width:100%">
                        <tr>
                            <td>
                                <canvas id="canvas2" height="138" width="138" style="margin:29px 0px 18px 10px"></canvas>
                            </td>
                            <td>
                                <table class="tile_info">
                                    <tr>
                                        <td><p><i class="fa fa-square green"></i>Today Departures </p></td>
                                    	<td>{{$todayDeparture}}</td>
                                    </tr>
                                    <tr>
                                        <td><p><i class="fa fa-square red"></i>Vacant/Dirty Rooms Remaining </p></td>
                                        <td>{{$vacantDirty}}</td>
                                    </tr>
                                    <tr>
                                        <td><p><i class="fa fa-square blue"></i>Total Rooms Available </p></td>
                                        <td>{{$availRoom}}</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>            
        </div>

        <div class="row">
        	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 zeroPadd">
                <div class="animated flipInY col-lg-12 col-md-12 col-sm-6 col-xs-12">
                    <div class="tile-stats">
                        <h4>Today Activities</h4>
                        <div class="widget_summary">
                            <div class="w_left w_25">
                                <span>Check-Ins</span>
                            </div>
                            <div class="w_center w_55">
                                <div class="progress marginBottom10">
                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:{{$TA_CheckInPrct}}%;"></div>
                                </div>
                            </div>
                            <div class="w_right w_20">
                                <span>{{$cmpltCheckIN}}/{{$todayCheckIN}}</span>
                            </div>
                        </div>
                        
                        <div class="widget_summary">
                            <div class="w_left w_25">
                                <span>Stayovers</span>
                            </div>
                            <div class="w_center w_55">
                                <div class="progress marginBottom10">
                                    <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:{{$TA_StayPrct}}%;"></div>
                                </div>
                            </div>
                            <div class="w_right w_20">
                                <span>{{$stayRoom}}/{{$TotalRooms}}</span>
                            </div>
                        </div>
                        
                        <div class="widget_summary">
                            <div class="w_left w_25">
                                <span>Vacant</span>
                            </div>
                            <div class="w_center w_55">
                                <div class="progress marginBottom10">
                                    <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:{{$TA_VacantRPrct}}%;"></div>
                                </div>
                            </div>
                            <div class="w_right w_20">
                                <span>{{$availRoom}}/{{$TotalRooms}}</span>
                            </div>
                        </div>
                        
                        <div class="widget_summary">
                            <div class="w_left w_25">
                                <span>Block/Maint..nce</span>
                            </div>
                            <div class="w_center w_55">
                                <div class="progress zeroMargin">
                                    <div class="progress-bar bg-red" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:{{$TA_BlockPrct}}%;"></div>
                                </div>
                            </div>
                            <div class="w_right w_20">
                                <span>{{$blockRoom}}/{{$TotalRooms+$blockRoom}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class="animated flipInY col-lg-12 col-md-12 col-sm-6 col-xs-12">
                    <div class="tile-stats">
                        <h4 class="">Wi-Fi Password</h4>
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                            <div class="input-group">
                                <input type="text" name="hotel_wifi_pass" id="hotel_wifi_pass" value="{{$HotelRst->hotel_wifi_pass}}" class="form-control col-md-7 col-xs-12" placeholder="Enter Wi-Fi Password" required="required">          
                                <span class="input-group-btn">
                                    <button type="submit" id="setButton" class="btn btn-primary">SET</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
            
            <div class="animated flipInY col-lg-5 col-md-5 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <h4>Room Availability</h4>
                    {{--*/ $PKJ = 1; /*--}}
                    @foreach($RoomListQry as $RoomListVal)
                    <div class="widget_summary">
                        <div class="w_left w_25">
                            <span>{{$RoomListVal->room_type}}</span>
                        </div>
                        <div class="w_center w_55">
                            <div class="progress{{(count($RoomListQry)==$PKJ)?' marginZero':' marginBottom10'}}">
                                <div class="progress-bar bg-blue-sky" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:{{($TotalRooms)?round(($RoomListVal->CNT/$RoomListVal->room_count)*100):0}}%;">$ {{$RoomListVal->room_rate}}</div>
                            </div>
                        </div>
                        <div class="w_right w_20">
                            <span>{{$RoomListVal->CNT}}/{{$RoomListVal->room_count}}</span>
                        </div>
                    </div>
                    {{--*/$PKJ++;/*--}}
                    @endforeach                    
                </div>
            </div>
            
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="weather-icon TB_Padd5">
                                <img src="{{$weatherSchene['image']}}" width="60" height="60" />
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="weather-text">
                                <div class="weather-text pull-right">
                                    <h3 class="degrees">{{$weatherSchene['high']}}</h3>
                                </div>
                                <h2>{{$weatherSchene['name']}}<br><i>{{$weatherCondit}}</i></h2>
                            </div>
                        </div>
                    </div>
                </div>
          	</div>
        </div>
        <div class="row">
            
        	<div class="animated flipInY col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="tile-stats">
                    <h4>Recent Activities</h4>
                	<div class="dashboard-widget-content">
                        <ul class="list-unstyled timeline widget">
                            <li>
                                <div class="block">
                                    <div class="block_content">
                                        <h2 class="title"><a>Who Needs Sundance When You've Got Crowdfunding?</a></h2>
                                        <div class="byline"><span>13 hours ago</span> by <a>Jane Smith</a></div>
                                        <p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they'd pay to Fast-forward and... <a>Read More</a></p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="block">
                                    <div class="block_content">
                                        <h2 class="title"><a>Who Needs Sundance When You've Got Crowdfunding?</a></h2>
                                        <div class="byline"><span>13 hours ago</span> by <a>Jane Smith</a></div>
                                        <p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they'd pay to Fast-forward and... <a>Read More</a></p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="block">
                                    <div class="block_content">
                                        <h2 class="title"><a>Who Needs Sundance When You've Got Crowdfunding?</a></h2>
                                        <div class="byline"><span>13 hours ago</span> by <a>Jane Smith</a></div>
                                        <p class="excerpt">Film festivals used to be do-or-die moments for movie makers. They were where you met the producers that could fund your project, and if the buyers liked your flick, they'd pay to Fast-forward and... <a>Read More</a></p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type = "text/javascript" >
function disableBackButton()
{
	window.history.forward();
}
setTimeout("disableBackButton()", 0);
</script>
@stop

@section('JavascriptSRC')
   	{!!Html::script('vendors/toastr/js/toastr.js')!!} 
@stop 

@section('jQuery')
<script>
$(document).ready(function() {
	$('#setButton').on('click', function(){
		$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		var hotel_wifi_pass = $('#hotel_wifi_pass').val();
		$.ajax({
			url: "{{ url('/hotel-wifi-password') }}",
			type: 'POST',
			data:{"hotel_wifi_pass":hotel_wifi_pass, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				if ( msg.status === 'success' ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.success( msg.response );
				}
				if ( msg.status === 'error' ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.error( msg.response );
				}
			},
			error: function( data ) {
				if ( data.status === 422 ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					toastr.error('Try Again!!!');
				}
			}
		});
		return false;
	});
});

<!-- Skycons -->
$(document).ready(function() {
	var icons = new Skycons({ "color": "#73879C" }),
  	list = [ "clear-day", "clear-night", "partly-cloudy-day", "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind", "fog" ],
  	i;

	for(i = list.length; i--;)
  		icons.set(list[i], list[i]);

	icons.play();
});


<!-- gauge.js -->
var opts = {
	lines: 12,
	angle: 0,
	lineWidth: 0.4,
	pointer: {
		length: 0.75,
		strokeWidth: 0.042,
		color: '#1D212A'
	},
	limitMax: 'false',
	colorStart: '#1ABC9C',
	colorStop: '#1ABC9C',
	strokeColor: '#F0F3F3',
	generateGradient: true
};
var target = document.getElementById('foo'),
	gauge = new Gauge(target).setOptions(opts);
	
var startGaugeVal = parseInt('{{$occupancyRoom}}');
if(startGaugeVal==0){
	startGaugeVal = 0.1;
}

gauge.maxValue = 100;
gauge.animationSpeed = 32;
gauge.set(startGaugeVal);
gauge.setTextField(document.getElementById("gauge-text"));

<!-- Doughnut Chart -->
$(document).ready(function(){
	if('{{$todayCheckIN}}' != '0')
	{
		var options = { legend: false, responsive: false };
		new Chart(document.getElementById("canvas1"), {
			type: 'doughnut',
			tooltipFillColor: "rgba(51, 51, 51, 0.55)",
			data: {
				labels: [ "Completed", "Remaining" ],
				datasets: [{
					data: [ '{{$cmpltCheckIN}}', '{{$rmainCheckIN}}' ],
					backgroundColor: [ "#26B99A", "#3498DB" ],
					hoverBackgroundColor: [ "#36CAAB", "#49A9EA" ]
				}]
			},
			options: options
		});
	}else{
		var options = { legend: false, responsive: false };
		new Chart(document.getElementById("canvas1"), {
			type: 'doughnut',
			tooltipFillColor: "rgba(51, 51, 51, 0.55)",
			data: {
				labels: [ "Room Available" ],
				datasets: [{
					data: [ '{{$availRoom}}' ],
					backgroundColor: [ "#F0F3F3" ],
					hoverBackgroundColor: [ "#F0F3F3" ]
				}]
			},
			options: options
		});
	}
});

<!-- Doughnut Chart -->
$(document).ready(function(){
	var options = { legend: false, responsive: false };
	new Chart(document.getElementById("canvas2"), {
		type: 'doughnut',
		tooltipFillColor: "rgba(51, 51, 51, 0.55)",
		data: {
			labels: [ "Departure", "Dirty", "Available" ],
			datasets: [{
				data: [ '{{$todayDeparture}}', '{{$vacantDirty}}', '{{$availRoom}}' ],
				backgroundColor: [ "#26B99A", "#E74C3C", "#3498DB" ],
				hoverBackgroundColor: [ "#36CAAB", "#E95E4F", "#49A9EA" ]
			}]
		},
		options: options
	});
});
</script>
@stop