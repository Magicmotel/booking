@extends('layouts.layout')
<? 	foreach($Results as $val){
		$outPut = $val;
	}
?>
@section('title')
	Cancel Reservation
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Cancel Reservation</h3></div>
            @if($outPut->RsrvSource)
                <div class="col-md-3 col-sm-3 col-xs-6 fltR zeroRightPadd">
                    <h3 class="AlgnCenter">{{$outPut->RsrvSource}}</h3>
                </div>
            @endif
        </div>
        {{--*/ $PayBalance = ($outPut->PayAmount-$outPut->PayDeposit); /*--}}
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('guest-info', $outPut->RsrvId)}}">Guest Info</a></li>
                <li><a href="{{url('payment-info', $outPut->RsrvId)}}">Payment Info</a></li>
                <li><a href="{{url('change-room', $outPut->RsrvId)}}">Room Change</a></li>
                <li><a href="{{url('guest-folio', $outPut->RsrvId)}}">Guest Folio</a></li>
                <li><a href="{{url('change-stay', $outPut->RsrvId)}}">Change Stay</a></li>
                <li><a href="{{url('view-changes', $outPut->RsrvId)}}">View Changes</a></li>
                @if($outPut->RsrvStat == '' && $PayBalance == 0)
                <li class="active"><a href="javascript:void(0);">Cancel Reservation</a></li>
                @endif
            </ul>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                    	<div class="col-md-5 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-4 col-sm-12 col-xs-12 labelText">Guest Name:</div>
                                <div class="col-md-8 col-sm-12 col-xs-12">{{$outPut->GuestLname}}, {{$outPut->GuestFname}}</div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10">
                                <div class="col-md-4 col-sm-12 col-xs-12 labelText">Reservation#:</div>
                                <div class="col-md-8 col-sm-12 col-xs-12">{{$outPut->RsrvId}}</div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10">
                                <div class="col-md-4 col-sm-12 col-xs-12 labelText">Arrival Date:</div>
                                <div class="col-md-8 col-sm-12 col-xs-12">{{$outPut->ArvlDate}}</div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10">
                                <div class="col-md-4 col-sm-12 col-xs-12 labelText">Departure Date:</div>
                                <div class="col-md-8 col-sm-12 col-xs-12">{{$outPut->DprtDate}}</div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10">
                                <div class="col-md-4 col-sm-12 col-xs-12 labelText">No of Nights:</div>
                                <div class="col-md-8 col-sm-12 col-xs-12">{{$outPut->StayNights}}</div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 marginTop10">
                                <div class="col-md-4 col-sm-12 col-xs-12 labelText">Estimated Cost:</div>
                                <div class="col-md-8 col-sm-12 col-xs-12">$ {{$outPut->EstAmount}}</div>
                            </div>
                      	</div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                        	@if ($message = Session::get('danger'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            @if($outPut->RsrvStat != 'Cancelled' && $outPut->RsrvStat != 'Checked Out')
                        	<form id="demo-form2" action="{!!url('cancel-reservation')!!}" method="post" class="form-horizontal form-label-left">
                                {!!csrf_field()!!}
                                <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                                <input type="hidden" name="room_reservation_id" id="room_reservation_id" value="{!!$outPut->RsrvId!!}">
                                <div class="form-group">
                                    <label class="control-label col-md-12 col-sm-3 col-xs-12 AlgnLeft">Reason for cancellation:</label>
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <textarea name="rsrv_comment" id="rsrv_comment" class="form-control col-md-7 col-xs-12" placeholder="Reason for cancellation..."></textarea>
                                    </div>
                               	</div>
                                <div class="form-group form-group-last">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <button type="submit" class="btn btn-danger" onclick="return ConfirmDelete();">Cancel Reservation</button>
                                    </div>
                                </div>
                            </form>
                            @else
                            <div class="alert alert-warning">
                                <p>Reservation# {{$outPut->RsrvId}} {{$outPut->RsrvStat}}</p>
                            </div>
                            @endif
                        </div>  
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function ConfirmDelete()
	{
		var x = confirm("Are you sure you want to cancel this Reservation?");
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop