{!!Html::style('vendors/bootstrap/dist/css/bootstrap.min.css')!!}
{!!Html::style('vendors/font-awesome/css/font-awesome.min.css')!!}
{!!Html::style('vendors/bootstrap-daterangepicker/datepicker.css')!!}
{!!Html::style('build/css/custom.min.css')!!}
{!!Html::script('vendors/daypilot/jquery-1.9.1.min.js')!!}
<body style="background:transparent; font-size:13px">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <meta name="_token" content="{{csrf_token()}}"/>
        <div class="x_content">
        	<div class="alert alert-danger marginZero marginTop20" id="ErrorMessage" style="display:none">
                <p>Error</p>
            </div>
            <form id="demo-form2" action="{!!url('updateReservation')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                {!!csrf_field()!!}
                <input type="hidden" name="room_reservation_id" id="room_reservation_id" value="{{$RsrvRst->room_reservation_id}}" />
                <input type="hidden" name="reserv_night" id="reserv_night" value="{{$RsrvRst->reserv_night}}" >
                <div class="page-title" style="height:auto">
                    <div class="title_left LR_Margin10"><h3>Edit Reservation</h3></div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Arrival Date <span class="required">*</span></label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        @if($RsrvRst->status != '')
                            <input type="hidden" name="from" id="from" value="{{date('m/d/Y', strtotime($RsrvRst->arrival))}}" >
                            <input type="text" name="from_show" id="from_show" class="form-control has-feedback-left" placeholder="Arrival Date" value="{{date('m/d/Y', strtotime($RsrvRst->arrival))}}" disabled >
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        @else
                        	<input type="text" name="from" id="from" class="form-control has-feedback-left" placeholder="Arrival Date" value="{{date('m/d/Y', strtotime($RsrvRst->arrival))}}" required="required">
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Departure Date <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    	@if($RsrvRst->RsrvStat == 'Checked Out' || $RsrvRst->RsrvStat == 'Cancelled')
                            <input type="hidden" name="to" id="to" value="{{date('m/d/Y', strtotime($RsrvRst->departure))}}" >
                            <input type="text" name="to_show" id="to_show" class="form-control has-feedback-left" placeholder="Departure Date" value="{{date('m/d/Y', strtotime($RsrvRst->departure))}}" disabled >
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        @else
                        	<input type="text" name="to" id="to" class="form-control has-feedback-left" placeholder="Departure Date" value="{{date('m/d/Y', strtotime($RsrvRst->departure))}}" required="required">
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        @endif
                        
                        
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Guest Name <span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="guest_name" id="guest_name" class="form-control col-md-7 col-xs-12" placeholder="Guest Name" value="{{$GstRst->guest_fname.' '.$GstRst->guest_lname}}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select id="status" name="status" class="form-control col-md-7 col-xs-12" disabled="disabled">
                        {{--*/	$options = array(""=>"New", "Checked In"=>"Checked In", "Checked Out"=>"Checked Out"); /*--}}
                        @foreach($options as $option) {
                            <option value="{{$option}}"{!!($option == $RsrvRst->status)?'Selected':''!!}>{{$option}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-last marginTop20">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <button type="submit" class="btn btn-success">SAVE</button>
                        <a href="javascript:close();" class="btn btn-primary">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
	{!!Html::script('vendors/jquery/dist/jquery.min.js')!!}
    {!!Html::script('vendors/jquery/dist/jquery-ui.min.js')!!}
    {!!Html::script('vendors/bootstrap/dist/js/bootstrap.min.js')!!}
    <!-- bootstrap-daterangepicker -->
    {!!Html::script('vendors/moment/min/moment.min.js')!!}
    {!!Html::script('vendors/bootstrap-daterangepicker/datepicker.js')!!}
    
    <!-- Custom Theme Scripts -->
    {!!Html::script('build/js/custom.min.js')!!}
	<script type="text/javascript">
    function close(result) {
        if (parent && parent.DayPilot && parent.DayPilot.ModalStatic) {
            parent.DayPilot.ModalStatic.close(result);
        }
    }
	
	$('#demo-form2').on('submit',function(){
		var f = $("#demo-form2");
		var reservation_id = $("#room_reservation_id").val();
		var arrival   	 = $("#from").val();
		var departure 	 = $("#to").val();
		var reserv_night = $("#reserv_night").val();
		
		$.ajax({
			url: f.attr("action"),
			type: 'POST',
			data:{"reservation_id":reservation_id, "arrival":arrival, "departure":departure, "reserv_night":reserv_night, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( data ) {
				if ( data.status === 'success' ) {
					close(eval(data));
					dp.message(data.message);
				}
				if ( data.status === 'error' ) {
					$("#ErrorMessage").html(data.message);
					$("#ErrorMessage").show();
				}
			},
			error: function( data ) {
				if ( data.status === 422 ) {
					close(eval(data));
				}
			}
		});
		return false;
	});
  
    $(document).ready(function () {
        $("#guest_name").focus();
    });
    </script>
    <script>
	$(document).ready(function() {
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		 
		var checkin = $('#from').datepicker({
			startDate: moment(),
			onRender: function(date){
				return date.valueOf() < now.valueOf() ? 'disabled' : '';
			}
		}).on('changeDate', function(ev){
			var startVal = $('#from').val();
			var endVal = $('#to').val();
			if(startVal >= endVal){
				var newDate = new Date(ev.date);
				newDate.setDate(newDate.getDate() + 1);
			}else{
				var newDate = new Date(endVal);
				newDate.setDate(newDate.getDate());
			}
			checkin.hide();
			checkout.setValue(newDate);
			var startVal = $('#from').val();
			var endVal = $('#to').val();
			if(startVal && endVal){
				$('#reserv_night').val(daydiff(parseDate(startVal), parseDate(endVal)));
			}
			$('#to')[0].focus();
		}).data('datepicker');
		
		var checkout = $('#to').datepicker({
			onRender: function(date) {
				return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
			}
		}).on('changeDate', function(ev){
			checkout.hide();
			var startVal = $('#from').val();
			var endVal = $('#to').val();
			if(startVal && endVal){
				$('#reserv_night').val(daydiff(parseDate(startVal), parseDate(endVal)));
			}
		}).data('datepicker');
		
		function parseDate(str) {
			var mdy = str.split('/');
			return new Date(mdy[2], mdy[0]-1, mdy[1]);
		}
		
		function daydiff(first, second) {
			return Math.round((second-first)/(1000*60*60*24));
		}
	});
	</script>
</body>