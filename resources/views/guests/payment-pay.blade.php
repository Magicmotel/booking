@extends('layouts.layout')

@section('title')
	Payment Option
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Please select the type of amount you would like to pay.</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-8 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="errorSummary"></div>
                        <form action="{!!url('payment-pay', $rsrv_Id)!!}" method="POST" id="myform" data-parsley-validate class="form-horizontal form-label-left">
                            {!!csrf_field()!!}
                            {!!method_field('PATCH')!!}
                            <input type="hidden" name="rsrv_type" id="rsrv_type" value="{{$rsrv_Type}}"/>
                            <input type="hidden" name="rsrv_rate" id="rsrv_rate" value="{{$rsrv_Rate}}"/>
                            <input type="hidden" name="rsrv_min_rate" id="rsrv_min_rate" value="{{$rsrv_Min_Rate}}"/>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-3">Reservation#</label>
                                <div class="control-value col-md-8 col-sm-6 col-xs-12 LR_Padd5">
                                    {{$rsrv_Id}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-3">Payment Option</label>
                                <div class="col-md-8 col-sm-6 col-xs-12 LR_Padd5">
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-12 col-sm-3 col-xs-12 zeroPadd">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="radio" name="payment_mode" id="payment_mode1" value="1" class="radioButtonCustom_Option" checked="checked"/>
                                                <div class="labelSwitchCheck">Cash</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-12 col-sm-3 col-xs-12 zeroPadd">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="radio" name="payment_mode" id="payment_mode4" value="4" class="radioButtonCustom_Option"/>
                                                <div class="labelSwitchCheck">Check</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-12 col-sm-3 col-xs-12 zeroPadd">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="radio" name="payment_mode" id="payment_mode3" value="3" class="radioButtonCustom_Option"/>
                                                <div class="labelSwitchCheck">Card Authentication</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-12 col-sm-3 col-xs-12 zeroPadd">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="radio" name="payment_mode" id="payment_mode2" value="2" class="radioButtonCustom_Option"/>
                                                <div class="labelSwitchCheck">Card Payment</div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($Profile_Id)
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-12 col-sm-6 col-xs-12 zeroPadd">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="radio" name="payment_mode" id="payment_mode5" value="5" class="radioButtonCustom_Option"/>
                                                <div class="labelSwitchCheck">Company Account</div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group" id="amountOption">
                                <label class="control-label col-md-4 col-sm-3">Amount To Pay</label>
                                <div class="col-md-8 col-sm-6 col-xs-12 LR_Padd5">
                                    @if($rsrv_Rate > 0)
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                                        <div class="col-md-4 col-sm-4 col-xs-12 zeroPadd TB_Padd5">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="radio" name="rsrv_amount" id="radioButtonCustom_Event" value="1" class="radioButtonCustom_Event" checked="checked"/>
                                                <div class="labelSwitchCheck">Total Balance</div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12 zeroPadd TB_Padd5">
                                            <div class="fltL control-value zeroPadd">
                                                <div class="labelSwitchCheck">$ {{$rsrv_Rate}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd" id="amount_checkOption">
                                        <div class="col-md-4 col-sm-4 col-xs-12 zeroPadd TB_Padd5">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="radio" name="rsrv_amount" id="radioButtonCustom_EventMinimum" value="2" class="radioButtonCustom_Event"{{($rsrv_Rate > 0)?'':' checked'}}/>
                                                <div class="labelSwitchCheck">Minimum</div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-12 zeroPadd TB_Padd5">
                                            <div class="fltL control-value zeroPadd">
                                                <div class="labelSwitchCheck">$ {{$rsrv_Min_Rate}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                                        <div class="col-md-4 col-sm-4 col-xs-12 zeroPadd TB_Padd5">
                                            <div class="fltL control-value zeroPadd">
                                                <input type="radio" name="rsrv_amount" id="radioButtonCustom_EventOther" value="3" class="radioButtonCustom_Event"/>
                                                <div class="labelSwitchCheck">Other</div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-3 col-xs-12 zeroPadd">
                                             <span class="input-group-btn fltL widthAuto"><div class="btn btn-primary cursorInitial">$</div></span>
                                             <input type="text" name="rsrv_custom" id="rsrv_custom" value="" placeholder="Enter Amount" class="form-control width50 LR_Padd5"  disabled="disabled" min="0.1" maxlength="3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group displayNone" id="checkOption">
                                <label class="control-label col-md-4 col-sm-3">Check Detail</label>
                                <div class="col-md-8 col-sm-8 col-xs-12 LR_Padd5">
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-8 col-sm-5 col-xs-12 zeroPadd">
                                            <input type="text" name="bank_name" id="bank_name" placeholder="Name of Bank" value="" class="fltL form-control col-md-12 col-xs-12 checkOptionInput"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-8 col-sm-5 col-xs-12 zeroPadd">
                                            <input type="text" name="holder_name" id="holder_name" placeholder="Account Holder Name" value="" class="fltL form-control col-md-12 col-xs-12 checkOptionInput"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-4 col-sm-3 col-xs-12 zeroPadd">
                                            <input type="text" name="check_number" id="check_number" placeholder="Check Number" value="" class="fltL form-control col-md-12 col-xs-12 checkOptionInput"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-4 col-sm-3 col-xs-12 zeroPadd">
                                            <input type="text" name="check_date" id="check_date" placeholder="Check Date" value="" class="fltL form-control has-feedback-left checkOptionInput" readonly="readonly"/>
                                            <span class="fa fa-calendar-o form-control-feedback left" style="left:0" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group displayNone marginTop20" id="ccOption">
                                <div class="col-md-6 col-sm-4 col-xs-4 col-md-offset-4 LR_Padd5">
                                	<div class="fltL btn btn-warning" id="reviewCreditCard"><i class="fa fa-check-circle-o"></i> Swipe to Review Credit Card</div>
                                    <input type="text" name="cc_reader" id="cc_reader" class="fltL form-control col-md-12 col-xs-12" value="" style="opacity:0;
filter:alpha(opacity=0);width:0;overflow:hidden;" />
                                </div>
                            </div>
                            <div class="form-group displayNone" id="crediTdOption">
                                <label class="control-label col-md-4 col-sm-3">Credit Card Detail</label>
                                <div class="col-md-8 col-sm-8 col-xs-12 LR_Padd5">
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-12 col-sm-5 col-xs-12 zeroPadd">
                                            <input type="text" name="cc_number" id="cc_number" placeholder="Card Number" value="" class="fltL form-control width40 crediTdOptionInput"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-5 col-sm-5 col-xs-12 zeroPadd">
                                            <input type="text" name="cc_holder_fname" id="cc_holder_fname" placeholder="Card Holder First Name" value="" class="fltL form-control crediTdOptionInput"/>
                                        </div>
                                        <div class="col-md-5 col-sm-5 col-xs-12 zeroPadd marginLeft10">
                                            <input type="text" name="cc_holder_lname" id="cc_holder_lname" placeholder="Card Holder Last Name" value="" class="fltL form-control crediTdOptionInput"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-3 col-sm-3 col-xs-12 zeroPadd">
                                            <input type="text" name="cc_exp_mm" id="cc_exp_mm" placeholder="Exp. MM" value="" class="fltL form-control crediTdOptionInput" maxlength="2"/>
                                        </div>
                                    	<div class="col-md-3 col-sm-3 col-xs-12 zeroPadd marginLeft10">
                                            <input type="text" name="cc_exp_yy" id="cc_exp_yy" placeholder="Exp. YY" value="" class="fltL form-control crediTdOptionInput" maxlength="2"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="commentOption">
                                <label class="control-label col-md-4 col-sm-3">Comment Box</label>
                                <div class="col-md-8 col-sm-8 col-xs-12 LR_Padd5">
                                    <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                                        <div class="col-md-8 col-sm-5 col-xs-12 zeroPadd">
                                            <textarea name="folio_comment" id="folio_comment" placeholder="Comment Box" class="fltL form-control col-md-12 col-xs-12" maxlength="70"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group form-group-last marginTop20" id="mainButtons">
                                <div class="col-md-6 col-sm-4 col-xs-4 col-md-offset-4 LR_Padd5">
                                	<button type="submit" class="btn btn-info"><i class="fa fa-check-circle-o"></i> Pay Now</button>
                                    @if($rsrv_Type == 'walkin')
                                    <a href="{{url('in-house')}}" class="btn btn-success marginLeft5"><i class="fa fa-stop-circle-o"></i> Pay Later</a>
                                    @elseif($rsrv_Type == 'create')
                                    <a href="{{url('incoming-reservation')}}?search_date={{$search_Date}}" class="btn btn-success marginLeft5"><i class="fa fa-stop-circle-o"></i> Pay Later</a>
                                    @endif
                                </div>
                            </div>
                        </form>
                   	</div>
               	</div>
          	</div>
           	<div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                            <label class="control-label col-md-6 col-sm-3 zeroMargin">Estimated Cost</label>
                            <div class="col-md-6 col-sm-8 col-xs-12 LR_Padd5">$ {{$esmt_Rate}}</div>
                        </div>
                    </div>
                </div>
        	</div>
            <div class="col-md-4 col-sm-12 col-xs-12 zeroPadd">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                            <label class="control-label col-md-6 col-sm-3">Chargeable Amt.</label>
                            <div class="col-md-6 col-sm-8 col-xs-12 LR_Padd5">$ {{$pay_Rate}}</div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd TB_Padd5">
                            <label class="control-label col-md-6 col-sm-3">Received Amount</label>
                            <div class="col-md-6 col-sm-8 col-xs-12 LR_Padd5">$ {{($dpst_Rate>0)?$dpst_Rate:0}}</div>
                        </div>
                        {{--*/ $PayBalance = ($pay_Rate-$dpst_Rate) /*--}}
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingTop5">
                            <label class="control-label col-md-6 col-sm-3 zeroMargin">Balance</label>
                            <div class="col-md-6 col-sm-8 col-xs-12 LR_Padd5">{!!($PayBalance < 0)?'- $ '.($PayBalance*-1):'$ '.$PayBalance!!}</div>
                        </div>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script>
function disableBackButton()
{
	window.history.forward();
}
setTimeout("disableBackButton()", 0);
</script>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
    {!!Html::script('vendors/cc_validator/jquery.validate.min.js')!!} 
    {!!Html::script('vendors/cc_validator/additional-methods.min.js')!!} 
@stop 

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var checkin = $('#check_date').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		checkin.hide();
	}).data('datepicker');
	
	$(document).on('keypress', '#cc_reader', function(e){
		if (e.which == '10' || e.which == '13') {
			e.preventDefault();
		}
	});
	
	$(document).on('keydown', '#cc_reader', function(e){
		if (e.which == '10' || e.which == '13') {
			e.preventDefault();
		}
	});
	
	$('input[class=radioButtonCustom_Event]').change( function() {
	   	var amountType = $(this).val();
		if(amountType == 3){
			$("#rsrv_custom").prop("disabled", false);
			$("#rsrv_custom").prop("required", true);
			$("#rsrv_custom").val("");
		}else{
			$("#rsrv_custom").prop("disabled", true);
			$("#rsrv_custom").prop("required", false);
			$("#rsrv_custom").val("");
		}
	});
	
	$('input[class=radioButtonCustom_Option]').change( function() {
	   	var paymentType = $(this).val();
		if(paymentType == 5 || paymentType == 3){
			$("#amountOption").hide();
		}else{
			$("#amountOption").show();
		}
		/*
		if(paymentType == 1 || paymentType == 4 || paymentType == 5){
			$("#commentOption").show();
			$("#folio_comment").val("");
		}else{
			$("#commentOption").hide();
			$("#folio_comment").val("");
		}*/
		
		if(paymentType == 2 || paymentType == 3){
			$("#ccOption").show();
			$("#crediTdOption").hide();
			$("#commentOption").hide();
			$("#mainButtons").hide();
			$("#cc_reader").val("");
			$("#cc_reader").focus();
			
			jQuery.validator.setDefaults({
			  success: "valid"
			});
			$( "#myform" ).validate({
			  rules: {
				cc_number: {
				  required: true,
				  creditcard: true
				}
			  }
			});
		}else{
			$("#ccOption").hide();
			$("#crediTdOption").hide();
			$("#commentOption").show();
			$(".crediTdOptionInput").prop("required", false);
			$("#mainButtons").show();
			$("#cc_reader").val("");
		}
		
		if(paymentType == 4){
			$("#checkOption").show();
			$("#amount_checkOption").hide();
			if('{{$rsrv_Rate}}' <= 0){
				$("#radioButtonCustom_EventOther").prop("checked", true);
				$("#rsrv_custom").prop("disabled", false);
				$("#rsrv_custom").prop("required", true);
				$("#rsrv_custom").val("");
			}
			$(".checkOptionInput").prop("required", true);
			$(".checkOptionInput").val("");
		}else{
			$("#checkOption").hide();
			$("#amount_checkOption").show();
			if('{{$rsrv_Rate}}' <= 0){
				$("#radioButtonCustom_EventMinimum").prop("checked", true);
				$("#rsrv_custom").prop("disabled", true);
				$("#rsrv_custom").prop("required", false);
				$("#rsrv_custom").val("");
			}
			$(".checkOptionInput").prop("required", false);
			$(".checkOptionInput").val("");
		}
	});
	$(document).on('click', '#reviewCreditCard', function(e){
		var cardTrack = $('#cc_reader').val();
		if(cardTrack == ""){
			var x = confirm('Are you sure you want to insert Card Detail Manually.');
			if(!x){
				$("#cc_reader").focus();
				return false;
			}
		}
		if(cardTrack){
			var cardDetail = cardTrack.split("^");
			var cardNumber = cardDetail[0].trim().replace("%B", "");
			var cardHolder = cardDetail[1].trim();
			var n = cardHolder.indexOf("/");
			if(n>=0){
				var cardHolder_N = cardHolder.split("/");
				var cardHolder_F = cardHolder_N[1].trim();
				var cardHolder_L = cardHolder_N[0].trim();
			}else{
				var cardHolder_N = cardHolder.split(" ");
				var valuE = cardHolder_N.length;
				var cardHolder_F = cardHolder.replace(cardHolder_N[valuE-1],'').trim();
				var cardHolder_L = cardHolder_N[valuE-1].trim();
			}
			
			var cardExpYY  = cardDetail[2].trim().substr(0, 2);
			var cardExpMM  = cardDetail[2].trim().substr(2, 2);
		}else{
			var cardNumber   = "";
			var cardHolder_F = "";
			var cardHolder_L = "";
			var cardExpYY    = "";
			var cardExpMM    = "";
		}
		var paymentMode= $('input[class=radioButtonCustom_Option]:checked').val();
		$("#crediTdOption").show();
		$("#cc_number").val(cardNumber);
		$("#cc_holder_fname").val(cardHolder_F);
		$("#cc_holder_lname").val(cardHolder_L);
		$("#cc_exp_mm").val(cardExpMM);
		$("#cc_exp_yy").val(cardExpYY);
		$(".crediTdOptionInput").prop("required", true);
		if(paymentMode == 2){
		$("#commentOption").show();
		}
		$("#mainButtons").show();
		$("#ccOption").hide();
	});
});
</script>
@stop