<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomRate extends Model
{
	protected $fillable= [
		'hotel_id', 'room_type_id', 'room_rate_from', 'room_rate_to', 'room_rate',
		'room_rate_mon', 'room_rate_tue', 'room_rate_wed', 'room_rate_thu', 'room_rate_fri', 'room_rate_sat', 'room_rate_sun',
		'trash', 'reg_ip', 'added_by'
	];
	protected $primaryKey = 'room_rate_id';
	protected $table = 'room_rate';
}
