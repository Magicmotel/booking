<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxes extends Model
{
    protected $fillable = [
        'tax_id_unq', 'hotel_id', 'tax_type', 'tax_value', 'tax_value_type',
		'status', 'reg_ip', 'added_by'
    ];
	protected $primaryKey = 'tax_id';
	protected $table = 'taxes';
}
