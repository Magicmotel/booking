@extends('layouts.layout')

@section('title')
	In House : {{date('F j, Y')}}
@stop

@section('CascadingSheet')
    <!-- Datatables -->
    {!!Html::style('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')!!}
    {!!Html::style('vendors/datatables.net-dragtable/css/dragtable.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>In House : {{date('F j, Y')}}</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <?	//table table-striped table-bordered dt-responsive nowrap sar-table   cellspacing="0" width="100%"> ?>
                            <table id="datatable-responsive" class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">
                                    <th id="inHouse_1"><div class="some-handle">Guest Name</div></th>
                                    <th id="inHouse_2"><div class="some-handle">City</div></th>
                                    <th id="inHouse_3"><div class="some-handle">Postal Code</div></th>
                                    <th id="inHouse_4"><div class="some-handle">Room Type</div></th>
                                    <th id="inHouse_5"><div class="some-handle">Arrival</div></th>
                                    <th id="inHouse_6"><div class="some-handle">Departure</div></th>
                                    <th id="inHouse_7"><div class="some-handle">Group Name</div></th>
                                    <th id="inHouse_8"><div class="some-handle">Payment Type</div></th>
                                    <th id="inHouse_9"><div class="some-handle">Balance</div></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Results as $vals)
                                <tr>
                                  <td><a href="#">{{$vals->GuestLname}}, {{$vals->GuestFname}}</a></td>
                                  <td>{{$vals->GuestCity}}</td>
                                  <td>{{$vals->GuestPostal}}</td>
                                  <td>{{$vals->RoomTypes}}</td>
                                  <td>{{$vals->ArvlDate}}</td>
                                  <td>{{$vals->DprtDate}}</td>
                                  <td>{{$vals->GuestGroup}}</td>
                                  <td>{{$vals->PayMode}}</td>
                                  <td>$ {{$vals->PayBalance}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                           	</table>
                            
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function ConfirmDelete()
	{
		var x = confirm("Are you sure you want to delete this User?");
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop     

@section('JavascriptSRC')
    <!-- Datatables -->
    {!!Html::script('vendors/datatables.net-dragtable/js/jquery.dragtable.js')!!}
    {!!Html::script('vendors/datatables.net/js/jquery.dataTables.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')!!}
    {!!Html::script('vendors/jszip/dist/jszip.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/pdfmake.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/vfs_fonts.js')!!} 
@stop  

@section('jQuery')
<script type="text/javascript">
  	$(document).ready(function() {
		$('#datatable-responsive').DataTable({
			"aaSorting": [[4, 'asc']],
			"bPaginate": false,
			"bFilter": true
		});
    	$('#datatable-responsive').dragtable({
			dragHandle:'.some-handle',
			persistState: function(table) {
				if (!window.sessionStorage) return;
				var ss = window.sessionStorage;
				table.el.find('th').each(function(i) {
					if(this.id != '') {table.sortOrder[this.id]=i;}
				});
				ss.setItem('tableorder',JSON.stringify(table.sortOrder));
			},
        	restoreState: eval('(' + window.sessionStorage.getItem('tableorder') + ')')
		});
	});
</script>
@stop
