<?php

/**
 * This file contains functions related to create,update, get and delete 
 * for Notes endpoints.
 * 
 * Notes class
 */

namespace App\Zeamsters\lib;

//include_once BASE_PATH . DS . 'includes/Logging.php';

use App\Zeamsters\lib\ZeamsterPaymentTransaction as zeamsterApi;
use Exception;
use App\Zeamsters\includes\Logging;

class Notes extends Exception {

    public $id = "";
    public $created_ts = "";
    public $resource_id = "";
    public $resource = "";
    public $modified_ts = "";
    public $note = "";
    public $reportable = "";
    public $selectable = "";
    public $visibility_group_id = "";

    /**
     * Create new notes record
     * 
     * @params object $objData 
     * 
     * @return json
     */
    public static function createNotes($objData) {
        try {
            $request_data = zeamsterApi::convertObjectToArray($objData);
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . '/notes';

            if (!empty($request_data)) {
                $method = 'POST';
                $request = array('note' => array_filter($request_data));
                $request = json_encode($request);

                $response = $objzeamsterApi::send_transaction($request_url, $method, null, $request);

                return $response;
            }
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Get / View notes record.
     * 
     * @params string $notes_id  
     * 
     * @return json 
     */
    public static function getNotesDetails($notes_id = null, $filter = null, $perPage = null, $sort = null, $pageNo = null) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            if (!empty($notes_id)) {
                $request_url = $api_url . "/notes/{$notes_id}";
            } else {
                if (!empty($perPage) || !empty($filter) || !empty($sort) || !empty($pageNo)) {
                    $request_url = $api_url . "/notes?{$filter}&page_size={$perPage}&sort={$sort}&page={$pageNo}";
                } else {
                    $request_url = $api_url . "/notes";
                }
            }

            $method = 'GET';
            $response = $objzeamsterApi::send_transaction($request_url, $method, null, null);


            return $response;
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Update notes record
     * 
     * @params object $objData 
     * 
     * @return json  
     */
    public static function updateNotes($objData, $notes_id) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/notes/{$notes_id}";
            $method = 'PUT';
            $request_data = zeamsterApi::convertObjectToArray($objData);
            if (!empty($request_data)) {

                $request = array('note' => array_filter($request_data));
                $request = json_encode($request);

                $response = $objzeamsterApi::send_transaction($request_url, $method, null, $request);


                return $response;
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Delete notes record.
     * 
     * @params string $notes_id  
     * 
     * @return json 
     */
    public static function deleteNotes($notes_id) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/notes/{$notes_id}";
            $method = 'DELETE';
            $response = $objzeamsterApi::send_transaction($request_url, $method);

            return $response;
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

}
