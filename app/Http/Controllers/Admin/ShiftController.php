<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Shift;
use Helper;
use DB;

class ShiftController extends Controller
{
	public function index(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		
		$Results = Shift::where([ ['hotel_id', $Hotel_ID] ])->orderBy('shift_name', 'ASC')->paginate(10);
		return view('employees.shifts')->with("Hotel_ID", $Hotel_ID)->with("Results", $Results)->with('i', ($request->input('page', 1) - 1) * 10);
    }

    public function create()
    {	
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
        return view('employees.shifts-create')->with('Hotel_ID', $Hotel_ID);
    }

    public function store(Request $request)
    {
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		if($Hotel_ID == $request->hotel_id)
		{
			$RequestArray = array();	
			$RequestArray[] = $Hotel_ID;
			$RequestArray[] = $request->shift_name;
			$RequestArray[] = $request->shift_from_time;
			$RequestArray[] = $request->shift_from_meridiem;
			$RequestArray[] = $request->shift_to_time;
			$RequestArray[] = $request->shift_to_meridiem;
			$RequestArray[] = $request->shift_color;
			$RequestArray[] = $_SERVER['REMOTE_ADDR'];
			$RequestArray[] = $admin_ID;
			DB::select('CALL Shifts_Insert_SP(?,?,?,?,?,?,?,?,?)', $RequestArray);
			
			return redirect('shifts')->with('success','Shift created successfully');
		}
		else
		{
			return redirect('shifts')->with("danger", "Please select your Motel");
		}
    }

    public function edit($id)
    {	
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$Results  = Shift::where([ ['shift_id', '=', $id], ['hotel_id', '=', $Hotel_ID] ])->first();
		if(count($Results))
		{
			if($Results->hotel_id == $Hotel_ID)
			{
				return view('employees.shifts-edit')->with("Results", $Results);
			}
		}
		return redirect('shifts')->with("danger", "Please select your Motel");
    }

    public function update(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$Results = Shift::where([ ['shift_id', $id], ['hotel_id', $Hotel_ID] ])->first();
		if(count($Results))
		{
			$Results->shift_name = $request->shift_name;
			$Results->shift_from_time = $request->shift_from_time;
			$Results->shift_from_meridiem = $request->shift_from_meridiem;
			$Results->shift_to_time = $request->shift_to_time;
			$Results->shift_to_meridiem = $request->shift_to_meridiem;
			$Results->shift_color = $request->shift_color;
			$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
			$Results->added_by = $admin_ID;
			$Results->save();
			return redirect('shifts')->with('success','Shift updated successfully');
		}
		else
		{
			return redirect('shifts')->with("danger", "Please select your Motel");
		}
		
    }
	
	public function destroy(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		if($request->hotel_id == $Hotel_ID)
		{
			Shift::destroy($id);
			return redirect('shifts')->with('success','Shift deleted successfully');
		}
		else
		{
			return redirect('shifts')->with('danger','Please select your Motel');
		}
	}
}
