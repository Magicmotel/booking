@extends('layouts.layout')

@section('title')
	Reservation# {{$RsrvRst->room_reservation_id}}, {{$GuestRst->guest_fname." ".$GuestRst->guest_lname}} :: Registration Card
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Registration Card</h3></div>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content" id="printableArea">
                        <table class="table table-striped">
                            <tr class="backgroundNone">
                                <td width="49%" class="borderNone">
                                	<table class="table">
                                        <tr class="backgroundNone marginTop10">
                                            <td width="50%" colspan="2" class="zeroPadd borderNone AlgnLeft labelText font16 LneHeight30" style="font-weight:600">{{$GuestRst->guest_fname." ".$GuestRst->guest_lname}}</td>
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft">Room:</td>
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft">{{$RoomNumber}}</td>
                                      	</tr>
                                        <tr class="backgroundNone marginTop10">
                                            <td width="50%" colspan="2" class="zeroPadd borderNone AlgnLeft labelText LneHeight30">Elite Status:</td>
                                            <td width="50%" colspan="2" class="zeroPadd borderNone AlgnLeft"></td>
                                      	</tr>
                                        <tr class="backgroundNone">
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft LneHeight20">Arrival:</td>
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft">{{date("m/d/Y", strtotime($RsrvRst->arrival))}}</td>
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft">Reservation#</td>
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft labelText">{{$RsrvRst->room_reservation_id}}</td>
                                      	</tr>
                                        <tr class="backgroundNone">
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft LneHeight20">Departure:</td>
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft">{{date("m/d/Y", strtotime($RsrvRst->departure))}}</td>
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft">Adults:</td>
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft">{{$RsrvRst->guest_adult}}</td>
                                            
                                      	</tr>
                                        <tr class="backgroundNone">
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft LneHeight20">Room Type:</td>
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft">{{$RoomTypeRst->room_display}}</td>
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft">Children:</td>
                                            <td width="25%" class="zeroPadd borderNone AlgnLeft">{{$RsrvRst->guest_child}}</td>
                                      	</tr>
                                        
                                  	</table><br />
                                    Your rate(s) are as follow:<br /><br />
                                    <table width="80%" style="line-height:22px;">
                                        <thead>
                                        <tr>
                                            <th width="35%">FROM</th>
                                            <th width="35%">Through</th>
                                            <th width="30%">Rate</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($FolioRst as $FolioVal)
                                        <tr>
                                            <td>{{date("m/d/Y", strtotime($FolioVal->folio_date))}}</td>
                                            <td>{{date("m/d/Y", strtotime($FolioVal->folio_date))}}</td>
                                            <td>$ {{$FolioVal->dayTotal}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft zeroPadd marginTop10 LneHeight20">
                                    Rate is based on length of stay. Additional charges may apply for early departure.
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft zeroPadd marginTop10 LneHeight20">
                                    Rate Acceptance: <u> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </u>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft zeroPadd marginTop10 LneHeight20">
                                    	{{$HotelRst->hotel_disclaimer}}
                                   	</div><br />
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft zeroPadd marginTop10 LneHeight20">
                                    	Initial<u> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </u>
                                   	</div><br />
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft zeroPadd marginTop10 LneHeight20">
                                    	If payment is by credit card, you are authorized to charge my account for the total amount due, Debit card funds will be on hold from you account immediately and may remain unavailable for up to 15 days.
                                  	</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft zeroPadd marginTop10 LneHeight20">
I agree that my liability for this bill is not waived and agree to be held personally liable in the event that the indicated person, company, or association fails to pay the full amount of these charges.
									</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 AlgnLeft zeroPadd marginTop10 LneHeight20">
                                    	Guest Signature: <u> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </u>
                                    </div> 
                              	</td>
                                <td width="49%" class="zeroPadd borderNone vAlgnTop font13">
                                	<div class="col-md-10 col-sm-12 col-xs-12 marginBottom5 AlgnCenter height140 vAlgnMiddle">
                                	{!!Html::image('motel/'.$HotelRst->hotel_logo, 'alt', array( 'title' => 'Motel Logo', 'style' => 'max-width:120px; max-height:120px;' ))!!}
                                    </div>
                                    <table class="table">
                                        <tr class="backgroundNone marginTop10">
                                            <td width="40%" class="zeroPadd borderNone AlgnRight LneHeight30">Your Room Number is:</td>
                                            <td width="50%" class="zeroPadd borderNone AlgnLeft paddingLeft10">{{$RoomNumber}}</td>
                                      	</tr>
                                        <tr class="backgroundNone">
                                            <td width="40%" class="zeroPadd borderNone AlgnRight LneHeight30">Our Telephone Number is:</td>
                                            <td width="50%" class="zeroPadd borderNone AlgnLeft paddingLeft10">{{$HotelRst->hotel_phone_1}}</td>
                                      	</tr>
                                        <tr class="backgroundNone">
                                            <td width="40%" class="zeroPadd borderNone AlgnRight LneHeight30">Our Fax Number is:</td>
                                            <td width="50%" class="zeroPadd borderNone AlgnLeft paddingLeft10">{{$HotelRst->hotel_fax_1}}</td>
                                      	</tr>
                                        <tr class="backgroundNone">
                                            <td width="40%" class="zeroPadd borderNone AlgnRight LneHeight30">Check Out Time:</td>
                                            <td width="50%" class="zeroPadd borderNone AlgnLeft paddingLeft10">11:00 AM</td>
                                      	</tr>
                                        <tr class="backgroundNone">
                                            <td width="40%" class="zeroPadd borderNone AlgnRight LneHeight30">Our Mailing Address is:</td>
                                            <td width="50%" class="zeroPadd borderNone AlgnLeft paddingLeft10">{{nl2br($HotelRst->hotel_address)}}</td>
                                      	</tr>
                                        <tr class="backgroundNone">
                                            <td width="40%" class="zeroPadd borderNone AlgnRight">&nbsp;</td>
                                            <td width="50%" class="zeroPadd borderNone AlgnLeft paddingLeft10">{{$HotelRst->hotel_city}}, {{$HotelRst->hotel_state}} {{$HotelRst->hotel_zip}}</td>
                                      	</tr>
                                        <tr class="backgroundNone">
                                            <td width="40%" class="zeroPadd borderNone AlgnRight LneHeight30">Our Email Address is:</td>
                                            <td width="50%" class="zeroPadd borderNone AlgnLeft paddingLeft10">{{$HotelRst->hotel_email}}</td>
                                      	</tr>
                                  	</table>
                                    <br /><br />
                                    <div class="col-md-10 col-sm-12 col-xs-12 marginBottom5 AlgnCenter">For Reservation at this hotel please call {{$HotelRst->hotel_phone_1}}.</div>
                                    <br /><br />
                                    <br /><br />
                                    <div class="col-md-10 col-sm-12 col-xs-12 marginBottom5 labelText AlgnCenter">Wi-Fi Password : {{$HotelRst->hotel_wifi_pass}}</div>
                                    <table class="table">
                                        <tr class="backgroundNone" style="position:absolute; bottom:0; right:0">
                                            <td width="100%" colspan="2" class="zeroPadd borderNone AlgnRight" style="vertical-align:bottom; opacity:0.4;">
                                                <div style="float:right; width:120px; font-size:10px; position:absolute; right:8px; bottom:25px;">Powered By</div>
                                                {!!Html::image('build/images/logo.jpg', 'MMK Logo', array( 'title' => 'MMK Logo', 'width' => 120, 'height' => 33 ))!!}
                                            </td>
                                        </tr>
                                   	</table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <button class="btn btn-primary marginTop20 margin5" onClick="printDiv('printableArea')"><i class="fa fa-print"></i> Print</button>
                    <a class="btn btn-default marginTop20 margin5" href="{{url('guest-info', $RsrvRst->room_reservation_id)}}"><i class="fa fa-chevron-left"></i> Back</a>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function printDiv(divName) {
		 var printContents = document.getElementById(divName).innerHTML;
		 var originalContents = document.body.innerHTML;
		 document.body.innerHTML = printContents;
		 window.print();
		 document.body.innerHTML = originalContents;
		 window.location.reload();
	}
</script>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/datatables.net/js/jquery.dataTables.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')!!}
    {!!Html::script('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')!!}
    {!!Html::script('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')!!}
    {!!Html::script('vendors/datatables.net-buttons/js/buttons.flash.min.js')!!}
    {!!Html::script('vendors/datatables.net-buttons/js/buttons.html5.min.js')!!}
    {!!Html::script('vendors/jszip/dist/jszip.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/pdfmake.min.js')!!}
   
@stop  

@section('jQuery')
<script>
$(document).ready(function() {
	var handleDataTableButtons = function() {
		if ($("#datatable-buttons").length) {
			$("#datatable-buttons").DataTable({
				"aaSorting": [[2, 'asc']],
				"bLengthChange": false,
				"bFilter": false,
				"bInfo": false,
				"bPaginate": false,
				dom: "Bfrtip",
				buttons: [
					{
					  extend: "copy",
					  className: "btn-sm"
					},
					{
					  extend: "csv",
					  className: "btn-sm"
					},
					{
					  extend: "pdf",
					  className: "btn-sm"
					},
				],
				responsive:true
			});
		}
	};

	TableManageButtons = function() {
		"use strict";
			return {
			init: function() {
			  	handleDataTableButtons();
			}
		};
	}();
	TableManageButtons.init();
});
</script>
@stop