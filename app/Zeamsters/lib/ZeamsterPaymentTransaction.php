<?php

/**
 * This file used to define global method to call API request for Zeamster API.
 */

namespace App\Zeamsters\lib;

//include_once BASE_PATH . DS . 'includes/Logging.php';

use Exception;
use App\Zeamsters\includes\Logging;

class ZeamsterPaymentTransaction extends Exception {

    const HTTP_RESPONSE_OK = 200;
    const PAYMENT_TYPE = 'authorize';
    const AVS = 'avsonly';
    const CHARGE = 'sale';
    const AUTHONLY = 'authonly';
    const AUTHCOMPLETE = 'authcomplete';
    const REFUND = 'refund';
    const VOID = 'void';
    const PARTIALREVERSAL = 'partialreversal';

    /**
     * @var string $environment use for set live or test.
     */
    public static $environment = 'sandbox';

    /**
     * @var string $write_log use for write log or not.
     */
    public static $write_log = true;    // True use for write log, false use for don't write

    /**
     * @var string The base sandbox URL for the Zeamster API.
     */
    public $gateway_url_sandbox = 'https://apiv2.sandbox.zeamster.com/v2';

    /**
     * @var string The base live URL for the Zeamster API.
     */
    public $gateway_url_live = '';     // set here live / production url
    // Hold an instance of the class
    private static $instance;

    /**
     * Call this method to get singleton
     *
     * @return object $instance To return this class object
     */
    public static function Instance() {
        if (!isset(self::$instance)) {
            self::$instance = new ZeamsterPaymentTransaction();
        }
        return self::$instance;
    }

    /**
     * Call this method to get gateway url
     *
     * @return string
     */
    public function get_gateway_url() {

        if (strtolower(self::$environment) == 'live') {
            return $this->gateway_url_live;
        } else {
            return $this->gateway_url_sandbox;
        }
    }

    /**
     * Return header information in array 
     *
     * @return array
     */
    public static function getHeadersParams() {

        return array(
            "cache-control: no-cache",
            "content-type: application/json",
            "developer-id: " . DEVELOPER_ID,
            "user-api-key: " . USER_API_KEY,
            "user-id: " . USER_ID
        );
    }

    /**
     * Call this method to send request and get response using Curl Method
     * 
     * @param string $url
     * @param string $method
     * @param array $header
     * @param json $request
     * @return mixed
     * @throws Exception
     */
    public static function send_transaction($url, $method = null, $header = null, $request = null) {

        try {

            $headers = self::getHeadersParams();

            $ch = curl_init();

            if (self :: $environment == 'sandbox') {
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            }

            if (!empty($method) && $method == 'PUT') {

                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            }
            if (!empty($method) && $method == 'POST') {
                curl_setopt($ch, CURLOPT_POST, true);
            }
            if (!empty($method) && $method == 'GET') {
                curl_setopt($ch, CURLOPT_HTTPGET, true);
            }
            if (!empty($method) && $method == 'DELETE') {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            }


            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            if (!empty($request)) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            }
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($ch);
            $result = self::response_handler($response);
            $err = curl_error($ch);

            if (self :: $write_log) {
                Logging::log(print_r($headers, true), ZEAMSTER_LOG);
                Logging::log($response, ZEAMSTER_LOG);
            }
            curl_close($ch);

            if (!empty($err)) {
                throw new Exception($err);
            }

            if (!empty($response['errors'])) {
                throw new Exception($response);
            }

            return $result;
        } catch (Exception $e) {
            $message = $e->getMessage();
            Logging::log($message, ZEAMSTER_LOG);
        }
    }

    /**
     * Convert object data into array 
     * 
     * @param object $objData
     * 
     * @return array
     */
    public static function convertObjectToArray($objData) {
        if (!empty($objData)) {
            if (is_object($objData)) {
                $requestArray = (array) $objData;
                return array_slice($requestArray, 0, -7);
            } else {
                return $objData;
            }
        }
    }

    /**
     * Response handler
     * 
     * @param json|mixed $response
     * 
     * @return mixed
     */
    public static function response_handler($response) {

        $result = json_decode($response, true);
        if ($result === NULL) {
            return $response;
        } else {
            return $result;
        }
    }

    /**
     * Convert array data into URL string
     * 
     * @param array $data
     * @return string
     */
    public static function convertArrayToUrlStr($data = array()) {

        if (is_array($data)) {
            $arrData = array();
            foreach ($data as $key => $value) {
                $arrData[] = $key . "=" . $value;
            }
            return "&" . implode("&", $arrData);
        }
    }

}
