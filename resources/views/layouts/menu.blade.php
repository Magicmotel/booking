<div class="col-md-3 left_col"><div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      	<a href="{!!url('dashboard')!!}" class="site_title"><i class="fa fa-paw"></i> <span>{!!Helper::getHotelName()!!}</span></a>
    </div>
    <div class="clearfix"></div>
	<!-- <a href="{!!url('dashboard')!!}" class="site_title"><i class="fa fa-paw"></i> <span>{!!Helper::getSiteName()!!}</span></a>-->
    <!-- menu profile quick info -->
    <!-- <div class="profile clearfix"><div class="profile_info">
        <span>Welcome, <div style="font-size:22px; color:#FFFFFF; display:inline;"></div></span>
        <h2>{!!Helper::getAdminUserName()!!}</h2>
    </div></div>
    <!-- /menu profile quick info -->

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <ul class="nav side-menu">
            @if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-money"></i> RESERVATIONS <span class="fa fa-chevron-right"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{!!url('reservation/walkin')!!}">Walk-in</a></li>
                        <li><a href="{!!url('reservation/create')!!}">Create Reservation</a></li>
                        <li><a href="{!!url('incoming-reservation')!!}">Incoming Reservation</a></li>
                        <li><a href="{!!url('reservation')!!}">Search Reservation</a></li>
                    </ul>
                </li>
            @endif
            @if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-bed"></i> ROOM & RATE MGMT <span class="fa fa-chevron-right"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{!!url('room/assign-room')!!}">Manage Rooms</a></li>
                        <li><a href="{!!url('room/room-rate')!!}">Room Rate & Availability</a></li>
                        <li><a href="{!!url('room/taxes')!!}">Manage Taxes & Discounts</a></li>
                    </ul>
                </li>
            @endif
            @if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-user"></i> GUEST MANAGEMENT <span class="fa fa-chevron-right"></span></a>
                    <ul class="nav child_menu">
                    	<li><a href="{!!url('front-desk/')!!}">Front Desk</a></li>
                        <li><a href="{!!url('in-house/')!!}">In House</a></li>
                        <li><a href="{!!url('departure/')!!}">Departures</a></li>
                        <li><a href="{!!url('checked-out-guests/')!!}">Checked Out Guests</a></li>
                    </ul>
                </li>
            @endif
            @if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-suitcase"></i> COMPANY PROFILE <span class="fa fa-chevron-right"></span></a>
                	<ul class="nav child_menu">
                        <li><a href="{!!url('company-profile')!!}">Manage Company Profile</a></li>
                    </ul>
                </li>
            @endif
            @if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-ban"></i> BLOCK ROOM <span class="fa fa-chevron-right"></span></a>
                	<ul class="nav child_menu">
                        <li><a href="{!!url('block-room')!!}">Block Room</a></li>
                    </ul>
                </li>
            @endif
            @if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-user-secret"></i> EMPLOYEE MANAGEMENT <span class="fa fa-chevron-right"></span></a>
                	<ul class="nav child_menu">
                        <?php /*?><li><a href="{!!url('record-shifts')!!}">Manage Shifts</a></li><?php */?>
                        <li><a href="{!!url('employees')!!}">Manage Employees</a></li>
                        <li><a href="{!!url('job-titles')!!}">Manage Job Titles</a></li>
                    </ul>
                </li>
            @endif
            
            <?php /*?>@if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-money"></i> CASH REGISTER <span class="fa fa-chevron-right"></span></a></li>
            @endif<?php */?>
            @if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-paint-brush "></i> HOUSE KEEPING <span class="fa fa-chevron-right"></span></a>
                	<ul class="nav child_menu">
                        <li><a href="{!!url('housekeeping-headquarters')!!}">HouseKeeping Central</a></li>
                    </ul>
                </li>
            @endif
            @if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-exclamation-triangle"></i> MAINTENANCE <span class="fa fa-chevron-right"></span></a>
                	<ul class="nav child_menu">
                        <li><a href="{!!url('maintenance')!!}">Manage Maintenance</a></li>
                    </ul>
                </li>
            @endif
            @if(Helper::getRoleisGranted() == true)
            	{{--*/ $ExpediaMapError = Helper::getExpediaMapErrorRecord(1); /*--}}
                <li><a><i class="fa fa-american-sign-language-interpreting"></i> CHANNEL PARTNERS <span class="fa fa-chevron-right"></span></a>
                	<ul class="nav child_menu">
                        <li><a href="{!!url('expedia-channel')!!}">Expedia Channel Partner</a></li>
                        <li><a href="{!!url('tripadvisor-channel')!!}">TripAdvisor Channel Partner</a></li>
                    </ul>
              	</li>
            @endif
            @if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-credit-card-alt"></i> PAYMENTS <span class="fa fa-chevron-right"></span></a>
                	<ul class="nav child_menu">
                        <li><a href="{!!url('zeamster')!!}">Zeamster</a></li>
                    </ul>
              	</li>
            @endif
            @if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-flag"></i> REPORTING <span class="fa fa-chevron-right"></span></a>
                	<ul class="nav child_menu">
                        <li><a href="{!!url('report-daily-financial')!!}">Financial Reports</a></li>
                        <li><a href="{!!url('report-inhouse-guest')!!}">Guest Reports</a></li>
                        <li><a href="{!!url('report-employee-log-history')!!}">Employee Reports</a></li>
                        <li><a href="{!!url('report-housekeeping')!!}">Housekeeping Reports</a></li>
                    </ul>
                </li>
            @endif
            @if(Helper::getRoleisGranted() == true)
            	{{--*/ $TicketValue = Helper::getTicketRecord(); /*--}}
                <li><a><i class="fa fa-life-ring"></i> SUPPORT{!!($TicketValue>0)?' <div class="errorMenubar" style="display:inline">'.$TicketValue.'</div>':'';!!} <span class="fa fa-chevron-right"></span></a>
                	<ul class="nav child_menu">
                        <li><a href="{!!url('support')!!}">Support{!!($TicketValue>0)?' <div class="errorMenubar" style="display:inline">'.$TicketValue.'</div>':'';!!}</a></li>
                    </ul>
                </li>
            @endif
            @if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-group"></i> ADMIN USERS <span class="fa fa-chevron-right"></span></a>
					<ul class="nav child_menu">
                        <li><a href="{!!url('users/')!!}">Manage Admin Users</a></li>
                    </ul>
                </li>
           	@endif
            @if(Helper::getRoleisGranted() == true)
                <li><a><i class="fa fa-h-square"></i> MOTEL MANAGEMENT <span class="fa fa-chevron-right"></span></a>
                	<ul class="nav child_menu">
                    	<li><a href="{!!url('motels')!!}">Motel Management</a></li>
                        <li><a href="{!!url('payment-motels')!!}">Payment Information</a></li>
                    </ul>
                </li>
            @endif
            </ul>
        </div>
    </div>
    <!-- /sidebar menu -->
</div></div>