<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignRoomTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('room_assign', function($table) {
			$table->increments('room_assign_id');
			$table->integer('hotel_id');
			$table->string('room_number');
			$table->integer('room_type_id');
			$table->timestamps();
			$table->ipAddress('reg_ip');
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('room_assign');
	}
}