<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomReservationFolio extends Model
{
	protected $fillable= [
		'hotel_id', 'room_type_id', 'room_reservation_id',
		'folio_date', 'folio_pay_type', 'folio_amount', 'folio_comment', 'folio_auto', 'folio_order',
		'reg_ip', 'added_by'
	];
	protected $primaryKey = 'folio_id';
	protected $table = 'room_reservation_folio';
}
