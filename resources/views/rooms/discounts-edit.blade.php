@extends('layouts.layout')

@section('title')
	Edit Discounts
@stop

<? 	foreach($Results as $val){
		$outPut = $val;
	}
?>

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Edit Discounts</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('room/taxes')}}">Manage Taxes</a></li>
                <li><a href="{{url('room/discounts')}}">Manage Discounts</a></li>
                <li class="active"><a href="javascript:void(0);">Edit Discount</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                   		{{--*/ $discountFrom = ($outPut->discount_from != '0000-00-00')?date("m/d/Y", strtotime($outPut->discount_from)):date("m/d/Y");
                        	   $discountTo   = ($outPut->discount_to != '0000-00-00')?date("m/d/Y", strtotime($outPut->discount_to)):"";
                                	/*--}}
                        @if($discountTo == '')
                        	{{--*/
                            $discountValid = 1;
                            $discountTo = '';
                            /*--}}
                        @else
                        	{{--*/
                            $discountValid = 2;
                            /*--}}
                        @endif
                        <form id="demo-form2" action="{!!url('room/discounts', $outPut->discount_id)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                            {!!method_field('PATCH')!!}
                            {!!csrf_field()!!}
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Discount Code <span class="required">*</span></label>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <input type="text" name="discount_code" id="discount_code" value="{{$outPut->discount_code}}" class="form-control col-md-3 col-xs-3" placeholder="Discount Code" required="required">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Value of Discount <span class="required">*</span></label>
                                <div class="col-md-1 col-sm-6 col-xs-12">
                                    <input type="text" name="discount_value" id="discount_value" value="{{$outPut->discount_value}}" class="form-control col-md-3 col-xs-3" placeholder="Discount Value" required="required" min=".1">
                                </div>
                                <div class="col-md-1 col-sm-6 col-xs-12 zeroLeftPadd">
                                    <select name="discount_type" id="discount_type" class="form-control col-md-3 col-xs-3 LR_Padd5">
                                        <option value="1"{{($outPut->discount_type==1)?" Selected":""}}>%</option>
                                        <option value="2"{{($outPut->discount_type==2)?" Selected":""}}>Flat</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Start From</label>
                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <input type="text" name="discount_from" id="discount_from" value="{{$discountFrom}}" class="form-control col-md-4 col-xs-12 has-feedback-left" placeholder="Start From" readonly="readonly"  required="required">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Valid upto</label>
                                <div class="col-md-7 col-sm-6 col-xs-12">
                                    <div class="fltL control-value marginRight10">
                                        <input type="radio" name="discount_valid" id="discount_valid" value="1"{!!($discountValid == 1)?" Checked":"";!!} onclick="getValidityofDiscount(this.value);" />
                                        <div class="labelSwitchCheck">Unlimited</div>
                                    </div>
                                    
                                    <div class="fltL control-value">
                                        <input type="radio" name="discount_valid" id="discount_valid" value="2"{!!($discountValid == 2)?" Checked":"";!!} onclick="getValidityofDiscount(this.value);" />
                                        <div class="labelSwitchCheck">Specific Date</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group {!!($discountValid==1)?'displayNone':''!!}" id="discountValid">
                                <div class="col-md-2 col-sm-6 col-xs-12 col-md-offset-4">
                                    <input type="text" name="discount_to" id="discount_to" value="{{$discountTo}}" class="form-control has-feedback-left" placeholder="Valid upto" readonly="readonly">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                </div>
                            </div>
                            <div class="form-group form-group-last marginTop24">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update Discount Code</button>
                                	<button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function getValidityofDiscount(str){
		if(str == 2){
			$('#discountValid').show();
			$("#discount_to").prop("required", true);
			$("#discount_to").val("");
		}else{
			$('#discountValid').hide();
			$("#discount_to").prop("required", false);
			$('#discount_to').val("");
		}
	}
</script>
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var discountFrom = $('#discount_from').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		var newDate = new Date(ev.date);
		newDate.setDate(newDate.getDate());
		discountTo.setValue(newDate);
		discountFrom.hide();
	}).data('datepicker');
	
	var discountTo = $('#discount_to').datepicker({
		onRender: function(date) {
			return date.valueOf() < discountFrom.date.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		discountTo.hide();
	}).data('datepicker');
});
</script>
@stop