<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChannelMotel extends Model
{
    
    protected $fillable = [
        'hotel_id', 'channel_id', 'channel_hotel_id',
		'channel_username', 'channel_password',
		'status', 'reg_ip', 'added_by'
    ];
	protected $primaryKey = 'cm_id';	
	protected $table = 'channel_motels';
	
}
