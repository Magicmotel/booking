@extends('layouts.layout')
<? 	foreach($Results as $val){
		$outPut = $val;
	}
?>
@section('title')
	Guest : {{$outPut->GuestLname}}, {{$outPut->GuestFname}}
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Guest : {{$outPut->GuestLname}}, {{$outPut->GuestFname}}</h3></div>
            @if($outPut->RsrvSource)
                <div class="col-md-3 col-sm-3 col-xs-6 fltR zeroRightPadd">
                    <h3 class="AlgnCenter">{{$outPut->RsrvSource}}</h3>
                </div>
            @endif
        </div>
        {{--*/ $PayBalance = ($outPut->PayAmount-$outPut->PayDeposit); /*--}}
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class="active"><a href="javascript:void(0);">Guest Info</a></li>
                <li><a href="{{url('payment-info', $outPut->RsrvId)}}">Payment Info</a></li>
                <li><a href="{{url('change-room', $outPut->RsrvId)}}">Room Change</a></li>
                <li><a href="{{url('guest-folio', $outPut->RsrvId)}}">Guest Folio</a></li>
                <li><a href="{{url('change-stay', $outPut->RsrvId)}}">Change Stay</a></li>
                <li><a href="{{url('view-changes', $outPut->RsrvId)}}">View Changes</a></li>
                @if($outPut->RsrvStat == '' && $PayBalance == 0)
                <li><a href="{{url('cancel-reservation', $outPut->RsrvId)}}">Cancel Reservation</a></li>
                @endif
            </ul>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                    	@if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @elseif ($message = Session::get('danger'))
                            <div class="alert alert-danger">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 zeroPadd">
                        	<div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	Guest#
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                            	{{$outPut->GuestId}}
                            </div>
                            @if($outPut->RsrvSource)
                                <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                                    {{$outPut->RsrvSource}} Reservation#
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    {{$outPut->SourceRsrvId}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 zeroPadd">
                        	@if($outPut->RsrvSource)
                                <div class="col-md-2 col-sm-12 col-xs-12 col-md-offset-6 labelText">
                                    Confirmation#
                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    {{$outPut->SourceRsrvCNFId}}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 zeroPadd">
                        	<div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	Name:
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                            	{{$outPut->GuestLname}}, {{$outPut->GuestFname}}
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	Reservation#
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                            	{{$outPut->RsrvId}}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 zeroPadd">
                        	<div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	Address:
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                            	{!!nl2br($outPut->GuestAddress)!!}
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	Room#
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12" id="RoomAssignId_{!!$outPut->RsrvId!!}">
                            	@if($outPut->RsrvStat == 'Checked Out' || $outPut->RsrvStat == 'Cancelled')
                                	<div class="fltL" style="position:relative">{{$outPut->RoomNumber}}</div>
                                    <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                @else
                                	@if($outPut->RoomNumber)
                                        @if($outPut->RsrvLock)
                                        	<div class="fltL" style="position:relative">{{$outPut->RoomNumber}}</div>
                                            <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                            <i class="fa fa-lock lockFeaturButton cursorPointer" id="unLockButton" data-id="{!!$outPut->RsrvId!!}" aria-hidden="true"></i>
                                        @else
                                            <div class="fltL" style="position:relative">
                                            <a href="{{url('change-room', $outPut->RsrvId)}}" class="btn-list-link">{{$outPut->RoomNumber}}</a>
                                            @if($outPut->RsrvStat == "")
                                            <i class="closeButton fa fa-times" data-id="{!!$outPut->RsrvId!!}"></i>
                                            @endif
                                            </div>
                                            <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                            <i class="fa fa-unlock lockFeaturButton cursorPointer" id="LockButton" data-id="{!!$outPut->RsrvId!!}" aria-hidden="true"></i> 
                                        @endif
                                    @else
                                        <a href="{{url('change-room', $outPut->RsrvId)}}" class="fltL btn-list-link">Select Room</a>
                                        <div class="fltL marginLeft15">{{" (".$outPut->RoomType.")"}}</div>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 zeroPadd">
                        	<div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	City, State:
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                            	{{$outPut->GuestCity}}{{($outPut->GuestCity && $outPut->GuestState)?", ".$outPut->GuestState:$outPut->GuestState}} {{$outPut->GuestPostal}}
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	Arrival Date:
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                            	{{$outPut->ArvlDate}}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 zeroPadd">
                        	<div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	Phone:
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                            	{{$outPut->GuestContact}}
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	Departure Date:
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                            	{{$outPut->DprtDate}}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 zeroPadd">
                        	<div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	Email:
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 UpperLetter">
                            	{{$outPut->GuestEmail}}
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	Nights:
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                            	{{$outPut->StayNights}}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 zeroPadd">
                        	<div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	DOB:
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                            	{{$outPut->GuestDOB}}
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	Total Estimated Cost:
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                            	$ {{$outPut->EstAmount}} {!!($outPut->StayNights > 1)?'<i class="fa fa-info-circle cursorPointer font16 marginLeft5" id="dayWiseRate" title="Daywise Room Charges" aria-hidden="true"></i>':''!!}
                            </div>
                        </div>
                        
                        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd">
                        	<div class="col-md-2 col-sm-12 col-xs-12 labelText">
                            	<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop20">
                            		Vehicle Detail:
                               	</div>
                                <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop20">
                                    Company Name:
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                            	<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop20 UpperLetter">
                            	{{($outPut->GuestVehicle)?$outPut->GuestVehicle:'&nbsp;'}}
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop20 UpperLetter">
                                {{($outPut->GuestCompany)?$outPut->GuestCompany:'&nbsp;'}}
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12 labelText marginTop24">
                            @if($outPut->RsrvStat == '' && $PayBalance == 0)
                            	<a href="{{url('cancel-reservation', $outPut->RsrvId)}}" class="btn btn-warning buTTonResize marginBottom10"><i class="fa fa-trash" aria-hidden="true"></i> Cancel Reservation</a>
                                @if(date('m/d/Y H:i') > date('m/d/Y', strtotime($outPut->ArvlDate.'+1 days')).' 00:45')
                                <a href="{{url('no-show-reservation', $outPut->RsrvId)}}" class="btn btn-danger buTTonResize marginBottom10" onclick="return ConfirmAction('Are you sure you want to No Show for this Reservation?')"><i class="fa fa-eye-slash" aria-hidden="true"></i> No Show</a>
                                @endif
                            @elseif($outPut->RsrvStat == 'Checked In' && date('m/d/Y H:i') <= date('m/d/Y', strtotime($outPut->ArvlDate.'+1 days')).' 00:45')
                                <a href="{{url('undo-check-in', $outPut->RsrvId)}}" class="btn btn-warning buTTonResize marginBottom10" onclick="return ConfirmAction('Are you sure you want to undo check-in for this Reservation?')"><i class="fa fa-undo" aria-hidden="true"></i> Undo Check-In</a>
                            @endif
                                <a href="{{url('view-changes', $outPut->RsrvId)}}" class="btn btn-info buTTonResize marginBottom10"><i class="fa fa-eye" aria-hidden="true"></i> View Changes</a><br />
                                <a href="{{url('confirmation-letter', $outPut->RsrvId)}}" class="btn btn-success buTTonResize marginBottom10"><i class="fa fa-check-square-o" aria-hidden="true"></i> Confirmation Letter</a>
                                <a href="{{url('registration-card', $outPut->RsrvId)}}" class="btn btn-info buTTonResize marginBottom10"><i class="fa fa-print" aria-hidden="true"></i> Print Registration Card</a>
                                <a href="{{url('folio-print', $outPut->RsrvId)}}" class="btn btn-primary buTTonResize marginBottom10"><i class="fa fa-print" aria-hidden="true"></i> Print Guest Folio</a>
                            </div>
                        </div>
                        @if($outPut->RsrvStat != 'Cancelled' && $outPut->RsrvStat != 'Checked Out')
                        <div class="col-md-12 col-sm-12 col-xs-12 marginTop10 zeroPadd">
                        	<div class="col-md-12 col-sm-12 col-xs-12">
                        	<a href="{{url('guest-info', $outPut->RsrvId)}}/edit" class="btn btn-info buTTonResize"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Guest Info</a>
                            </div>
                      	</div>
                        @endif
                  	</div>
                </div>
         	</div>
            @if($outPut->RsrvSource)
                {{--*/
                $sourceSql = DB::select("SELECT * FROM ".strtolower($outPut->RsrvSource)."_bn_request
                                         WHERE expedia_reservation_id = '".$outPut->SourceRsrvId."' AND mmk_reservation_id = '".$outPut->RsrvId."'
                                         ORDER BY bn_id ASC
                                        ");
                /*--}}
                @if(count($sourceSql))
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="table-responsive">
                                <table class="table table-striped jambo_table bulk_action">
                                    <thead>
                                        <tr class="headings">
                                            <th class="column-title">Date</th>
                                            <th class="column-title">Expedia Reqest ID</th>
                                            <th class="column-title">Request Type</th>
                                            <th class="column-title">Expedia Reservation ID</th>
                                            <th class="column-title">Confirmation ID</th>
                                            <th class="column-title last">Motel Reservation ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sourceSql as $sourceRst)
                                        <tr class="even pointer RQ_CNF_{{$sourceRst->request_type_id}}">
                                            <td class="">{{date("m/d/Y H:i:s", strtotime($sourceRst->created_at))}}</td>
                                            <td class=" ">{{$sourceRst->request_id}}</td>
                                            <td class=" ">{{$sourceRst->request_type}}</td>
                                            <td class=" ">{{$sourceRst->expedia_reservation_id}}</td>
                                            <td class=" ">{{$sourceRst->reservation_confirm_id}}</td>
                                            <td class=" ">{{$sourceRst->mmk_reservation_id}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
           	@endif
        	
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    	<form id="demo-form2" action="{!!url('special-request', $outPut->RsrvId)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                		{!!csrf_field()!!}
                            <span class="section">SPECIAL REQUESTs:</span>
                            <div class="form-group">
                                <textarea name="special_request" id="special_request" rows="5" placeholder="Special Request" class="form-control col-md-12 col-xs-12">{!!$outPut->SpecialRequest!!}</textarea>
                            </div>
                            <div class="form-group form-group-last marginTop24">
                                <div class="col-md-6 col-sm-6 col-xs-12 zeroPadd">
                                    <button type="submit" class="btn btn-success buTTonResize"><i class="fa fa-check-square-o"></i> Update Request</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
      	</div>
    </div>
</div>

<!-- checkedOutModel -->
<div id="checkedOutModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="model_RsrvId"></h4>
            </div>
            <div class="modal-footer AlgnLeft" id="modal-matter"></div>
        </div>
    </div>
</div>
<div id="fc_create" data-toggle="modal" data-target="#checkedOutModel"></div>
<script>
	function ConfirmAction(str)
	{
		var x = confirm(str);
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!}
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	$(document).on('click', '.closeButton', function(e){
		var x = confirm("Are you sure you want to un assigned this Room Number?");
		if (x){
			var rsrvId = $(this).attr('data-id');
			$("#main_container_Loading").show();
			$("#main_container_overlay").show();
			$.ajax({
				url: "{{ url('/unAssignRoomNumber') }}",
				type: 'POST',
				data:{"rsrvId":rsrvId, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( msg.status === 'success' ) {
						$('#RoomAssignId_'+rsrvId).html("<a href=\"{{url('change-room')}}/"+rsrvId+"\" class=\"btn-list-link\">Select Room</a>");
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( data.status === 422 ) {
						toastr.error('Cannot delete the Assigned Room');
					}
				}
			});
			return true;
		}else{
			return false;
		}
	});
	
	$(document).on('click', '.lockFeaturButton', function(e){
		var selfId = $(this).attr('id');
		if(selfId == 'LockButton'){
			var C_selfId = 'unLockButton';
			var rsrvLock = 1;
			var Msg = "Are you sure you want to Lock this Room Number?";
		}else{
			var C_selfId = 'LockButton';
			var rsrvLock = 0;
			var Msg = "Are you sure you want to Unlock this Room Number?";
		}
		var x = confirm(Msg);
		if (x){
			var rsrvId = $(this).attr('data-id');
			$("#main_container_Loading").show();
			$("#main_container_overlay").show();
			$.ajax({
				url: "{{ url('/lockAssignRoomNumber') }}",
				type: 'POST',
				data:{"rsrvId":rsrvId, "rsrvLock":rsrvLock, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( msg.status === 'success' ) {
						$('#RoomAssignId_'+rsrvId).html(msg.content);
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					$("#main_container_Loading").hide();
					$("#main_container_overlay").hide();
					if ( data.status === 422 ) {
						toastr.error('Cannot handle Request.');
					}
				}
			});
			return true;
		}else{
			return false;
		}
	});
	
	$(document).on('click', '#dayWiseRate', function(e){
		$("#main_container_Loading").show();
		$("#main_container_overlay").show();
		var rsrvId = '{{$outPut->RsrvId}}';
		$.ajax({
			url: "{{ url('/dayWiseRatePlan') }}",
			type: 'POST',
			data:{"rsrvId":rsrvId, "_token": "{{ csrf_token() }}" },
			dataType: 'json',
			success: function( msg ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				if ( msg.status === 'success' ) {
					$("#model_RsrvId").html("Reservation ID: "+rsrvId);
					$("#modal-matter").html(msg.response);
					$('#fc_create').click();
				}
				if ( msg.status === 'error' ) {
					toastr.error( msg.response );
				}
			},
			error: function( data ) {
				$("#main_container_Loading").hide();
				$("#main_container_overlay").hide();
				if ( data.status === 422 ) {
					toastr.error('Try Again.');
				}
			}
		});
		return false;
	});
});
</script>
@stop