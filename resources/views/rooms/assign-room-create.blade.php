@extends('layouts.layout')

@section('title')
	Assign Room No
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Assign Room Number</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <br />
                    <form id="demo-form2" action="{!!url('room/assign-room')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
               		{!!csrf_field()!!}
                    <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_type">Room Number <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<input type="text" name="room_number" id="room_number" class="form-control col-md-7 col-xs-12" placeholder="Room Number" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="room_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Room Type <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<select name="room_type_id" id="room_type_id" class="form-control col-md-3 col-xs-6" required="required">
                                	<option value="">Select Room Type</option>
                                    @foreach( App\Roomtype::all(['room_type as RoomType', 'room_type_id as RoomTypeId']) as $val )
                                    <option value="{!!$val->RoomTypeId!!}">{!!$val->RoomType!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    	<div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">ADD</button>
                                <button type="reset" class="btn btn-primary">CANCEL</button>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop