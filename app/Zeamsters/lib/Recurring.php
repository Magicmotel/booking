<?php

/**
 * This file contains functions related to create,update, get and delete 
 * for recurring endpoints.
 * 
 * Recurring class  
 * 
 */

namespace App\Zeamsters\lib;

//include_once BASE_PATH . DS . 'includes/Logging.php';

use App\Zeamsters\lib\ZeamsterPaymentTransaction as zeamsterApi;
use Exception;
use App\Zeamsters\includes\Logging;

class Recurring extends Exception {

    public $id = "";
    public $active = "";
    public $created_ts = "";
    public $description = "";
    public $end_date = "";

    /**
     * @var string $interval(Required) Zeamster API to use for requests.
     */
    public $interval = "";

    /**
     * @var string $interval_type(Required) Zeamster API to use for requests.
     */
    public $interval_type = "";
    public $modified_ts = "";
    public $next_run_date = "";
    public $notification_days = "";
    public $payment_method = "";

    /**
     * @var string $start_date(Required) Zeamster API to use for requests.
     */
    public $start_date = "";
    public $status = "";

    /**
     * @var string $transaction_amount(Required) Zeamster API to use for requests.
     */
    public $transaction_amount = "";
    public $location_id = "";
    public $account_vault_id = "";
    public $product_transaction_id = "";

    /**
     * Create new recurring transaction
     * 
     * @param object $objData
     * 
     * @return json 
     */
    public static function createRecurringRecord($objData) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/recurrings";

            $request_data = zeamsterApi::convertObjectToArray($objData);

            if (!empty($request_data)) {
                $request = array('recurring' => array_filter($request_data));
                $request = json_encode($request);

                $response = $objzeamsterApi::send_transaction($request_url, 'POST', null, $request);

                return $response;
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Update recurring transaction
     * 
     * @param object $objData
     * @param string $recurring_id
     * 
     * @return json 
     */
    public static function updateRecurringRecord($objData, $recurring_id) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/recurrings/{$recurring_id}";
            $request_data = zeamsterApi::convertObjectToArray($objData);
            if (!empty($request_data)) {
                $request = array('recurring' => array_filter($request_data));
                $request = json_encode($request);

                $response = $objzeamsterApi::send_transaction($request_url, 'PUT', null, $request);

                return $response;
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Get / View  record.
     * 
     * @param string $recurring_id
     * @return json 
     */
    public static function getRecurringRecord($recurring_id = null, $filter = null, $perPage = null, $sort = null, $pageNo = null) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            if (!empty($recurring_id)) {
                $request_url = $api_url . "/recurrings/{$recurring_id}";
            } else {
                if (!empty($perPage) || !empty($filter) || !empty($sort) || !empty($pageNo)) {
                    $request_url = $api_url . "/recurrings?{$filter}&page_size={$perPage}&sort={$sort}&page={$pageNo}";
                } else {
                    $request_url = $api_url . "/recurrings";
                }
            }
            $method = 'GET';

            $response = $objzeamsterApi::send_transaction($request_url, $method, null, null);

            return $response;
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

    /**
     * Delete record
     * 
     * @param stgring $recurring_id
     * 
     * @return json
     */
    public static function deleteRecurringRecordList($recurring_id) {
        try {
            $objzeamsterApi = zeamsterApi::Instance();
            $api_url = $objzeamsterApi->get_gateway_url();
            $request_url = $api_url . "/recurrings/{$recurring_id}";
            $method = 'DELETE';
            $response = $objzeamsterApi::send_transaction($request_url, $method, null, null);

            return $response;
        } catch (Exception $e) {

            $message = $e->getMessage();
            if (zeamsterApi::$write_log) {
                Logging::log($message, ZEAMSTER_LOG);
            }
        }
    }

}
