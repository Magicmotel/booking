<!DOCTYPE html>
<html lang="en">
<head>
    <title>MMK Admin Panel</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{url('/favicon.png')}}"/>
    {!!Html::style('vendors/bootstrap/dist/css/bootstrap.min.css')!!}
    {!!Html::style('vendors/font-awesome/css/font-awesome.min.css')!!}
    {!!Html::style('vendors/nprogress/nprogress.css')!!}
    {!!Html::style('vendors/animate.css/animate.min.css')!!}
    {!!Html::style('build/css/custom.min.css')!!}
</head>

<body class="login">
	<div>
    	<a class="hiddenanchor" id="signup"></a>
      	<a class="hiddenanchor" id="signin"></a>
		<div class="login_wrapper">
            @if (count($errors))
                <ul class="errorFormMessage">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <section class="login_content">
                <form name="" action="{!!url('login')!!}" method="post">
                	{!!csrf_field()!!}
                    <h1>Login Form</h1>
                    <div>
                        <input type="text" name="hotel_id" class="form-control" placeholder="Hotel ID eg : MMKiD____" required/>
                    </div>
                    <div>
                        <input type="email" name="email" class="form-control" placeholder="Username" required/>
                    </div>
                    <div>
                    	<input type="password" name="password" class="form-control" placeholder="Password" required/>
                    </div>
                    <div>
                    	<input type="submit" name="button" class="btn btn-default submit" value="Log in to Admin Panel"/>
                    </div>
                    
                    <div class="separator">
                    	<p class="change_link">Forgot your Password, <a href="{!!url('password/reset')!!}" class="to_register">Click Here to reset!</a></p>
                        <div>
                            <h1><i class="fa fa-paw"></i> {!!Helper::getSiteName()!!}!</h1>
                            <p>&copy;2016, All Rights Reserved.</p>
                        </div>
                    </div>
                </form>
            </section>
		</div>
	</div>
</body>
</html>