<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentOption extends Model
{
	protected $table = 'payment_option';
	protected $primaryKey = 'log_id';
	protected $fillable= [
		'transaction_id', 'payment_mode', 'hotel_id', 'room_reservation_id', 'gateway_transaction_id', 'bank_name', 'holder_name', 'check_number', 'check_date', 'reg_ip', 'added_by'
	];
}
