@extends('layouts.layout')

@section('title')
	Manage Job Titles
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Job Titles</h3></div>
        </div>
        
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class="active"><a href="javascript:void(0);">Manage Job Titles</a></li>
                <li><a href="{{url('job-titles/create')}}">Create Job Title</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @elseif ($message = Session::get('danger'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                    	<th>No</th>
                                    	<th>Job Titles</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            
                                <tbody>
                                @if(count($Results))
                                	@foreach($Results as $vals)
                                    <tr class="even pointer">
                                        <td>{{ ++$i }}</td>
                                        <td>{!!$vals->name!!}</td>
                                        <td>
                                            @if($vals->hotel_id)
                                            <a href="{!!url('job-titles', $vals->id)!!}/edit" class="left btn btn-info buTTonResize"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                            <form action="{!!url('job-titles', $vals->id)!!}" method="post" onsubmit="return ConfirmAction('Are you sure you want to delete this Job Title?')" class="left">
                                            {!!method_field('DELETE')!!}
                                            {!!csrf_field()!!}
                                            {!!Form::hidden('hotel_id', $Hotel_ID)!!}
                                            {!!Form::button('<i class="fa fa-trash-o"></i> Delete', ['class'=>'left btn btn-danger buTTonResize marginLeft5', 'type'=>'submit'])!!}
                                            </form>
                                            @endif
                                        </td>
                                    </tr>
                                   	@endforeach
                                @else
                               		<tr class="even pointer">
                                        <td colspan="6" class="errorMessageTR">No Job Titile Found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function ConfirmAction(str)
	{
		var x = confirm(str);
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop      
