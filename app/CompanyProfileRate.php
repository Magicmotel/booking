<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyProfileRate extends Model
{
	protected $table = 'company_profile_rate';
	protected $primaryKey = 'cpr_id';
	protected $fillable= [
		'hotel_id', 'profile_id', 'room_type_id', 'company_rate', 'reg_ip', 'added_by'
	];
}
