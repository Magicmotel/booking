<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomReservationFolioExempt extends Model
{
	protected $fillable= [
		'hotel_id', 'room_reservation_id', 'folio_type', 'folio_date', 'folio_pay_type', 'folio_amount',
		'folio_data', 'folio_order', 'folio_from', 'reg_ip', 'added_by'
	];
	protected $primaryKey = 'folio_id';
	protected $table = 'room_reservation_folio_exempt';
}
