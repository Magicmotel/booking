<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyProfile extends Model
{
	protected $table = 'company_profile';
	protected $primaryKey = 'profile_id';
	protected $fillable= [
		'hotel_id', 'company_name', 'company_address', 'company_zip', 'company_city', 'company_state',
		'company_lang', 'company_email', 'company_phone', 'company_fax', 'contact_name', 'contact_title',
		'company_notes', 'company_tax', 'reg_ip', 'added_by'
	];
}
