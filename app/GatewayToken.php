<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GatewayToken extends Model
{
    
    protected $fillable = [
        'guest_id', 'room_reservation_id', 'gateway_id', 'card_type',
		'first_six', 'last_four', 'ticket', 'ticket_stat', 'reg_ip', 'added_by'
    ];
	protected $primaryKey = 'gt_id';	
	protected $table = 'gateway_token';
	
}
