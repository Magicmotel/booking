<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomtypeFacility extends Model
{
	protected $fillable= [
		'rtf_name', 'rtf_key'
	];
	protected $primaryKey = 'rtf_id';
	protected $table = 'roomtype_facilities';
}
