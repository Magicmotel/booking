@extends('layouts.layout')

@section('title')
	Manage Room Types
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Room Types</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('room/assign-room')}}">Manage Room Numbers</a></li>
                <li class="active"><a href="javascript:void(0);">Manage Room Types</a></li>
                <li><a href="{{url('room/room-type/create')}}">Create Room Type</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @elseif ($message = Session::get('danger'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th>Room Type</th>
                                        <th>Type</th>
                                        <th>No of Rooms</th>
                                        <th>No of Guest</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            
                                <tbody>
                                @if(count($Results))
                                	@foreach($Results as $vals)
                                    <tr class="even pointer">
                                        <td class="UpperLetter">{!!$vals->room_type!!}</td>
                                        <td class="UpperLetter">{!!($vals->room_smoking_type==1)?"N":""!!}{!!$vals->room_bed_size!!}</td>
                                        <td>{!!$vals->room_count!!}</td>
                                        <td>{!!$vals->guest_adult!!} Adult / {!!$vals->guest_child!!} Children</td>
                                        <td>
                                            <a href="{!!url('room/room-type', $vals->room_type_id)!!}/edit" class="left btn btn-info buTTonResize"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                            <form action="{!!url('room/room-type', $vals->room_type_id)!!}" method="post" class="left" onsubmit="return ConfirmAction('Are you sure you want to delete this Room Type?')">
                                            
                                            {!!method_field('DELETE')!!}
                                            {!!csrf_field()!!}
                                            {!!Form::button('<i class="fa fa-trash-o"></i> Delete', ['class'=>'btn btn-danger buTTonResize marginLeft5', 'type'=>'submit'])!!}
                                            </form>
                                            {{--*/ $BlankValue = Helper::getBlankRecord_RoomType($vals->room_type_id); /*--}}
                                            @if($BlankValue>0)
                                            <a href="{!!url('room/assign-room', $vals->room_type_id)!!}/edit" class="left btn btn-success buTTonResize marginLeft5"><i class="fa fa-check-square-o"></i> <i>Assign {!!$BlankValue!!}{!!($BlankValue==1)?" Room Number": " Room Numbers";!!} </a>
                                            @endif
                                            
                                            {{--*/ $MaintentValue = Helper::getMaintenance_RoomType($vals->room_type_id, $vals->room_count); /*--}}
                                            @if($MaintentValue > 0)
                                                <a href="{!!url('room/assign-room?RoomTypeSelectedId=')!!}{{$vals->room_type_id}}&RoomAssignId=2" class="left btn btn-warning buTTonResize marginLeft5"><i class="fa fa-exclamation-triangle"></i> <i>{!!$MaintentValue!!} {!!($MaintentValue==1)?" Room": " Rooms";!!} Under Maintenance</i></a>
                                            @endif
                                            
                                            {{--*/ $MisMatchValue = Helper::getMismatchRecord_RoomType($vals->room_type_id, $vals->room_count); /*--}}
                                            @if($MisMatchValue > 0)
                                                <a href="{!!url('room/assign-room/create')!!}/{{$vals->room_type_id}}?add_room={!!$MisMatchValue!!}" class="left btn btn-success buTTonResize marginLeft5"><i class="fa fa-plus"></i> <i>Add {!!$MisMatchValue!!} {!!(($MisMatchValue)==1)?" Room Number": " Room Numbers";!!}</i></a>
                                            @elseif($MisMatchValue < 0)
                                                <a href="{!!url('room/assign-room?RoomTypeSelectedId=')!!}{{$vals->room_type_id}}" class="left btn btn-warning buTTonResize marginLeft5"><i class="fa fa-trash"></i> <i>{!!$MisMatchValue * -1!!} {!!(($MisMatchValue * -1)==1)?" Extra Room Number": " Extra Room Numbers";!!}</i></a>
                                            @endif
                                        </td>
                                    </tr>
                                   	@endforeach
                              	@else
                                    <tr class="even pointer">
                                        <td colspan="5" class="errorMessageTR">No Room Type Found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $Results->render() !!}
                        </div>
                  </div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function ConfirmAction(str)
	{
		var x = confirm(str);
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop