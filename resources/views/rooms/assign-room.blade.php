@extends('layouts.layout')

@section('title')
	Manage Room Numbers
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Room Numbers</h3></div>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2 fltR zeroRightPadd marginBottom5">
        <select name="room_assign_status" id="room_assign_status" class="form-control col-md-3 col-xs-6 LR_Padd5" onchange="FilterSource('{{$request->RoomTypeSelectedId}}', this.value)">
            <option value="" >Select Status</option>
            <option value="1"{!!($request->RoomAssignId == 1)?" Selected":""!!}>Active</option>
            <option value="2"{!!($request->RoomAssignId == 2)?" Selected":""!!}>Under Maintenance</option>
        </select>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-6 fltR zeroRightPadd">
        {{--*/
        $roomRst = DB::select("SELECT RT.room_type_id as RoomTypeId, RT.room_type as RoomType, CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomCode
                               FROM roomtypes as RT WHERE RT.hotel_id = ".$Hotel_ID." AND RT.trash = 0 ORDER BY RT.room_type ASC");
        /*--}}
        <select name="room_type_id" id="room_type_id" class="form-control col-md-3 col-xs-6 LR_Padd5 uPPerLetter" onchange="FilterSource(this.value, '{{$request->RoomAssignId}}')">
            <option value="">Select Room Type</option>
            @foreach( $roomRst as $val )
            <option value="{!!$val->RoomTypeId!!}" class="uPPerLetter"{!!($request->RoomTypeSelectedId == $val->RoomTypeId)?" Selected":""!!}>{!!$val->RoomType!!}</option>
            @endforeach
        </select>
        </div>
        
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class="active"><a href="javascript:void(0);">Manage Room Numbers</a></li>
                <li><a href="{{url('room/room-type')}}">Manage Room Types</a></li>
                <li><a href="{{url('room/room-type/create')}}">Create Room Type</a></li>
            </ul>
        </div> 
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                        <th>Room No</th>
                                        <th>Room Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            
                                <tbody>
                               	@if(count($Results))
                                	@foreach($Results as $vals)
                                    <tr class="even pointer">
                                        <td>{!!$vals->RoomNumber!!}</td>
                                        <td class=" UpperLetter">{{$vals->RoomType}} &nbsp; ({!!($vals->SmokeType==1)?"N":""!!}{!!$vals->BedSize!!})</td>
                                        <td>
                                            <a href="{!!url('room/assign-room', $vals->RoomTypeId)!!}/edit" class="left btn btn-info buTTonResize"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                            <form action="{!!url('room/assign-room', $vals->RoomId)!!}" method="post" class="left" onsubmit="return ConfirmAction('Are you sure you want to delete this Room No?')">
                                            {!!method_field('DELETE')!!}
                                            {!!csrf_field()!!}
                                            {!!Form::button('<i class="fa fa-trash-o"></i> Delete', ['class'=>'left btn btn-danger buTTonResize marginLeft5', 'type'=>'submit'])!!}
                                            </form>
                                            @if($vals->RoomNumber == '')
                                            <a href="{!!url('room/assign-room', $vals->RoomTypeId)!!}/edit" class="left btn btn-success buTTonResize marginLeft5"><i class="fa fa-plus"></i> Add Room Number</a>	
                                            @endif
                                            @if($vals->RoomStatus == 0)
                                            {{--*/
                                                $Sql = App\Maintenance::where([ ['room_assign_id', $vals->RoomId], ['hotel_id', $Hotel_ID], ['status', 0] ])->first();
                                            /*--}}
                                            <a href="{!!url('maintenance', $Sql->maintenance_id)!!}/reply" class="left btn btn-warning buTTonResize marginLeft5"><i class="fa fa-exclamation-triangle"></i> Under Maintenance</a>
                                            @endif
                                            
                                        </td>
                                    </tr>
                                	@endforeach
                                @else
                                    <tr class="even pointer">
                                        <td colspan="3" class="errorMessageTR">No Room Numbers Found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $Results->render() !!}
                        </div>
                  </div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function FilterSource(str, str1){
		if(str || str1){
			RedirectStr = '?';
			if(str){
				RedirectStr += "RoomTypeSelectedId="+str+"&";
			}
			if(str1){
				RedirectStr += "RoomAssignId="+str1+"&";
			}
			RedirectStr = RedirectStr.replace(/&+$/,'');
			window.location.href="{!!url('room/assign-room')!!}"+RedirectStr;
		}else if(!str && !str1){
			window.location.href="{!!url('room/assign-room')!!}";
		}
	}
	function ConfirmAction(str)
	{
		var x = confirm(str);
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop