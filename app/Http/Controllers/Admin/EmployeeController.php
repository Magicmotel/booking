<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
use Validator;
use Helper;
use DB;

class EmployeeController extends Controller
{
	public function index(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		
		if($request->job_title && $request->shift_type)
		{
			$Results = DB::table('employees as Emp')
						->select('R.name as RoleName', 'SF.shift_name as ShiftType', 'Emp.emp_id as EmpId',
								 'Emp.emp_fname as Fname', 'Emp.emp_mname as Mname', 'Emp.emp_lname as Lname', 'Emp.emp_phone as EmpPhone', 'Emp.emp_email as EmpEmail')
						->join('roles as R', 'R.id', '=', 'Emp.role_id')
						->join('shifts as SF', 'SF.shift_id', '=', 'Emp.shift_id')
						->where([ ['Emp.hotel_id', $Hotel_ID], ['R.id', $request->job_title], ['SF.shift_id', $request->shift_type] ])
						->orderBy('R.id', 'ASC')->orderBy('Emp.emp_fname', 'ASC')
						->paginate(10);
		}
		elseif($request->job_title)
		{
			$Results = DB::table('employees as Emp')
						->select('R.name as RoleName', 'SF.shift_name as ShiftType', 'Emp.emp_id as EmpId',
								 'Emp.emp_fname as Fname', 'Emp.emp_mname as Mname', 'Emp.emp_lname as Lname', 'Emp.emp_phone as EmpPhone', 'Emp.emp_email as EmpEmail')
						->join('roles as R', 'R.id', '=', 'Emp.role_id')
						->join('shifts as SF', 'SF.shift_id', '=', 'Emp.shift_id')
						->where([ ['Emp.hotel_id', $Hotel_ID], ['R.id', $request->job_title] ])
						->orderBy('R.id', 'ASC')->orderBy('Emp.emp_fname', 'ASC')
						->paginate(10);
		}
		elseif($request->shift_type)
		{
			$Results = DB::table('employees as Emp')
						->select('R.name as RoleName', 'SF.shift_name as ShiftType', 'Emp.emp_id as EmpId',
								 'Emp.emp_fname as Fname', 'Emp.emp_mname as Mname', 'Emp.emp_lname as Lname', 'Emp.emp_phone as EmpPhone', 'Emp.emp_email as EmpEmail')
						->join('roles as R', 'R.id', '=', 'Emp.role_id')
						->join('shifts as SF', 'SF.shift_id', '=', 'Emp.shift_id')
						->where([ ['Emp.hotel_id', $Hotel_ID], ['SF.shift_id', $request->shift_type] ])
						->orderBy('R.id', 'ASC')->orderBy('Emp.emp_fname', 'ASC')
						->paginate(10);
		}
		else
		{
			$Results = DB::table('employees as Emp')
						->select('R.name as RoleName', 'SF.shift_name as ShiftType', 'Emp.emp_id as EmpId',
								 'Emp.emp_fname as Fname', 'Emp.emp_mname as Mname', 'Emp.emp_lname as Lname', 'Emp.emp_phone as EmpPhone', 'Emp.emp_email as EmpEmail')
						->join('roles as R', 'R.id', '=', 'Emp.role_id')
						->join('shifts as SF', 'SF.shift_id', '=', 'Emp.shift_id')
						->where([ ['Emp.hotel_id', $Hotel_ID] ])
						->orderBy('R.id', 'ASC')->orderBy('Emp.emp_fname', 'ASC')
						->paginate(10);
		}
		return view('employees.employees')->with("Hotel_ID", $Hotel_ID)->with("request", $request)->with("Results", $Results)->with('i', ($request->input('page', 1) - 1) * 10);
	}

    public function create()
    {	
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
        return view('employees.employees-create')->with('Hotel_ID', $Hotel_ID);
    }

    public function store(Request $request)
    {
		$Helper = new Helper();
		$unique_ID = $Helper->getUniqueId();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		if($Hotel_ID == $request->hotel_id)
		{
			$validator = Validator::make($request->all(), [
				'emp_fname' => 'Required|Min:3|Max:80|Alpha',
				'emp_lname' => 'Required|Min:3|Max:80|Alpha',
				'emp_dob'	=> 'Required|date',
				'emp_email'	=> 'Between:3,64|Email',
				'role_id'  	=> 'Required'
				//'shift_id' 	=> 'Required'
			]);
			
			if ($validator->fails()) {
				return redirect('employees/create')->withErrors($validator)->withInput();
			}
			else
			{	
				$RequestArray = array();	
				$RequestArray[] = $unique_ID;
				$RequestArray[] = $Hotel_ID;		
				$RequestArray[] = $request->role_id;
				//$RequestArray[] = $request->shift_id;		
				$RequestArray[] = $request->emp_fname;
				$RequestArray[] = $request->emp_mname;
				$RequestArray[] = $request->emp_lname;
				$RequestArray[] = date("Y-m-d", strtotime($request->emp_dob));
				$RequestArray[] = $request->emp_email;
				$RequestArray[] = $request->emp_phone;
				$RequestArray[] = $request->emp_address;
				$RequestArray[] = $request->emp_zip;
				$RequestArray[] = $request->emp_city;
				$RequestArray[] = $request->emp_state;
				$RequestArray[] = ($request->emp_admin==1)?$request->emp_admin:0;
				$RequestArray[] = $_SERVER['REMOTE_ADDR'];
				$RequestArray[] = $admin_ID;

				DB::select('CALL Employee_Insert_SP(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', $RequestArray);
				
				return redirect('employees')->with('success','Employee created successfully');;
			}
		}else{
			return redirect('employees')->with("danger", "Please select your Motel");
		}
    }

    public function edit($id)
    {	
		$Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$Results  = Employee::where([ ['emp_id', '=', $id], ['hotel_id', '=', $Hotel_ID] ])->first();
		if(count($Results))
		{
			if($Results->hotel_id == $Hotel_ID)
			{
				return view('employees.employees-edit')->with("Results", $Results)->with('Hotel_ID', $Hotel_ID);
			}
		}
		return redirect('employees')->with("danger", "Please select your Motel");
    }

    public function update(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$validator = Validator::make($request->all(), [
			'emp_fname' => 'Required|Min:3|Max:80|Alpha',
			'emp_lname' => 'Required|Min:3|Max:80|Alpha',
			'emp_dob'	=> 'Required|date',
			'emp_email'	=> 'Between:3,64|Email',
			'role_id'  	=> 'Required'
			//'shift_id' 	=> 'Required'
		]);
		
		if ($validator->fails()) {
            return redirect('employees/'.$id.'/edit')->withErrors($validator)->withInput();
        }
		else
		{
			$Results = Employee::where([ ['emp_id', '=', $id], ['hotel_id', '=', $Hotel_ID] ])->first();
			if(count($Results))
			{
				$Results->emp_fname = $request->emp_fname;
				$Results->emp_mname = $request->emp_mname;
				$Results->emp_lname = $request->emp_lname;
				$Results->emp_dob = date("Y-m-d", strtotime($request->emp_dob));
				$Results->emp_email = $request->emp_email;
				$Results->emp_phone = $request->emp_phone;
				$Results->emp_address = $request->emp_address;
				$Results->emp_zip = $request->emp_zip;
				$Results->emp_city = $request->emp_city;
				$Results->emp_state = $request->emp_state;
				$Results->emp_admin = ($request->emp_admin==1)?$request->emp_admin:0;
				$Results->role_id = $request->role_id;
				//$Results->shift_id = $request->shift_id;
				$Results->reg_ip = $_SERVER['REMOTE_ADDR'];
				$Results->added_by = $admin_ID;
				$Results->save();
				return redirect('employees')->with('success','Employee updated successfully');
			}else{
				return redirect('employees')->with("danger", "Please select your Motel");
			}
		}
    }
	
	public function destroy(Request $request, $id)
    {
		$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		if($request->hotel_id == $Hotel_ID)
		{
			Employee::destroy($id);
			return redirect('employees')->with('success','Employee deleted successfully');
		}
		else
		{
			return redirect('employees')->with('danger','Please select your Motel');
		}
	}
}
