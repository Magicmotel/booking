<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomAssign extends Model
{
	protected $fillable= [
		'hotel_id', 'room_number', 'room_type_id', 'room_stat',
		'status', 'trash', 'reg_ip', 'added_by'
	];
	protected $primaryKey = 'room_assign_id';
	protected $table = 'room_assign';
}
