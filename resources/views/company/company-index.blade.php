@extends('layouts.layout')

@section('title')
	Company Profiles
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Manage Company Profiles</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li class="active"><a href="javascript:void(0);">Manage Company Profiles</a></li>
                <li><a href="{{url('company-profile/create')}}">Create Company Profile</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                  	<div class="x_content">
                        <div class="table-responsive">
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                    <tr class="headings">
                                    	<th class="column-title">No</th>
                                        <th class="column-title">Company Name</th>
                                        <th class="column-title">Zip Code</th>
                                        <th class="column-title">Phone Number</th>
                                        <th class="column-title">Tax Exempt</th>
                                        <th class="column-title no-link last">Action</th>
                                    </tr>
                                </thead>
                            
                                <tbody>
                                @if(count($Results))
                                	@foreach($Results as $vals)
                                    	<tr class="even pointer">
                                        	<td>{{ ++$PSJ }}</td>
                                            <td>{!!$vals->company_name!!}</td>
                                            <td>{!!$vals->company_zip!!}</td>
                                            <td>{!!$vals->company_phone!!}</td>
                                            <td>{!!($vals->company_tax === 1)?'Yes':''!!}</td>
                                            <td class="last">
                                                <a href="{!!url('company-profile', $vals->profile_id)!!}/edit" class="left btn btn-info buTTonResize"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                                <form action="{!!url('company-profile', $vals->profile_id)!!}" method="post" style="float:left;width:auto; margin:0;" onsubmit="return ConfirmAction('are you sure you want to delete this Company?')">
                                                {!!method_field('DELETE')!!}
                                                {!!csrf_field()!!}
                                            	{!!Form::hidden('hotel_id', $Hotel_ID)!!}
                                                {!!Form::button('<i class="fa fa-trash-o"></i> Delete', ['class'=>'left btn btn-danger buTTonResize marginLeft10', 'type' => 'submit'])!!}
                                                </form>
                                          	</td>
                                        </tr>
                                   	@endforeach
                               	@else
                                    <tr class="even pointer">
                                        <td colspan="6" class="errorMessageTR">No Company Profile Found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            {!! $Results->render() !!}
                        </div>
                  	</div>
                </div>
         	</div>
      	</div>
    </div>
</div>
<script>
	function ConfirmAction(str)
	{
		var x = confirm(str);
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop      
