@extends('layouts.layout')

@section('title')
	Create Maintenance Query
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Create Maintenance Query</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('maintenance')}}">Manage Maintenance Queries</a></li>
                <li class="active"><a href="javascript:void(0);">Create Maintenance Query</a></li>
            </ul>
        </div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                    <form id="demo-form2" action="{!!url('maintenance')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
               			{!!csrf_field()!!}
                    	<input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Hotel_ID!!}">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_type">Room Type <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            	{{--*/
                                $roomRst = DB::select("SELECT RT.room_type_id as RoomTypeId, RT.room_type as RoomTypes,
                                					   CONCAT(IF(RT.room_smoking_type != 1, '', 'N'), RT.room_bed_size) as RoomCode
                                                       FROM roomtypes as RT WHERE RT.hotel_id = ".$Hotel_ID." AND RT.trash = 0 ORDER BY RT.room_type ASC");
                                /*--}}
                                <select name="room_type_id" id="room_type_id" class="form-control col-md-3 col-xs-6 LR_Padd5 uPPerLetter" required="required" onchange="FilterSource(this.value)">
                                    <option value="">Select Room Type</option>
                                    @foreach( $roomRst as $val )
                                    <option value="{!!$val->RoomTypeId!!}" class="uPPerLetter"{!!($request->RoomTypeSelectedId == $val->RoomTypeId)?" Selected":""!!}>{!!$val->RoomTypes!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_count">Room Number <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            	<select name="room_assign_id" id="room_assign_id" class="form-control col-md-3 col-xs-6 LR_Padd5" required="required">
                                    <option value="">Select Room Number</option>
                                    @if($request->RoomTypeSelectedId)
                                    @foreach( App\RoomAssign::where([ ['hotel_id', $Hotel_ID], ['trash', 0], ['status', 1], ['room_type_id', $request->RoomTypeSelectedId] ])->orderBy('room_number', 'ASC')->get() as $rooms )
                                    <option value="{!!$rooms->room_assign_id!!}">
                                        {!!$rooms->room_number!!}
                                    </option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="guest_adult" class="control-label col-md-3 col-sm-3 col-xs-12">Maintenance For <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<textarea name="maintenance_for" id="maintenance_for" class="form-control col-md-7 col-xs-12" placeholder="Maintenance For" required="required"></textarea>
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop20">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Submit Query</button>
                                <button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                    	</div>                    
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript">
	function FilterSource(str){
		if(str || str1){
			RedirectStr = '?';
			if(str){
				RedirectStr += "RoomTypeSelectedId="+str+"&";
			}
			RedirectStr = RedirectStr.replace(/&+$/,'');
			window.location.href="{!!url('maintenance/create')!!}"+RedirectStr;
		}else if(!str){
			window.location.href="{!!url('maintenance/create')!!}";
		}
	}
</script>
@stop