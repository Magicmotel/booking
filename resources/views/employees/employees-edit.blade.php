@extends('layouts.layout')

@section('title')
	Edit Employee
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Edit Employee</h3></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 zeroPadd marginTop5">
        	<ul id="myTab" class="nav nav-tabs nav-Top-Tab">
                <li><a href="{{url('employees')}}">Manage Employees</a></li>
                <li class="active"><a href="javascript:void(0);">Edit Employee</a></li>
                <li><a href="{{url('employees/create')}}">Create Employee</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                    <div class="x_content">
                    @if (count($errors))
                        <ul class="errorFormMessage">
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    @endif
                    {{--*/ $empDOB = date("m/d/Y", strtotime($Results->emp_dob)); /*--}}
                    <form id="demo-form2" action="{!!url('employees', $Results->emp_id)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                        {!!method_field('PATCH')!!}
                        {!!csrf_field()!!}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span></label>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            	<input type="text" name="emp_fname" id="emp_fname" value="{!!$Results->emp_fname or old('emp_fname')!!}" class="form-control col-md-7 col-xs-12" placeholder="First Name" required="required">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            	<input type="text" name="emp_mname" id="emp_mname" value="{!!$Results->emp_mname or old('emp_mname')!!}" class="form-control col-md-7 col-xs-12" placeholder="Middle Name">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                            	<input type="text" name="emp_lname" id="emp_lname" value="{!!$Results->emp_lname or old('emp_lname')!!}" class="form-control col-md-7 col-xs-12" placeholder="Last Name" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Date Of Birth <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-6">
                            	<input type="text" name="emp_dob" id="emp_dob" value="{!!$empDOB or old('emp_dob')!!}" class="date-picker form-control col-md-7 col-xs-12" placeholder="mm/dd/yyyy" required="required" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="emp_address" id="emp_address" class="form-control col-md-7 col-xs-12" placeholder="Address">{!!$Results->emp_address or old('emp_address')!!}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Zip Code</label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="emp_zip" id="emp_zip" value="{!!$Results->emp_zip or old('emp_zip')!!}" maxlength="5" class="form-control col-md-7 col-xs-12" placeholder="Zip Code">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="text" name="emp_city" id="emp_city" value="{!!$Results->emp_city or old('emp_city')!!}" class="form-control col-md-7 col-xs-12" placeholder="City">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">State</label>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            <select name="emp_state" id="emp_state" class="form-control col-md-2 col-xs-12 LR_Padd5">
                                <option value="">Select State</option>
                                @foreach(App\ZipCityState::groupBy('zip_state')->get() as $stateVal)
                                <option value="{{$stateVal->zip_state}}"{{($Results->emp_state == $stateVal->zip_state)?" Selected":""}}>{{$stateVal->zip_state}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<input type="email" name="emp_email" id="emp_email" value="{!!$Results->emp_email or old('emp_email')!!}" class="form-control col-md-7 col-xs-12" placeholder="Email Address">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Cell Phone Number</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" name="emp_phone" id="emp_phone" value="{!!$Results->emp_phone or old('emp_phone')!!}" class="form-control col-md-7 col-xs-12" placeholder="Cell Phone Number">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="room_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Role <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	{{--*/
                                $roleQry = DB::select("SELECT name as RoleType, id as RoleId FROM roles
                                                       WHERE (hotel_id = ".$Hotel_ID." OR hotel_id = 0) AND id != 1 ORDER BY hotel_id ASC, id ASC, name ASC");
                                /*--}}
                            	<select name="role_id" id="role_id" class="form-control col-md-3 col-xs-6" required="required">
                                	<option value="">SELECT ROLE</option>
                                    @foreach( $roleQry as $roles )
                                    <option value="{!!$roles->RoleId!!}"{!!($Results->role_id == $roles->RoleId)?" Selected":"";!!}>{!!strtoupper($roles->RoleType)!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <?php /*?><div class="form-group">
                            <label for="room_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Shift <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<select name="shift_id" id="shift_id" class="form-control col-md-3 col-xs-6" required="required">
                                	<option value="">Select Shift</option>
                                    @foreach( App\Shift::where([ ['hotel_id', $Hotel_ID] ])->get(['shift_name as ShiftType', 'shift_id as ShiftId']) as $vals )
                                    <option value="{!!$vals->ShiftId!!}"{!!($Results->shift_id == $vals->ShiftId)?" Selected":"";!!}>{!!$vals->ShiftType!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div><?php */?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Admin User</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="fltL control-value paddingRight5">
                                	<input type="checkbox" name="emp_admin" id="emp_admin" value="1" class="js-switch"{!!($Results->emp_admin == 1)?" Checked":"";!!} />
                               	</div>
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop20">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update Employee</button>
                                <button type="reset" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/toastr/js/toastr.js')!!} 
@stop

@section('jQuery')
<script>
$(document).ready(function() {
	$('input, textarea').keyup(function () {
		$(this).val(this.value.toUpperCase());
	});
	
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var dateOFbirth = $('#emp_dob').datepicker({
		startDate: moment(),
		onRender: function(date){
			return date.valueOf() > now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev){
		dateOFbirth.hide();
	}).data('datepicker');
	
	$("#emp_zip").on("keyup", function() {
		var zipCode = $(this).val().substring(0, 5);
		
		if (zipCode.length == 5 && /^[0-9]+$/.test(zipCode))
		{
			$.ajax({
				url: "{{ url('/getCityData') }}",
				type: 'POST',
				data:{"zipCode":zipCode, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					if ( msg.status === 'success' ) {
						$("#emp_city").val(msg.zip_city.toUpperCase());
						$("#emp_state").val(msg.zip_state.toUpperCase());
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						$("#emp_city").val(msg.zip_city);
						$("#emp_state").val(msg.zip_state);
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					if ( data.status === 422 ) {
						toastr.error('Cannot find the result');
					}
				}
			});
			return false;
		}
	});
});
</script>
@stop