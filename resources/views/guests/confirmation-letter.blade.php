@extends('layouts.layout')

@section('title')
	Reservation# {{$RsrvRst->room_reservation_id}}, {{$GuestRst->guest_fname." ".$GuestRst->guest_lname}} :: Confirmation Letter
@stop

@section('CascadingSheet')
    {!!Html::style('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')!!}
    {!!Html::style('vendors/toastr/css/toastr.css')!!}
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Confirmation Letter</h3></div>
        </div>
        <div class="row">
          	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel borderTopNone">
                	{{--*/ 	$ciExplode = explode(' : ', $HotelRst->hotel_checkin_time);
                    	   	$CheckInTime = $ciExplode[0].':'.$ciExplode[1].' '.$ciExplode[2];
                           
                           	$coExplode = explode(' : ', $HotelRst->hotel_checkout_time);
                    	   	$CheckOutTime = $coExplode[0].':'.$coExplode[1].' '.$coExplode[2];
                           	
                            $HoursCheckIn = $ciExplode[0];
                            if($ciExplode[2] == 'PM'){
                            	$HoursCheckIn = ($ciExplode[0]+12);
                            }
                           	$DateCP = $RsrvRst->arrival.' '.$HoursCheckIn.':'.$ciExplode[1].':00';
                          	$TimeCP = date('m/d/Y h:i A', strtotime($DateCP.'-'.$HotelRst->hotel_cancel_period.' hours'));
                    /*--}}
                                    
                  	<div class="x_content" id="printableArea">
                        <table class="table table-striped">
                            <tr class="backgroundNone">
                                <td width="20%" class="borderNone height140">
                                	{!!Html::image('motel/'.$HotelRst->hotel_logo, 'alt', array( 'title' => 'Motel Logo', 'style' => 'max-width:120px; max-height:120px;' ))!!}
                              	</td>
                                <td width="72%" class="zeroPadd borderNone AlgnCenter vAlgnTop font16 " style="">
                                	<div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 labelText AlgnLeft">{{$HotelRst->hotel_name}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft">{{nl2br($HotelRst->hotel_address)}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft">{{$HotelRst->hotel_city}}, {{$HotelRst->hotel_state}} {{$HotelRst->hotel_zip}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft">{{$HotelRst->hotel_phone_1}}</div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 marginBottom5 AlgnLeft">{{$HotelRst->hotel_email}}</div>
                                </td>
                            </tr>
                            <tr class="backgroundNone"><br />
                            	<td width="20%" class="zeroPadd borderNone">
                                	{{date("l, F jS, Y")}}
                              	</td>
                                <td width="72%" class="zeroPadd borderNone AlgnRight">
                                	Reservation# <b>{{$RsrvRst->room_reservation_id}}</b>
                              	</td>
                            </tr>
                            <tr class="backgroundNone">
                            	<td colspan="2" width="100%" class="zeroPadd borderNone"><br /><br />
                            		Dear {{$GuestRst->guest_fname." ".$GuestRst->guest_lname}},<br /><br />
                                    We are delighted that you will be staying with us from {{date("l, F jS, Y", strtotime($RsrvRst->arrival))}} to {{date("l, F jS, Y", strtotime($RsrvRst->departure))}}. We have reserved a room with {{$RoomTypeRst->room_type}} ({{$RoomTypeRst->room_display}}) for you. Check in begins at {{$CheckInTime}} and check out is by {{$CheckOutTime}} on {{date("l, F jS, Y", strtotime($RsrvRst->departure))}}. Your rate(s) are as follow<br /><br />
                                    	<table width="50%" style="margin-left:25%; line-height:22px;">
                                        <thead>
                                        <tr>
                                            <th>FROM</th>
                                            <th>Through</th>
                                            <th>Rate</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($FolioRst as $FolioVal)
                                        <tr>
                                            <td>{{date("m/d/Y", strtotime($FolioVal->folio_date))}}</td>
                                            <td>{{date("m/d/Y", strtotime($FolioVal->folio_date))}}</td>
                                            <td>$ {{$FolioVal->dayTotal}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <br />
                                    Rates quoted are per day and may fluctuate dependent upon local tax laws. Where required by law, rates and total package cost may include taxes.<br /><br />
                                    If you are exempt from any of these taxes, please present all required documentation at time of check in.<br /><br />
                                    Your reservation has been guaranteed by CREDIT CARD, If you need to cancel this reservation, you must do so by {{$TimeCP}} local hotel time, {{$HotelRst->hotel_cancel_period}} hour(s) before the date of arrival to avoid a cancellation penalty.<br /><br />
                                    If you have any questions, please do not hesitate to call our hotel staff at {{$HotelRst->hotel_phone_1}}<br /><br />
                                    
								</td>
                         	</tr>
                            <tr class="backgroundNone">
                            	<td width="50%" class="zeroPadd borderNone">
                                	Sincerely.<br /><br />
									{{$HotelRst->hotel_m_fname.", ".$HotelRst->hotel_m_lname}}<br />
                                    General Manager
                              	</td>
                                <td width="72%" class="zeroPadd borderNone AlgnRight" style="position:relative; vertical-align:bottom; opacity:0.4;">
                                	<div style="float:right; width:120px; font-size:10px; position:absolute; right:8px; bottom:25px;">Powered By</div>
                                    {!!Html::image('build/images/logo.jpg', 'MMK Logo', array( 'title' => 'MMK Logo', 'width' => 120, 'height' => 33 ))!!}
                              	</td>
                            </tr>
                        </table>
                    </div>
                    <button class="btn btn-primary marginTop20 margin5" onClick="printDiv('printableArea')"><i class="fa fa-print"></i> Print</button>
                    <button class="btn btn-success marginTop20 margin5" id="emailButton"><i class="fa fa-envelope-o"></i> Email</button>
                    <a class="btn btn-default marginTop20 margin5" href="{{url('guest-info', $RsrvRst->room_reservation_id)}}"><i class="fa fa-chevron-left"></i> Back</a>
                    
                </div>
         	</div>
      	</div>
    </div>
</div>
<!-- confirmMailModel -->
<div id="confirmMailModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title" id="model_RsrvId">E-mail Confirmation</h4>
            </div>
            <div class="modal-email-matter">
            	<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd AlgnCenter">
                    The E-mail confirmation notice will be sent via regular internet mail and will NOT contain the guest's address or credit card information.
                </div>
            	<div class="col-md-12 col-sm-12 col-xs-12 zeroPadd paddingTop15">
                    <label class="control-label col-md-4 col-sm-4 col-xs-12 AlgnRight" style="margin-top:7px;">E-mail Address</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <form id="demo-form2" action="{!!url('room/room-type')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                        <input type="text" name="guest_email" id="guest_email" value="{{$GuestRst->guest_email}}" class="form-control col-md-7 col-xs-12" placeholder="Email Address" required="required">                        
                    </div>
                </div>
            </div>
            <div class="modal-footer AlgnCenter">
                <button type="button" class="btn btn-primary antosubmit" data-dismiss="modal">SEND</button>
                <button type="button" class="btn btn-default antoclose" data-dismiss="modal">CANCEL</button>
            </div></form>
        </div>
    </div>
</div>
<div id="fc_create" data-toggle="modal" data-target="#confirmMailModel"></div>
<script type="text/javascript" language="javascript">
	function printDiv(divName) {
		 var printContents = document.getElementById(divName).innerHTML;
		 var originalContents = document.body.innerHTML;
	
		 document.body.innerHTML = printContents;
	
		 window.print();
	
		 document.body.innerHTML = originalContents;
		 window.location.reload();
	}
</script>
@stop

@section('JavascriptSRC')
    {!!Html::script('vendors/datatables.net/js/jquery.dataTables.min.js')!!}
    {!!Html::script('vendors/datatables.net-responsive/js/dataTables.responsive.min.js')!!}
    {!!Html::script('vendors/datatables.net-buttons/js/dataTables.buttons.min.js')!!}
    {!!Html::script('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')!!}
    {!!Html::script('vendors/datatables.net-buttons/js/buttons.flash.min.js')!!}
    {!!Html::script('vendors/datatables.net-buttons/js/buttons.html5.min.js')!!}
    {!!Html::script('vendors/jszip/dist/jszip.min.js')!!}
    {!!Html::script('vendors/pdfmake/build/pdfmake.min.js')!!}
   	{!!Html::script('vendors/toastr/js/toastr.js')!!} 
@stop  

@section('jQuery')
<script>
$(document).ready(function() {
	$('#emailButton').on('click', function(){
		$('#fc_create').click();
		$(".antosubmit").on("click", function() {
			var rsrv_id = '{{$RsrvRst->room_reservation_id}}';
			var emailContent = $('#printableArea').html();
			var emailAddress = $('#guest_email').val();
			var emailFrom = '{{$HotelRst->hotel_email}}';
			var emailFromName = '{{$HotelRst->hotel_name}}';
			$.ajax({
				url: "{{ url('/confirmation-mail') }}",
				type: 'POST',
				data:{"rsrv_id":rsrv_id, "emailContent":emailContent, "emailAddress":emailAddress, "emailFrom":emailFrom, "emailFromName":emailFromName, "_token": "{{ csrf_token() }}" },
				dataType: 'json',
				success: function( msg ) {
					if ( msg.status === 'success' ) {
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.success( msg.response );
					}
					if ( msg.status === 'error' ) {
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.error( msg.response );
					}
				},
				error: function( data ) {
					if ( data.status === 422 ) {
						$("#main_container_Loading").hide();
						$("#main_container_overlay").hide();
						toastr.error('Cannot find the result');
					}
				}
			});
			return true;
		});
	});
});
</script>
@stop