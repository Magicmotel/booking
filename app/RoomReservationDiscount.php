<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomReservationDiscount extends Model
{
	protected $fillable= [
		'hotel_id', 'room_reservation_id', 'discount_id',
		'reg_ip', 'added_by'
	];
	protected $primaryKey = 'rsrv_discount_id';
	protected $table = 'room_reservation_discounts';
}
