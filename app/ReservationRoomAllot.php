<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationRoomAllot extends Model
{
	protected $fillable= [
		'hotel_id', 'room_reservation_id', 'room_assign_id', 'reg_ip', 'added_by'
	];
	protected $primaryKey = 'allot_id';
	protected $table = 'reservation_room_allot';
}