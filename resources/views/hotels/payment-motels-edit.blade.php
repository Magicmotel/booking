@extends('layouts.layout')

@section('title')
	Edit Motel Payment Information
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Edit Motel Payment Information</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<form id="demo-form2" action="{!!url('payment-motels', $Results->payment_id)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                {!!method_field('PATCH')!!}
                {!!csrf_field()!!}
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="form-horizontal form-label-left">
                                <span class="section">CREDIT CARD:</span>
                                <div class="form-group">
                                    <label class="control-label col-md-5 col-sm-5">Cardholder - First Name:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="cc_fname" id="cc_fname" value="{!!$Results->cc_fname or old('cc_fname')!!}" placeholder="Cardholder - First Name" class="form-control col-md-7 col-xs-12" required="required">
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-5 col-sm-5">Cardholder - Middle Name:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="cc_mname" id="cc_mname" value="{!!$Results->cc_mname or old('cc_mname')!!}" placeholder="Cardholder - Middle Name" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-col-md-5 col-sm-5">Cardholder - Last Name:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="cc_lname" id="cc_lname" value="{!!$Results->cc_lname or old('cc_lname')!!}" placeholder="Cardholder - Last Name" class="form-control col-md-7 col-xs-12" required="required">
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-col-md-5 col-sm-5">Card Number:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="cc_card_no" id="cc_card_no" value="{!!$Results->cc_card_no or old('cc_card_no')!!}" placeholder="Card Number" class="form-control col-md-7 col-xs-12" required="required">
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-col-md-5 col-sm-5">Expiration Date (MM/YY):</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="cc_expiry" id="cc_expiry" value="{!!$Results->cc_expiry or old('cc_expiry')!!}" placeholder="Expiration Date (MM/YY)" class="form-control col-md-7 col-xs-12" required="required">
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-col-md-5 col-sm-5">CVV Code (3 digit code):</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="cc_cvv" id="cc_cvv" value="{!!$Results->cc_cvv or old('cc_cvv')!!}" placeholder="CVV Code (3 digit code)" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-col-md-5 col-sm-5">Billing Address:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<textarea name="cc_address" id="cc_address" placeholder="Billing Address" class="form-control col-md-7 col-xs-12">{!!nl2br($Results->cc_address)!!}</textarea>
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-col-md-5 col-sm-5">Billing City, State:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="cc_city" id="cc_city" value="{!!$Results->cc_city or old('cc_city')!!}" placeholder="Billing City, State" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                                <div class="form-group form-group-last">
                                    <label class="control-label col-col-md-5 col-sm-5">Billing Zip Code:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="cc_zip" id="cc_zip" value="{!!$Results->cc_zip or old('cc_zip')!!}" placeholder="Billing Zip Code" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="form-horizontal form-label-left">
                                <span class="section">BANK ACCOUNT:</span>
                                <div class="form-group">
                                    <label class="control-label col-md-5 col-sm-5">BA Holder - First Name:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="ba_fname" id="ba_fname" value="{!!$Results->ba_fname or old('ba_fname')!!}" placeholder="BA Holder - First Name" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-5 col-sm-5">BA Holder - Middle Name:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="ba_mname" id="ba_mname" value="{!!$Results->ba_mname or old('ba_mname')!!}" placeholder="BA Holder - Middle Name" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-col-md-5 col-sm-5">BA Holder - Last Name:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="ba_lname" id="ba_lname" value="{!!$Results->ba_lname or old('ba_lname')!!}" placeholder="BA Holder - Last Name" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-col-md-5 col-sm-5">Bank Name:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="ba_bankname" id="ba_bankname" value="{!!$Results->ba_bankname or old('ba_bankname')!!}" placeholder="Bank Name" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-col-md-5 col-sm-5">Bank Account Number:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="ba_account" id="ba_account" value="{!!$Results->ba_account or old('ba_account')!!}" placeholder="Bank Account Number" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-col-md-5 col-sm-5">Routing Number:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="ba_routing" id="ba_routing" value="{!!$Results->ba_routing or old('ba_routing')!!}" placeholder="Routing Number" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-col-md-5 col-sm-5">Billing Address:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<textarea name="ba_address" id="ba_address" placeholder="Billing Address" class="form-control col-md-7 col-xs-12">{!!$Results->ba_address or old('ba_address')!!}</textarea>
                                   	</div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-col-md-5 col-sm-5">Billing City, State:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="ba_city" id="ba_city" value="{!!$Results->ba_city or old('ba_city')!!}" placeholder="Billing City, State" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                                <div class="form-group form-group-last">
                                    <label class="control-label col-col-md-5 col-sm-5">Billing Zip Code:</label>
                                    <div class="col-md-7 col-sm-7">
                                    	<input type="text" name="ba_zip" id="ba_zip" value="{!!$Results->ba_zip or old('ba_zip')!!}" placeholder="Billing City, State" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-12 col-sm-12 col-xs-12">     
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="form-horizontal form-label-left">
                                <span class="section">PAYPAL:</span>
                                <div class="form-group form-group-last">
                                    <label class="control-label col-md-3 col-sm-3 AlgnLeft">Link to PayPal Account:</label>
                                    <div class="col-md-8 col-sm-8">
                                    	<input type="text" name="paypal_link" id="paypal_link" value="{!!$Results->paypal_link or old('paypal_link')!!}" placeholder="LINK TO MMK BANK ACCOUNT" class="form-control col-md-7 col-xs-12">
                                   	</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        		<div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                        <div class="form-horizontal form-label-left">
                            <div class="form-group form-group-last">
                                <div class="col-md-3 col-sm-8 col-xs-12 col-md-offset-4 AlgnCenter">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update</button>
                                	<button type="reset" class="btn btn-default"><i class="fa fa-undo"></i> Cancel</button>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop