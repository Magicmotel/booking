<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\RoomReservation;
use App\RoomReservationFolio;
use App\RoomReservationFolioMeta;
use App\Roomtype;
use App\GatewayPayment;
use App\GatewayCredential;
use App\GatewayToken;
use App\PaymentOption;
use Helper;
use DB;

class ZeamsterController extends Controller
{
	public function payment_amount(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		
		$rsrv_Id   = $request->rsrv_id;
		$rsrv_Type = $request->type;
		
		$Results = DB::select("SELECT RSV.guest_profile_id, RSV.arrival,
							   ( SELECT SUM(RF.folio_amount) FROM room_reservation_folio as RF
								 WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0
							   ) as EstAmount,
							   ( ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
								 ) - 
								 ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
								 )
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.room_reservation_id = ".$rsrv_Id." AND RSV.hotel_id = ".$Hotel_ID."
							  ");
		if(count($Results))
		{
			foreach($Results as $Value){
				$search_Date= date('m/d/Y', strtotime($Value->arrival));
				$esmt_Rate  = $Value->EstAmount;
				$pay_Rate   = $Value->PayAmount;
				$dpst_Rate  = $Value->PayDeposit;
				$Profile_Id = $Value->guest_profile_id;
			}
			$rsrv_Min_Rate = "1";
			$rsrv_Rate = $pay_Rate-$dpst_Rate;
			if($pay_Rate<=0){
				$rsrv_Rate = $esmt_Rate-$dpst_Rate;
			}
		
			return view('zeamster.payment')->with("Hotel_ID", $Hotel_ID)->with("rsrv_Id", $rsrv_Id)->with("rsrv_Rate", $rsrv_Rate)->with("rsrv_Min_Rate", $rsrv_Min_Rate)->with("Profile_Id", $Profile_Id)->with("rsrv_Type", $rsrv_Type)->with("esmt_Rate", $esmt_Rate)->with("pay_Rate", $pay_Rate)->with("dpst_Rate", $dpst_Rate)->with("search_Date", $search_Date);
		}else{
			return redirect('in-house')->with("warning", "Reservation not Found");
		}
	}
	
	public function payment_card(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		$admin_ID = $Helper->getAdminUserId();
		
		$Gateway_ID = 1;
		
		$rsrv_Id     = $request->rsrv_id;
		$rsrv_Type   = $request->rsrv_Type;
		$folio_Comment = $request->folio_comment;
		$paymentMode = $request->payment_mode;
		if($paymentMode == 1){
			$pay_Mode = 'cash';
		}elseif($paymentMode == 2){
			$pay_Mode = 'sale';
		}elseif($paymentMode == 3){
			$pay_Mode = 'auth';
		}elseif($paymentMode == 4){
			$pay_Mode = 'check';
		}elseif($paymentMode == 5){
			$pay_Mode = 'company';
		}
		
		$amount_Type = $request->rsrv_amount;
		if($amount_Type == 1){
			$pay_Amount = $request->rsrv_rate;
		}elseif($amount_Type == 2){
			$pay_Amount = $request->rsrv_min_rate;
		}elseif($amount_Type == 3){
			$pay_Amount = $request->rsrv_custom;
		}
		
		$bank_name    = $request->bank_name;
		$holder_name  = $request->holder_name;
		$check_number = $request->check_number;
		$check_date   = $request->check_date;
		
		$Result = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $rsrv_Id] ])->first();
		$RoomTypeId = $Result->room_type_id;
		$GuestId    = $Result->guest_id;
		if($paymentMode == 1)
		{
			$unique_ID = "CSH".$Helper->getUniqueId().$rsrv_Id;
			
			$PaymentOptionOBJ = new PaymentOption;
			$PaymentOptionOBJ->transaction_id = $unique_ID;
			$PaymentOptionOBJ->payment_mode   = $paymentMode;
			$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
			$PaymentOptionOBJ->room_reservation_id = $rsrv_Id;
			$PaymentOptionOBJ->reg_ip   = $_SERVER['REMOTE_ADDR'];
			$PaymentOptionOBJ->added_by = $admin_ID;
			$PaymentOptionOBJ->save();
			
			$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
			$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
			$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
			$RoomReservationFolioMetaOBJ->folio_type = 0;
			$RoomReservationFolioMetaOBJ->folio_date = date("Y-m-d");
			$RoomReservationFolioMetaOBJ->folio_pay_type = 'Cash Payment';
			$RoomReservationFolioMetaOBJ->folio_amount = $pay_Amount;
			$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
			$RoomReservationFolioMetaOBJ->folio_auto  = 0;
			$RoomReservationFolioMetaOBJ->folio_order = 0;
			$RoomReservationFolioMetaOBJ->log_id = $PaymentOptionOBJ->log_id;
			$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
			$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
			$RoomReservationFolioMetaOBJ->save();
			
			return redirect('guest-folio/'.$rsrv_Id);
		}
		elseif($paymentMode == 4)
		{
			$unique_ID = "CHK".$Helper->getUniqueId().$rsrv_Id;
			
			$PaymentOptionOBJ = new PaymentOption;
			$PaymentOptionOBJ->transaction_id = $unique_ID;
			$PaymentOptionOBJ->payment_mode   = $paymentMode;
			$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
			$PaymentOptionOBJ->room_reservation_id = $rsrv_Id;
			$PaymentOptionOBJ->bank_name   	= $bank_name;
			$PaymentOptionOBJ->holder_name  = $holder_name;
			$PaymentOptionOBJ->check_number = $check_number;
			$PaymentOptionOBJ->check_date 	= $check_date;
			$PaymentOptionOBJ->reg_ip   = $_SERVER['REMOTE_ADDR'];
			$PaymentOptionOBJ->added_by = $admin_ID;
			$PaymentOptionOBJ->save();
			
			$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
			$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
			$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
			$RoomReservationFolioMetaOBJ->folio_type = 0;
			$RoomReservationFolioMetaOBJ->folio_date = date("Y-m-d");
			$RoomReservationFolioMetaOBJ->folio_pay_type = 'Check Payment';
			$RoomReservationFolioMetaOBJ->folio_amount = $pay_Amount;
			$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
			$RoomReservationFolioMetaOBJ->folio_auto  = 0;
			$RoomReservationFolioMetaOBJ->folio_order = 0;
			$RoomReservationFolioMetaOBJ->log_id = $PaymentOptionOBJ->log_id;
			$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
			$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
			$RoomReservationFolioMetaOBJ->save();
			
			return redirect('guest-folio/'.$rsrv_Id);
		}
		elseif($paymentMode == 5)
		{
			$unique_ID = "CMP".$Helper->getUniqueId().$rsrv_Id;
			
			$PaymentOptionOBJ = new PaymentOption;
			$PaymentOptionOBJ->transaction_id = $unique_ID;
			$PaymentOptionOBJ->payment_mode   = $paymentMode;
			$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
			$PaymentOptionOBJ->room_reservation_id    = $rsrv_Id;
			$PaymentOptionOBJ->reg_ip   = $_SERVER['REMOTE_ADDR'];
			$PaymentOptionOBJ->added_by = $admin_ID;
			$PaymentOptionOBJ->save();
			
			$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
			$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
			$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
			$RoomReservationFolioMetaOBJ->folio_type = 0;
			$RoomReservationFolioMetaOBJ->folio_date = date("Y-m-d");
			$RoomReservationFolioMetaOBJ->folio_pay_type = 'Company Payment';
			$RoomReservationFolioMetaOBJ->folio_amount = $pay_Amount;
			$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
			$RoomReservationFolioMetaOBJ->folio_auto  = 0;
			$RoomReservationFolioMetaOBJ->folio_order = 0;
			$RoomReservationFolioMetaOBJ->log_id = $PaymentOptionOBJ->log_id;
			$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
			$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
			$RoomReservationFolioMetaOBJ->save();
			
			return redirect('guest-folio/'.$rsrv_Id);
		}
		
		$credentialRst = GatewayCredential::where([ ['hotel_id', $Hotel_ID], ['gateway_id', $Gateway_ID] ])->first();
		
		$ticket_hash_key = $credentialRst->ticket_hash_key; // MMK Ticket Hask Key
		$location_id     = $credentialRst->location_id;		// MMK Location Id
		$timestamp       = time();
		$order_id        = $rsrv_Id;
		
		$data = $location_id . $timestamp . $order_id;
		$signature = hash_hmac('sha256', $data, $ticket_hash_key);
		
		$CardGuestRst = DB::select("SELECT GT.first_six as firstSix, GT.last_four as lastFour, GT.ticket as ticketToken,
								   (SELECT GPC.card_name FROM gateway_payment_card as GPC WHERE GPC.card_alias = GT.card_type) as cardName,
								   (SELECT GPS.stat_name FROM gateway_payment_stat as GPS WHERE GPS.stat_id = GT.ticket_stat) as statName
		    					   FROM gateway_token as GT
								   WHERE guest_id = ".$GuestId." AND room_reservation_id = ".$rsrv_Id." AND gateway_id = ".$Gateway_ID."
								  ");
		$CardGuestNum = count($CardGuestRst);
		
		$CardGuaranteeRst = DB::select("SELECT CG.guarantee_id as CardId, CG.card_holder as CardHolder, CG.card_number as CardNumber,
										CG.card_expiry as CardExpiry, CG.card_cvv as CardCVV, CG.postal_code as CardZIP,
										(SELECT GPC.card_name FROM gateway_payment_card as GPC WHERE GPC.card_alias = CG.card_code) as CardName
		    					   		FROM reservation_card_guarantee as CG WHERE CG.room_reservation_id = ".$rsrv_Id."
								  	   ");
		$CardGuaranteeNum = count($CardGuaranteeRst);
		
		return view('zeamster.payment-card')->with("Hotel_ID", $Hotel_ID)->with("timestamp", $timestamp)
									   		->with("signature", $signature)->with("location_id", $location_id)
											->with("order_id", $order_id)->with("pay_Amount", $pay_Amount)->with("pay_Mode", $pay_Mode)
											->with("CardGuestRst", $CardGuestRst)->with("CardGuestNum", $CardGuestNum)
											->with("CardGuaranteeRst", $CardGuaranteeRst)->with("CardGuaranteeNum", $CardGuaranteeNum);
	}
	
	public function payment_post(Request $request)
    {
		return $request;
        $Helper = new Helper();
		$Hotel_ID   = $Helper->getHotelId();
		$Gateway_ID = 1;
		
		if (!empty($_POST)) {
			//this portion should strip-out the card number, exp date, cvv and zip
			var_dump($_POST);
			die;
		}
		// Define the secret ticket_hash_key used for hashing the variables
		$ticket_hash_key = 'be70492a391094089eb63c89';    						// MMK Ticket Hask Key
		
		// Define variables for generating the required hash
		$location_id = '11e6f1f6fa3a680085f260f0';								// MMK Location Id
		$timestamp = time();
		$order_id = mt_rand();
		
		// Generate the secure hash, making sure the variables
		// are in the proper sequence.
		$data = $location_id . $timestamp . $order_id;
		$signature = hash_hmac('sha256', $data, $ticket_hash_key);
		
		return view('zeamster.payment')->with("Hotel_ID", $Hotel_ID)->with("timestamp", $timestamp)
									   ->with("signature", $signature)->with("location_id", $location_id)->with("order_id", $order_id);
	}
	
	public function payment_reversal(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID = $Helper->getHotelId();
		
		$rsrv_Id   = $request->rsrv_id;
		
		$Results = DB::select("SELECT RSV.guest_profile_id, RSV.arrival,
							   ( SELECT SUM(RF.folio_amount) FROM room_reservation_folio as RF
								 WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0
							   ) as EstAmount,
							   ( ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 1
								 ) - 
								 ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order > 0 AND RF.folio_type = 2
								 )
							   ) as PayAmount,
							   (	( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 0
									) - 
									( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   		  FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id AND RF.folio_order = 0 AND RF.folio_type = 2
									)
							   ) as PayDeposit,
							   ( ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id
								   AND RF.folio_order = 0 AND RF.folio_type = 0 AND RF.folio_pay_type = 'Cash Payment'
								 ) - 
								 ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id
								   AND RF.folio_order = 0 AND RF.folio_type = 2 AND RF.folio_pay_type = 'Cash Payment'
								 )
							   ) as CashAmount,
							   ( ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id
								   AND RF.folio_order = 0 AND RF.folio_type = 0 AND RF.folio_pay_type = 'Check Payment'
								 ) - 
								 ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id
								   AND RF.folio_order = 0 AND RF.folio_type = 2 AND RF.folio_pay_type = 'Check Payment'
								 )
							   ) as CheckAmount,
							   ( ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id
								   AND RF.folio_order = 0 AND RF.folio_type = 0 AND RF.folio_pay_type = 'CC Payment'
								 ) - 
								 ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id
								   AND RF.folio_order = 0 AND RF.folio_type = 2 AND RF.folio_pay_type = 'CC Payment'
								 )
							   ) as CCAmount,
							   ( ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id
								   AND RF.folio_order = 0 AND RF.folio_type = 0 AND RF.folio_pay_type = 'CC Payment Auth Only'
								 ) - 
								 ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id
								   AND RF.folio_order = 0 AND RF.folio_type = 2 AND RF.folio_pay_type = 'CC Payment Auth Only'
								 )
							   ) as CCAuthAmount,
							   ( ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id
								   AND RF.folio_order = 0 AND RF.folio_type = 0 AND RF.folio_pay_type = 'Company Payment'
								 ) - 
								 ( SELECT CASE WHEN SUM(RF.folio_amount) IS NOT NULL THEN SUM(RF.folio_amount) ELSE 0 END
							   	   FROM room_reservation_folio_meta as RF WHERE RF.room_reservation_id = RSV.room_reservation_id
								   AND RF.folio_order = 0 AND RF.folio_type = 2 AND RF.folio_pay_type = 'Company Payment'
								 )
							   ) as CompanyAmount
							   FROM guest as GST INNER JOIN room_reservation as RSV ON RSV.guest_id = GST.guest_id
							   WHERE RSV.room_reservation_id = ".$rsrv_Id." AND RSV.hotel_id = ".$Hotel_ID."
							  ");
		if(count($Results))
		{
			foreach($Results as $Value){
				$search_Date= date('m/d/Y', strtotime($Value->arrival));
				$Profile_Id = $Value->guest_profile_id;
				$esmt_Rate  = $Value->EstAmount;
				$pay_Rate   = $Value->PayAmount;
				$dpst_Rate  = $Value->PayDeposit;
				
				$CashAmount   = $Value->CashAmount;
				$CheckAmount  = $Value->CheckAmount;
				$CCAmount     = $Value->CCAmount;
				$CCAuthAmount = $Value->CCAuthAmount;
				$CompanyAmount= $Value->CompanyAmount;
			}
			$rsrv_Rate = $dpst_Rate-$pay_Rate;
			if($rsrv_Rate > 0)			
			{
				return view('zeamster.reversal')->with("Hotel_ID", $Hotel_ID)->with("rsrv_Id", $rsrv_Id)->with("rsrv_Rate", $rsrv_Rate)->with("Profile_Id", $Profile_Id)
												->with("esmt_Rate", $esmt_Rate)->with("pay_Rate", $pay_Rate)->with("dpst_Rate", $dpst_Rate)->with("search_Date", $search_Date)
												->with("CashAmount", $CashAmount)->with("CheckAmount", $CheckAmount)->with("CCAmount", $CCAmount)
												->with("CCAuthAmount", $CCAuthAmount)->with("CompanyAmount", $CompanyAmount);
			}else{
				return redirect('guest-folio/'.$rsrv_Id)->with("warning", "Reservation# ".$rsrv_Id." have not refundable amount");
			}
		}else{
			return redirect('in-house')->with("warning", "Reservation# ".$rsrv_Id." not Found");
		}
	}
	
	public function payment_reversal_card(Request $request)
    {
        $Helper = new Helper();
		$Hotel_ID   = $Helper->getHotelId();
		$admin_ID   = $Helper->getAdminUserId();
		$Gateway_ID = 1;
		
		$rsrv_Id = $request->rsrv_id;
		$Result = RoomReservation::where([ ['hotel_id', $Hotel_ID], ['room_reservation_id', $rsrv_Id] ])->first();
		if(count($Result))
		{
			$paymentMode   = $request->payment_mode;
			if($paymentMode == 1){
				$refund_Mode   = 'cash';
				$refund_Amount = $request->folio_amount;
			}elseif($paymentMode == 2){
				$refund_Mode   = 'sale';
				$refund_Amount = $request->folio_amount;
			}elseif($paymentMode == 3){
				$refund_Mode   = 'auth';
				$refund_Amount = $request->folio_amount;
			}elseif($paymentMode == 4){
				$refund_Mode   = 'check';
			}elseif($paymentMode == 5){
				$refund_Mode   = 'company';
				$refund_Amount = $request->folio_amount;
			}
			$folio_Comment = $request->folio_comment;
			
			$RoomTypeId = $Result->room_type_id;
			$GuestId    = $Result->guest_id;
			if($paymentMode == 1)
			{
				$unique_ID = "CSH".$Helper->getUniqueId().$rsrv_Id;
				
				$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
				$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
				$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
				$RoomReservationFolioMetaOBJ->folio_type = 2;
				$RoomReservationFolioMetaOBJ->folio_date = date("Y-m-d");
				$RoomReservationFolioMetaOBJ->folio_pay_type = 'Cash Refund';
				$RoomReservationFolioMetaOBJ->folio_amount = $refund_Amount;
				$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
				$RoomReservationFolioMetaOBJ->folio_auto  = 0;
				$RoomReservationFolioMetaOBJ->folio_order = 0;
				$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
				$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
				$RoomReservationFolioMetaOBJ->save();
				
				$PaymentOptionOBJ = new PaymentOption;
				$PaymentOptionOBJ->transaction_id = $unique_ID;
				$PaymentOptionOBJ->payment_mode   = $paymentMode;
				$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
				$PaymentOptionOBJ->room_reservation_id = $rsrv_Id;
				$PaymentOptionOBJ->reg_ip   = $_SERVER['REMOTE_ADDR'];
				$PaymentOptionOBJ->added_by = $admin_ID;
				$PaymentOptionOBJ->save();
				
				return redirect('guest-folio/'.$rsrv_Id);
			}
			elseif($paymentMode == 4)
			{
				$unique_ID = "CHK".$Helper->getUniqueId().$rsrv_Id;
				
				$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
				$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
				$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
				$RoomReservationFolioMetaOBJ->folio_type = 2;
				$RoomReservationFolioMetaOBJ->folio_date = date("Y-m-d");
				$RoomReservationFolioMetaOBJ->folio_pay_type = 'Check Refund';
				$RoomReservationFolioMetaOBJ->folio_amount = $refund_Amount;
				$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
				$RoomReservationFolioMetaOBJ->folio_auto  = 0;
				$RoomReservationFolioMetaOBJ->folio_order = 0;
				$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
				$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
				$RoomReservationFolioMetaOBJ->save();
				
				$PaymentOptionOBJ = new PaymentOption;
				$PaymentOptionOBJ->transaction_id = $unique_ID;
				$PaymentOptionOBJ->payment_mode   = $paymentMode;
				$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
				$PaymentOptionOBJ->room_reservation_id = $rsrv_Id;
				$PaymentOptionOBJ->bank_name   	= $bank_name;
				$PaymentOptionOBJ->holder_name  = $holder_name;
				$PaymentOptionOBJ->check_number = $check_number;
				$PaymentOptionOBJ->check_date 	= $check_date;
				$PaymentOptionOBJ->reg_ip   = $_SERVER['REMOTE_ADDR'];
				$PaymentOptionOBJ->added_by = $admin_ID;
				$PaymentOptionOBJ->save();
				
				return redirect('guest-folio/'.$rsrv_Id);
			}
			elseif($paymentMode == 5)
			{
				$unique_ID = "CMP".$Helper->getUniqueId().$rsrv_Id;
				
				$RoomReservationFolioMetaOBJ = new RoomReservationFolioMeta;
				$RoomReservationFolioMetaOBJ->hotel_id = $Hotel_ID;
				$RoomReservationFolioMetaOBJ->room_reservation_id = $rsrv_Id;
				$RoomReservationFolioMetaOBJ->folio_type = 2;
				$RoomReservationFolioMetaOBJ->folio_date = date("Y-m-d");
				$RoomReservationFolioMetaOBJ->folio_pay_type = 'Company Refund';
				$RoomReservationFolioMetaOBJ->folio_amount = $refund_Amount;
				$RoomReservationFolioMetaOBJ->folio_comment = $folio_Comment;
				$RoomReservationFolioMetaOBJ->folio_auto  = 0;
				$RoomReservationFolioMetaOBJ->folio_order = 0;
				$RoomReservationFolioMetaOBJ->reg_ip = $_SERVER['REMOTE_ADDR'];
				$RoomReservationFolioMetaOBJ->added_by = $admin_ID;
				$RoomReservationFolioMetaOBJ->save();
				
				$PaymentOptionOBJ = new PaymentOption;
				$PaymentOptionOBJ->transaction_id = $unique_ID;
				$PaymentOptionOBJ->payment_mode   = $paymentMode;
				$PaymentOptionOBJ->hotel_id 	  = $Hotel_ID;
				$PaymentOptionOBJ->room_reservation_id    = $rsrv_Id;
				$PaymentOptionOBJ->reg_ip   = $_SERVER['REMOTE_ADDR'];
				$PaymentOptionOBJ->added_by = $admin_ID;
				$PaymentOptionOBJ->save();
				
				return redirect('guest-folio/'.$rsrv_Id);
			}
			
			$credentialRst = GatewayCredential::where([ ['hotel_id', $Hotel_ID], ['gateway_id', $Gateway_ID] ])->first();
			
			$ticket_hash_key = $credentialRst->ticket_hash_key; // MMK Ticket Hask Key
			$location_id     = $credentialRst->location_id;		// MMK Location Id
			$timestamp       = time();
			$order_id        = $rsrv_Id;
			
			$data = $location_id . $timestamp . $order_id;
			$signature = hash_hmac('sha256', $data, $ticket_hash_key);
			
			$CardGuestRst = DB::select("SELECT GT.first_six as firstSix, GT.last_four as lastFour, GT.ticket as ticketToken,
									   (SELECT GPC.card_name FROM gateway_payment_card as GPC WHERE GPC.card_alias = GT.card_type) as cardName,
									   (SELECT GPS.stat_name FROM gateway_payment_stat as GPS WHERE GPS.stat_id = GT.ticket_stat) as statName
									   FROM gateway_token as GT
									   WHERE guest_id = ".$GuestId." AND room_reservation_id = ".$rsrv_Id." AND gateway_id = ".$Gateway_ID."
									  ");
			$CardGuestNum = count($CardGuestRst);
			
			$CardGuaranteeRst = DB::select("SELECT CG.guarantee_id as CardId, CG.card_holder as CardHolder, CG.card_number as CardNumber,
											CG.card_expiry as CardExpiry, CG.card_cvv as CardCVV, CG.postal_code as CardZIP,
											(SELECT GPC.card_name FROM gateway_payment_card as GPC WHERE GPC.card_alias = CG.card_code) as CardName
											FROM reservation_card_guarantee as CG WHERE CG.room_reservation_id = ".$rsrv_Id."
										   ");
			$CardGuaranteeNum = count($CardGuaranteeRst);
			
			return view('zeamster.payment-card')->with("Hotel_ID", $Hotel_ID)->with("timestamp", $timestamp)
												->with("signature", $signature)->with("location_id", $location_id)
												->with("order_id", $order_id)->with("pay_Amount", $pay_Amount)->with("pay_Mode", $pay_Mode)
												->with("CardGuestRst", $CardGuestRst)->with("CardGuestNum", $CardGuestNum)
												->with("CardGuaranteeRst", $CardGuaranteeRst)->with("CardGuaranteeNum", $CardGuaranteeNum);
		}else{
			return redirect('in-house')->with("warning", "Reservation# ".$rsrv_Id." not Found");
		}
	}
}
