@extends('layouts.layout')

@section('title')
	Edit Room Type
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Edit Room Type</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    <br />
                    <form id="demo-form2" action="{!!url('room/room-type', $Results->room_type_id)!!}" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                    {!!method_field('PATCH')!!}
               		{!!csrf_field()!!}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_type">Room Type <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<input type="text" name="room_type" value="{!!$Results->room_type!!}" id="room_type" class="form-control col-md-7 col-xs-12" placeholder="Room Type" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_count">No. of Rooms <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<input type="number" name="room_count" value="{!!$Results->room_count!!}" id="room_count" class="form-control col-md-7 col-xs-12" min="1" max="200" placeholder="No. of Rooms" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="guest_adult" class="control-label col-md-3 col-sm-3 col-xs-12">No. of Guests <span class="required">*</span></label>
                            <div class="col-md-2 col-sm-3 col-xs-6">
                            	<input type="number" name="guest_adult" value="{!!$Results->guest_adult!!}" id="guest_adult" class="form-control col-md-3 col-xs-6" min="1" max="10" placeholder="Adult" required="required">
                            </div>
                            <div class="col-md-2 col-sm-3 col-xs-6">
                            	<input type="number" name="guest_child" value="{!!$Results->guest_child!!}" id="guest_child" class="form-control col-md-3 col-xs-6" min="0" max="10" placeholder="Children">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="room_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Rate <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<input type="text" name="room_rate" value="{!!$Results->room_rate!!}" id="room_rate" class="form-control col-md-3 col-xs-6" placeholder="Rate" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="room_tax" class="control-label col-md-3 col-sm-3 col-xs-12">Taxes <span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                            	<input type="text" name="room_tax" value="{!!$Results->room_tax!!}" id="room_tax" class="form-control col-md-3 col-xs-6" placeholder="Taxes" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="room_pic_1" class="control-label col-md-3 col-sm-3 col-xs-12">Image of Sample Room <span class="required">*</span></label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            	{!!Html::image('room_type/'.$Results->room_pic_1, 'alt', array( 'width' => 200, 'height' => 150 ))!!}
                                <input type="file" name="room_pic_1" id="room_pic_1" class="form-control col-md-3 col-xs-6">
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            	{!!Html::image('room_type/'.$Results->room_pic_2, 'alt', array( 'width' => 200, 'height' => 150 ))!!}
                            	<input type="file" name="room_pic_2" id="room_pic_2" class="form-control col-md-3 col-xs-6">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="room_pic_1" class="control-label col-md-3 col-sm-3 col-xs-12">&nbsp;</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            	@if($Results->room_pic_3)
                                	{!!Html::image('room_type/'.$Results->room_pic_3, 'alt', array( 'width' => 200, 'height' => 150 ))!!}
                                @endif
                            	<input type="file" name="room_pic_3" id="room_pic_3" class="form-control col-md-3 col-xs-6">
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                            	@if($Results->room_pic_4)
                            		{!!Html::image('room_type/'.$Results->room_pic_4, 'alt', array( 'width' => 200, 'height' => 150 ))!!}
                                @endif
                            	<input type="file" name="room_pic_4" id="room_pic_4" class="form-control col-md-3 col-xs-6">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="total_rate" class="control-label col-md-3 col-sm-3 col-xs-12">Description <span class="required">*</span></label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <textarea id="description" name="description" rows="7" class="form-control ckeditor" placeholder="Description">{!!$Results->description!!}</textarea>
                                <script type="text/javascript">
                                         CKEDITOR.replace( 'description',
                                         {
                                          	customConfig : 'config.js',
                                          	toolbar : 'advance'
                                         })
                                </script>
                          	</div>
                        </div>
                        <div class="form-group">
                            <label for="taxes" class="control-label col-md-3 col-sm-3 col-xs-12">Facilities</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            	@foreach( App\RoomtypeFacility::all(['rtf_name as FacilityName', 'rtf_key as FacilityKey']) as $val )
                                <div class="col-md-3 col-sm-3 col-xs-6">
                            		<input type="checkbox" name="room_faclty[]" id="faclty_{!!$val->FacilityKey!!}" value="{!!$val->FacilityKey!!}" class="flat"{!!($Results->{"$val->FacilityKey"} == '1')?" checked":"";!!}/> {!!$val->FacilityName!!}
                                </div>
                                @endforeach
                                <br /><br /><br />
                            </div>
                        </div>
                    	<div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">UPDATE</button>
                                <button type="reset" class="btn btn-primary">CANCEL</button>
                            </div>
                    	</div>
                    
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
@stop