@extends('layouts.layout')

@section('title')
	Zeamster Credentials
@stop

@section('body')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"><h3>Zeamster Credentials</h3></div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_content">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @elseif ($message = Session::get('danger'))
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <form id="demo-form2" action="{!!url('zeamster-credentials')!!}" method="post" data-parsley-validate class="form-horizontal form-label-left">
               			{!!csrf_field()!!}
                        <input type="hidden" name="gc_id" id="gc_id" value="{!!$Results->gc_id or ''!!}">
                        <input type="hidden" name="hotel_id" id="hotel_id" value="{!!$Results->hotel_id or $Hotel_ID!!}">
                    	<div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">User Id <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6">
                            <input type="text" name="user_id" id="user_id" value="{!!$Results->user_id or old('user_id')!!}" placeholder="User Id" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">User Api Key <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6">
                            <input type="text" name="user_api_key" id="user_api_key" value="{!!$Results->user_api_key or old('user_api_key')!!}" placeholder="User Api Key" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Developer Id <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6">
                            <input type="text" name="developer_id" id="developer_id" value="{!!$Results->developer_id or old('developer_id')!!}" placeholder="Developer Id" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Location Id <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6">
                            <input type="text" name="location_id" id="location_id" value="{!!$Results->location_id or old('location_id')!!}" placeholder="Location Id" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3">Ticket Hash Key <span class="required">*</span></label>
                            <div class="col-md-4 col-sm-6">
                            <input type="text" name="ticket_hash_key" id="ticket_hash_key" value="{!!$Results->ticket_hash_key or old('ticket_hash_key')!!}" placeholder="Ticket Hash Key" class="form-control col-md-7 col-xs-12" required="required">
                            </div>
                        </div>
                    	<div class="form-group form-group-last marginTop20">
                            <div class="col-md-4 col-sm-4 col-xs-4 col-md-offset-3">
                                <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> Update Credential</button>
                                <button type="reset" class="btn btn-default marginLeft5"><i class="fa fa-undo"></i> Cancel</button>
                            </div>
                    	</div>
                    </form>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
<script>
	function ConfirmAction(msg)
	{
		var x = confirm(msg);
		if (x)
			return true;
		else
			return false;
	}
</script>
@stop      
