<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
use App\EmployeeShift;
use Helper;

class ShiftAssignController extends Controller
{
    public function index(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		return view('employees.assign-shifts')->with("Hotel_ID", $Hotel_ID)->with("request", $request);
    }

    public function store(Request $request)
    {
		$Helper = new Helper();
		$admin_ID  = $Helper->getAdminUserId();
		$Hotel_ID  = $Helper->getHotelId();
		
		if($Hotel_ID == $request->hotel_id)
		{
			$job_title  = $request->job_title;
			$emp_id     = $request->emp_id;
			$shift_from = $request->shift_from;
			$shift_to   = $request->shift_to;
			$shift_id   = implode(",", $request->shift_id);
			
			if($shift_from && $shift_to)
			{
				$emp_shift_from = date('Y-m-d', strtotime($shift_from));
				$emp_shift_to   = date('Y-m-d', strtotime($shift_to));
			}
			elseif(!$shift_from && $shift_to)
			{
				$emp_shift_from = date('Y-m-d');
				$emp_shift_to   = date('Y-m-d', strtotime($shift_to));
			}
			elseif($shift_from && !$shift_to)
			{
				$emp_shift_from = date('Y-m-d', strtotime($shift_from));
				$emp_shift_to   = date('Y-m-d', strtotime($shift_from));
			}
			else
			{
				$emp_shift_from = date('Y-m-d');
				$emp_shift_to   = date('Y-m-d', strtotime($emp_shift_from.'+3 months'));
			}
			
			$EmpRst  = Employee::find($emp_id);
			$EmpName = $EmpRst->emp_fname;
			
			$date1 = date_create($emp_shift_from);
			$date2 = date_create($emp_shift_to);
			if($date1 && $date2)
			{
				$diff = date_diff($date1, $date2);
				$dayNum = $diff->format("%a");
				for($i = 1; $i <= $dayNum+1; $i++)
				{
					if(('$emp_shift_from') <= ('$emp_shift_to'))
					{
						$EmpSftRst = EmployeeShift::where([ ['hotel_id', $Hotel_ID], ['emp_id', '=', $emp_id], ['shift_date', $emp_shift_from] ])->first();
						if(!count($EmpSftRst)){
							$EmpSftRst = new EmployeeShift;	
						}
						$EmpSftRst->hotel_id   = $Hotel_ID;
						$EmpSftRst->emp_id     = $emp_id;
						$EmpSftRst->shift_date = $emp_shift_from;
						$EmpSftRst->shift_id   = $shift_id;
						$EmpSftRst->reg_ip 	   = $_SERVER['REMOTE_ADDR'];
						$EmpSftRst->added_by   = $admin_ID;
						$EmpSftRst->save();						
					} 
					$emp_shift_from = date('Y-m-d', strtotime($emp_shift_from.'+1 days'));
				}
			}
			return redirect('assign-shifts?job_title='.$job_title.'&emp_id='.$emp_id)->with('success','Employee Shift added successfully for '.$EmpName.'.');
		}
		else
		{
			return redirect('assign-shifts')->with("danger", "Please select your Motel");
		}
    }

    public function record(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		return view('employees.record-shifts')->with("Hotel_ID", $Hotel_ID)->with("request", $request);
    }
	
	public function setassignShift(Request $request)
    {
       	$Helper = new Helper();
		$Hotel_ID  = $Helper->getHotelId();
		$admin_ID  = $Helper->getAdminUserId();
		
		$emp_shift_id = $request->emp_shift_id;
		if(isset($request->shift_id) && !empty($request->shift_id)){
			$shift_id = implode(",", $request->shift_id);
		}else{
			$shift_id = '';
		}
		
		$Results = EmployeeShift::where([ ['hotel_id', '=', $Hotel_ID], ['emp_shift_id', '=', $emp_shift_id] ])->first();
		$Results->shift_id = $shift_id;
		$Results->save();
		if($request->ajax()) {
			return response()->json(['response' => 'Employee Shift updated successfully', 'status' => 'success']);
		}else{
			return response()->json(['response' => 'Ajax not working', 'status' => 'error']);
		}
    }
}