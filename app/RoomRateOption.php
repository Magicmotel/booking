<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomRateOption extends Model
{
	protected $fillable= [
		'hotel_id', 'room_type_id', 'room_rate_date', 'room_rate_price',
		'trash', 'reg_ip', 'added_by'
	];
	protected $primaryKey = 'room_rate_option_id';
	protected $table = 'room_rate_option';
}
