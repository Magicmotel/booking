<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChannelPartner extends Model
{
    
    protected $fillable = [
        'channel_name', 'reg_ip', 'added_by'
    ];
	protected $primaryKey = 'channel_id';	
	protected $table = 'channel_partners';
	
}
